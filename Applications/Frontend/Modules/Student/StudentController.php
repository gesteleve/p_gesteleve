<?php

namespace Applications\Frontend\Modules\Student;

use Applications\Entities\Student;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class StudentController extends BackController
{
    

    
    /****************************************************************************
    Nom:        is_master
    Auteur:     ?
    Date:       07.08.2014
    But:        Indique si la personne connecdtée est le maitre de classe de 
                l'élève passé en paramètre
    Parametre:  $student  --> éleve concerné (tableau) 
    Retour:     Vrai si il s'agit du MC   
    *****************************************************************************/
    private function is_master(&$student) 
    {
        if (!$this->app->user()->isAuthenticated()) 
        { 
            return false; 
        }

        $tmp = @$this->managers->getManagerOf('schoolClass')->getUniqueByColleague($this->app->user()->getAttribute('user')->id());
        if ($tmp == null) 
        {
            return false;
        } 
        else 
        {

            foreach ($tmp as $class){

                if ($student->school_class_id() == $class->id()){
                    return true;
                }
            }
            return false;
        }
    }

    function executeIndex(HTTPRequest $request) {
        $this->page->addVar('title', 'Liste des élèves');
        $this->page->addVar('description', 'Liste des élèves de l\'ETML');
        $this->page->addVar('keywords', 'liste, élèves');

        $students = $this->managers->getManagerOf('Student')->getList();
        $classes = $this->managers->getManagerOf('schoolClass')->getList();
        $professions = $this->managers->getManagerOf('Profession')->getList();
        $sections = $this->managers->getManagerOf('Profession')->getStudentsBySection();

        $this->page->addVar('professions', $professions);
        $this->page->addVar('students', $students);
        $this->page->addVar('classes', $classes);
        $this->page->addVar('sections', $sections);
		
		 

         if ($request->postExists('filter') && $request->postExists('SearchValue')) 
         {
            $result = array();
            $pattern = $request->postData('SearchValue');

            if($request->postData('filter') == "profession")
            {
                $result['Profession'] = $this->managers->getManagerOf('Student')->filterSection($pattern);
               
                //$result['Profession'] = $request->postData('filter');

            }

            if($request->postData('filter') == "classe")
            {
                
                $result['Class'] = $this->managers->getManagerOf('Student')->filterClass($pattern);
               // $result['Class'] = $request->postData('filter');
            }

             if($request->postData('filter') == "all")
            {
                
                $result['Student'] = $this->managers->getManagerOf('Student')->filterStudent($pattern);
                 //$result['Student'] = $request->postData('filter');
                
            }
            $result['value'] = $pattern;
            $this->page->addVar('result', $result);
            
        }
    }

    function executeView(HTTPRequest $request) {
		
        if ($request->getExists('id')) {
			
				$student = $this->managers->getManagerOf('Student')->getUnique($request->getData('id'));
			
            if ($student) {
                $follows = $this->managers->getManagerOf('Follow')->getListByStudent($student->id());


                $modify_right = 0;
                $summary_right = 0;

                // vrai si la personne connectée est le maitre de classe
                $student_master=$this->is_master($student);

                if ($this->app->user()->isAuthenticated()) {
                    $right = $this->app->user()->getAttribute('right');
					if ($this->is_master($student)) {

                        $modify_right = true;
                        $summary_right = true;
                    } else {

                        if (($right[FOLLOW] & MODIFY) && ($right[FOLLOW] & SPECIFIC)) {
                            $modify_right = true;
                        }

                        if (($right[STUDENT_SUMMARY] & VIEW_ALL) || ($right[STUDENT_SUMMARY] & SPECIFIC) && ($student->id() == $this->app->user()->getAttribute('user')->id())) {
                            $summary_right = true;
                        }
                    }

                }

                $this->page->addVar('nbDoors',$nbDoors = $this->managers->getManagerOf('Student')->getAllDoors($student->id()));
                $this->page->addVar('nbLates',$nbLates = $this->managers->getManagerOf('Student')->getAllLates($student->id()));

                $this->page->addVar('nbAbsencesTheo',$nbAbsencesTheo = $this->managers->getManagerOf('Student')->getAllAbsencesTheo($student->id()));
                $this->page->addVar('nbJustiAbsencesTheo',$nbJustiAbsencesTheo = $this->managers->getManagerOf('Student')->getAllJustifiedAbsencesTheo($student->id()));
                $this->page->addVar('nbRemedialTheo',$nbRemedialTheo = $this->managers->getManagerOf('Student')->getAllRemedialTheo($student->id()));



                $this->page->addVar('nbAbsencesPrat',$nbAbsencesPrat = $this->managers->getManagerOf('Student')->getAllAbsencesPrat($student->id()));
                $this->page->addVar('nbJustiAbsencesPrat',$nbJustiAbsencesPrat = $this->managers->getManagerOf('Student')->getAllJustifiedAbsencesPrat($student->id()));
                $this->page->addVar('nbRemedialPrat',$nbRemedialPrat = $this->managers->getManagerOf('Student')->getAllRemedialPrat($student->id()));





                $MasterClass = $this->managers->getManagerOf('Student')->getMasterClass($student->id());
                $documents = $this->managers->getManagerOf('Document')->getListOf($student->id());

                $this->page->addVar('title', $student->first_name().' '.$student->name());
                $this->page->addVar('description', 'Fiche de l\'élève '.$student->first_name().' '.$student->name());
                $this->page->addVar('keywords', 'fiche, élève, '.$student->id().', '.$student->first_name().', '.$student->name().', '.$student->school_class_id());
                $this->page->addVar('student', $student);
                $this->page->addVar('documents', $documents);
                $this->page->addVar('documentTypes', $this->managers->getManagerOf('documentType')->getList());
                $this->page->addvar('follows', $follows);
                $this->page->addVar('role', $this->app->user()->getAttribute('role'));
                $this->page->addVar('master', $this->is_master($student));
                $this->page->addVar('modify_right', $modify_right);
                $this->page->addVar('summary_right', $summary_right);
                $this->page->addVar('student_master', $student_master);
                $this->page->addVar('MasterClass', $MasterClass);
            } else {
                $this->app->httpResponse()->redirect404();
            }
        }
		
    }
      
     
     function executeImport( HTTPRequest $request)
    {
        function wd_remove_accents($str, $charset='utf-8')
        {
            $str = htmlentities($str, ENT_NOQUOTES, $charset);
        
            $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
            $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
            $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

            $str = strtolower($str);
            $str = str_replace(chr(32),"", $str);
        
            return $str;
        }

        function GETNpa($string,$len)
        {
            return substr($string, 0,$len);
        }
        
        function DateConvert($date)
        {
            $date = explode('.', $date);
            $date = array_reverse($date);
            $date = implode('-', $date);
            return $date;
        }

        $manager = $this->managers->getManagerOf('Student');


          
        // Met dans la variable, le fichier télécharger !
        $csv = $request->fileData('filFileToUpload');

        // Contrôle que nous avons télécharger le fichier
        if($request->fileData('filFileToUpload'))
        {
                
            $row = 1;

            if (($handle=fopen($csv['tmp_name'],'r')) !== FALSE) 
            { 
                while (($data = fgetcsv($handle,1000,';')) !== FALSE) 
                { 
                    if($row > 1)
                    {
                                     
                        $Name = utf8_encode($data[2]); 
                        $First_name = utf8_encode($data[3]);
                                    
                        $Login_Name=$Name;
                        $Login_Firstname=$First_name;

                        //Enlever tous caractères spéciaux ainsi que les espace et passer en miniscule
                        $Login_Name=wd_remove_accents($Login_Name);
                        $Login_Firstname=wd_remove_accents($Login_Firstname);

                        //Limiter le nombre de caractère
                        $Login_Name=substr($Login_Name,0,8);
                        $Login_Firstname=substr($Login_Firstname,0,2); 
                                   


                        $id = $Login_Name.$Login_Firstname;


                        $comment="-";
                        $email = "-";
                        $class =$data[4];
                        $work =utf8_encode($data[5]);
                        $birthday = $data[6];
                        $phone  =$data[7];
                        $mobile =$data[8];
                        $canton = $data[12];
                        $npa_locality =$data[11];
                        $address =utf8_encode($data[10]);
                        $addressco = utf8_encode($data[11]);
                        $responsible_firstname =$data[15];
                        $responsible_name =$data[14];
                        $responsible_phone = $data[15];
                        $responsible_npalocality =$data[16];
                        $responsible_address =$data[15];
                        $entry_date = $data[17];
                        $exit_date =$data[18];
                        $tiers =$data[19];
                        print_r($data);
                        $GetNpa = GetNpa($npa_locality,4);
                        $GetLocality = utf8_encode(substr($npa_locality,4,150));
                        $GetDateEntry = substr($entry_date,6,4);
                        $GetDateExit = substr($exit_date,6,4);
                        $BirthDate = DateConvert($birthday);
                        
                                    


                        $student = new Student(array(
                        'id' => $id,
                        'name' => $Name,
                        'first_name' =>$First_name,
                        'birth_date' => $BirthDate,
                        'zip' =>$GetNpa ,
                        'city' =>$GetLocality ,
                        'address' => $address,
                        'mobile' =>$mobile ,
                        'phone' =>$phone ,
                        'email' =>$email ,
                        'entry_date' =>$GetDateEntry ,
                        'comment' =>$comment ,
                        'exit_date' =>$GetDateExit ,
                        'school_class_id' =>$class 
                        ));
//print_r($student);




                        $manager->loadData($student);             
                                                   

                    }
                    $row++;     
                }
                fclose($handle);
            }       
        }    
   }
                   
    function executeAdd(HTTPRequest $request) {


        if ($this->app->user()->isAuthenticated()) {
                $right = $this->app->user()->getAttribute('right');

             $summary_right = 0;
            if (@$right[STUDENT_SUMMARY] & MODIFY) {
                if(@$right[STUDENT_SUMMARY] & SPECIFIC){
                    if(($master_class) && $this->is_master($student)){ 
                        $summary_right = 1;
                    }
                }
                else {
                    $summary_right = 1;
                }
            }
            //droit a revoir dls
            if (!$summary_right){
                $this->app->httpResponse()->redirect404();
            }

        }
        else
        {
            $this->app->httpResponse()->redirect404();
        }


       


        $classes_manager = $this->managers->getManagerOf('schoolClass');
        $manager = $this->managers->getManagerOf('Student');
        $classes = $classes_manager->getList();

        $this->page->addVar('title', 'Ajout d\'un élève');
        $this->page->addVar('description', 'Ajout d\'un élève');
        $this->page->addVar('keywords', 'ajout, élève');
        $this->page->addVar('classes', $classes);

        $errors = '';

        if($request->postsExists(array('name', 'first_name', 'id', 'class', 'entry_date', 'exit_date', 'address', 'zip', 'city', 'email', 'phone', 'mobile', 'birth_date', 'x1', 'y1', 'w', 'h'))) {
            if (!$request->postsEmpty(array('name', 'first_name', 'id', 'class', 'entry_date', 'exit_date', 'address', 'zip', 'city', 'email', 'birth_date'))) {
                if ($request->postData('exit_date') < $request->postData('entry_date')) {
                    $errors .= '<li>Est-ce que votre élève est un voyageur du temps ?</li>';
                }
                if (!$this->in_object($request->postData('class'), $classes, array('id'))) {
                    $errors .= '<li>Cette classe n\'existe pas.</li>';
                }
                if ($manager->getUnique($request->postData('id'))) {
                    $errors .= '<li>Un <a href="'.$this->page->html()->url('student/'.$request->postData('id')).'" target="_blank">élève</a> avec ce nom login est déjà présent.</li>';
                }
                if ($errors == '') {
                    // Ajoute les données dans la bd
                    $student = new Student(array(
                        'id' => $request->postData('id'),
                        'name' => $request->postData('name'),
                        'first_name' => $request->postData('first_name'),
                        'birth_date' => $request->postData('birth_date'),
                        'zip' => $request->postData('zip'),
                        'city' => $request->postData('city'),
                        'address' => $request->postData('address'),
                        'mobile' => $request->postData('mobile'),
                        'phone' => $request->postData('phone'),
                        'email' => $request->postData('email'),
                        'entry_date' => $request->postData('entry_date'),
                        'comment' => $request->postData('comment'),
                        'exit_date' => $request->postData('exit_date'),
                        'school_class_id' => $request->postData('class')
                        ));
                    $manager->add($student);

                    if (($request->postData('h') != 0) && ($request->postData('h') != '')) {
                        // Récupère les ifos pour le recardrage
                        $resizeInfos['x1'] = $request->postData('x1');
                        $resizeInfos['y1'] = $request->postData('y1');
                        $resizeInfos['w'] = $request->postData('w');
                        $resizeInfos['h'] = $request->postData('h');
                        // On créé une image de la taille de la sélection
                        $imgCuted = imagecreatetruecolor($resizeInfos['w'], $resizeInfos['h']);
                        // On charge l'image source
                        $imgSource = imagecreatefromjpeg(WEBROOT.'tmp'.DS.$this->app->user()->getAttribute('photoUpload').'.jpg');
                        // On recadre l'image
                        imagecopy($imgCuted, $imgSource, 0, 0, $resizeInfos['x1'], $resizeInfos['y1'], $resizeInfos['w'], $resizeInfos['h']);
                        // On crée une image de la taille finale
                        $imgResized = imagecreatetruecolor(PHOTO_WIDTH, PHOTO_HEIGHT);
                        // On redimensionne l'image
                        imagecopyresampled($imgResized, $imgCuted, 0, 0, 0, 0, PHOTO_WIDTH, PHOTO_HEIGHT, $resizeInfos['w'], $resizeInfos['h']);

                        // On regarde si le dossier de l'année existe
                        if (!is_dir(WEBROOT.'img'.DS.'students'.DS.$request->postData('entry_date'))) {
                            //Si c'est pas le cas on crée le dossier (images)
                            mkdir(WEBROOT.'img'.DS.'students'.DS.$request->postData('entry_date'));
                        }
                        // On enregistre l'image
                        imagejpeg($imgResized, WEBROOT.'img'.DS.'students'.DS.$request->postData('entry_date').DS.$request->postData('id').'.jpg', 100);
                        unlink(WEBROOT.'tmp'.DS.$this->app->user()->getAttribute('photoUpload').'.jpg');
                        // Supprimme les variables de session
                        $this->app->user()->unsetAttribute('photoUpload');
                        $this->app->user()->unsetAttribute('photoResize');
                    }
                    $this->app->user()->setFlash('L\'élève à été ajouté.', 'success');
                    // Redirige sur la page de l'élève ajouté
                    $this->app->httpResponse()->redirect($this->page->html()->url('student/'.$request->postData('id')));
                } else {
                    $this->app->user()->setFlash($errors, 'error');
                    unset($errors);
                }

            } else {
                $this->app->user()->setFlash('<li>Tous les champs obligatoires ne sont pas renseignés.</li>', 'error');
            }
        }
        elseif ($this->app->user()->getAttribute('photoUpload') != null) {
            // Supprime le fichier
            unlink(WEBROOT.'tmp'.DS.$this->app->user()->getAttribute('photoUpload').'.jpg');
            // Supprime les variables de session
            $this->app->user()->unsetAttribute('photoUpload');
            $this->app->user()->unsetAttribute('photoResize');
        }
    }

    function executeEdit(HTTPRequest $request) {
        if ($request->getExists('id')) {
            $manager = $this->managers->getManagerOf('Student');
            $class_manager = $this->managers->getManagerOf('schoolClass');
            $student = $manager->getUnique($request->getData('id'));
            $master_class = $class_manager->getUniqueByColleague($this->app->user()->getAttribute('user')->id());

            if ($this->app->user()->isAuthenticated()) {
                $right = $this->app->user()->getAttribute('right');
            }


            $summary_right = 0;
            if (@$right[STUDENT_SUMMARY] & MODIFY) {
                if(@$right[STUDENT_SUMMARY] & SPECIFIC){
                    if(($master_class) && $this->is_master($student)){ 
                        $summary_right = 1;
                    }
                }
                else {
                    $summary_right = 1;
                }
            }

            //droit a revoir dls
            if (!$summary_right){
                $this->app->httpResponse()->redirect404();
            }

            if ($student) {
                if (!$request->postsExists(array('name', 'first_name', 'class', 'exit_date', 'address', 'zip', 'city', 'email', 'phone', 'mobile', 'birth_date'))) {
                    $this->page->addVar('title', $student->first_name().' '.$student->name());
                    $this->page->addVar('description', 'Modification de l\'élève '.$student->first_name().' '.$student->name());
                    $this->page->addVar('keywords', 'modification élève '.$student->first_name().' '.$student->name());

                    $classes = $class_manager->getList();

                    $this->page->addVar('student', $student);
                    $this->page->addVar('classes', $classes);
                    $this->page->addVar('user', $this->app->user());
                    $this->page->addVar('summary_right', $summary_right);
                } else {

                    if ($summary_right) {
                        if ((!$request->postEmpty('summary')) && (strlen($request->postData('summary')) > 20)) {
                            $summary = $request->postData('summary').'<span class="mod_date">Mis à jour le '.date('j F Y').'</span>';
                        }
                        else {
                            $summary = ' ';
                        }
                    }
                    else {
                        $summary = null;
                    }

                    $student = new Student(array(
                        'id' => $request->getData('id'),
                        'name' => $request->postData('name'),
                        'first_name' => $request->postData('first_name'),
                        'birth_date' => $request->postData('birth_date'),
                        'zip' => $request->postData('zip'),
                        'city' => $request->postData('city'),
                        'address' => $request->postData('address'),
                        'mobile' => $request->postData('mobile'),
                        'phone' => $request->postData('phone'),
                        'email' => $request->postData('email'),
                        'comment' => $request->postData('comment'),
                        'summary' => $summary,
                        'exit_date' => $request->postData('exit_date'),
                        'active' => $request->postData('active'),
                        'school_class_id' => $request->postData('class')
                        ));

                    $manager->save($student);
                    $this->app->httpResponse()->redirect($this->page->html()->url('student/'.$request->getData('id')));
                }
            } else {
                $this->app->httpResponse()->redirect404();
            }
        } else {
            $this->app->httpResponse()->redirect404();
        }
    }

    function executeDelete(HTTPRequest $request) {
        if ($request->getExists('id')) {
            $manager = $this->managers->getManagerOf('Student');
            $success = $manager->delete($request->getData('id'));

            if ($success) {
                $this->app->user()->setFlash('L\'utilisateur a été correctement supprimé', 'success');
            } else {
                $this->app->user()->setFlash('Une erreur c\'est produite lors de la suppression de l\'utilisateur', 'error');
            }

            $this->app->httpResponse()->redirect($this->page->html()->url('student'));
        } else {
            $this->app->httpResponse()->redirect404();
        }
    }

    function executeUpload(HTTPRequest $request) {
        $this->page->setLayout();
        if($request->fileExists('photo')){
            $date = new \DateTime;
            $photoFile = $request->fileData('photo');

            if(file_exists(WEBROOT.'tmp/'.$this->app->user()->getAttribute('photoUpload').'.jpg')){
                unlink(WEBROOT.'tmp/'.$this->app->user()->getAttribute('photoUpload').'.jpg');
            }

            if($photoFile['error'] > 0){
                $result = '<span class="uploadInfo">Erreur d\'envois</span>';
            }
            else {
                if($photoFile['type'] != 'image/jpeg') {
                    $result = '<span class="uploadInfo">Format non supporté</span>';
                }
                else {
                    $this->app->user()->setAttribute('photoUpload', sha1($date->getTimestamp().$photoFile['name']));
                    move_uploaded_file($photoFile['tmp_name'], WEBROOT.'tmp'.DS.$this->app->user()->getAttribute('photoUpload').'.jpg');
                    $result = '<img class="center" src="'.$this->page->html()->url('tmp/'.$this->app->user()->getAttribute('photoUpload').'.jpg').'" />';

                    //Récupère la taille de l'image
                    $size = getimagesize(WEBROOT.'tmp'.DS.$this->app->user()->getAttribute('photoUpload').'.jpg');
                    //Vérifie que l'iamge soit assez grande
                    if(($size[0] >= PHOTO_WIDTH)&&($size[1] >= PHOTO_HEIGHT)) {
                        //Calcule les coordonnées pour initier le recadrage
                        $selection['origW'] = $size[0];
                        $selection['origH'] = $size[1];
                        // Si c'est une image horizontale
                        if($size[0] >= $size[1]) {
                            // Calcule la zone de séléction d'une image en mode paysage
                            $selection['y2'] = $selection['origH']/1.5;
                            $selection['x2'] = $selection['y2']*0.75;
                            $selection['x1'] = $selection['x2']*0.1;
                            $selection['y1'] = $selection['y2']*0.1;
                            $selection['x2'] *= 1.1;
                            $selection['y2'] *= 1.1;
                        }
                        // Si c'est une image verticale
                        else {
                            // Calcule la zone de séléction d'une image en mode portarait
                            $selection['y2'] = $selection['origW']/1.5;
                            $selection['x2'] = $selection['y2']*0.75;
                            $selection['x1'] = $selection['x2']*0.1;
                            $selection['y1'] = $selection['y2']*0.1;
                            $selection['x2'] *= 1.1;
                            $selection['y2'] *= 1.1;
                        }
                    $this->page->addVar('resizeSelect', $selection);
                    $this->app->user()->setAttribute('photoResize', $selection);
                    }
                    else {
                        $result = '<span class="uploadInfo">Min '.PHOTO_WIDTH.'x'.PHOTO_HEIGHT.' px</span>';
                    }
                }
            }

            // Supprimme les possibles photos temporaires plus anciennes de trois heures
            $delete_time = time() - (PHOTO_TMP_DELETE * 60 * 60);
            $file_list = scandir(WEBROOT.'tmp');
            foreach ($file_list as $file) {
                if(filemtime(WEBROOT.'tmp'.DS.$file) < $delete_time){
                    unlink(WEBROOT.'tmp'.DS.$file);
                }
            }
            unset($delete_time);
            unset($file_list);
            $this->page->addVar('uploadResult', $result);
        }

        //$request->postData('');
        //$this->app->user()->setAttribute('', '');
        //$this->app->user()->getAttribute('');
    }
	
	function executePrint(HTTPRequest $request) {
		
		if ($request->postExists('optionList'))
		 {
			 
			$showPhoto = $request->postData('optionList');
			$textHeight = $request->postData('tailleList');
			$policeType = $request->postData('policeList');
			$showFollow = $request->postData('suivisList');
			$btnPressed = $request->postData('print');
			
			$this->page->addVar('showPhoto', $showPhoto);
			$this->page->addVar('textHeight', $textHeight);
			$this->page->addVar('policeType', $policeType);
			$this->page->addVar('showFollow', $showFollow);
			$this->page->addVar('btnPressed', $btnPressed);
		 }
		 
		if ($request->getExists('id')) {
            $student = $this->managers->getManagerOf('Student')->getUnique($request->getData('id'));
            if ($student) {
                $follows = $this->managers->getManagerOf('Follow')->getListByStudent($student->id());


                $modify_right = 0;
                $summary_right = 0;

                // vrai si la personne connectée est le maitre de classe
                $student_master=$this->is_master($student);

                if ($this->app->user()->isAuthenticated()) {
                    $right = $this->app->user()->getAttribute('right');
					if ($this->is_master($student)) {

                        $modify_right = true;
                        $summary_right = true;
                    } else {

                        if (($right[FOLLOW] & MODIFY) && ($right[FOLLOW] & SPECIFIC)) {
                            $modify_right = true;
                        }

                        if (($right[STUDENT_SUMMARY] & VIEW_ALL) || ($right[STUDENT_SUMMARY] & SPECIFIC) && ($student->id() == $this->app->user()->getAttribute('user')->id())) {
                            $summary_right = true;
                        }
                    }

                }


                $MasterClass = $this->managers->getManagerOf('Student')->getMasterClass($student->id());
                $documents = $this->managers->getManagerOf('Document')->getListOf($student->id());

                $this->page->addVar('title', $student->first_name().' '.$student->name());
                $this->page->addVar('description', 'Fiche de l\'élève '.$student->first_name().' '.$student->name());
                $this->page->addVar('keywords', 'fiche, élève, '.$student->id().', '.$student->first_name().', '.$student->name().', '.$student->school_class_id());
                $this->page->addVar('student', $student);
                $this->page->addVar('documents', $documents);
                $this->page->addVar('documentTypes', $this->managers->getManagerOf('documentType')->getList());
                $this->page->addvar('follows', $follows);
                $this->page->addVar('role', $this->app->user()->getAttribute('role'));
                $this->page->addVar('master', $this->is_master($student));
                $this->page->addVar('modify_right', $modify_right);
                $this->page->addVar('summary_right', $summary_right);
                $this->page->addVar('student_master', $student_master);
                $this->page->addVar('MasterClass', $MasterClass);
            } else {
                $this->app->httpResponse()->redirect404();
            }
        } else {
            $this->app->httpResponse()->redirect404();
        }
	}
	
	function executePrintfollow(HTTPRequest $request) {
		
			
			if ($request->postExists('optionList'))
			 {
				 
				$showPhoto = $request->postData('optionList');
				$textHeight = $request->postData('tailleList');
				$policeType = $request->postData('policeList');
				$showFollow = $request->postData('suivisList');
				$btnPressed = $request->postData('print');
				
				$this->page->addVar('showPhoto', $showPhoto);
				$this->page->addVar('textHeight', $textHeight);
				$this->page->addVar('policeType', $policeType);
				$this->page->addVar('showFollow', $showFollow);
				$this->page->addVar('btnPressed', $btnPressed);
			 }
			 
			if ($request->getExists('id')) {
				$student = $this->managers->getManagerOf('Student')->getUnique($request->getData('id'));
				if ($student) {
					$follows = $this->managers->getManagerOf('Follow')->getListByStudent($student->id());

					$modify_right = 0;
					$summary_right = 0;

					// vrai si la personne connectée est le maitre de classe
					$student_master=$this->is_master($student);

					if ($this->app->user()->isAuthenticated()) {
						$right = $this->app->user()->getAttribute('right');
						if ($this->is_master($student)) {

							$modify_right = true;
							$summary_right = true;
						} else {

							if (($right[FOLLOW] & MODIFY) && ($right[FOLLOW] & SPECIFIC)) {
								$modify_right = true;
							}

							if (($right[STUDENT_SUMMARY] & VIEW_ALL) || ($right[STUDENT_SUMMARY] & SPECIFIC) && ($student->id() == $this->app->user()->getAttribute('user')->id())) {
								$summary_right = true;
							}
						}
					}
					if ($summary_right || $student_master) {
						
						$MasterClass = $this->managers->getManagerOf('Student')->getMasterClass($student->id());
						$documents = $this->managers->getManagerOf('Document')->getListOf($student->id());

						$this->page->addVar('title', $student->first_name().' '.$student->name());
						$this->page->addVar('description', 'Fiche de l\'élève '.$student->first_name().' '.$student->name());
						$this->page->addVar('keywords', 'fiche, élève, '.$student->id().', '.$student->first_name().', '.$student->name().', '.$student->school_class_id());
						$this->page->addVar('student', $student);
						$this->page->addVar('documents', $documents);
						$this->page->addVar('documentTypes', $this->managers->getManagerOf('documentType')->getList());
						$this->page->addvar('follows', $follows);
						$this->page->addVar('role', $this->app->user()->getAttribute('role'));
						$this->page->addVar('master', $this->is_master($student));
						$this->page->addVar('modify_right', $modify_right);
						$this->page->addVar('summary_right', $summary_right);
						$this->page->addVar('student_master', $student_master);
						$this->page->addVar('MasterClass', $MasterClass);
					}
					else{
						$this->app->httpResponse()->redirect404();
					}
				} else {
					$this->app->httpResponse()->redirect404();
				}
			} else {
				$this->app->httpResponse()->redirect404();
			}
	}
}
