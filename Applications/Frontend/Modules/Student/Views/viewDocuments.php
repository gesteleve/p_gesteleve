<?php 
//*********************************************************
// Societe: ETML
// Auteur : Lanz Romain
// Date : inconnue
// But : Afficher un élève
//*********************************************************
// Modifications:
// Date : 20.05.2014
// Auteur : Lukyantsev  Vladislav
// Raison : Ajout du lien imprimer dans le menu option + ajouter la modalbox d'impression
//*********************************************************


if ($user->isAuthenticated()) { 
$right = $user->getAttribute('right'); 



//Création du menu option pour le controller student avec comme parametre l'id du student
//avec les options Editer ,Supprimer et immprimer
//droits à revoir DLS en profondeur ! différencier le MC de l'enseignant ..
if ($student_master){
    $menu=array(
            "student",$student->id(),"",@STUDENT,$right,
            '<i class="icon-pencil"></i> Editer',"/edit","","",@MODIFY,
            '<i class="icon-trash"></i> Supprimer',"/delete","","",@DELETE,
            '<i class="icon-print"></i> Imprimer',"","Modal","PrintModal",@VIEW_ALL
            );
 
}
else{
    $menu=array(
            "student",$student->id(),"",@STUDENT,$right,
            '<i class="icon-pencil"></i> Editer',"/edit","","",@DELETE,
            '<i class="icon-trash"></i> Supprimer',"/delete","","",@DELETE,
            '<i class="icon-print"></i>  Imprimer',"","Modal","PrintModal",@VIEW_ALL
            );
    }

echo $this->html()->optionmenu($menu);
} 

?>
                      
  <!-- 
Modal pour impression 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur imprimer dans le menu option
-->
<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

		<!-- Titre de la modal box -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel">Impression</h4>
		</div>
			<form id="printForm" method="POST" action="<?php echo $student->id()?>/print">
			<div class="modal-body">
				<!-- Liste déroulante type d'impression -->
				<div class="btn-group">
					<select name="optionList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
						<option selected value="imgOn">Avec photo</option>
						<option value="imgOff">Sans photo</option>
					</select>
				</div>
				      
<?php if(isset($right) && @$right[STUDENT] & MODIFY)
{ ?>
				<!-- Liste déroulante suivis -->
				<div class="btn-group">
					<select name="suivisList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" >
							<option selected value="Followyes">Avec Suivi</option>
							<option value="Followno">Sans Suivi</option>  
						</select>
					</div>
<?php } ?>
				</div>
 <!-- Bouton modal box (fermer,imprimer) -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
					<button type="submit" name="print" value="print" class="btn btn-primary" >Imprimer</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Fin de la modal box d'impression -->                                     

<div id="content">

	<!--<div class="action-button-student">
        <?php if (isset($right) && @$right[STUDENT] & MODIFY):?>
        <a class="btn btn-small" href="<?php echo $this->html()->url('student/'.$student->id().'/edit');?>">Editer l'élève</a>&nbsp;
        <?php endif; ?>
        <?php if (isset($right) && @$right[STUDENT] & DELETE):?>
        <a class="btn btn-small" href="<?php echo $this->html()->url('student/'.$student->id().'/delete');?>">Supprimer l'élève</a>
        <?php endif; ?>
    </div>-->
    <div id="profile">
		<div>
			<h1><?php  echo $student->first_name(); ?> <?php echo $student->name(); ?></h1>
        </div>
		
        <div class="span12 ml0">

			<div id="student-picture" class="span6 ml0">
				<?php
				$photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';

				if (file_exists(WEBROOT.$photo)) {
					?><img class="profile" src="<?php echo $this->html()->url($photo); ?>"> 
                    <?php
				} else {
					?><img class="profile" src="<?php echo $this->html()->url('img/nophoto.jpg'); ?>"> <?php
				}
				?>
			</div>
            <div class="span6 student-info">
                <table>
                    <tr>
                        <td class="grey w150p">
                            Profession
                        </td>
						<?php
							$id = $student->profession_name();
							switch($id)
							{		
								case "Informaticien-ne":
								$id_Profession = "1";
								break;
								
								case "MPT post CFC":
								$id_Profession = "2";
								break;
								
								case "Préapprentissage":
								$id_Profession = "3";
								break;
								
								case "Ebéniste":
								$id_Profession = "4";
								break;
								
								case "Electronicien-ne":
								$id_Profession = "5";
								break;
								
								case "Polymécanicien-ne":
								$id_Profession = "6";
								break;
								
								case "Automaticien-ne":
								$id_Profession = "7";
								break;
								
								case "Mécatronicien-ne d'automobiles":
								$id_Profession = "8";
								break;
								
								case "Menuisier-ère":
								$id_Profession = "9";
								break;
								
								case "SIE - Service Informatique":
								$id_Profession = "10";
								break;
								
								case "Conciergerie":
								$id_Profession = "11";
								break;
								
								case "Administratif":
								$id_Profession = "12";
								break;
								
								case "Social - Vie ETML":
								$id_Profession = "13";
								break;
								
								case "ES - Ecole supérieure":
								$id_Profession = "14";
								break;
								
								case "EST - Enseignement scientifique":
								$id_Profession = "15";
								break;
							
								case "ECG - Enseignements Culture Général":
								$id_Profession = "16";
								break;
							
								case "Ebéniste - Menuisier-ère":
								$id_Profession = "17";
								break;
								
								case "Bois":
								$id_Profession = "18";
								break;

							}
						?>
                        <td>
							<a class="info-link" href="<?php echo $this->html()->url('profession/'.$id_Profession); ?>"><?php echo $student->profession_name(); ?></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="grey w150p">
                            Classe
                        </td>
                        <td>
                            <a class="info-link" href="<?php echo $this->html()->url('class/'.$student->school_class_id()); ?>"><?php echo $student->school_class_id(); ?></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="grey w150p">
                            Ville
                        </td>
                        <td>
                            <?php echo $student->zip()."  ".$student->city(); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="grey w150p">
                            Maître de classe
                        </td>
                        <td>
                        <a class="info-link" href="<?php echo $this->html()->url('colleague/'.$MasterClass->id()) ?>"><?php echo $MasterClass->first_name().' '.ucfirst(strtolower($MasterClass->name())); ?></a>
                        </td>
                    </tr>
                    <?php if ($summary_right):?>
                    <tr>
                        <td class="grey w150p">
                            Adresse
                        </td>
                        <td>
                            <?php echo $student->address(); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="grey w150p">
                            Anniversaire
                        </td>
                        <td>
                            <?php 
                            $date_birth = date("d.m.Y", strtotime($student->birth_date()));    
                            echo $date_birth; ?>
                        </td>
                    </tr>
                    
					<tr>
						<td class="grey w150p">
							Téléphone Mobile
						</td>
						<td>
						<?php echo $student->mobile(); ?>
						</td>
					</tr>
					<tr>
						<td class="grey w150p">
							Téléphone Fixe
						</td>
						<td>
							<?php echo $student->phone(); ?>
						</td>
					</tr>
					<tr>
						<td class="grey w150p">
							Email
						</td>
						<td>
							<?php echo $student->email(); ?>
						</td>
					</tr>
                    <?php endif; ?>
                </table>
            </div>
			<?php if(($summary_right)): ?>
			<div class="span12 ml0 student-summary small-top">
				<div class="widget-header">
					<i class="icon-tags"></i>
					<h5>Résumé</h5>
				</div>
				<div class="summary_box slimscroll">
					<?php if (($student->summary() != null) && ($student->summary() != ' ')): ?>
					<?php echo $student->summary(); ?>
					<?php else: ?>
					L' élève n'a pas de résumé
					<?php endif ?>
				</div>
			</div>
			<?php endif; ?>
        </div>
    </div>

    <!--<?php if(($summary_right)): ?>
    <div class="span5">
        <div class="summary_box">
            <?php if (($student->summary() != null) && ($student->summary() != ' ')): ?>
            <?php echo $student->summary(); ?>
            <?php else: ?>
            L' élève n'a pas de résumé
            <?php endif ?>
        </div>
    </div>
    <?php endif; ?>-->
    <!-- Désactivé
    <div class="span6">
        <div class="overview_boxes">
            <div class="box_row clearfix" style="display: inline-block;">
                <div class="widget-tasks-statistics">
                    <div class="userstats clearfix" style="margin-top: 25px;">
                        <div class="white">
                            <i style="color:#E28271" class="icon-eye-open"></i>
                            <p style="color:#E28271">+35%</p>
                        </div>
                        <div>
                            <div>
                                <input class="knob" data-width="120" data-height="120" data-displayinput="false" data-readonly="true" data-thickness=".15" value="35">
                            </div>
                        </div>
                        <p>
                            <strong>+ 4 jours</strong>Abscences
                        </p>
                    </div>
                </div>
                <div class="widget-tasks-statistics">
                    <div class="userstats clearfix" style="margin-top: 25px;">
                        <div class="white">
                            <i style="color:#E28271" class="icon-eye-open"></i>
                            <p style="color:#E28271">+85%</p>
                        </div>
                        <div>
                            <div>
                                <input class="knob" data-width="120" data-height="120" data-displayinput="false" data-readonly="true" data-thickness=".15" value="85">
                            </div>
                        </div>
                        <p>
                            <strong>+ 7</strong>Arrivées tardives
                        </p>
                    </div>
                </div>
                <div class="widget-tasks-statistics">
                    <div class="userstats clearfix" style="margin-top: 25px;">
                        <div class="white">
                            <i style="color:#E28271" class="icon-eye-open"></i>
                            <p style="color:#E28271">+85%</p>
                        </div>
                        <div>
                            <div>
                                <input class="knob" data-width="120" data-height="120" data-displayinput="false" data-readonly="true" data-thickness=".15" value="0">
                            </div>
                        </div>
                        <p>
                            <strong>0</strong>Mise à la porte
                        </p>
                    </div>
                </div>
                <div class="widget-tasks-statistics">
                    <div class="userstats clearfix" style="margin-top: 25px;">
                        <div class="white">
                            <i style="color:#E28271" class="icon-eye-open"></i>
                            <p style="color:#E28271">+55%</p>
                        </div>
                        <div>
                            <div>
                                <input class="knob" data-width="120" data-height="120" data-displayinput="false" data-readonly="true" data-thickness=".15" value="85">
                            </div>
                        </div>
                        <p>
                            <strong>+11</strong>Diminution
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
<!-- Coordonnée (Déplacer dans les infos général
    <div class="span12 ml0" id="coordonnee">
        <?php if (isset($right) && @$right[STUDENT] & VIEW_ALL):?>
        <div class="span4 ml0">
            <p><span class="grey"><i class="icon-mobile-phone"></i></span> <?php echo $student->mobile(); ?></p>
        </div>
        <div class="span4">
            <p><span class="grey"><i class="icon-phone"></i></span> <?php echo $student->phone(); ?></p>
        </div>
        <div class="span4">
            <p><span class="grey"><i class="icon-envelope"></i></span> <?php echo $student->email(); ?></p>
        </div>
        <?php else: ?>
            <p><span class="grey">Pour avoir accès à toutes les informations il faut vous connecter.</span></p>
        <?php endif; ?>
    </div>-->

    <?php  if (isset($right) && @$right[FOLLOW] & VIEW_ALL):?>
    <div id="widget-follow" class="widget span6 ml0 small-top">
        <div class="widget-header">
            <i class="icon-eye-open"></i>
            <h5>Suivis <!--de <?php echo $student->first_name(); ?> <?php echo $student->name(); ?>--></h5>
        </div>
        <div class="widget-body">
            <div class="widget-tickets clearfix slimscroll" style="overflow: auto; width: auto; height: 310px;">
                <ul id="followList">
                    <?php foreach($follows as $follow): ?>
					<?php $is_student = ($follow->student_id()   == $this->app->user()->getAttribute('user')->id()) ? 1 : 0; ?>
					<?php $is_author  = ($follow->colleague_id() == $this->app->user()->getAttribute('user')->id()) ? 1 : 0; ?>
                    <?php $is_mainMaster  = (($this->app->user()->getAttribute('group') == 10) &&  ($this->app->user()->getAttribute('user')->profession_id() == $id_Profession)) ? 1 : 0; ?>
                    <?php $is_doyen  = ($this->app->user()->getAttribute('group') == 9) ? 1 : 0; ?>
                    <?php $is_direction  = ($this->app->user()->getAttribute('group') == 3) ? 1 : 0; ?>
                    <?php $is_admin  = ($this->app->user()->getAttribute('group') == 2) ? 1 : 0; ?>
                    <?php if (($is_student && $follow->right() != 1) || $is_author || $master || $is_mainMaster || $is_doyen || $is_direction || $is_admin || @$right[FOLLOW] & DELETE): ?>
                    <?php $modify_btn = (($follow->colleague_id() == $this->app->user()->getAttribute('user')->id()) || (@$right[FOLLOW] & MODIFY)) ? 1 : 0; ?>
                    <li>
                        <a data-toggle="modal" href="<?php echo $this->html()->url('follow/'.$follow->id().'/view'); ?>" data-target="#viewFollow">
                        <h5>Le <?php echo date_format($follow->add_date(), 'j F Y'); ?>
                            par <?php echo $follow->colleague_first_name().' '.ucfirst(strtolower($follow->colleague_name())); ?>
                            <?php // echo ($follow->right()) ? '<i class="icon-lock"></i>' : ''; ?>
                        </h5>
                        </a>
                        
						<?php 
                            $LimitFollow = substr($follow->content(), 0, 40); 
                            $LimitFollow = str_replace("\n", " ", $LimitFollow);

                        ?>
                        <p><?php echo $LimitFollow."..."; ?></p>

                        <?php if($follow->mod_colleague_id() != null): ?>
                        <div class="mod <?php echo ($modify_btn) ? 'nearby-btn' : ''; ?>"><?php echo date_format($follow->mod_date(), 'd.m.y');?> par <?php echo $follow->mod_colleague_first_name().' '.ucfirst(strtolower($follow->mod_colleague_name())); ?></div>
                        <?php endif; ?>

                        <?php if ($modify_btn): ?>
                        <a data-toggle="modal" href="<?php echo $this->html()->url('follow/'.$follow->id().'/edit'); ?>" data-target="#modFollow" class="btn mod"><i class="icon-pencil"></i></a>
                        <?php endif; ?>
                    </li>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="widget-footer">
            <?php if (isset($right) && @$right[FOLLOW] & ADD):?>
            <a href="#addFollow" role="button" class="pull-right btn btn-small" data-toggle="modal"><i class="icon-plus"></i> Ajouter un suivi</a>
            <a href="<?php echo $this->html()->url('student/'.$student->id().'/print/follow')?>" role="button" class="pull-right btn btn-small" data-toggle="modal"><i class="icon-print"></i> Imprimer</a>
            <?php endif; ?>
        </div>
    </div>

    <form id="follow" action="<?php echo $this->html()->url('follow/add'); ?>" name="follow" method="post">
        <div id="addFollow" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Ajout de suivi pour <?php echo $student->first_name(); ?> <?php echo $student->name(); ?></h3>
            </div>
            <div class="modal-body">
                <textarea rows="2" class="input-block-level" name="content"></textarea>
                <input type="hidden" name="student_id" value="<?php echo $student->id(); ?>"/>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit">Ajouter</button>
            </div>
        </div>
    </form>

    <div id="modFollow" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Modification du suivi</h3>
        </div>
        <div class="modal-body follow"></div>
        <div class="modal-footer">
            <button id="btn-delete-follow" class="btn btn-danger l">Supprimer le suivi</button>
            <button id="btn-edit-follow" class="btn btn-primary" type="submit">Modifier</button>
        </div>
    </div>
	
	    <div id="viewFollow" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 id="myModalLabel">Affichage du suivi</h3>
        </div>
        <div class="modal-body follow"></div>
		<div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn-edit-follow" data-dismiss="modal" aria-hidden="true">Fermer</button>
        </div>
		</div>
	
    <?php endif; ?>

    <?php if (isset($right) && @$right[STUDENT] & VIEW_ALL):?>
    <div id="widget-document" class="widget span6 small-top">
        <div class="widget-header">
            <i class="icon-folder-close"></i>
            <h5>Documents</h5>
            <select id="doc-filter" class="pull-right , doc-filter" name="documentType" id="documentType">
                <?php $i = 0; ?>
                <?php foreach ($documentTypes as $documentType): ?>
                    <?php if ($i == 0) echo '<option value="0">Tout</option>'; $i = 1; ?>
                    <option value="<?php echo $documentType->id() + 1; ?>"><?php echo $documentType->name(); ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="widget-body">
            <div class="widget-tickets clearfix slimscroll" style="overflow: auto; width: auto; height: 310px;">
                <ul>
                    <?php $is_administrator  = ($this->app->user()->getAttribute('group')== ADMINISTRATOR)? 1 : 0; ?>
                    <?php $is_mainMaster  = (($this->app->user()->getAttribute('group') == 10) &&  ($this->app->user()->getAttribute('user')->profession_id() == $id_Profession)) ? 1 : 0; ?>
                    <?php $is_doyen  = ($this->app->user()->getAttribute('group') == 9) ? 1 : 0; ?>
                    <?php $is_direction  = ($this->app->user()->getAttribute('group') == 3) ? 1 : 0; ?>
                    <?php $is_admin  = ($this->app->user()->getAttribute('group') == 2) ? 1 : 0; ?>

                    <?php foreach ($documents as $document): ?>

                        <?php $is_student = ($document->student_id()   == $this->app->user()->getAttribute('user')->id())? 1 : 0; ?>
                        <?php $is_author  = ($document->colleague_id() == $this->app->user()->getAttribute('user')->id())? 1 : 0; ?>

                        <?php $is_teacher  = ($document->right()==2)? 1 : 0 ; ?>
                        <?php $is_student_visible  = ($is_student && ($document->right()==1))? 1 : 0 ; ?>

                        <?php  $is_visible =($is_student_visible || $master || $is_author || $is_mainMaster || $is_doyen || $is_direction || $is_admin || ($is_teacher and !$is_student ))? 1 : 0 ; ?>



                        <?php $role = 2; ?>
                        <?php if ($is_visible || $is_administrator): ?>

                        <li class="file <?php echo $document->types_document_id() + 1; ?>">
                            <div class="file-uploaded">
                                <a data-target="#dl-vw-document" href="<?php echo $this->html()->url('document/'.$document->id().'/action'); ?>" data-toggle="modal" data-target="#dl-vw-document">
                                    <div class="document-info">
                                        <div class="document-info-element">
                                            <?php echo $document->title(); ?>
                                        </div>
                                        <div>
                                            <b>Description:</b> <?php echo $document->description(); ?>
                                        </div>
                                        <div>
                                            <span><b>Date:</b> <?php echo date_format($document->add_date(), 'd.m.Y H:i'); ?> </span>
                                        </div>
                                        <div>
                                            <span><b>Taille:</b> <?php echo number_format($document->size()/1024,2) ?> Kb</span>
                                        </div>
                                    </div>
                                </a>
                                <?php if ($master || $is_author || $is_administrator ): ?>
                                    <div class="document-edit">
                                        <a class="btn mod" data-toggle="modal" data-target="#edit-document" href="<?php echo $this->html()->url('document/'.$document->id().'/edit'); ?>"><i class="icon-pencil"></i></a>
                                    </div>
                                <?php endif ?>
                            </div>
                        </li>

                        <?php endif ?>
                    <?php endforeach ?>

                </ul>
            </div>
        </div>

        <div class="widget-footer">

            <?php if (($master && isset($right)) || $is_administrator ):?>
            <a href="<?php echo $this->html()->url('student/'.$student->id().'/add/document'); ?>" class="pull-right btn btn-small"><i class="icon-plus"></i>&nbsp;Ajouter un document</a>
            <?php endif; ?>
        </div>
    </div>

    <div id="dl-vw-document" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="document-label" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="document-label">Document</h3>
        </div>

        <div class="modal-body"></div>

        <div class="modal-footer">
            <button id="btn-view-document" class="btn btn-primary l" aria-hidden="true">Afficher le document</button>
            <button id="btn-download-document" class="btn btn-success">Télécharger le document</button>
        </div>
    </div>

    <div id="edit-document" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="edit-document-label" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="edit-document-label">Modification du document</h3>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
            <button id="btn-delete-document" class="btn btn-danger l" type="submit">Supprimer le document</button>
            <button id="btn-edit-document" class="btn btn-primary" type="submit">Modifier</button>
        </div>
    </div>
    <?php endif; ?>
</div>

<!-- Desactivé
<div id="example_modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Modal header</h3>
     </div>
    <div class="modal-body">
            <p>One fine body…</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
    </div>
</div>-->

<script>
	
$(window).keypress(function(event) {
	
	if(event.ctrlKey && event.which == 112){
		$('#PrintModal').modal('toggle');
		event.preventDefault();
		return false;
	}
});
	
</script>
