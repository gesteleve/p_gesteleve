<?php
	echo($uploadResult);
?>
<?php if(isset($resizeSelect)): ?>
<div id="defaultResize" class="hidden">
	<span class="x1"><?php echo $resizeSelect['x1']; ?></span>
	<span class="x2"><?php echo $resizeSelect['x2']; ?></span>
	<span class="y1"><?php echo $resizeSelect['y1']; ?></span>
	<span class="y2"><?php echo $resizeSelect['y2']; ?></span>
	<span class="origW"><?php echo $resizeSelect['origW']; ?></span>
	<span class="origH"><?php echo $resizeSelect['origH']; ?></span>
</div>
<?php endif; ?>