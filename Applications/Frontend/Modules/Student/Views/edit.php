<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>

<?php $this->html()->js('jquery.imgareaselect.min.js'); ?>
<?php $this->html()->js('../assets/ckeditor/ckeditor.js'); ?>

<div id="content">
    <form action="" method="post">
		<div class="span12">
			<div class="span8">
				<fieldset>
					<legend>Informations générales</legend>
					<div class="span6 ml0">
						<label>Nom</label>
						<input type="text" name="name" tabindex="1" class="input-block-level" value="<?php echo $student->name(); ?>">
						<label>Login</label>
						<input type="text" name="id" tabindex="3" class="input-block-level" value="<?php echo $student->id(); ?>" disabled>
						<div class="span6 ml0">
							<label>Date de début</label>
							<select tabindex="5" name="entry_date" class="input-block-level" disabled>
								<?php
									for($i=date("Y")+5; $i>=date("Y")-5; $i--) {
										if($i == $student->entry_date()) {
											echo '<option value="'.$i.'" selected>'.$i.'</option>';
										}
										else {
											echo '<option value="'.$i.'">'.$i.'</option>';
										}
									}
								?>
							</select>
						</div>

						<div class="span6">
							<label>Date de fin</label>
							<select tabindex="6" name="exit_date" class="input-block-level">
								<?php
									for($i=date("Y")+10; $i>=date("Y")-10; $i--) {
										if($i == $student->exit_date()) {
											echo '<option value="'.$i.'" selected>'.$i.'</option>';
										}
										else {
											echo '<option value="'.$i.'">'.$i.'</option>';
										}
									}
								?>
							</select>
						</div>
					</div>
					<div class="span6">
						<label>Prénom</label>
						<input type="text" name="first_name" tabindex="2" class="input-block-level" value="<?php echo $student->first_name(); ?>">
						<label>Classe</label>
						<select tabindex="4" name="class" class="input-block-level">
							<?php
								// Parcours les classes
								foreach($classes as $class) {
									// Insère les classes dans le select
									if ($class->id() == $student->school_class_id()) {
										echo '<option value="'.$class->id().'" selected>'.$class->id().'</option>';
									} else {
										echo '<option value="'.$class->id().'">'.$class->id().'</option>';
									}
								}
							?>
						</select>
					</div>
				</fieldset>

				<fieldset>
					<legend>Informations personelles</legend>
					<label>Adresse</label>
					<input type="text" name="address" tabindex="6" class="input-block-level" value="<?php echo $student->address(); ?>">
					<div class="span6 ml0">
						<div class="span3">
							<label>NPA</label>
							<input type="text" name="zip" tabindex="7" class="input-block-level" value="<?php echo $student->zip(); ?>">
						</div>
						<div class="span9">
							<label>Ville</label>
							<input type="text" name="city" tabindex="8" class="input-block-level" value="<?php echo $student->city(); ?>">
						</div>
						<label>Téléphone</label>
						<input type="text" name="phone" tabindex="10" class="input-block-level" value="<?php echo $student->phone(); ?>">
						<label>Date de naissance</label>
						<input type="date" name="birth_date" tabindex="12" class="input-block-level" value="<?php echo $student->birth_date(); ?>">
					</div>
					<div class="span6">
						<label>E-Mail</label>
						<input type="text" name="email" tabindex="9" class="input-block-level" value="<?php echo $student->email(); ?>">
						<label>Téléphone portable</label>
						<input type="text" name="mobile" tabindex="11" class="input-block-level" value="<?php echo $student->mobile(); ?>">
					</div>
				</fieldset>
				<?php 
				if ($summary_right):?>
					<fieldset>
						<legend>Résumé</legend>
						<textarea class="ckeditor" name="summary" rows="2"><?php echo substr($student->summary(), 0, strrpos($student->summary(), '<span class="mod_date">')); ?></textarea>
					</fieldset>
				<?php endif; ?>
			</div>
			<div class="span12 center">
				<div class="span12">
					<fieldset>
						<legend>Photo</legend>
						<img id="studentPicture" class="center" src="
						<?php
						$photo = '/img/students/'.$student->entry_date().'/'.$student->id().'.jpg';
						if (file_exists(WEBROOT.$photo)) {
							echo $this->html()->url($photo);
						} else {
							echo $this->html()->url('/img/nophoto.jpg');
						}
						?>">
						<button type="button" class="btn center">Charger la photo</button>
					</fieldset>
				</div>
			</div>
		</div>
        <div class="span12 center small-top">
            <fieldset>
                <button class="btn btn-large btn-primary center" type="submit">Enregistrer</button>
            </fieldset>
        </div>

    </form>

</div>
<script type="text/javascript">
$(document).ready(function () {
    $('img#studentPicture').imgAreaSelect({
        handles: true,
    });
});
</script>
