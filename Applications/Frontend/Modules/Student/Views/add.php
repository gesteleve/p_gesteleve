<?php $this->html()->css('jasny-bootstrap.min.css'); ?>
<?php $this->html()->css('jasny-bootstrap-responsive.min.css'); ?>
<?php $this->html()->js('jquery.imgareaselect.min.js'); ?>
<?php $this->html()->js('jasny-bootstrap.min.js'); ?>
<?php $this->html()->js('jquery.form.js'); ?>

<div id="content">
	<form id="student" action="" name="student" method="post">
		<div class="span8">
	        <fieldset>
	            <legend>Informations générales</legend>
	            <div class="span6 ml0">
	            	<label>Nom</label>
	            	<input type="text" name="name" tabindex="1" class="input-block-level" value="<?php echo isset($_POST['name']) ? $_POST['name'] : null ?>" required>
	            	<label>Login</label>
	            	<input type="text" name="id" tabindex="3" class="input-block-level" value="<?php echo isset($_POST['id']) ? $_POST['id'] : null ?>" required>
	            	<div class="span6 ml0">
	            		<label>Date de début</label>
		            	<select tabindex="5" name="entry_date" class="input-block-level">
		            		<?php
		            			$date = date('Y');
		            			if (isset($_POST['entry_date'])) {
		            				for($i=$date+5; $i>=$date-5; $i--) {
			            				if ($i == $_POST['entry_date']) {
			            					echo '<option value="'.$i.'" selected>'.$i.'</option>';
			            				} else {
			            					echo '<option value="'.$i.'">'.$i.'</option>';
			            				}
			            			}
		            			} else {
		            				for($i=$date+5; $i>=$date-5; $i--) {
			            				if($i != $date) {
			            					echo '<option value="'.$i.'">'.$i.'</option>';
			            				}
			            				else {
			            					echo '<option value="'.$i.'" selected>'.$i.'</option>';
			            				}
			            			}
		            			}
		            			unset($date);
		            		?>
		            	</select>
		            </div>

	            	<div class="span6">
	            		<label>Date de fin</label>
	            		<select tabindex="6" name="exit_date" class="input-block-level">
	            			<?php
	            			$date = date('Y');
		            			if (isset($_POST['exit_date'])) {
		            				for($i=$date+10; $i>=$date-10; $i--) {
			            				if ($i == $_POST['exit_date']) {
			            					echo '<option value="'.$i.'" selected>'.$i.'</option>';
			            				} else {
			            					echo '<option value="'.$i.'">'.$i.'</option>';
			            				}
			            			}
		            			} else {
		            				for($i=$date+10; $i>=$date-10; $i--) {
			            				if($i != $date+4) {
			            					echo '<option value="'.$i.'">'.$i.'</option>';
			            				}
			            				else {
			            					echo '<option value="'.$i.'" selected>'.$i.'</option>';
			            				}
			            			}
		            			}
		            			unset($date);
		            		?>
	            		</select>
	            	</div>
	            </div>
	            <div class="span6">
	            	<label>Prénom</label>
	            	<input type="text" name="first_name" tabindex="2" class="input-block-level" value="<?php echo isset($_POST['first_name']) ? $this->h($_POST['first_name']) : null ?>" required>
	            	<label>Classe</label>
	            	<select tabindex="4" name="class" class="input-block-level">
	  					<?php
	  						// Parcours les classes
	  						foreach($classes as $class) {
	  							if (isset($_POST['class'])) {
			            			if ($_POST['class'] == $class->id()) {
			            				echo '<option value="'.$class->id().'" selected>'.$class->id().'</option>';
			            			}
			            			else {
			  							// Insère les classes dans le select
			  							echo '<option value="'.$class->id().'">'.$class->id().'</option>';
			  						}
			            		}
			            		else {
		  							// Insère les classes dans le select
		  							echo '<option value="'.$class->id().'">'.$class->id().'</option>';
		  						}
	  						}
	  					?>
					</select>
	            </div>
	        </fieldset>

	        <fieldset>
	            <legend>Informations personelles</legend>
	            <label>Adresse</label>
	            <input type="text" name="address" tabindex="6" class="input-block-level" value="<?php echo isset($_POST['address']) ? $_POST['address'] : null ?>" required>
	            <div class="span6 ml0">
	            	<div class="span3">
	            		<label>NPA</label>
	            		<input type="text" name="zip" tabindex="7" class="input-block-level" value="<?php echo isset($_POST['zip']) ? $_POST['zip'] : null ?>" required>
	            	</div>
	            	<div class="span9">
	            		<label>Ville</label>
	            		<input type="text" name="city" tabindex="8" class="input-block-level" value="<?php echo isset($_POST['city']) ? $_POST['city'] : null ?>" required>
	            	</div>
	            	<label>Téléphone</label>
	            	<input type="text" name="phone" tabindex="10" class="input-block-level" value="<?php echo isset($_POST['phone']) ? $_POST['phone'] : null ?>">
	            	<label>Date de naissance</label>
	            	<input type="date" name="birth_date" tabindex="12" class="input-block-level" value="<?php echo isset($_POST['birth_date']) ? $_POST['birth_date'] : null ?>" required>
	            </div>
	            <div class="span6">
	            	<label>E-Mail</label>
	            	<input type="email" name="email" tabindex="9" class="input-block-level" value="<?php echo isset($_POST['email']) ? $_POST['email'] : null ?>" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
	            	<label>Téléphone portable</label>
	            	<input type="text" name="mobile" tabindex="11" class="input-block-level" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : null ?>">
	            	<label>Commentaire</label>
	            	<input type="text" name="comment" tabindex="13" class="input-block-level" value="<?php echo isset($_POST['comment']) ? $_POST['comment'] : null ?>">
	            </div>
	        </fieldset>
	        <div class="hidden">
	        	<input type="text" name="x1">
	        	<input type="text" name="y1">
	        	<input type="text" name="x2">
	        	<input type="text" name="y2">
	        	<input type="text" name="w">
	        	<input type="text" name="h">
	        </div>
    	</div>
    </form>
    <!--<form id="formPhoto" action="add/upload">
		<div class="span12 center">
			<div class="span12">
				<fieldset>
					<legend>Photo</legend>
					<div class="span12 center">
						<div id="studentPicture" class="center">
							<?php if(($this->app->user()->getAttribute('photoUpload') != null)&&(isset($_POST['w']))): ?>
							<img class="center" src="<?php echo $this->html()->url('tmp/'.$this->app->user()->getAttribute('photoUpload').'.jpg');?>" />
							<div id="defaultResize" class="hidden">
								<?php $photoResize = $this->app->user()->getAttribute('photoResize'); ?>
								<span class="x1"><?php echo isset($_POST['x1']) ? $_POST['x1'] : null ?></span>
								<span class="x2"><?php echo isset($_POST['x2']) ? $_POST['x2'] : null ?></span>
								<span class="y1"><?php echo isset($_POST['y1']) ? $_POST['y1'] : null ?></span>
								<span class="y2"><?php echo isset($_POST['y2']) ? $_POST['y2'] : null ?></span>
								<span class="origW"><?php echo $photoResize['origW'] ?></span>
								<span class="origH"><?php echo $photoResize['origH'] ?></span>
							</div>
							<?php else: ?>
							<span class="uploadInfo center">Aucune photo</span>
							<?php endif; ?>
						</div>
					</div>
					<div class="span12 center" data-provides="fileupload">
						<span class="center">
							<span class="fileupload-new"></span>
							<span class="fileupload-exists"></span>
							<input id="selectedPhoto" name="photo" type="file" accept="image/jpg" class="center"/>
						</span>
						<span class="fileupload-preview"></span>
						<!--<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>-->
					<!--</div>
				</fieldset>
			</div>
		</div>-->
    	<div class="placeholder"></div>
			<div class="span12 center">
				<fieldset>
					<legend>Photo</legend>
					<div class="span12 center">
						<div id="studentPicture" class="span4 center">
							<?php if(($this->app->user()->getAttribute('photoUpload') != null)&&(isset($_POST['w']))): ?>
							<img class="center" src="<?php echo $this->html()->url('tmp/'.$this->app->user()->getAttribute('photoUpload').'.jpg');?>" />
							<div id="defaultResize" class="hidden">
								<?php $photoResize = $this->app->user()->getAttribute('photoResize'); ?>
								<span class="x1"><?php echo isset($_POST['x1']) ? $_POST['x1'] : null ?></span>
								<span class="x2"><?php echo isset($_POST['x2']) ? $_POST['x2'] : null ?></span>
								<span class="y1"><?php echo isset($_POST['y1']) ? $_POST['y1'] : null ?></span>
								<span class="y2"><?php echo isset($_POST['y2']) ? $_POST['y2'] : null ?></span>
								<span class="origW"><?php echo $photoResize['origW'] ?></span>
								<span class="origH"><?php echo $photoResize['origH'] ?></span>
							</div>
							<?php else: ?>
							<span class="uploadInfo">Aucune photo</span>
							<?php endif; ?>
						</div>
					</div>
					<div class="center fileupload fileupload-new" data-provides="fileupload">
						<span class="btn btn-file center">
							<span class="fileupload-new"></span>
							<span class="fileupload-exists"></span>
							<input id="selectedPhoto" name="photo" type="file" accept="image/jpg" />
						</span>
					  <span class="fileupload-preview"></span>
					  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
					</div>
				</fieldset>
			</div>
    </form>
    <div class="placeholder"></div>
    <div class="span12 ml0">
	    <fieldset>
	        <button class="btn btn-large btn-primary center mt30" type="button" onClick="$('form#student').submit();">Enregistrer</button>
	    </fieldset>
    </div>

</div>
<script type="text/javascript">
function setAeraSelect() {
	$('#studentPicture img').imgAreaSelect({
	    handles: true,
	    aspectRatio: '3:4',
	    minHeight: 240,
	    minWidth: 180,
	    imageHeight: $('#defaultResize .origH').text(),
	    imageWidth: $('#defaultResize .origW').text(),
	    x1: $('#defaultResize .x1').text(), y1: $('#defaultResize .y1').text(), x2: $('#defaultResize .x2').text(), y2: $('#defaultResize .y2').text(),
	    onSelectEnd: function (img, selection) {
	    	if(selection.height == 0)
	    	{
	    		$('#studentPicture img').imgAreaSelect({
			        handles: true,
			        aspectRatio: '3:4',
			        minHeight: 240,
			        minWidth: 180,
			        imageHeight: $('#defaultResize .origH').text(),
			        imageWidth: $('#defaultResize .origW').text(),
	    			x1: $('#defaultResize .x1').text(), y1: $('#defaultResize .y1').text(), x2: $('#defaultResize .x2').text(), y2: $('#defaultResize .y2').text()
	    		});
	    		$("input[name='x1']").val($('#defaultResize .x1').text());
	        	$("input[name='y1']").val($('#defaultResize .y1').text());
	        	$("input[name='x2']").val($('#defaultResize .x2').text());
	        	$("input[name='y2']").val($('#defaultResize .y2').text());
	        	$("input[name='w']").val($('#defaultResize .x2').text()-$('#defaultResize .x1').text());
	        	$("input[name='h']").val($('#defaultResize .y2').text()-$('#defaultResize .y1').text());
	    	}
	    	else {
	        	$("input[name='x1']").val(selection.x1);
	        	$("input[name='y1']").val(selection.y1);
	        	$("input[name='x2']").val(selection.x2);
	        	$("input[name='y2']").val(selection.y2);
	        	$("input[name='w']").val(selection.width);
	        	$("input[name='h']").val(selection.height);
	        }
	    }
	});
}

function gen_login() {
	$("input[name='id']").val($("input[name='name']").val().toLowerCase().replace(/[\'\ ]/g,'').substr(0, 8)+$("input[name='first_name']").val().toLowerCase().replace(/[\'\ ]/g,'').substr(0, 2));
}

$(document).ready(function () {

	if($('#defaultResize .x1').length != 0) {
	  	setAeraSelect();
	  }
    $('#selectedPhoto').change(function(e){
      $('#studentPicture img').imgAreaSelect({
      	remove: true
      });

      $("input[name='x1']").val(0);
      $("input[name='y1']").val(0);
      $("input[name='x2']").val(0);
      $("input[name='y2']").val(0);
      $("input[name='w']").val(0);
      $("input[name='h']").val(0);

      $('#selectedPhoto').attr("name", "photo");

	  if($(this).val() != ''){
	  	$('#formPhoto').ajaxSubmit({
                success: function (response){
                	$('#studentPicture').html(response);
                	$("input[name='x1']").val($('#defaultResize .x1').text());
		        	$("input[name='y1']").val($('#defaultResize .y1').text());
		        	$("input[name='x2']").val($('#defaultResize .x2').text());
		        	$("input[name='y2']").val($('#defaultResize .y2').text());
		        	$("input[name='w']").val($('#defaultResize .x2').text()-$('#defaultResize .x1').text());
		        	$("input[name='h']").val($('#defaultResize .y2').text()-$('#defaultResize .y1').text());
    				setAeraSelect();
                },
                beforeSend: function() {
	                $('#studentPicture').html('<span class="uploadInfo">0%</span>');
	            },
	            uploadProgress: function(event, position, total, percentComplete) {
	                $('#studentPicture').html('<span class="uploadInfo">'+percentComplete+'%</span>');
	            }
            });
	  }
	  else{
	  	$('#studentPicture').html('<span class="uploadInfo">Aucune photo</span>');
	  }

	});
	
	$("input[name='name']").bind('keyup', function() { gen_login(); });
	$("input[name='first_name']").bind('keyup', function() { gen_login(); });
});
</script>
