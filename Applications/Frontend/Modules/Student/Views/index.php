﻿<?php $this->html()->js('isotope.min.js'); ?>
<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>

<!-- DESACTIVE 


<ul id="filters">
  <div id="content"></div>
<div class="container">
  <div class="row">

        
     <form class="form-search" method="POST" action="#">
                       
            <div class="btn-group pull-right" data-toggle="buttons-radio">
              <input type="text" class="span2" name="SearchValue" style="width: 150px;">
                <button class="btn active" name="filter" value="all">Etudiant</button>
                <button class="btn" name="filter" value="profession">Section</button>
                <button class="btn" name="filter" value="classe">Classes</button>
            </div>
            
        </form>
        
    </div>
</div>
</ul>
-->

<div class="mt30">
  <div id="container">
    
<?php 
    //print_r($result["Student"]);
  ?>
    <?php
    if (!empty($result['schoolClass']))
    {
       $SearchResult=$result["schoolClass"];
            //print_r($SearchResult);

    }
    elseif(!empty($result['Profession']))
    {
       $SearchResult=$result["Profession"];
             //print_r($SearchResult);
             //print("<br><br>");

    }
    elseif(!empty($result['Student']))
    {
       $SearchResult=$result["Student"];
      //print_r($SearchResult);

    }
    else
    {
      $SearchResult=$students;
    }

          ?>

            <h3><?php echo 'Nb étudiants : '. count($SearchResult);?></h3>


        <?php

   
  foreach ($sections as $section){

            echo(' <ul class="thumbnailsClassList">');

?>
            <a class="info-link" href="<?php echo $this->html()->url('profession/'.$section->profession_id()); ?>">
            <br/><H3>
            <?php echo $section->name()." (".$section->student_nbre().")"; ?></a>
            </H3><br/>
<?php

            //echo "<br/><H3>".$section->name()." (".$section->student_nbre().")</H3><br/>";

      foreach ($SearchResult as $student){
        if($section->name() == $student->profession_name() ){?>
    
          <div class="post <?php echo $student->school_class_id().' '.$student->profession_id(); ?>">
            <a href="<?php echo $this->html()->url('student/'.$student->id()); ?>" data-toggle="modal">
                <div class="picture">
                    <?php
                    $photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';

                   if (file_exists(WEBROOT.$photo)) {
                        ?><img src="<?php echo $this->html()->url($photo); ?>" alt=""><?php
                    } else {
                        ?><img src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
                    }
                    
                    ?>
                </div>
                <div class="information">
                  <h5>
                    <a class="info-link" href="<?php echo $this->html()->url('student/'.$student->id()); ?>"><?php echo $student->name(); ?> <?php echo $student->first_name(); ?></a>
                  </h5>

                  <p><?php echo $student->id(); ?></p>
                  <p><a class="info-link" href="<?php echo $this->html()->url('class/'.$student->school_class_id()); ?>"><?php echo $student->school_class_id(); ?></a></p>
                  <p><a class="info-link" href="<?php echo $this->html()->url('profession/'.$student->profession_id()); ?>"><?php echo $student->profession_name(); ?></a></p>
                </div>
            </a>
          </div>
        <?php }
      }
    
  echo('</ul>'); } ?>
  </div>
</div>