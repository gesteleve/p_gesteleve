<?
	require('assets/fpdf/fpdf.php');

	if(!isset($showPhoto) or !isset($student)){
		echo "<script>location.href = history.back(-1);</script>";
	}
	
	$GLOBALS['Student'] = $student;
	
	$GLOBALS['Logo'] = 'img/logo.png';
	
	//Infos list. Name, adress, ...
	$infos = array(	array("Profession", $student->profession_name()),
					array("Classe", $student->school_class_id()),
					array("Ville", $student->zip()."  ".$student->city()),
					array("Ma�tre de classe",  $MasterClass->first_name().' '.ucfirst(strtolower($MasterClass->name()))),
					array("Adresse",$student->address()),
					array("Anniversaire", date("d.m.Y", strtotime($student->birth_date()))),
					array("T�l�phone Mobile", $student->mobile()),
					array("T�l�phone Fixe", $student->phone()),
					array("Email", $student->email()),
				);
	
	//student face
	$photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';
	
	if(!file_exists(WEBROOT.$photo))
	{
		$photo = 'img/nophoto.jpg';
	}
	
	$cursorY = 40;
	$marginLeft = 10;
	
	//fonts
	$fontSize = 12;
	$titleSize = 20;
	$lineHeight = 5;
	$font = 'Times';
	
	
	//function hex2dec
	//returns an associative array (keys: R,G,B) from
	//a hex html code (e.g. #3FE5AA)
	function hex2dec($couleur = "#000000"){
		$R = substr($couleur, 1, 2);
		$rouge = hexdec($R);
		$V = substr($couleur, 3, 2);
		$vert = hexdec($V);
		$B = substr($couleur, 5, 2);
		$bleu = hexdec($B);
		$tbl_couleur = array();
		$tbl_couleur['R']=$rouge;
		$tbl_couleur['V']=$vert;
		$tbl_couleur['B']=$bleu;
		return $tbl_couleur;
	}

	//conversion pixel -> millimeter at 72 dpi
	function px2mm($px){
		return $px*25.4/72;
	}

	function txtentities($html){
		$trans = get_html_translation_table(HTML_ENTITIES);
		$trans = array_flip($trans);
		return strtr($html, $trans);
	}
	////////////////////////////////////
	
	class PDF_HTML extends FPDF
	{
		//variables of html parser
		var $B;
		var $I;
		var $U;
		var $HREF;
		var $fontList;
		var $issetfont;
		var $issetcolor;

		function PDF_HTML($orientation='P', $unit='mm', $format='A4')
		{
			//Call parent constructor
			$this->FPDF($orientation,$unit,$format);
			//Initialization
			$this->B=0;
			$this->I=0;
			$this->U=0;
			$this->HREF='';
			$this->fontlist=array('arial', 'times', 'courier', 'helvetica', 'symbol');
			$this->issetfont=false;
			$this->issetcolor=false;
		}

		function WriteHTML($html)
		{
			//HTML parser
			$html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>"); //supprime tous les tags sauf ceux reconnus
			$html=str_replace("\n",' ',$html); //remplace retour � la ligne par un espace
			$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //�clate la cha�ne avec les balises
			foreach($a as $i=>$e)
			{
				if($i%2==0)
				{
					//Text
					if($this->HREF)
						$this->PutLink($this->HREF,$e);
					else
						$this->Write(5,stripslashes(txtentities($e)));
				}
				else
				{
					//Tag
					if($e[0]=='/')
						$this->CloseTag(strtoupper(substr($e,1)));
					else
					{
						//Extract attributes
						$a2=explode(' ',$e);
						$tag=strtoupper(array_shift($a2));
						$attr=array();
						foreach($a2 as $v)
						{
							if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
								$attr[strtoupper($a3[1])]=$a3[2];
						}
						$this->OpenTag($tag,$attr);
					}
				}
			}
		}

		function OpenTag($tag, $attr)
		{
			//Opening tag
			switch($tag){
				case 'STRONG':
					$this->SetStyle('B',true);
					break;
				case 'EM':
					$this->SetStyle('I',true);
					break;
				case 'B':
				case 'I':
				case 'U':
					$this->SetStyle($tag,true);
					break;
				case 'A':
					$this->HREF=$attr['HREF'];
					break;
				case 'IMG':
					if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
						if(!isset($attr['WIDTH']))
							$attr['WIDTH'] = 0;
						if(!isset($attr['HEIGHT']))
							$attr['HEIGHT'] = 0;
						$this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
					}
					break;
				case 'TR':
				case 'BLOCKQUOTE':
				case 'BR':
					$this->Ln(5);
					break;
				case 'P':
					$this->Ln(10);
					break;
				case 'FONT':
					if (isset($attr['COLOR']) && $attr['COLOR']!='') {
						$coul=hex2dec($attr['COLOR']);
						$this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
						$this->issetcolor=true;
					}
					if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
						$this->SetFont(strtolower($attr['FACE']));
						$this->issetfont=true;
					}
					break;
			}
		}

		function CloseTag($tag)
		{
			//Closing tag
			if($tag=='STRONG')
				$tag='B';
			if($tag=='EM')
				$tag='I';
			if($tag=='B' || $tag=='I' || $tag=='U')
				$this->SetStyle($tag,false);
			if($tag=='A')
				$this->HREF='';
			if($tag=='FONT'){
				if ($this->issetcolor==true) {
					$this->SetTextColor(0);
				}
				if ($this->issetfont) {
					$this->SetFont('arial');
					$this->issetfont=false;
				}
			}
		}

		function SetStyle($tag, $enable)
		{
			//Modify style and select corresponding font
			$this->$tag+=($enable ? 1 : -1);
			$style='';
			foreach(array('B','I','U') as $s)
			{
				if($this->$s>0)
					$style.=$s;
			}
			$this->SetFont('',$style);
		}

		function PutLink($URL, $txt)
		{
			//Put a hyperlink
			$this->SetTextColor(0,0,255);
			$this->SetStyle('U',true);
			$this->Write(5,$txt,$URL);
			$this->SetStyle('U',false);
			$this->SetTextColor(0);
		}

	}//end of class
	
	class PDF extends PDF_HTML
	{
		// En-t�te
		function Header()
		{
			// Logo
			$this->Image($GLOBALS['Logo'],10,10,26);
			// Police Arial gras 15
			$this->SetFont('Arial','B',15);
			// D�calage � droite
			$this->Cell(80);
			// Titre
			$this->Cell(30,10,utf8_decode($GLOBALS['Student']->first_name()." ". $GLOBALS['Student']->name()));
			// Saut de ligne
			$this->Ln(20);
		}

		// Pied de page
		function Footer()
		{
			// Positionnement � 1,5 cm du bas
			$this->SetY(-15);
			// Police Arial italique 8
			$this->SetFont('Arial','I',8);
			// Num�ro de page
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
		
		function Title($title,$fontSize, $titleSize, $font){
			$this->SetFont($font,'',$titleSize);
			$this->Cell( 0, 0, $title, 0, 0, 'L' );
			$this->SetDrawColor(23,45,123);
			$this->Line(10, $this->GetY() + 3, 200, $this->GetY() + 3 );
			$this->SetFont($font,'',$fontSize);
		}
		
		function InfoLine($desc, $val, $pos, $align = 'R'){
			$this->Cell( 0, 0, '', 0, 0, $align );
			$this->SetTextColor(23,45,123);
			$this->Cell( -50, $pos, $desc, 0, 0, $align );
			$this->SetTextColor(0,0,0,'DF');
			$this->Cell( 0, $pos,$val, 0, 0, $align );
		}
		
		function writeArray($infos, $x, $h, &$cursorY){
			$y = 0;
			for($i = 0; $i < count($infos); $i++){
				$y = $x + $h * $i;
				$this->InfoLine($infos[$i][0],$infos[$i][1], $y);
			}
			$cursorY = $y;
		}
	}
	

	//init
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont($font,'',$fontSize);

	//image
	if($showPhoto == "imgOn")
		$pdf->Image($photo,$marginLeft,$cursorY,40); //path, x, y, width
	
	//detail infos
	$pdf-> SetX(200);
	$pdf-> SetY($cursorY);
	$pdf->writeArray($infos, 0, 15, $cursorY);
		
	$pdf -> SetY($cursorY); 
	$pdf -> SetX($marginLeft);
	
	//Summary
	if($student->summary() != "")
	{
		$pdf->Title("R�sum�",$fontSize, $titleSize, $font);
		
		$cursorY+=10;
		$pdf -> SetY($cursorY); 
		
		$pdf->SetX($marginLeft);
		
		//$pdf->MultiCell(0,$lineHeight,utf8_decode(str_replace("&nbsp;","", strip_tags($student->summary()))),0,'J');
		
		$text = $student->summary();//utf8_decode(str_replace("&nbsp;","", strip_tags($student->summary())));
		
		if(ini_get('magic_quotes_gpc')=='1')
			$text=stripslashes($text);
		
		$pdf->WriteHTML(utf8_decode($text));
	}

	$cursorY = $pdf->GetY() + 10;
	$pdf->SetY($cursorY);
	
	//follow
	if($showFollow == "Followyes" && count($follows) > 0){
		$pdf->Title("Suivis",$fontSize, $titleSize, $font);	
		
		foreach($follows as $follow){
			
			$val = 'Le '.date_format($follow->add_date(), 'j F Y').' par '.$follow->colleague_first_name().' '.ucfirst(strtolower($follow->colleague_name()));
			
			$cursorY += 10;
			
			$pdf->SetFont($font,'',14);
			$pdf->SetY($cursorY);
			$pdf->Cell( 0, 0,utf8_decode($val), 0, 0, 'L' );
			$pdf->SetFont($font,'',$fontSize);
			
			$cursorY += 5;
			$pdf->SetY($cursorY);
			$pdf->SetX($marginLeft + 10);
			$pdf->MultiCell(0,$lineHeight,utf8_decode($follow->content()),0,'J');
			$cursorY = $pdf->GetY();
		}
		
	}
	
	$pdf->Output();
	
	
	$this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();