<?php

namespace Applications\Frontend\Modules\Follow;

use Applications\Entities\Follow;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class FollowController extends BackController
{
     function executeIndex(HTTPRequest $request) {
        // On vérifie que l'utilisateur est authentifié
        if ($this->app->user()->isAuthenticated()) {

            // Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');
            // On Vérifie que ce soit quelqu'un qui ait les droits d'ajout
            if (@$right[FOLLOW] & ADD) {
                $manager = $this->managers->getManagerOf('Follow');
                // Récupère la liste de suivis concernant l'élève
                $follows = $manager->getListByColleague($this->app->user()->getAttribute('user')->id());

                $this->page->addVar('title', 'Mes suivis');
                $this->page->addVar('description', 'Liste des suivis d\'un collaborateur de l\'ETML');
                $this->page->addVar('keywords', 'liste suivis');

                $this->page->addVar('follows', $follows);
            }
            else {
                $this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());
            }
        }
        else {
            $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            // Redirige l'utilisateur vers la page précédente
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
     }

     function executeClass(HTTPRequest $request) {
        // On vérifie que l'utilisateur est authentifié
        if ($this->app->user()->isAuthenticated()) {

            // Charge le manager des classes
            $class_manager = $this->managers->getManagerOf('schoolClass');
            // Récupère la classe de l'utilisateur authentifié
            $master_class = $class_manager->getUniqueByColleague($this->app->user()->getAttribute('user')->id());

            // Vérifie que ce soit un maître de classe
            if ($master_class ) {


                $manager = $this->managers->getManagerOf('Follow');
                // Récupère la liste de suivis concernant l'élève
                $follows = $manager->getListByMaster($this->app->user()->getAttribute('user')->id());

                $this->page->addVar('title', 'Les suivis de ma classe');
                $this->page->addVar('description', 'Liste des suivis de la classe d\'un collaborateur de l\'ETML');
                $this->page->addVar('keywords', 'liste suivis classe');

                $this->page->addVar('follows', $follows);
            }
            else if (($this->app->user()->getAttribute('group') == 10)) {

                $manager = $this->managers->getManagerOf('Follow');
                // Récupère la liste de suivis concernant l'élève
                $follows = $manager->getListByMainMaster($this->app->user()->getAttribute('user')->profession_id());

                $this->page->addVar('title', 'Les suivis de mes classes');
                $this->page->addVar('description', 'Liste des suivis de la classe d\'un collaborateur de l\'ETML');
                $this->page->addVar('keywords', 'liste suivis classe');

                $this->page->addVar('follows', $follows);
            }
            else {
                $this->app->user()->setFlash('Vous n\'êtes pas un maître de classe', 'error');
                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());
            }
        }
        else {
            $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            // Redirige l'utilisateur vers la page précédente
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
     }

	 function executeAdd(HTTPRequest $request) {
        if($request->postsExists(array('content', 'student_id'))){
            // Vérifié que l'utilisateur soit authentifié
            if ($this->app->user()->isAuthenticated()) {

                // Récupère les droits de l'utilisateur
                $right = $this->app->user()->getAttribute('right');
                // Vérifie que l'utilisateur ait les droits d'ajout
                if(@$right[FOLLOW] & ADD) {
                    if (!$request->postsEmpty(array('content', 'student_id'))){
                        // Récupère le manager du module Students
                        $manager = $this->managers->getManagerOf('Student');
                        // Récupère les données de l'étudiant
                        $student = $manager->getUnique($request->postData('student_id'));
                        // Vérifie que la requête nous ait retourné quelque chose
                        if ($student) {
                            // Récupère le manager du module Follow
                            $follows_manager = $this->managers->getManagerOf('Follow');
                            $follow = new Follow(array(
                                'content' => $request->postData('content'),
                                'right' => $request->postData('right'),
                                'student_id' => $student->id(),
                                'colleague_id' => $this->app->user()->getAttribute('user')->id()));
                            $success = $follows_manager->add($follow);

                            if ($success) {
                                $this->app->user()->setFlash('Le suivi a été ajouté', 'success');
                            } else {
                                $this->app->user()->setFlash('Une erreur c\'est produite lors de l\'ajout du suivi', 'error');
                            }
                        }
                        else {
                            $this->app->user()->setFlash('L\'élève spécifié n\'existe pas', 'error');
                        }
                    }
                    else {
                        $this->app->user()->setFlash('Il manque des données, vérifiez que vous avez rempli tous les champs');
                    }
                }
                else {
                    $this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                }
            }
            else {
                $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            }
            // Redirige l'utilisateur vers la page précédente
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
        else {
            // Redirige l'utilisateur vers une page de type 404
            $this->app->httpResponse()->redirect404();
        }
	 }

	 function executeEdit(HTTPRequest $request) {
	 	if ($request->getExists('id')) {

            // Supprime le template par defaut de la page
	 		$this->page->setLayout();

            // On vérifie que l'utilisateur soit autentifié
            if ($this->app->user()->isAuthenticated()) {

    	 		$manager = $this->managers->getManagerOf('Follow');
                $follow = $manager->getUnique($request->getData('id'));

                // On vérifie que le suivi existe
                if ($follow) {
                    $errors = '';

        	 		if($request->postExists('content')){
                        if (!$request->postEmpty('content')){

                            // Charge le manager des classes
                            $class_manager = $this->managers->getManagerOf('schoolClass');
                            // Récupère la classe de l'utilisateur authentifié
                            $master_class = $class_manager->getUniqueByColleague($this->app->user()->getAttribute('user')->id());
                            // Cherge le manager des élèves
                            $student_manager = $this->managers->getManagerOf('Student');

                            // Récupère l'élève concerné par le suivi
                            $student = $student_manager->getUnique($follow->student_id());

                            // Vérifie si l'utilisateur identifié est le maître de classe de l'élève
                            $is_master = (($master_class) && ($master_class[0]->id() == $student->school_class_id())) ? 1 : 0;

                            // check if colleague connected is the author of the follow
                            $is_author = ($this->app->user()->getAttribute('user')->id() == $follow->colleague_id()) ? 1 : 0;

                            
                            // Récupère les droits de l'utilisateur
                            $right = $this->app->user()->getAttribute('right');
                            
                            // Défini le droit de supression du suivi de l'utilisateur authentifié
                            $modify_right = 0;
							if (@$right[FOLLOW] & MODIFY || $is_master || $is_author) {
                                $modify_right = 1;
                            }

                            // On vérifie que l'utilisateur ait les droits de modification
                            if ($modify_right) {

                                $followUpdate = new Follow(array(
                	 				'id' => $request->getData('id'),
                                 	'content' => $request->postData('content'),
                                    'right' => $request->postData('right'),
                                    'mod_colleague_id' => $this->app->user()->getAttribute('user')->id()
                                    ));
                                $success = $manager->update($follow, $followUpdate);

                                if ($success) {
                                    $this->app->user()->setFlash('Le suivi a été modifié', 'success');
                                } else {
                                    $this->app->user()->setFlash('Une erreur c\'est produite lors de la modification du suivi', 'error');
                                }
                            }
                            else {
                                $this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                            }

                        }
                        else {
                            $this->app->user()->setFlash('Il manque des données, vérifiez que vous avez rempli tous les champs');
                        }
                        // Redirige l'utilisateur à la page précédente
                        $this->app->httpResponse()->redirect($request->httpReferer());
                    }
                    // Envois les le suivi ainsi que l'id à la vue
                    $this->page->addVar('id', $request->getData('id'));
                    $this->page->addVar('follow', $follow);
                }
                else {
                    $this->app->user()->setFlash('Le suivi que vous essayez de modifier n\'existe pas', 'error');
                    // Redirige l'utilisateur vers la page précédente
                    $this->app->httpResponse()->redirect($request->httpReferer());
                }
            }
            else {
                $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());

            }
	   }
       else {
        // Redirige l'utilisateur vers une page de type 404
        $this->app->httpResponse()->redirect404();
       }
    }

	 function executeView(HTTPRequest $request) {

        // Supprime le template par defaut de la page
        $this->page->setLayout();
		
		$manager = $this->managers->getManagerOf('Follow');
		$follow = $manager->getUnique($request->getData('id'));
		
		$this->page->addVar('follow', $follow);
    }	
	
	 function executeDelete(HTTPRequest $request) {
	 	if ($request->getExists('id')) {
            // On vérifie que l'utilisateur est authentifié
            if ($this->app->user()->isAuthenticated()) {

                // Récupère les droits de l'utilisateur
                $right = $this->app->user()->getAttribute('right');

    	 		$manager = $this->managers->getManagerOf('Follow');
                // Charge le manager des classes
                $class_manager = $this->managers->getManagerOf('schoolClass');
                // Récupère la classe de l'utilisateur authentifié
                $master_class = $class_manager->getUniqueByColleague($this->app->user()->getAttribute('user')->id());
                // Cherge le manager des élèves
                $student_manager = $this->managers->getManagerOf('Student');

                // Récupère le suivi
                $follow = $manager->getUnique($request->getData('id'));
                // Récupère l'élève concerné par le suivi
                $student = $student_manager->getUnique($follow->student_id());

                // Vérifie si l'utilisateur identifié est le maître de classe de l'élève
                $is_master = (($master_class) && ($master_class[0]->id() == $student->school_class_id())) ? 1 : 0;

                // check if colleague connected is the author of the follow
                $is_author = ($this->app->user()->getAttribute('user')->id() == $follow->colleague_id()) ? 1 : 0;

                //Défini le droit de supression du suivi de l'utilisateur authentifié
                $delete_right = 0;
                /*if (@$right[FOLLOW] & DELETE) {
                    if(@!($right[FOLLOW] & SPECIFIC))     
                        $delete_right = 1;
                }*/
				
				if (@$right[FOLLOW] & DELETE || $is_author) {
					$delete_right = 1;
				}
				
                // Vérifie si l'utilisateur à le droit de le suprimmer
                if ($delete_right) {

                    //if (confirm('Voulez vous vraiment supprimer ce suivi ?')){
                        $success = $manager->delete($request->getData('id'));

                        if ($success) {
                            $this->app->user()->setFlash('Le suivi a été correctement supprimé', 'success');
                        } else {
                            $this->app->user()->setFlash('Une erreur c\'est produite lors de la suppression du suivi', 'error');
                        }

                        $this->app->httpResponse()->redirect($request->httpReferer());
                    //}
                }
                else {
                    $this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                }
            }
            else {
                $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            }

            $this->app->httpResponse()->redirect($request->httpReferer());
        } else {
            $this->app->httpResponse()->redirect404();
        }
    }
}
