<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>
	<form id="view-follow" name="follow" method="post">
	    <h5><?php echo ($follow->right()) ? '<i class="icon-lock"></i>' : ''; ?>
	    	Le <?php echo date_format($follow->add_date(), 'j F Y'); ?>
	        de <?php echo $follow->colleague_id();  ?>
	        <?php if($follow->mod_colleague_id() != ""):?>
	        modifié par <?php echo $follow->mod_colleague_id(); ?>
	    <?php endif ?>
	    </h5>
		<?php echo nl2br($follow->content()); ?>
	</form>