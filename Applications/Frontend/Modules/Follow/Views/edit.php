<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>
	<form id="update-follow" action="<?php echo $this->html()->url('follow/'.$id.'/edit'); ?>" name="follow" method="post">
		<textarea rows="2" class="input-block-level" name="content"><?php echo str_replace("\n", "", $follow->content()); ?></textarea>
	</form>
	<a id="deleteFollow" class="hidden" href="<?php echo $this->html()->url('follow/'.$id.'/delete'); ?>"></a>