<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); 

//Création du menu option pour le controller colleague avec comme parametre l'id du colleague
//avec les options Editer ,Supprimer et immprimer
$menu=array(
    "colleague",$colleague->id(),"",@COLLEAGUE,$right,
    "Editer","/edit","","",@MODIFY,
    "Supprimer","/delete","","",@DELETE,
    "Imprimer","","Modal","PrintModal",@VIEW_ALL
    );
echo $this->html()->optionmenu($menu);
}
?>
 <!-- 
Modal pour impression 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur imprimer dans le menu option
-->
<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

 <!-- Titre de la modal box -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Impression</h4>
      </div>

      <div class="modal-body">
    
 <!-- Liste déroulante type d'impression -->
        <div class="btn-group">
          <select id="optionList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('optionList').value);">
            <option selected="selected">Type d'impression</option>
            <option value="imgOn">Avec photo</option>
            <option value="imgOff">Sans photo</option>
          </select>
        </div>
 <!-- Liste déroulante format police -->
        <div class="btn-group">
          <select id="tailleList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('tailleList').value);">
            <option selected="selected">Format de texte</option>
            <option value="x-small">Très petit</option>
            <option value="small">Petit</option>
            <option value="medium">Moyen</option>
            <option value="large">Grand</option>
            <option value="x-large">Très Grand</option>
          </select>
        </div>
<br/><br/>
<!-- Liste déroulante police -->
        <div class="btn-group">
          <select id="policeList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('policeList').value);">
            <option selected="selected">Police</option>
            <option value="TNR">Times New Roman</option>
            <option value="arial">Arial</option>  
            <option value="CenturyGothic">Century Gothic</option>
            <option value="EcoFont">EcoFont</option>   
          </select>
        </div>

<!-- Liste déroulante en-tete et pied de page -->
        <div class="btn-group">
          <select id="enteteList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('enteteList').value);">
            <option selected="selected">En-tete</option>
            <option value="ETPPyes">oui</option>
            <option value="ETPPno">non</option>  
          </select>
        </div>


      </div>
 <!-- Bouton modal box (fermer,imprimer) -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrintPDF();">Enregistrer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrint();">Imprimer</button>
      </div>

    </div>
  </div>
</div>

<!-- Fin de la modal box d'impression -->    


<!-- 
Modal pour les option sur les collegues 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur edit dans le menu option
-->

<!--
Avant une pop-up d'édition s'ouvrait maintenant on  passe par une autre pafe le modal est donc mit en commentaire

	<div class="modal fade" id="EditColleague" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		
		<form action="" method="post">
			 Titre de la modal box 
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			  <h3  id="myModalLabel">Modification d'un collègue</h3>     
			</div>
			  <div class="span4">
				<fieldset>
					<legend>Photo</legend>
					<img id="studentPicture" class="center" src="<?php echo $this->html()->url();?>img/nophoto.jpg">
					<button type="button" class="btn">Charger la photo</button>
				</fieldset>
			</div>
			<div class="span8">
				<fieldset>
					<legend>Informations générales</legend>
					<div class="span6 ml0">
						<label>Nom</label>
						<input type="text" name="name" tabindex="1" class="input-block-level">
						<label>Login</label>
						<input type="text" name="id" tabindex="3" class="input-block-level">
					</div>
					<div class="span6">
						<label>Prénom</label>
						<input type="text" name="firstname" tabindex="2" class="input-block-level">
					</div>
				</fieldset>

				<fieldset>
					<legend>Informations personelles</legend>
					<label>Adresse</label>
					<input type="text" name="address" tabindex="6" class="input-block-level">
					<div class="span6 ml0">
						<div class="span3">
							<label>NPA</label>
							<input type="text" name="zip" tabindex="7" class="input-block-level">
						</div>
						<div class="span9">
							<label>Ville</label>
							<input type="text" name="city" tabindex="8" class="input-block-level">
						</div>
						<label>Téléphone Privé</label>
						<input type="text" name="phone" tabindex="10" class="input-block-level">
						<label>Date de naissance</label>
						<input type="date" name="birthDate" tabindex="12" class="input-block-level">
					</div>
					<div class="span6">
						<label>E-Mail</label>
						<input type="text" name="email" tabindex="9" class="input-block-level">
						<label>Téléphone Interne</label>
						<input type="text" name="mobile" tabindex="11" class="input-block-level">
					</div>
				</fieldset>
			</div>

			

			<div class="span4">
				<fieldset>
					<button class="btn btn-large btn-primary center" type="submit">Enregistrer</button>
				</fieldset>
			</div>

		</form>











	   
	   </div>
	  </div>
	</div>
-->
<!-- Fin de la modal box sur l'edition d'un collegue -->







<div id="content">
    <div id="profile" class="span10">
        <div>
            <h1><?php echo $colleague->first_name(); ?> <?php echo $colleague->name(); ?></h1>
        </div>
        <div class="span4">
            <?php
            $photo = '/img/colleagues/'.$colleague->id().'.jpg';
            if (file_exists(WEBROOT.$photo)) {
                ?><img class="profile" src="<?php echo $this->html()->url($photo); ?>" alt="<?php echo $colleague->first_name(). ' ' .$colleague->name(); ?>"><?php
            } else {
                ?><img class="profile" src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
            }
            ?>
        </div>
        <div class="span6 student-info">
            <table>
                <tr>
                    <td class="grey w150p">
                        Identifiant :
                    </td>

                    <td>
                        <?php echo $colleague->id(); ?>
                    </td>
                </tr>
                <tr>
                    <td class="grey w150p">
                        Interphone :
                    </td>

                    <td>
                        <?php echo $colleague->inter_phone(); ?>
                    </td>
                </tr>
                <tr>
                    <td class="grey w150p">
                        Téléphone :
                    </td>

                    <td>
                        <?php echo $colleague->phone(); ?>
                    </td>
                </tr>
                <tr>
                    <td class="grey w150p">
                        Email :
                    </td>

                    <td>
                        <?php echo $colleague->email(); ?>
                    </td>
                </tr>
                <tr>
                    <td class="grey w150p">
                        Classe :
                    </td>
                    <td>
                    <?php
                        // links do not work for some odd
                        foreach ($classes as $class){ 
                            if($class->colleague_id() == $colleague->id() ){
                            ?> 
                                <a class="info-link" href="<?php echo $this->html()->url('class/'.$class->id()); ?>"><?php echo $class->id()." "; ?> </a> 
                            <?php
                            }
                        }
                    ?>
                    </td>
                </tr>
                <tr>
                    <td class="grey w150p">
                        Rôle :
                    </td>

                    <td>
                        <?php
                            foreach($colleagueRoles as $colleagueRole){
                                echo $colleagueRole->name().'<br>';
                            }
                        ?>
                    </td>
                </tr>
				<tr>
                    <td class="grey w150p">
                        Salle :
                    </td>

                    <td>
                        <?php
                            foreach($roomInfo as $room){
                                echo $room->room_id();
                            }
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    
    </div>    
</div>

