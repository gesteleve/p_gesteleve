<?php $this->html()->js('jquery.imgareaselect.min.js'); ?>

<div id="content">
    <form action="" method="post">
        <div class="span8">
            <fieldset>
                <legend>Informations générales</legend>
                <div class="span6 ml0">
                    <label>Nom</label>
                    <input type="text" name="name" tabindex="1" class="input-block-level">
                    <label>Login</label>
                    <input type="text" name="id" tabindex="3" class="input-block-level">
                </div>
                <div class="span6">
                    <label>Prénom</label>
                    <input type="text" name="firstname" tabindex="2" class="input-block-level">
                </div>
            </fieldset>

            <fieldset>
                <legend>Informations personelles</legend>
                <label>Adresse</label>
                <input type="text" name="address" tabindex="6" class="input-block-level">
                <div class="span6 ml0">
                    <div class="span3">
                        <label>NPA</label>
                        <input type="text" name="zip" tabindex="7" class="input-block-level">
                    </div>
                    <div class="span9">
                        <label>Ville</label>
                        <input type="text" name="city" tabindex="8" class="input-block-level">
                    </div>
                    <label>Téléphone Privé</label>
                    <input type="text" name="phone" tabindex="10" class="input-block-level">
                    <label>Date de naissance</label>
                    <input type="date" name="birthDate" tabindex="12" class="input-block-level">
                </div>
                <div class="span6">
                    <label>E-Mail</label>
                    <input type="text" name="email" tabindex="9" class="input-block-level">
                    <label>Téléphone Interne</label>
                    <input type="text" name="mobile" tabindex="11" class="input-block-level">
                </div>
            </fieldset>
        </div>

        <div class="span4">
            <fieldset>
                <legend>Photo</legend>
                <img id="studentPicture" class="center" src="<?php echo $this->html()->url();?>img/nophoto.jpg">
                <button type="button" class="btn">Charger la photo</button>
            </fieldset>
        </div>

        <div class="span4">
            <fieldset>
                <button class="btn btn-large btn-primary center" type="submit">Enregistrer</button>
            </fieldset>
        </div>

    </form>

</div>
<script type="text/javascript">
$(document).ready(function () {
    $('img#studentPicture').imgAreaSelect({
        handles: true,
    });
});
</script>
