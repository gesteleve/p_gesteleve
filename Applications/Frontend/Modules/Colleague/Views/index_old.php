<?php 
//*********************************************************
// Societe: ETML
// Auteur : Lanz Romain
// Date : inconnue
// But : Afficher une liste des collaborateurs etml
//*********************************************************
// Modifications:
// Date : 19.05.2014
// Auteur : Lukyantsev  Vladislav
// Raison : Mise en place de la modal box d’impression pour les collaborateurs
//*********************************************************
if ($user->isAuthenticated()) { 

$right = $user->getAttribute('right'); 
//initialisation du menu option 
$menu=array(
    "colleague","","",@COLLEAGUE,$right,
    "Imprimer","","Modal","PrintModal",@VIEW_ALL
    );

echo $this->html()->optionmenu($menu);
}
?>
<?php $this->html()->js('isotope.min.js'); ?>
<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>

<!-- 
Modal pour impression 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur imprimer dans le menu option
-->
<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

 <!-- Titre de la modal box -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Impression</h4>
      </div>

      <div class="modal-body">
    
 <!-- Liste déroulante type d'impression -->
        <div class="btn-group">
          <select id="optionList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('optionList').value);">
            <option selected="selected">Type d'impression</option>
            <option value="imgOn">Avec photo</option>
            <option value="imgOff">Sans photo</option>
          </select>
        </div>
 <!-- Liste déroulante format police -->
        <div class="btn-group">
          <select id="tailleList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('tailleList').value);">
            <option selected="selected">Format de texte</option>
            <option value="x-small">Très petit</option>
            <option value="small">Petit</option>
            <option value="medium">Moyen</option>
            <option value="large">Grand</option>
            <option value="x-large">Très Grand</option>
          </select>
        </div>
<br/><br/>
<!-- Liste déroulante police -->
        <div class="btn-group">
          <select id="policeList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('policeList').value);">
            <option selected="selected">Police</option>
            <option value="TNR">Times New Roman</option>
            <option value="arial">Arial</option>  
          </select>
        </div>

<!-- Liste déroulante en-tete et pied de page -->
        <div class="btn-group">
          <select id="enteteList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('enteteList').value);">
            <option selected="selected">En-tete</option>
            <option value="ETPPyes">oui</option>
            <option value="ETPPno">non</option>  
          </select>
        </div>


        <br/><br/>
      </div>
 <!-- Bouton modal box (fermer,imprimer) -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrintPDF();">Enregistrer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrint();">Imprimer</button>
      </div>

    </div>
  </div>
</div>

<!-- Fin de la modal box d'impression -->

<div class="mt30">
  <div id="container">
      <h3><?php echo 'Nb collaborateurs: '. count($colleagues);?></h3>

    <?php foreach ($colleagues as $colleague): ?>
      <div class="post">
          <a href="<?php echo $this->html()->url('colleague/'.$colleague->id()); ?>" data-toggle="modal">
              <div class="picture">
                  <?php
                  $photo = '/img/colleagues/'.$colleague->id().'.jpg';
                  if (file_exists(WEBROOT.$photo)) {
                      ?><img src="<?php echo $this->html()->url($photo); ?>" alt="<?php echo $colleague->first_name(). ' ' .$colleague->name(); ?>"><?php
                  } else {
                      ?><img src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
                  }
                  ?>
              </div>
              <div class="information">
                <h5>
                  <?php echo $colleague->first_name(); ?> <?php echo $colleague->name(); ?>
                </h5>

                <p><?php echo $colleague->id(); ?></p>
				<p><?php echo $colleague->inter_phone(); ?> <?php echo $colleague->phone(); ?></p>
              </div>
          </a>
      </div>
    <?php endforeach ?>
  </div>
</div>
