<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<?php $this->html()->js('jquery.imgareaselect.min.js'); ?>
<?php $this->html()->js('../assets/ckeditor/ckeditor.js'); ?>
	


<!-- style liste déroulante des roles-->
<style type="text/css">	
	.multiselect {
    width:100%;
    height:100px;
    border:solid 1px #c0c0c0;
    overflow:auto;
	}
	 
	.multiselect label {
	    display:block;
	}
	 
	.multiselect-on {
	    color:#ffffff;
	    background-color:#000099;
	}

</style>

<div id="content">
    <form action="" method="post">
		<div class="span12">
			<div class="span8">
				<fieldset>
					<legend>Informations générales</legend>
					<div class="span4">
						<label>Nom</label>
						<input type="text" name="name" tabindex="1" class="input-block-level" value="<?php echo $colleague->name(); ?>">
					</div>

					<div class="span4">
						<label>Prénom</label>
						<input type="text" name="first_name" tabindex="3" class="input-block-level" value="<?php echo $colleague->first_name(); ?>">
					</div>

					<div class="span3">	
						<label>Login</label>
						<input type="text" name="id" tabindex="2" class="input-block-level" value="<?php echo $colleague->id(); ?>" disabled>
					</div>

					<div class="span4">
						<label>E-Mail</label>
						<input type="text" name="email" tabindex="4" class="input-block-level" value="<?php echo $colleague->email(); ?>">
					</div>

					<div class="span4">
						<label>Téléphone</label>
						<input type="text" name="phone" tabindex="5" class="input-block-level" value="<?php echo $colleague->phone(); ?>">
					</div>

					<div class="span3">
						<label>Interphone</label>
						<input type="text" name="interphone" tabindex="6" class="input-block-level" value="<?php echo $colleague->inter_phone(); ?>">
					</div>

					<div class="span4">
						<label>Classe</label>
						<div class="multiselect">
							<?php
							asort($All_classes);
								foreach($All_classes as $All_classe) 
								{
									$str_Check = "";
									
									if($colleague->id() == $All_classe->colleague_id() )
									{
										$str_Check = "checked";
									}//if

									
									echo '<label><input type="checkbox" name="option[]" style ="margin-left:10px;" value="'.$All_classe->id().'" '.$str_Check.' /> '. $All_classe->id().'</label>';
								}//foreach
							?>

						</div>
					</div>

					<div class="span4">
						<label>Rôle</label>
						<div class="multiselect">
							<?php
								foreach($All_roles as $All_role) 
								{
									$str_Check = "";
									foreach ($roles as $role) 
									{
										if($All_role->name() == $role->name() )
										{
											$str_Check = "checked";
										}//if
									}//foreach
									
									echo '<label><input type="checkbox" name="option[]" style ="margin-left:10px;" value="'.$All_role->name().'" '.$str_Check.' /> '. $All_role->name().'</label>';
								}//foreach
							?>

						</div>
					</div>

					<div class="span3">
						<label>Salle</label>
						<div class="multiselect">
							<?php
								foreach($AllRoom as $OneRoom) 
								{
									$str_Check = "";
									foreach ($colleagueRooms as $colleagueRoom) 
									{
										if($OneRoom->room_id() == $colleagueRoom->room_id())
										{
											$str_Check = "checked";
										}//if
									}//foreach
									
									echo '<label><input type="checkbox" name="option[]" style ="margin-left:10px;" value="'.$OneRoom->room_id().'" '.$str_Check.' /> '.$OneRoom->room_id().'</label>';
								}//foreach
							?>

						</div>
					</div>

				</fieldset>
			</div>
			<div class="span12 center">
					<img id="studentPicture" style="width:40px; height:300px; margin-top:10px;" class="center" src="
					<?php
						$photo = 'img/colleagues/'.$colleague->id().'.jpg';

						if (file_exists(WEBROOT.$photo)) {
							echo $this->html()->url($photo);
						} else {
							echo $this->html()->url('/img/nophoto.jpg');
						}
					?>" />
					<div>
						<div class="span5"></div>
						<button type="button" class="btn btn-large m10">Changer</button>
						<button class="btn btn-large btn-primary" type="submit">Enregistrer</button>
					</div>
			</div>
		</div>

    </form>

</div>
<script type="text/javascript">
$(document).ready(function () {
    $('img#studentPicture').imgAreaSelect({
        handles: true,
    });
});



//jquerry poûr les liste déroulante a checkbox
jQuery.fn.multiselect = function() {
    $(this).each(function() {
        var checkboxes = $(this).find("input:checkbox");
        checkboxes.each(function() {
            var checkbox = $(this);
            // Highlight pre-selected checkboxes
            if (checkbox.prop("checked"))
                checkbox.parent().addClass("multiselect-on");
 
            // Highlight checkboxes that the user selects
            checkbox.click(function() {
                if (checkbox.prop("checked"))
                    checkbox.parent().addClass("multiselect-on");
                else
                    checkbox.parent().removeClass("multiselect-on");
            });
        });
    });
};

$(function() {
     $(".multiselect").multiselect();
});
</script>