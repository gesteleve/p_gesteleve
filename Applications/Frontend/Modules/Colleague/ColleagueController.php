<?php

namespace Applications\Frontend\Modules\Colleague;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class ColleagueController extends BackController
{
    function executeIndex() {
        $this->page->addVar('title', 'Liste des collègues');
        $this->page->addVar('description', 'Liste des collègues de l\'ETML');
        $this->page->addVar('keywords', 'liste, collègues');

        $manager = $this->managers->getManagerOf('Colleague');
        $colleagues = $manager->getList();
        $this->page->addVar('colleagues', $colleagues);

        $colleaguesRoles = $manager->getColleaguesRoles();
        $this->page->addVar('colleaguesRoles', $colleaguesRoles);
        
        $professions = $manager->getListSectionColleague();
        $this->page->addVar('professions', $professions);

        $manager = $this->managers->getManagerOf('schoolClass');
        $classes = $manager->getListClass();
        $this->page->addVar('classes', $classes);
		

    }

    function executeView(HTTPRequest $request) {
        $right = '1';
        if (!empty($right) & VIEW_ALL) {
            if ($request->getExists('id') && !$request->getEmpty('id')) {
                $manager = $this->managers->getManagerOf('Colleague');
                $colleague = $manager->getUnique($request->getData('id'));
                $colleagueRoles = $manager->getColleagueRoles($request->getData('id'));
                $roomInfo  = $manager->getColleagueRoom($request->getData('id'));

                $manager = $this->managers->getManagerOf('schoolClass');
                $classes = $manager->getListClass();

                if ($colleague) {
                    $this->page->addVar('title', $colleague->first_name().' '.$colleague->name());
                    $this->page->addVar('description', 'Fiche du collègue '.$colleague->first_name().' '.$colleague->name());
                    $this->page->addVar('keywords', 'fiche, professeur, collègue, '.$colleague->id().', '.$colleague->first_name().', '.$colleague->name());
                    $this->page->addVar('colleague', $colleague);
                    $this->page->addVar('colleagueRoles', $colleagueRoles);
                    $this->page->addVar('classes', $classes);
					$this->page->addVar('roomInfo', $roomInfo);
                } else
                    $this->app->httpResponse()->redirect404();
            } else
                $this->app->httpResponse()->redirect404();
        } else {
            $this->app->user()->setFlash('Vous n\'avez pas accès à cette section','error');
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
    }

    function executeImport( HTTPRequest $request)
    {
        

        $manager = $this->managers->getManagerOf('Colleague');

      
          
        // Met dans la variable, le fichier télécharger !
        $csv = $request->fileData('filFileToUpload');

        // Contrôle que nous avons télécharger le fichier
        if($request->fileData('filFileToUpload'))
        {
                
            $row = 1;

            if (($handle=fopen($csv['tmp_name'],'r')) !== FALSE) 
            { 
                while (($data = fgetcsv($handle,1000,';')) !== FALSE) 
                {
                        
                    if($row > 4)
                    {

                        $id = utf8_encode($data[0]); 
                        // $NameAndFirstName = utf8_encode($data[2]);
                        // $Lotus=utf8_encode($data[3]);
                        $First_Name=utf8_encode($data[3]);
                        $Name=utf8_encode($data[4]);
                        // $Section=utf8_encode($data[6]);
                        // $role=utf8_encode($data[7]);
                        if(empty($data[7]))
                        {
                            $email='-';  
                        }
                        else
                        {
                            $email = utf8_encode($data[7]);   
                        }
                        
                        if(empty($data[11]))
                        {
                            $inter_phone='-';  
                        }
                        else
                        {
                            $inter_phone = $data[11];   
                        }

                        if(empty($data[10]))
                        {
                            $phone='-'; 
                        }
                        else
                        {
                            $phone = $data[10];
                        }
                        //$mailFile = utf8_encode($data[9]);
                        //$No=$data[9]

                        
                        
                        /*$colleague = new Colleague(array(
                        'id' => $id,
                        'name' => $Name,
                        'first_name' =>$First_Name,
                        'inter_phone' =>$inter_phone ,
                        'phone' =>$phone ,
                        'email' =>$email ,
                         
                        ));*/
                        
                        

                       /* $colleague = new Colleague(array(
                        'id' => $id,
                        'name' => $Name,
                        'first_name' =>$First_Name,
                        'inter_phone' =>$inter_phone,
                        'phone' =>$phone ,
                        'email' =>$email ,
                        ));*/

                        //$manager->loadData($colleague); 
                        $manager->loadData($id,$Name,$First_Name,$email,$inter_phone,$phone);            
                    }

                    $row++; 
                }
                fclose($handle);
            }     
        }    
   }
    
    function executeAdd(HTTPRequest $request) {

         if ($this->app->user()->isAuthenticated()) {
                $right = $this->app->user()->getAttribute('right');


            if (!(@$right[COLLEAGUE] & MODIFY) ){
                
            
                $this->app->httpResponse()->redirect404();
            }

        }
        else
        {
            $this->app->httpResponse()->redirect404();
        }


    }

    function executeEdit(HTTPRequest $request) {
	
		$manager = $this->managers->getManagerOf('Colleague');
        $colleague = $manager->getUnique($request->getData('id'));     
        $this->page->addVar('colleague', $colleague);

        $manager = $this->managers->getManagerOf('Colleague');
        $roles = $manager->getColleagueRoles($request->getData('id'));     
        $this->page->addVar('roles', $roles);

        $manager = $this->managers->getManagerOf('Colleague');
        $All_roles = $manager->getAllRoles();     
        $this->page->addVar('All_roles', $All_roles);

        $manager = $this->managers->getManagerOf('schoolClass');
        $All_classes = $manager->getListClass();     
        $this->page->addVar('All_classes', $All_classes);
		
		$manager = $this->managers->getManagerOf('Colleague');
        $AllRoom = $manager->getAllRoom();     
        $this->page->addVar('AllRoom', $AllRoom);

        $manager = $this->managers->getManagerOf('Colleague');
        $colleagueRooms = $manager->getColleagueRoom($request->getData('id'));     
        $this->page->addVar('colleagueRooms', $colleagueRooms);
		
        if ($this->app->user()->isAuthenticated()) {
                $right = $this->app->user()->getAttribute('right');


            if (!(@$right[COLLEAGUE] & MODIFY) ){
                
            
                $this->app->httpResponse()->redirect404();
            }

        }
        else
        {
            $this->app->httpResponse()->redirect404();
        }


    }

    function executeDelete(HTTPRequest $request) {
        if ($this->app->user()->isAuthenticated()) {
                $right = $this->app->user()->getAttribute('right');


            if (!(@$right[COLLEAGUE] & MODIFY) ){
                
            
                $this->app->httpResponse()->redirect404();
            }

        }
        else
        {
            $this->app->httpResponse()->redirect404();
        }


    }
}
