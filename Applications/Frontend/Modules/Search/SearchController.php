<?php
//*********************************************************
// Societe: ETML
// Auteur : Vladislav Lukyantsev
// Date : 23.06.2014
// But : Permet d'afficher un texte de bienvenue ou le modifier
//*********************************************************
// Modifications:
// Date : 
// Auteur : 
// Raison : 
//*********************************************************
// Date :
// Auteur :
// Raison :
//*********************************************************
namespace Applications\Frontend\Modules\Search;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class SearchController extends BackController
{

    public function executeIndex(HTTPRequest $request) 
    {
        $manager = $this->managers->getManagerOf('News');
		$smanager = $this->managers->getManagerOf('Student');
		$pmanager = $this->managers->getManagerOf('Colleague');

		$studs = $smanager->getList();
		$this->page->addVar('liststuds', $studs);
		
		$time = date("Y-m-d");
					
		$studsBirth = $smanager->getBirthList($time);
		$this->page->addVar('listbirthstuds', $studsBirth);
		
		$studsBirth = $pmanager->getBirthList($time);
		$this->page->addVar('listbirthcolleague', $studsBirth);
		
		$ptuds = $pmanager->getList();
		$this->page->addVar('listptuds', $ptuds);
		
        // Liste des news que "tous le monde" peut voir -> droit 1
        $news = $manager->getListNewsEveryBody();
        $Title = $manager->getSearchTitle();

        // Envoie de la variable contenant les news à afficher
        $this->page->addVar('listNews', $news);

        // Envoie de la variable contenant les news à afficher
        $this->page->addVar('title', $Title);

        


        if ($request->postExists('pattern')) {
            $result = array();
            $pattern = $request->postData('pattern');
			$active = "0";
			if ($request->postExists('active')) {
				$active = "1";
				if($request->postData('active') == "true")
				{
					$active = "true";
				}
				else
				{
					$active = "false";
				}
			}
			$this->page->addVar('active', $active);
            $manager = $this->managers->getManagerOf('Student');
            $result['Student'] = $manager->search($pattern);

            $manager = $this->managers->getManagerOf('schoolClass');
            $result['Class'] = $manager->search($pattern);

            $manager = $this->managers->getManagerOf('Colleague');
            $result['Colleague'] = $manager->search($pattern);
			
			//$manager = $this->managers->getManagerOf('Colleague_roles');
            //$result['Colleague_roles'] = $manager->search($pattern);

            $this->page->addVar('result', $result);
            $this->page->setLayout();
            $this->page->setContentFile(__DIR__.DS.'Views'.DS.'search.php');
        }
    }

    public function executeEdit(HTTPRequest $request) 
    {
        $manager = $this->managers->getManagerOf('Search');
        // Récupère la news à modifier
        $content = $request->postData('content');
        $this->page->addVar('result', $content);
        //Envoie le tableau dans la fonction de modification de news
        $success = $manager->getedit($content);
    }
}
