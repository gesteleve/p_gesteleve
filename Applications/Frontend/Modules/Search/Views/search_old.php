<?php if (empty($result['Student']) && empty($result['Colleague']) && empty($result['Class'])/* && empty($result['Colleague_roles'])*/) { ?>
    <div class="noresult">Il n'y a pas de résultat</div>
<?php } else { ?>
    <?php if (!empty($result['Student'])) { ?>
        <div class="result-box span4">
            <h3><?php echo $this->h('Elève(s)') ?></h3>
            <ul>
                <?php foreach ($result['Student'] as $student) { ?>
                <li class="search-box-user">
                    <a href="<?php echo $this->html()->url('student/'.$student->id()); ?>">
                        <div class="itemPicture">
                            <?php $photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg'; ?>

                            <?php $webroot = $this->html()->url('img/nophoto.jpg'); ?>

                            <?php if(file_exists(WEBROOT.$photo)) {
                                $photo = $this->html->url($photo); ?>
                            <img src="<?php echo $photo ?>">
                            <?php } else { ?>
                            <img src="<?php echo $webroot ?>">
                            <?php } ?>
                        </div>
                    </a>
                    <div class="itemInfo">
                        <h6>
                            <a href="<?php echo $this->html()->url('student/'.$student->id()); ?>">
                                <?php echo $student->first_name(); ?>
                                <?php echo $student->name(); ?>
                            </a>
                        </h6>
						
                        <?php
							$id = $student->profession_name();
							switch($id)
							{		
								case "Informaticien-ne":
								$id_Profession = "1";
								break;
								
								case "MPT post CFC":
								$id_Profession = "2";
								break;
								
								case "Préapprentissage":
								$id_Profession = "3";
								break;
								
								case "Ebéniste":
								$id_Profession = "4";
								break;
								
								case "Electronicien-ne":
								$id_Profession = "5";
								break;
								
								case "Polymécanicien-ne":
								$id_Profession = "6";
								break;
								
								case "Automaticien-ne":
								$id_Profession = "7";
								break;
								
								case "Mécatronicien-ne d'automobiles":
								$id_Profession = "8";
								break;
								
								case "Menuisier-ère":
								$id_Profession = "9";
								break;
								
								case "SIE - Service Informatique":
								$id_Profession = "10";
								break;
								
								case "Conciergerie":
								$id_Profession = "11";
								break;
								
								case "Administratif":
								$id_Profession = "12";
								break;
								
								case "Social - Vie ETML":
								$id_Profession = "13";
								break;
								
								case "ES - Ecole supérieure":
								$id_Profession = "14";
								break;
								
								case "EST - Enseignement scientifique":
								$id_Profession = "15";
								break;
							
								case "ECG - Enseignements Culture Général":
								$id_Profession = "16";
								break;
							
								case "Ebéniste - Menuisier-ère":
								$id_Profession = "17";
								break;
								
								case "Bois":
								$id_Profession = "18";
								break;

							}
						?>
						
                        <a href="<?php echo $this->html()->url('class/'.$student->school_class_id()); ?>"><?php echo $student->school_class_id(); ?></a><br>
                        <a href="<?php echo $this->html()->url('profession/'.$id_Profession); ?>"><?php echo $student->profession_name(); ?></a>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <?php if ((!empty($result['Colleague'])) || (!empty($result['Roles']))) { ?>
        <div class="result-box span4">
            <h3><?php echo $this->h('Collègue(s)'); ?></h3>

            <ul>
                <?php foreach ($result['Colleague'] as $colleague) { ?>
                <li class="search-box-user">
                    <div class="itemPicture">
                        <?php $photo = 'img/colleagues/'.$colleague->id().'.jpg'; ?>

                        <?php $webroot = $this->html()->url('img/nophoto.jpg'); ?>

                        <?php if(file_exists(WEBROOT.$photo)) {
                            $photo = $this->html->url($photo); ?>
                        <img src="<?php echo $photo ?>">
                        <?php } else { ?>
                        <img src="<?php echo $webroot ?>">
                        <?php } ?>
                    </div>
                    <div class="itemInfo">
                        <h6>
                            <a href="<?php echo $this->html()->url('colleague/'.$colleague->id()); ?>">
                                <?php echo $colleague->first_name(); ?>
                                <?php echo $colleague->name(); ?>
                            </a>
                        </h6>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <?php if (!empty($result['Class'])) { ?>
        <div class="result-box span4">
            <h3 class="txtcenter"><?php echo $this->h('Classe(s)') ?></h3>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $this->h('Type'); ?></th>
                        <th><?php echo $this->h('Maître de classe'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($result['Class'] as $class) { ?>
                    <tr>
                        <td><a class="info-link" href="<?php echo $this->html()->url('class/'.$class->id()); ?>"><?php echo $class->id(); ?></a></td>
                        <td><a class="info-link" href="<?php echo $this->html()->url('class/'.$class->id()); ?>"><?php echo $class->profession_name(); ?></a></td>
                        <td><a class="info-link" href="<?php echo $this->html()->url('colleague/'.$class->colleague_id()); ?>"><?php echo $class->colleague_id(); ?></a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } ?>
<?php } ?>
