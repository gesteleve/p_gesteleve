<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>
<?php $this->html()->js('../assets/ckeditor/ckeditor.js'); ?>


<?php
    // Recupère les droits de l'utilisateur logé-->
    if ($user->isAuthenticated()) { 
    $right = $user->getAttribute('right'); 
    } 
?>

<script type="text/javascript">

function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = stopRKey;

</script> 

<?php
    $i = 0;
?>

<div id="content">
    <h4 class="header"><?php echo $this->h('Bienvenue'); ?></h4>
    <div class="span12 ml0 student-summary small-top">                        
        <div class="summary_box">
            
              

            <?php
            print($title[0]->SearchContent());
            if (@$right[COLLEAGUE] & ADD)
            {
                // Affichage des bouton de modification et d'archivage
                print('
                <button class="btn" data-toggle="modal" data-target="#TitleModif"><i class="icon-pencil"></i>Modifier</button>
                ');
            }
        ?>                          
            
        </div>
        
    </div> 

    <!--Début de la modal de modification, grâce au $i, une modal est crée pour chaque news-->
      <div class="modal fade" id="TitleModif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <!--Titre de la modal-->
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel">Modifier une news</h3>
            </div>

            <!--Contenu-->
            <div class="modal-body"> 

                  <!--Début du formulaire de modification-->
                  <form role="form" action="<?php echo $this->html()->url('search/edit'); ?>" id="formEdit" name="formEdit" method="post">
                    <!--Contenu-->
                  <label for="content">Contenu</label>
                  <textarea class="ckeditor" id="content" name="content" rows="2"><?php echo $title[0]->SearchContent(); ?></textarea>
                    
                                
                  </form>

            <!--Fin du contenu-->
            </div>

            <!--Pied de page-->
            <div class="modal-footer">

              <!--Bouton d'envoie du formulaire-->      
              <button id="btn-edit-news" class="btn" onClick="$('form#<?php echo "formEdit"; ?>').submit();">Enregistrer</button>
            
            </div>
          </div>
        </div>

      <!--Fin de la modal-->  
      </div>   


    <div class="NewTest">
        <h4 class="TitleNews"><?php echo $this->h('Dernière news'); ?></h4>

        <ul class="thumbnails">
            <?php 
                foreach ($listNews as $listeNews) 
                { 
                    if($i < 3) 
                    {
            ?> 
                        <li class="NewsSpan">
                            <a href='./news/view/<?php echo ($listeNews->id()); ?>' class="ThumbnailNews">
                                

                                <h5 class="TitreNews"><?php echo($listeNews->title()); ?></h5>
                                <div class="TitreNews"><p class="TitreNews"><?php echo substr($listeNews->content(), 0, 50)."..."; ?></p></div>
                              
                            </a>
                        </li>
                    

            <?php  
                    }
                    $i++;          
                } 
            ?> 
            <a class="down" href='./news'>Voir plus de news</a> 
        </ul>

    </div>

            
             
         
    

    
    <div id="HeadSearch">
        
        <h4 class="header"><?php echo $this->h('Recherche dans le système Gestion Elèves'); ?></h4>
        <em><?php echo $this->h('Taper pour rechercher..'); ?></em>

        <form action="<?php echo $this->html()->url(); ?>" method="post">
            <input class="search" id="pattern" name="pattern" type="text" autocomplete="off">
        </form>
        
    </div>

    



    <!-- Désactivé
    <div class="span4">
            <div class="widget-header"><i class="icon-signal"></i>
                <h5>Statistique ETML</h5>
            </div>

            <div class="widget-header-under">Statistique du mois</div>

            <div class="widget-body clearfix">
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <th>Données</th>
                          <th>Chiffres</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Arrivées tardives</td>
                        <td><strong>34</strong></td>
                      </tr>
                      <tr>
                        <td>Absence</td>
                        <td><strong>2</strong></td>
                      </tr>
                      <tr>
                        <td>Congé</td>
                        <td><strong>0</strong></td>
                      </tr>
                    </tbody>
                </table>
            </div>
    </div>-->

    <div class="span12 offset4" id="ajax-loader">
        <div id="loaderImage"></div>
    </div>

    <div id="results" class="span12">

    </div>


    <!--<div id="stream" class="span12">
        <h4 class="header">ETML LIVE</h4>
        <div class="item">
            <img src="<?php //echo $this->html()->img('students/2012/alvesda.jpg') ?>" class="avatar">
            <p class="date">A l'instant</p>
            <h4>Daniel alves</h4>
            <div class="descr">
                Feuille d'excuse apporté.
            </div>
        </div>
        <div class="item">
            <div class="stream-icon stream-warning">
                <i class="icon-warning-sign icon-white"></i>
            </div>
            <p class="date">Il y a 24 min</p>
            <h4>Congé</h4>
            <div class="descr">
                Le cours de judo des CIN4a est annulé.
            </div>
        </div>
        <div class="item">
            <img src="<?php //echo $this->html()->img('students/2009/dupuyel.jpg'); ?>" class="avatar">
            <p class="date">Il y a 2 jours</p>
            <h4>Eliott Dupuy</h4>
            <div class="descr">
                Est arrivé en retard. 8h07.
            </div>
        </div>
        <div class="item">
            <div class="stream-icon stream-danger">
                <i class="icon-warning-sign icon-white"></i>
            </div>
            <p class="date">Il y a 2 jours</p>
            <h4>Alerte à la bombe</h4>
            <div class="descr">
                Hier près de 17h, une bombe à été découverte, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel libero risus. In hac habitasse platea dictumst. Vivamus ut neque sit amet mauris ornare porta. Vestibulum ullamcorper, nibh ut vehicula egestas, mi nisi molestie leo, ac iaculis libero nibh id mi. Phasellus in cursus leo. Praesent tristique, tellus pulvinar luctus molestie, magna eros fringilla nisi, sit amet eleifend justo metus sit amet nisi. Pellentesque rutrum nisl pretium orc
            </div>
        </div>
        <div class="item">
            <div class="stream-icon stream-info">
                <i class="icon-warning-sign icon-white"></i>
            </div>
            <p class="date">Il y a 2 jours</p>
            <h4>Romain Lanz</h4>
            <div class="descr">
                Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam pretium odio id felis accumsan et malesuada elit sodales. Maecenas condimentum elit
            </div>
        </div>
        <div class="item">
            <div class="stream-icon stream-success">
                <i class="icon-warning-sign icon-white"></i>
            </div>
            <p class="date">Il y a 2 jours</p>
            <h4>Eliott Dupuy</h4>
            <div class="descr">
                Donec augue sem, venenatis eget lacinia sed !
            </div>
        </div>
    </div>-->
</div>