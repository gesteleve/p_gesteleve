<?php
//*********************************************************
// Societe: ETML
// Date : 20.05.2014
// Auteur : Lymberis Dimitrios
// Raison : liste des élèves d'un maitre de classe
//*********************************************************

 if ($user->isAuthenticated()) {
$this->html()->js('isotope.min.js'); 
$right = $user->getAttribute('right');
}

?>


<?php foreach ($classes as $class){
    if ($class->student_nbre()>0) { ?>
        <div id="content">
              
                <div id="profile" class="span12">
                    <div class="span10">
                        <h1>
                            <?php echo $class->id();?>
                            <a href="<?= $this->html()->url('absence/'.$class->id().'/validate'); ?>" class="btn btn-large btn-success" style="float: right; margin-left: 15px;"><i class="fa fa-calendar"></i> Valider les absences</a>
                            <a href="<?= $this->html()->url('absence/'.$class->id()); ?>" class="btn btn-large btn-primary" style="float: right;"><i class="fa fa-calendar"></i> Absences</a>
                        </h1>

                        <div class="second">
                            <table>
                                <tr>
                                    <td class="grey w150p">
                                        Nb Elèves
                                    </td>
                                    <td>
                                            <?php echo $class->student_nbre()  ;?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grey w150p">
                                        Profession
                                    </td>
                                    <td>

                                        <a class="info-link" href="<?php echo $this->html()->url('profession/'.$class->profession_id()) ?>"><?php echo $class->profession_name(); ?></a>
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>

                <?php foreach ($students as $student)
                    {
                        if($student->school_class_id() == $class->id())
                        { 
                            ?>
                            <div class="post <?php echo $student->school_class_id().' '.$student->profession_id(); ?>">
                                <a href="<?php echo $this->html()->url('student/'.$student->id()); ?>" data-toggle="modal">
                            
                                    <div class="picture">
                                        <?php
                                        $photo = '/img/students/'.$student->entry_date().'/'.$student->id().'.jpg';

                                        if (file_exists(WEBROOT.$photo)) 
                                        {
                                            ?><img src="<?php echo $this->html()->url($photo); ?>" alt=""><?php
                                        } 
                                        else 
                                        {
                                            ?><img src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
                                        }?>
                                    </div>

                                    <div class="information">
                                        <h5>
                                            <a class="info-link" href="<?php echo $this->html()->url('student/'.$student->id()); ?>"><?php echo $student->first_name(); ?> <?php echo $student->name(); ?></a>
                                        </h5>

                                        <p><?php echo $student->id(); ?></p>
                                    </div>
                                </a>
                            </div><?php
                        }  
                    }?>         

                
        </div>
<?php 
    }
} ?>
