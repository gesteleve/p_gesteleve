<?php $this->html()->css('jasny-bootstrap.min.css'); ?>
<?php $this->html()->css('jasny-bootstrap-responsive.min.css'); ?>
<?php $this->html()->js('jquery.imgareaselect.min.js'); ?>
<?php $this->html()->js('jasny-bootstrap.min.js'); ?>
<?php $this->html()->js('jquery.form.js'); ?>

<div id="school_class">
	<form id="student" action="" name="class" method="post">
		<div class="span8">
	        <fieldset>
	            <legend>Informations générales</legend>
	            <div class="span6 ml0">
	            	<div class="span6 ml0">
	            	<label>Nom</label>
	            	<input type="text" name="name" tabindex="1" class="input-block-level" value="<?php echo isset($_POST['name']) ? $_POST['name'] : null ?>" required>
	            	
	            	
	            	
		            	<label>Maitre de classe</label>
		            	<select tabindex="5" name="colleague" class="input-block-level">
			            	<?php foreach ($colleagues as $colleague): 
	                
	                  			echo '<option value="'.$colleague->id().'">'.$colleague->name(). ' ' .$colleague->first_name().'</option>'; 
	                
	                		endforeach ?>

			            </select>

		            	<label>Section</label>
			            <select tabindex="5" name="section" class="input-block-level">
			            	<?php foreach ($professions as $profession): 
	                
	                  			echo '<option value="'.$profession->id().'">'.$profession->name().'</option>'; 
	                
	                		endforeach ?>

			            </select>

		            </div>

	            
	            </div>
	            
    </form>
    
    <div class="placeholder"></div>
    <div class="span12 ml0">
	    <fieldset>
	        <button class="btn btn-large btn-primary center mt30" type="button" onClick="$('form#student').submit();">Enregistrer</button>
	    </fieldset>
    </div>

</div>

