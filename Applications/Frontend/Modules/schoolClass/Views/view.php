<?php
//*********************************************************
// Societe: ETML
// Auteur : Lanz Romain
// Date : inconnue
// But : afficher une classe
//*********************************************************
// Modifications:
// Date : 12.05.2014
// Auteur : Lukyantsev  Vladislav
// Raison : Rajouter le lien imprimer dans le menu option et créer la modalbox d'impression
//*********************************************************
// Modifications:
// Date : 20.05.2014
// Auteur : Lukyantsev  Vladislav
// Raison : Rajouter une verification sur les droits d'accès (menu option et modal print et code)
//*********************************************************
if ($user->isAuthenticated()) {
$this->html()->js('isotope.min.js'); 
$right = $user->getAttribute('right');

//appel du menu option
$menu=array(
    "class",$class->id(),"",@SCHOOLCLASS,$right,
    "Editer","","Modal","editSchoolClass",@DELETE,
    "Imprimer","","Modal","PrintModal",@VIEW_ALL
    );

echo $this->html()->optionmenu($menu);

}
?>
<script> 

</script>
<div id="content">
  
<!-- 
Modal pour impression 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur imprimer dans le menu option
-->
<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

 <!-- Titre de la modal box -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Impression</h4>
      </div>

      <div class="modal-body">
    
 <!-- Liste déroulante type d'impression -->
        <div class="btn-group">
          <select id="optionList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('optionList').value);">
            <option selected="selected">Type d'impression</option>
            <option value="imgOn">Avec photo</option>
            <option value="imgOff">Sans photo</option>
          </select>
        </div>
 <!-- Liste déroulante format police -->
        <div class="btn-group">
          <select id="tailleList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('tailleList').value);">
            <option selected="selected">Format de texte</option>
            <option value="x-small">Très petit</option>
            <option value="small">Petit</option>
            <option value="medium">Moyen</option>
            <option value="large">Grand</option>
            <option value="x-large">Très Grand</option>
          </select>
        </div>
<br/><br/>
<!-- Liste déroulante police -->
        <div class="btn-group">
          <select id="policeList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('policeList').value);">
            <option selected="selected">Police</option>
            <option value="TNR">Times New Roman</option>
            <option value="arial">Arial</option> 
            <option value="CenturyGothic">Century Gothic</option>
            <option value="EcoFont">EcoFont</option>   
          </select>
        </div>

<!-- Liste déroulante en-tete et pied de page -->
        <div class="btn-group">
          <select id="enteteList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('enteteList').value);">
            <option selected="selected">En-tete</option>
            <option value="ETPPyes">oui</option>
            <option value="ETPPno">non</option>  
          </select>
        </div>
<br/><br/>
<?php if(isset($right) && @$right[SCHOOLCLASS] & MODIFY)
{ ?>
<!-- Liste déroulante Detail -->
        <div class="btn-group">
          <select id="detailList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('detailList').value);">
            <option selected="selected">Détails</option>
            <option value="Detailyes">oui</option>
            <option value="Detailno">non</option>  
          </select>
        </div>
<?php } ?>
        <br/><br/>
      </div>
 <!-- Bouton modal box (fermer,imprimer) -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrintPDF();">Enregistrer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrint();">Imprimer</button>
      </div>

    </div>
  </div>
</div>

<!-- Fin de la modal box d'impression -->

 
<!-- 
Modal pour les option sur les classes 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur edit dans le menu option
-->
<div class="modal fade" id="editSchoolClass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <form id="schoolClass" action="<?php echo $this->html()->url('class/'.$class->id().'/edit'); ?>" name="schoolClass" method="post">
        <!-- Titre de la modal box -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3  id="myModalLabel">Modification d'une classe</h3>     
        </div>
        
        <!-- Contenu de la modal box -->
        <div class="modal-body">
          
            <div class="span6 ml0">
              <div class="span6 ml0">
              <label>Nom</label>
              <input type="text" name="schoolClassName" tabindex="1" class="input-block-level" value="<?php echo $class->id(); ?>"required> 
                                          
                <label>Maitre de classe</label>
                <select tabindex="5" name="colleague" class="input-block-level" required>
                  <?php foreach ($colleaguesList as $colleagueList): 
                
                       // Insère les collegues dans le select
                        if ($colleagueList->id()==$class->colleague_id()) {
                            echo '<option value="'.$colleagueList->id().'" selected>'.$colleagueList->name(). ' ' .$colleagueList->first_name().'</option>';
                        } else {
                            echo '<option value="'.$colleagueList->id().'" >'.$colleagueList->name(). ' ' .$colleagueList->first_name().'</option>';
                        }
                
                    endforeach ?>

                </select>

                <label>Section</label>
                <select tabindex="5" name="section" class="input-block-level" required>
                  <?php foreach ($Listprofessions as $Listprofession): 
        
               
                       // Insère les collegues dans le select
                        if ($Listprofession->name()==$class->profession_name()) {
                            echo '<option value="'.$Listprofession->id().'" selected>'.$Listprofession->name(). '</option>';
                        } else {
                            echo '<option value="'.$Listprofession->id().'">'.$Listprofession->name().'</option>';
                        }
  
                    endforeach ?>

                </select>

              </div>      
            </div>

          
        </div>
        <div class="modal-footer">
            <button id="btn-delete-follow" class="btn btn-danger l">Supprimer la classe</button>
          <button class="btn btn-primary" type="submit">Modifier</button>
        </div>
      </form>
   
   </div>
  </div>
</div>

<!-- Fin de la modal box sur les options de la liste de classes -->




    <div id="profile" class="span12">
        <div class="span10">
            <h1>
                <?php echo $class->id();?>
                <?php if($this->app->user()->isAuthenticated()): ?>
                    <?= ($classOfUser[0]->id() == $class->id()) ? '<a href="'.$this->html()->url('absence/'.$class->id().'/validate').'" class="btn btn-large btn-success" style="float: right; margin-left: 15px;"><i class="fa fa-calendar"></i> Valider les absences</a>' : ''; ?>
                    <a href="<?= $this->html()->url('absence/'.$class->id()); ?>" class="btn btn-large btn-primary" style="float: right;"><i class="fa fa-calendar"></i> Absences</a>
                <?php endif; ?>
            </h1>

            <div class="second">
                <table>
                    <tr>
                        <td class="grey w150p">
                            Nb Elèves
                        </td>
                        <td>
                            <?php echo count($students)  ;?>
                        </td>
                    </tr>
                    <tr>
                        <td class="grey w150p">
                            Profession
                        </td>
                        <td>

                            <a class="info-link" href="<?php echo $this->html()->url('profession/'.$class->profession_id()) ?>"><?php echo $class->profession_name(); ?></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="grey w150p">
                            Maître de classe
                        </td>
                        <td>
                            <?php if ($colleague): ?>
                            <a class="info-link" href="<?php echo $this->html()->url('colleague/'.$class->colleague_id()) ?>"><?php echo $colleague->first_name().' '.ucfirst(strtolower($colleague->name())); ?></a>
                            <?php else: ?>
                            Non renseigné
                            <?php endif ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="mt30">
        <div id="container">
            <?php foreach ($students as $student): ?>
                <div class="post <?php echo $student->school_class_id().' '.$student->profession_id(); ?>">
                    <a href="<?php echo $this->html()->url('student/'.$student->id()); ?>" data-toggle="modal">
                        <div class="picture">
                        <?php
                        //print_r($students);
                        $photo = '/img/students/'.$student->entry_date().'/'.$student->id().'.jpg';
                        if (file_exists(WEBROOT.$photo)) {
                            ?><img src="<?php echo $this->html()->url($photo); ?>" alt=""><?php
                        } else {
                            ?><img src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
                        }

                        ?>
                        </div>
                        <div class="information">
                            <h5>
                                <a class="info-link" href="<?php echo $this->html()->url('student/'.$student->id()); ?>"><?php echo $student->first_name(); ?> <?php echo $student->name(); ?></a>
                            </h5>

                            <?php echo $student->id()."<br/>"; ?>


                            <?php if(isset($right) && @$right[SCHOOLCLASS] & MODIFY)
                            {
                                echo "<h6 class='printDetail'>".$student->birth_date()."<br/>";

                                echo $student->address()."<br/>";
                                echo $student->zip()." ".$student->city()."<br/>";

                                echo $student->phone()."<br/>";
                                echo $student->email()."<h6><br/>";
                            }
                            ?>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            <input id="nbreleve" type="hidden" value="<?php echo count($students); ?>"/>
        </div>
    </div>

            <!--<div class="widget-body clearfix">
                <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?php echo $this->h('Classe'); ?></th>
                        <th><?php echo $this->h('Profession'); ?></th>
                        <th><?php echo $this->h('Maître de classe'); ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($classes as $class): ?>
                    <tr>
                        <td><a href="<?php echo $this->html()->url('class/'.$class->id()); ?>"><?php echo $class->id(); ?></a></td>
                        <td><a href="<?php echo $this->html()->url('profession/'.$class->profession_id()); ?>"><?php echo $class->profession_name(); ?></a></td>
                        <td><a href="<?php echo $this->html()->url('colleague/'.$class->colleague_id()); ?>"><?php echo $class->colleague_id(); ?></a></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>-->
</div>

