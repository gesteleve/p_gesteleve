<?php
//*********************************************************
// Societe: ETML
// Auteur : Lanz Romain
// Date : inconnue
// But : afficher la liste des classes
//*********************************************************
// Modifications:
// Date : 29.07.2014
// Auteur : Lymberis Dimitrios
// Raison : Rajouter menu option avec ajout de classe
//*********************************************************

 if ($user->isAuthenticated()) 
 {
  $this->html()->js('isotope.min.js'); 
  $right = $user->getAttribute('right');

  //appel du menu option
  $menu=array(
      "schoolClass","","",@SCHOOLCLASS,$right,
      "Ajouter","/add","Modal","SchoolClassModalAdd",@DELETE
      );

  echo $this->html()->optionmenu($menu);
}
?>

<!-- 
Modal pour les option sur les classes 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur ajouter dans le menu option
-->
<div class="modal fade" id="SchoolClassModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <form id="schoolClass" action="<?php echo $this->html()->url('class/add'); ?>" name="schoolClass" method="post">
        <!-- Titre de la modal box -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3  id="myModalLabel">Ajout d'une classe</h3>     
        </div>
        
        <!-- Contenu de la modal box -->
        <div class="modal-body">
          
            <div class="span6 ml0">
              <div class="span6 ml0">
              <label>Nom</label>
              <input type="text" name="schoolClassName" tabindex="1" class="input-block-level" required>
              
              
              
                <label>Maitre de classe</label>
                <select tabindex="5" name="colleague" class="input-block-level" required>
                  <?php foreach ($colleagues as $colleague): 
                
                        echo '<option value="'.$colleague->id().'">'.$colleague->name(). ' ' .$colleague->first_name().'</option>'; 
                
                    endforeach ?>

                </select>

                <label>Section</label>
                <select tabindex="5" name="section" class="input-block-level" required>
                  <?php foreach ($Listprofessions as $Listprofession): 
                
                        echo '<option value="'.$Listprofession->id().'">'.$Listprofession->name().'</option>'; 
                
                    endforeach ?>

                </select>

              </div>      
            </div>

          
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="submit">Ajouter</button>
        </div>
      </form>
   
   </div>
  </div>
</div>

<!-- Fin de la modal box sur les options de la liste de classes -->


<div class="m10">
  <div id="container">


   
        

                <?php 
                //print_r($students);

   foreach ($professions as $profession)
   {
      echo(' <ul class="thumbnailsClassList">');
      echo "<br/><H3>".$profession->profession_name()."</H3><br/>";


      foreach ($classes as $class)
      {   
          
        if($class->profession_name() == $profession->profession_name() && $class->student_nbre() > 0)
        {
          
?>
          <li>
            <div class="thumbnailClassList">
              <h5><a class="info-link" href="<?php echo $this->html()->url('class/'.$class->id()); ?>"><?php echo $class->id(); ?></a></h5>
              
              <a class="info-link" href="<?php echo $this->html()->url('colleague/'.$class->colleague_id()); ?>"><?php echo $class->colleague_id(); ?></a>
               <!-- <p><?php //echo $class->student_nbre(); ?></p>-->
            </div>
          </li>
          
<?php 

        
        }                  
      }
      echo('</ul>');
    } 
?>            
   </div>
</div>
