<?php

namespace Applications\Frontend\Modules\schoolClass;

use Applications\Entities\schoolClass;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class schoolClassController extends BackController
{
    function executeIndex() {
        $this->page->addVar('title', 'Liste des classes');
        $this->page->addVar('description', 'Liste des classes de l\'ETML');
        $this->page->addVar('keywords', 'liste, classes');

        $classes = $this->managers->getManagerOf('schoolClass')->getListClass();
        $this->page->addVar('classes', $classes);

        $professions = $this->managers->getManagerOf('schoolClass')->getListSectionClass();
        $this->page->addVar('professions', $professions);    

        //on recupere un tableau avec la liste des professions
        $Listprofessions = $this->managers->getManagerOf('Profession')->getListSection();
        //on initialise une variable avec la liste des prfessions qui sera visible dans la page de vue d'ajout
        $this->page->addVar('Listprofessions', $Listprofessions);
        
        //on recupere un tableau avec la liste des collegues
        $colleagues = $this->managers->getManagerOf('Colleague')->getList();
        
        $this->page->addVar('colleagues', $colleagues);        
    }

    

    function executeView(HTTPRequest $request) {
        $class = $this->managers->getManagerOf('schoolClass')->getUnique($request->getData('id'));
        $students = $this->managers->getManagerOf('schoolClass')->getStudents($request->getData('id'));
        $colleague = $this->managers->getManagerOf('Colleague')->getUnique($class->colleague_id());

        //on recupere un tableau avec la liste des collegues
        $Listcolleagues = $this->managers->getManagerOf('Colleague')->getList();
        $this->page->addVar('colleaguesList', $Listcolleagues); 

        //on recupere un tableau avec la liste des professions
        $Listprofessions = $this->managers->getManagerOf('Profession')->getListSection();
        //on initialise une variable avec la liste des prfessions qui sera visible dans la page de vue d'ajout
        $this->page->addVar('Listprofessions', $Listprofessions);

        $this->page->addVar('title', 'Classe '.$class->id());
        $this->page->addVar('description', 'Classe '.$class->id());
        $this->page->addVar('keywords', 'classe, '.$class->id());
        $this->page->addVar('class', $class);
        $this->page->addVar('students', $students);
        $this->page->addVar('colleague', $colleague);

        // Si l'utilisateur est authentifié, on vérifie si il est le maître la de classe courante
        if($this->app->user()->isAuthenticated()){
            $class_manager = $this->managers->getManagerOf('schoolClass');
            $classOfUser = $class_manager->getUniqueByColleague($this->app->user()->getAttribute('user')->id());
            $this->page->addVar('classOfUser', $classOfUser);
        }
    }

    function executeMyclass(HTTPRequest $request) {
        $this->app->user()->restricted($request);
        $classes = $this->managers->getManagerOf('schoolClass')->getUniqueByColleague($request->getData('id'));
        $this->page->addVar('classes', $classes);


        $students = $this->managers->getManagerOf('schoolClass')->getListStudentByMC($request->getData('id'));
        $this->page->addVar('students', $students);

        //on recupere un tableau avec la liste des professions
        $Listprofessions = $this->managers->getManagerOf('Profession')->getListSection();
        //on initialise une variable avec la liste des prfessions qui sera visible dans la page de vue d'ajout
        $this->page->addVar('Listprofessions', $Listprofessions);
    }




    //*********************************************************
    // Societe: ETML
    // Auteur : -
    // Date : inconnue
    // But : -
    //*********************************************************
    // Modifications:
    // Date : 29.07.2014
    // Auteur : Lymberis Dimitrios
    // Raison : mise en place de la fonction d'ajout de classe
    //*********************************************************
    function executeAdd(HTTPRequest $request) {
    
        if($request->postsExists(array('schoolClassName', 'colleague','section'))){
            // Vérifié que l'utilisateur soit authentifié
            if ($this->app->user()->isAuthenticated()) {

                // Récupère les droits de l'utilisateur
                $right = $this->app->user()->getAttribute('right');
                // Vérifie que l'utilisateur ait les droits d'ajout
                if(@$right[FOLLOW] & ADD) {
                    if (!$request->postsEmpty(array('schoolClassName', 'colleague','section'))){
                        // Récupère la liste des classes
                        $classes = $this->managers->getManagerOf('schoolClass');

                        // On vérifie si la classe existe déja
                        $classSearch = $classes->getUnique($request->postData('schoolClassName'));
                        // Vérifie que la requête nous ait retourné quelque chose
                        if ($classSearch) {
                            // la classe existe deja
                             $this->app->user()->setFlash('La classe existe déjà !', 'error');
                        }
                        else {

                            $classNew = new schoolClass(array(
                                'id'            => $request->postData('schoolClassName'),
                                'profession_id' => intval($request->postData('section')),
                                'colleague_id'  => $request->postData('colleague')));
                            $success = $classes->add($classNew);

                            if ($success) {
                                $this->app->user()->setFlash('La classe a été ajoutée', 'success');
                            } else {
                                $this->app->user()->setFlash('Une erreur c\'est produite lors de l\'ajout d\'une classe', 'error');
                            }
                        }
                    }
                    else {
                        $this->app->user()->setFlash('Il manque des données, vérifiez que vous avez rempli tous les champs');
                    }
                }
                else {
                    $this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                }
            }
            else {
                $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            }
            // Redirige l'utilisateur vers la page précédente
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
        else {
            // Redirige l'utilisateur vers une page de type 404
            $this->app->httpResponse()->redirect404();
        }
     

    }

    function executeEdit(HTTPRequest $request) {

        if($request->postsExists(array('schoolClassName', 'colleague','section'))){
            // Vérifié que l'utilisateur soit authentifié
            if ($this->app->user()->isAuthenticated()) {

                // Récupère les droits de l'utilisateur
                $right = $this->app->user()->getAttribute('right');
                // Vérifie que l'utilisateur ait les droits d'ajout ou de modification
                if(@$right[FOLLOW] & ADD) {
                    if (!$request->postsEmpty(array('schoolClassName', 'colleague','section'))){
                        // Récupère la liste des classes
                        $classes = $this->managers->getManagerOf('schoolClass');

                        // On vérifie si la classe existe déja
                        $classSearch = $classes->getUnique($request->postData('schoolClassName'));
                        // Vérifie que la requête nous ait retourné quelque chose
                        //if ($classSearch) {
                        //    // la classe existe deja
                        //     $this->app->user()->setFlash('La classe existe déjà !', 'error');
                        //}
                        //else {

                            $classNew = new schoolClass(array(
                                'id'            => $request->postData('schoolClassName'),
                                'profession_id' => intval($request->postData('section')),
                                'colleague_id'  => $request->postData('colleague')));

                            
                            $success = $classes->modify($classNew);

                            if ($success) {
                                $this->app->user()->setFlash('La classe a été modifiée', 'success');
                            } else {
                                $this->app->user()->setFlash('Une erreur c\'est produite lors de la modification d\'une classe', 'error');
                            }
                        //}
                    }
                    else {
                        $this->app->user()->setFlash('Il manque des données, vérifiez que vous avez rempli tous les champs');
                    }
                }
                else {
                    $this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                }
            }
            else {
                $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            }
            // Redirige l'utilisateur vers la page précédente
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
        else {
            // Redirige l'utilisateur vers une page de type 404
            $this->app->httpResponse()->redirect404();
        }

   }

    function executeDelete(HTTPRequest $request) {

    }
}
