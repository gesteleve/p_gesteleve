<?php $this->html()->js('isotope.min.js'); ?>
<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>

<ul id="filters">
  <li>
    <form>
      <input type="text">
    </form>
  </li>
  <li>
    <a href="#">Recherche</a>
  </li>
  <li><a href="#" class="cat classe">Classes</a>
    <ul class="second">
        <li><a href="#" class="active first txtcenter" data-filter="*">Tous afficher</a></li>
        <br>
      <?php foreach ($classes as $class): ?>
        <li>
            <a href="#" class="w50p txtcenter" data-filter=".<?php echo $class->id(); ?>"><?php echo $class->id(); ?></a>
        </li>
      <?php endforeach ?>
    </ul>
  </li>
  <li><a href="#" class="cat job">Métiers</a>
    <ul class="second">
        <li><a href="#" class="active first txtcenter" data-filter="*">Tous afficher</a></li>
        <br>
      <?php foreach ($professions as $profession): ?>
        <li>
            <a href="#" class="w100p txtcenter" data-filter=".<?php echo $profession->id(); ?>"><?php echo $profession->name(); ?></a>
        </li>
      <?php endforeach ?>
    </ul>
  </li>
</ul>

<div id="second-step">
  <div class="caddy-number"></div>
  <button class="btn btn-primary">Continuer</button>
</div>

<div class="mt30">
  <div id="container" class="event">
    <?php foreach ($students as $student): ?>
      <div class="post <?php echo $student->school_class_id().' '.$student->profession_id(); ?>">
          <a  href="#" data-toggle="modal">
              <div class="picture">
                  <?php
                  $photo = '/img/students/'.$student->entry_date().'/'.$student->id().'.jpg';
                  $photo = $test;
                  if (file_exists(WEBROOT.$photo)) {
                      ?><img src="<?php echo $this->html()->url($photo); ?>" alt=""><?php
                  } else {
                      ?><img src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
                  }
                  ?>
              </div>
              <div class="information">
                <h5>
                  <?php echo $student->first_name(); ?> <?php echo $student->name(); ?>
                </h5>
                <p><?php echo $student->school_class_id(); ?></p>
                <p><?php echo $student->profession_name(); ?></p>
              </div>
          </a>
      </div>
    <?php endforeach ?>
  </div>
</div>
