<?php

namespace Applications\Frontend\Modules\Event;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class EventController extends BackController
{
    function executeIndex() {
        $this->page->addVar('title', 'Liste des élèves');
        $this->page->addVar('description', 'Liste des élèves de l\'ETML');
        $this->page->addVar('keywords', 'liste élèves');

        $manager = $this->managers->getManagerOf('Event');
        $events = $manager->getList();

        $manager = $this->managers->getManagerOf('Student');
        $students = $manager->getList();

        $manager = $this->managers->getManagerOf('schoolClass');
        $classes = $manager->getList();

        $manager = $this->managers->getManagerOf('Profession');
        $professions = $manager->getList();

        $this->page->addVar('events', $events);
        $this->page->addVar('professions', $professions);
        $this->page->addVar('students', $students);
        $this->page->addVar('classes', $classes);
    }

    function executeView(HTTPRequest $request) {
        if ($request->getExists('id')) {
            $manager = $this->managers->getManagerOf('Event');
            $event = $manager->getUnique($request->getData('id'));

            if ($event) {
                $this->page->addVar('event', $event);
            } else {
                $this->app->httpResponse()->redirect404();
            }
        } else {
            $this->app->httpResponse()->redirect404();
        }
    }

    function executeAdd(HTTPRequest $request) {

    }

    function executeChoice(HTTPRequest $request) {

    }

    function executeSchedule(HTTPRequest $request) {

    }

    function executeEdit(HTTPRequest $request) {

    }

    function executeDelete(HTTPRequest $request) {

    }
}
