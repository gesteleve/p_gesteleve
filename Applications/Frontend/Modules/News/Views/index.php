<!--//*********************************************************
// Societe: ETML
// Auteur : Alexis Gonzalez
// Date : 26.05.2014
// But : Affichage et gestion des news.
//*********************************************************
// Modifications:
// Date : 23.06.2014
// Auteur : lukyantsev vladislav
// Raison : Rajout de verification de l'auteur de la news
//*********************************************************
// Date :
// Auteur :
// Raison :
//*********************************************************-->

<!--Recupère les droits de l'utilisateur logé-->
<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>

<!--Implémentation des fichiers javascript-->
<?php $this->html()->js('isotope.min.js'); ?>
<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>
<?php $this->html()->js('../assets/ckeditor/ckeditor.js'); ?>

<!--Déclaration de la variable permettant de différencier les modal-->
<?php 
  $i = 0;
  $today = date("Y-m-d");
?>


<!--<script type="text/javascript">
  submitfilter()
  { 
    
    if(document.getElementById("filterDate").value == "3")
    {        
      var elem = document.getElementById("filterDate");
      elem.value = "2";

        /*elem = document.getElementById("filterDate");
        elem.value = "2";*/
        //echo document.getElementById("filterDate").value;
    }
    else if(document.getElementById("filterDate").value == "2")
    {
      elem = document.getElementById("filterDate");
      elem.value = "3";
    }

    $('form#formFiltreDate').submit();
  }

</script>-->
<!--Début de l'affichage des news-->
<div class="m10">    

  <!--Affichage du titre-->
  <div class="widget-header"><i class="icon-pushpin"></i>
      <h5><?php echo $this->h('News'); ?></h5>
  </div>

  <div class="widget-body clearfix">
    <table class="table table-striped">

      <!--En-tête-->
      <thead>
        <tr>

          <!--Date-->
          <th>
            <form role="form" action="./news" id="formFiltreDate" name="formFiltreDate" method="post">              
              <button class="btn"><i class="icon-chevron-down" onClick="submitfilter();"></i> Date</button>
              <input id="filterDate" type="hidden" name="filterDate" value="2">
            </form>
          </th>

          <!--Titre-->
          <th><?php echo $this->h('Titre'); ?></th>

          <!--Contenu-->
          <th><?php echo $this->h('Contenu'); ?></th>

          <!--Modification-->
          <th><?php echo $this->h('Dernière modification'); ?></th>

          <!--Auteur-->
          <th>
            <form role="form" action="./news" id="formFiltreAutor" name="formFiltreAutor" method="post">
              <button class="btn"><i class="icon-chevron-down" onClick="$(\'form#formFiltreAutor\').submit();"></i> Auteur</button>
              <input type="hidden" name="filterAutor" value="2">
            </form>
          </th>

          <th>
            <div class="col-xs-6 col-md-4">
              <form role="form" action="./news" id="formFiltreNormal" name="formFiltreNormal" method="post">
                <button id="btn-edit-news" class="btn" onClick="$(\'form#formFiltreNormal\').submit();">Réinitialiser les filtres</button>
                <input type="hidden" name="filterNormal" value="1">
              </form>
            </div>
          </th>

        </tr>
      </thead>

      <!--Affichage du contenu-->
      <tbody>
        <?php   
	
          foreach ($listNews as $listeNews) 
          { 
            // Test si la news doit être affichées plus tard  
            if($listeNews->start_date() <= $today)
            {
                                                              
        ?>
              <tr>

                <!--Date de début d'affichage-->
                <td><?php echo $listeNews->start_date(); ?></td>

                <!--Titre de la news-->
                <td><?php echo $listeNews->title(); ?></td>

                <!--Contenu de la news, qui ne dépasse pas 18 caractères-->
                <td><?php echo substr($listeNews->content(), 0, 18); ?></td>

                <!--Dernière modification de la news-->
                <td><?php if(is_null($listeNews->mod_date())){echo "-";}else{echo $listeNews->mod_date();} ?></td>

                <!--Contenu de la news, qui ne dépasse pas 18 caractères-->
                <td><?php echo $listeNews->idx_colleague(); ?></td>

                <td>

                  <!--Bouton d'affichage des détails de la news-->
                  <a href="<?php echo $this->html()->url('news/view/'.$listeNews->id().''); ?>" class="btn mod"><i class="icon-zoom-in"></i></a>
                  
                  <?php

                  //Vérification des droits de l'utilisateurs
                  if ((@$right[NEWS] & MODIFY) && (@$right[NEWS] & DELETE))
                  {   
					//Vérifie si cet news appartient à l'utilisateur courant				  
					if($listeNews->idx_colleague() == $user->getAttribute('user')->id())
					{
						// Affichage des bouton de modification et d'archivage
						print('
						<button class="btn" data-toggle="modal" data-target="'."#ModNews".$i.'"><i class="icon-pencil"></i></button>
						<a href="'.$this->html()->url('news/'.$listeNews->id().'/archiveNews').'" class="btn mod" onclick="return confirm(\'Voulez vous vraiment archiver cette news ?\')"><i class="icon-folder-close"></i></a>
						');
					}
                  }
                  ?>
                
                </td>
              </tr>

              <!--Début de la modal de modification, grâce au $i, une modal est crée pour chaque news-->
              <div id="<?php echo "ModNews".$i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!--Titre de la modal-->
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h3 id="myModalLabel">Modifier une news</h3>
                    </div>

                    <!--Contenu-->
                    <div class="modal-body"> 

                          <!--Début du formulaire de modification-->
                          <form role="form" action="<?php echo $this->html()->url('news/edit/'.$listeNews->id().''); ?>" id="<?php echo "formEdit".$i; ?>" name="<?php echo "formEdit".$i; ?>" method="post">
                              
                              <!--Titre de la news-->
                              <label for="title">Titre</label>
                              <input type="text" name="title" id="title" class="form-control" value="<?php echo $listeNews->title(); ?>">

                              <!--Contenu-->
                              <label for="content">Contenu</label>
                              <textarea class="ckeditor" name="content" rows="2"><?php echo($listeNews->content()); ?></textarea>
                                
                              <!--Dates d'affichage-->
                              <label for="start_date">De:</label>
                              <input type="date" name="start_date" class="form-control" placeholder="" value="<?php echo $listeNews->start_date(); ?>">
                              <label for="end_date">A:</label>
                              <input type="date" name="end_date" class="form-control" placeholder="" value="<?php echo $listeNews->end_date(); ?>">
                                
                              <!--Droits de la news-->
                              <label for="right">Droits</label>
                              <select name="right" class="form-control">
                                <option value="2" <?php if($listeNews->rights()=="2"){echo "selected";} ?> >Tout le monde</option>
                                <option value="3" <?php if($listeNews->rights()=="3"){echo "selected";} ?> >Ma classe</option>
                                <option value="1" <?php if($listeNews->rights()=="1"){echo "selected";} ?> >Enseignant</option>            
                              </select>                            
                          </form>

                    <!--Fin du contenu-->
                    </div>

                    <!--Pied de page-->
                    <div class="modal-footer">

                      <!--Bouton d'envoie du formulaire-->      
                      <button id="btn-edit-news" class="btn" onClick="$('form#<?php echo "formEdit".$i; ?>').submit();">Enregistrer</button>
                    
                    </div>
                  </div>
                </div>

              <!--Fin de la modal-->  
              </div>            
        <?php
            }

            // Incrémentation de la variable i            
            $i++;  
          }
          //endforeach
        ?>
      </tbody>
    </table>
  </div>

  <!--Espace-->
  <br></br>

  <?php

    //Bouton d'ajout de news
    if (@$right[NEWS] & ADD)
    {
      print('

      <button class="btn" data-toggle="modal" data-target="#AddNews"><i class="icon-plus"></i> Ajouter une news</a></button>
      <a href="./news/archive" class="btn"><i class="icon-book"></i> News archivées</a>

      ');

    }

  ?>

<!--Fin de l'affichage des news-->
</div>    

<!--Création de la modal d'ajout-->
<div id="AddNews" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <!--En-tête-->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <!--Titre de la modal-->
          <h3 id="myModalLabel">Ajouter une news</h3>
        </div>

        <!--Contenu-->
        <div class="modal-body"> 

            <!--Début du formulaire d'ajout-->
            <form role="form" action="<?php echo $this->html()->url('news/add'); ?>" id="news" name="news" method="post">

                <!--Titre-->
                <div class="form-group">
                  <label for="title">Titre</label>
                  <input type="text" name="title" class="form-control" value="">
                </div>

                <!--Contenu-->
                <div class="form-group">
                  <label for="content">Contenu</label>
                  <textarea class="ckeditor" name="content" rows="2"></textarea>
                </div>

                <!--Date de début d'affcihage-->
                <div class="form-group">
                  <label for="start_date">De:</label>
                  <input type="date" name="start_date" class="form-control" placeholder="">
                </div>                

                <span class="help-block">Date au format: YYYY-MM-JJ</span>

                <!--Date de fin d'affichage-->
                <div class="form-group">
                  <label for="end_date">A:</label>
                  <input type="date" name="end_date" class="form-control" placeholder="">
                </div>

                <span class="help-block">Date au format: YYYY-MM-JJ</span>

                <!--Droits-->
                <div class="form-group">
                  <label for="right">Droits</label>
                  <select name="right" class="form-control">            
                    <option value="2" >Tout le monde</option>
                    <option value="3" >Ma classe</option>
                    <option value="1" >Enseignant</option>
                  </select>
                </div>

            <!--Fin formulaire-->                
            </form>
        
        <!--Fin du contenu-->
        </div>

        <!--Pied de page de la modal-->
        <div class="modal-footer">

          <!--Bouton d'envoie--> 
          <button id="btn-edit-news" class="btn" onClick="$('form#news').submit();">Enregistrer</button>
        
        </div>

      </div>
    </div> 

<!--Fin de la modal d'ajout--> 
</div>