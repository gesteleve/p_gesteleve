<!--//*********************************************************
// Societe: ETML
// Auteur : Alexis Gonzalez
// Date : 26.05.2014
// But : Affichage et gestion des news.
//*********************************************************
// Modifications:
// Date : 
// Auteur : 
// Raison : 
//*********************************************************
// Date :
// Auteur :
// Raison :
//*********************************************************-->

<!--Recupère les droits de l'utilisateur logé-->
<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>

<!--Implémentation des fichiers javascript-->
<?php $this->html()->js('isotope.min.js'); ?>
<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>
<?php $this->html()->js('../assets/ckeditor/ckeditor.js'); ?>


<!--Début de l'affichage des news-->
<div class="m10">    

  <!--Affichage du titre-->
  <div class="widget-header"><i class="icon-pushpin"></i>
      <h5><?php echo $this->h('News'); ?></h5>
  </div>

  <div class="widget-body clearfix">
    <table class="table table-striped">

      <!--En-tête-->
      <thead>
        <tr>
          
           <!--Date-->
          <th><?php echo $this->h('Date'); ?></th>

          <!--Titre-->
          <th><?php echo $this->h('Titre'); ?></th>                

          <!--Contenu-->
          <th><?php echo $this->h('Contenu'); ?></th>

          <!--Modification-->
          <th><?php echo $this->h('Dernière modification'); ?></th>
          
          <!--Auteur-->
          <th><?php echo $this->h('Auteur'); ?></th> 

        </tr>
      </thead>

      <!--Affichage du contenu-->
      <tbody>
        <?php          
          foreach ($listNews as $listeNews) 
          {                                            
        ?>
            <tr>

              <!--Date de début d'affichage-->
              <td><?php echo $listeNews->start_date(); ?></td>

              <!--Titre de la news-->
              <td><?php echo $listeNews->title(); ?></td>

              <!--Contenu de la news, qui ne dépasse pas 18 caractères-->
              <td><?php echo substr($listeNews->content(), 0, 18); ?></td>

              <!--Dernière modification de la news-->
              <td><?php if(is_null($listeNews->mod_date())){echo "-";}else{echo $listeNews->mod_date();} ?></td>

              <!--Contenu de la news, qui ne dépasse pas 18 caractères-->
              <td><?php echo $listeNews->idx_colleague(); ?></td>

              <td>
                <!--Bouton d'affichage des détails de la news-->
                <a href="<?php echo $this->html()->url('news/view/'.$listeNews->id().''); ?>" class="btn mod"><i class="icon-zoom-in"></i></a> 

                <!--Supprimer définitivement une news-->             
                <?php
                  print('
                    <a href="'.$this->html()->url('news/'.$listeNews->id().'/delete').'" class="btn mod" onclick="return confirm(\'Voulez vous vraiment supprimer définitivement cette news ?\')"><i class="icon-trash"></i></a>
                  ');
                ?>
              </td>
            </tr>  

        <?php   

          }
          //endforeach
        ?>
      </tbody>
    </table>
  </div>

  <!--Bouton retour-->
  <div class="span6 ml0">
    <label for="return"></label>
    <input type="button" name="return" class="btn" value="Retour" onclick="javascript:history.back()">  
  </div>

<!--Fin de l'affichage des news-->
</div>    

