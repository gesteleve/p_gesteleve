<!--//*********************************************************
// Societe: ETML
// Auteur : Alexis Gonzalez
// Date : 26.05.2014
// But : Fichier permettant d'afficher les informtions d'une news.
//*********************************************************
// Modifications:
// Date : 
// Auteur : 
// Raison : 
//*********************************************************
// Date :
// Auteur :
// Raison :
//*********************************************************-->


<!--Recupère les droits de l'utilisateur logé-->
<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>

<!--Inclus les fichiers javascripts-->
<?php $this->html()->js('isotope.min.js'); ?>
<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>


<div id="content">

	<!--Début du formulaire d'affichage-->
	<form id="modNews" action="" name="news" method="post">
		<div class="span12">
			<div class="span8">

				<!--Balise permettant de bloquer tous les champs-->
				<fieldset disabled>
					<legend>Informations de la News</legend>
					
				    <div class="span6 ml0">		

				    	<!--Titre-->		    
				    	<label for="title">Titre </label>				   
				    	<input type="text" name="title" class="input-block-level" value="<?php print($news->title()); ?>">

				    	<!--Date d'affichage-->				    	
						<div class="span6 ml0">
							<label for="start_date">De</label>
							<input type="date" name="start_date" class="input-block-level" placeholder="" value="<?php echo $news->start_date(); ?>">
						</div>
						<div class="span6">
							<label for="end_date">A</label>
							<input type="date" name="end_date" class="input-block-level" placeholder="" value="<?php echo $news->end_date(); ?>">
						</div>

						<!--Affichage des droits-->
						<label for="right">Droits</label>
						<select name="right" class="input-block-level">
				    		<option value="2" <?php if($news->rights()=="2"){echo "selected";} ?> >Tout le monde</option>
				        	<option value="3" <?php if($news->rights()=="3"){echo "selected";} ?> >Classe</option>
				        	<option value="1" <?php if($news->rights()=="1"){echo "selected";} ?> >Enseignant</option>            
				    	</select>

				    	<!--Affiche la classe de la news sauf si elle est vide-->
				    	<?php
					    	if ($news->idx_schoolClasse()!="") 
					    	{
					    ?>
					    		<label for="classe">Classe</label>
						    	<select name="classe" class="input-block-level">
							    	<option value=""><?php echo $news->idx_schoolClasse(); ?></option>					      	
							    </select>
					    <?php	
					    	}
				    	?>	

				    </div>

				    <!--Affichage du contenu-->
				    <div class="span12 ml0 student-summary small-top">
						<div class="widget-header">
							<i class="icon-tags"></i>
							<h5>Contenu</h5>
						</div>
						<div class="summary_box">
							
							<?php echo $news->content(); ?>
							
						</div>
					</div>

				</fieldset>

				<!--Bouton retour-->
				<div class="span6 ml0">
					<label for="return"></label>
					<input type="button" name="return" class="btn" value="Retour" onclick="javascript:history.back()">	
				</div>
			</div>
		</div>
	</form>
</div>