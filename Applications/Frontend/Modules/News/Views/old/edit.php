<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>
<?php $this->html()->js('isotope.min.js'); ?>
<?php $this->html()->js('jquery.infinitescroll.min.js'); ?>

<?php //print_r($news);?>

<form id="modNews" action="<?php echo $this->html()->url('news/edit/'.$news->id().''); ?>" name="news" method="post">
	<!--Titre-->
    <div class="form-group">
    	<label for="title">Titre</label>
    	<input type="text" name="title" class="form-control" value="<?php print($news->title()); ?>">
    </div>
    <!--Contenu-->
	<div class="form-group">
    	<label for="content">Contenu</label>
		<textarea class="ckeditor" name="content" rows="2"><?php print($news->content()); ?></textarea>
	</div>
	<!--Date début-->
	<div class="form-group">
        <label for="start_date">De:</label>
        <input type="date" name="start_date" class="form-control" placeholder="" value="<?php echo $news->start_date(); ?>">
    </div>
    <!--Date de fin-->
    <div class="form-group">
    	<label for="end_date">A:</label>
        <input type="date" name="end_date" class="form-control" placeholder="" value="<?php echo $news->end_date(); ?>">
    </div>
    <select name="right" class="form-control">
    	<option value="2" <?php if($news->right()=="2"){echo "selected";} ?> >Tout le monde</option>
        <option value="3" <?php if($news->right()=="3"){echo "selected";} ?> >Classe</option>
        <option value="1" <?php if($news->right()=="1"){echo "selected";} ?> >Enseignant</option>            
    </select>
    <div class="form-group">
	    <label for="classe">Classe:</label>
	    <select name="classe" class="form-control">
	    	<option value="<?php echo $news->idx_schoolClasse(); ?>"><?php echo $news->idx_schoolClasse(); ?></option>
	      	<option value="">Aucune</option> 
	      <?php
	        foreach ($classes as $classe) 
	        {
	        	if($classe->id() != $news->idx_schoolClasse())
	        	{
	          		echo('<option value="'.$classe->id().'">'.$classe->id().'</option>');
	        	}
	        } 
	      ?>      
	    </select>
	</div>

</form>