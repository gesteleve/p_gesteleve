<?php
//*********************************************************
// Societe: ETML
// Auteur : Alexis Gonzalez
// Date : 12.05.2014
// But : Fichier permettant l'ajout d'une news
//*********************************************************
// Modifications:
// Date : 
// Auteur : 
// Raison : 
//*********************************************************
?>
<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } ?>
<?php $this->html()->js('../assets/ckeditor/ckeditor.js'); ?>
<?php $this->html()->js('jquery.form.js'); ?>



      
<form role="form" action="<?php echo $this->html()->url('news/add'); ?>" id="news" name="news" method="post">
  <!--Titre-->
  <div class="form-group">
    <label for="title">Titre</label>
    <input type="text" name="title" class="form-control" value="<?php echo isset($_POST['title']) ? $_POST['title'] : null ?>">
  </div>

    <p class="help-block">Example block-level help text here.</p>

    <!--Contenu-->
  <div class="form-group">

    <label for="content">Contenu</label>
    <!--<textarea class="ckeditor" name="summary" rows="3"></textarea>-->
    <textarea class="ckeditor" name="content" rows="2"><?php echo isset($_POST['content']) ? $_POST['content'] : null ?></textarea>
  </div>

  <div class="form-group">
    <label for="start_date">De:</label>
    <input type="date" name="start_date" class="form-control" placeholder="">
  </div>
  <!--Date de fin-->
  <div class="form-group">
    <label for="end_date">A:</label>
    <input type="date" name="end_date" class="form-control" placeholder="">
  </div>

  <div class="form-group">
    <label for="right">Droits</label>
    <select name="right" class="form-control">            
      <option value="2" <?php if(isset($_POST['right']) && $_POST['right']=="2") echo "selected"; ?> >Tout le monde</option>
      <option value="3" <?php if(isset($_POST['right']) && $_POST['right']=="3") echo "selected"; ?> >Classe</option>
      <option value="1" <?php if(isset($_POST['right']) && $_POST['right']=="1") echo "selected"; ?> >Enseignant</option>
    </select>
  </div>

  <div class="form-group">
    <label for="classe">Classe:</label>
    <select name="classe" class="form-control">
      <option value="">Aucune</option> 
      <?php
        foreach ($classes as $classe) 
        {
          ?><option value="<?php echo $classe->id();?>"><?php echo $classe->id();?></option><?php
        } 
      ?>      
    </select>
  </div>
</form>

      <!--</div>-->

      <!--Footer-->
      <!--<div class="modal-footer">

        <button type="submit" class="btn btn-default" onClick="$('form#news').submit();">Enregistrer</button>
        
      </div>
    </div>
  </div>
</div>-->

<?php

  /*echo $this->html()->modalbox(
    'showBegin',

    'Informations de base',

    '<h1>Ajout news</h1>

    <form role="form" action="" id="news" name="news" method="post">
      <!--Titre-->
      <div class="form-group">
        <label for="title">Titre</label>
        <input type="text" name="title" class="form-control" placeholder="" required>
      </div>

        <p class="help-block">Example block-level help text here.</p>

        <!--Contenu-->
      <div class="form-group">
        <label for="content">Contenu</label>
        <!--<textarea class="ckeditor" name="summary" rows="3"></textarea>-->
        <textarea class="ckeditor" name="content" rows="2"></textarea>
      </div>',

      '<button type="button" class="btn btn-default" data-dismiss="modal" data-target="#showDate">Suivant</button>'
  );*/

?>

<!--Affiche ShowDate lorsque la premiere modal se ferme-->
<!--<script type="text/javascript">
  $('#showBegin').on('hide.bs.modal', function (e) {
    $(document).ready(function () {
      $('#showDate').modal('show');
    });
  });
</script>-->

<?php

  /*echo $this->html()->modalbox(
    'showDate',

    'Afficher la news',

    '<!--Contenu-->
      
      <div class="form-group">
        <label for="start_date">De:</label>
        <input type="date" name="start_date" class="form-control" placeholder="">
      </div>
      <!--Date de fin-->
      <div class="form-group">
        <label for="end_date">A:</label>
        <input type="date" name="end_date" class="form-control" placeholder="">
      </div>',

      '<button type="button" class="btn btn-default" data-dismiss="modal" data-target="#showRight">Suivant</button>'
  );*/

?>
  
<!--Affiche ShowDate lorsque la premiere modal se ferme-->
<!--<script type="text/javascript">
  $('#showDate').on('hide.bs.modal', function (e) {
    $(document).ready(function () {
      $('#showRight').modal('show');
    });
  });
</script>-->

<?php

  /*echo $this->html()->modalbox(
    'showRight',

    'Droits de la news',

    '<!--Contenu-->
      
      <div class="radio">
        <label>
          <input type="radio" name="right" id="everybody" value="1" checked>
          Tout le monde
        </label>
      </div>
      <div class="radio">
        <label>
          <input type="radio" name="right" id="schoolClass" value="2">
          Classe
        </label>
      </div>
      <div class="radio">
        <label>
          <input type="radio" name="right" id="Teacher" value="0">
          Enseignant
        </label>
      </div>',

    '<button type="submit" class="btn btn-default" onClick="$("form#news").submit();">Enregistrer</button>'
  );*/

?>


