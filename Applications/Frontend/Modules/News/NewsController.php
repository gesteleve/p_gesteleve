<?php
//*********************************************************
// Societe: ETML
// Auteur : Alexis Gonzalez
// Date : 26.05.2014
// But : Contrôleur du module news.
//*********************************************************
// Modifications:
// Date : 
// Auteur : 
// Raison : 
//*********************************************************
// Date :
// Auteur :
// Raison :
//*********************************************************

namespace Applications\Frontend\Modules\News;

// Implémentation des fichier nécessaire au contrôleur
use Applications\Entities\News;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class NewsController extends BackController
{
    // *******************************************************************
    // Nom : executeArchive
    // But : Envoie les news archivée à la page
    // Retour: -
    // Param.: HTTPRequest $request: contient les données passée en post 
    // *******************************************************************
    function executeArchive()
    {
        // Déclaration du manager "News"
        $manager = $this->managers->getManagerOf('News');

        //Vérifie si la personne est authentifiée
        if($this->app->user()->isAuthenticated())            
        {
            //Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');

            //Récupère le nom d'utilisateur logé
            $idUser = $this->app->user()->getAttribute('user')->id();

            // Affichage des news pour un prof ou son équivalent
            if (@$right[NEWS] & ADD) 
            {
                // Ajoute la classe de l'enseignant à la news envoyée
                $tab_NewsArchived = $manager->getNewsArchived();

                // Envoie la variable à la page
                $this->page->addVar('listNews', $tab_NewsArchived);

            }
            
        }
        else
        {           
            // Si une personne n'est pas logée et cherche à ajouter une news, elle sera automatiquement redirigée sur la page d'accueil
            $this->app->httpResponse()->redirect('../');            
        }
    }

    
    // *******************************************************************
    // Nom : executeIndex
    // But : Vérifie et envoie les news pour qu'elles soient affichées dans la page d'index
    // Retour: -
    // Param.: HTTPRequest $request: contient les données passée en post 
    // *******************************************************************
    function executeIndex(HTTPRequest $request) 
    {
        // Déclaration du manager "News"
        $manager = $this->managers->getManagerOf('News');

        //////////////////////////////////////////////////////////////////////////////////
        // Met à jour les news qui doivent être archivée.
        // Récupère toutes les news et vérifie si leurs périodes d'affichage est révolue
        //////////////////////////////////////////////////////////////////////////////////

        $testNews = $manager->getList();

        //Date d'aujourd'hui
        $today = date("Y-m-d");

        foreach ($testNews as $newsArchive) 
        {
            if($newsArchive->end_date() < $today)
            {
                $success = $manager->archiveNews($newsArchive->id());
            }
        }

        //////////////////////////////////////////////////////////////////////////////////
        // Fin de la  mise à jour
        //////////////////////////////////////////////////////////////////////////////////

        //Vérifie si la personne est authentifiée
        if($this->app->user()->isAuthenticated())            
        {

            //Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');

            //Récupère le nom d'utilisateur logé
            $idUser = $this->app->user()->getAttribute('user')->id();

            // Affichage des news pour un prof ou son équivalent
            if (@$right[NEWS] & ADD) 
            {   
                // Liste des news qu'un enseignant peut voir -> news des enseigants et news de tous le monde -> droits 1 et 2
                $tab_newsTeacher = $manager->getListNewsTeacher($idUser);

                // Liste des news qu'un enseignant peut voir -> news des enseigants et news de tous le monde -> droits 1 et 2
                $tab_newsTeacherFilterDate = $manager->getListNewsTeacherByDate($idUser);

                // Liste des news qu'un enseignant peut voir -> news des enseigants et news de tous le monde -> droits 1 et 2
                $tab_newsTeacherFilterAutor = $manager->getListNewsTeacherByAutor($idUser);

                if($request->postData('filterAutor'))
                {
                    // Envoie de la variable contenant les news à afficher
                    $this->page->addVar('listNews', $tab_newsTeacherFilterAutor);
                    
                }
                elseif ($request->postData('filterDate')) 
                {

                    // Envoie de la variable contenant les news à afficher
                    $this->page->addVar('listNews', $tab_newsTeacherFilterDate);

                }
                elseif ($request->postData('filterNormal')) 
                {

                    // Envoie de la variable contenant les news à afficher
                    $this->page->addVar('listNews', $tab_newsTeacher);   

                } 
                else
                {
                    // Envoie de la variable contenant les news à afficher choix, si aucun filtre n'est choisi
                    $this->page->addVar('listNews', $tab_newsTeacher);
                }
            }

            //Affichage des news pour un élève ou son équivalent
            else if (@$right[NEWS] & VIEW_ALL) 
            {            

                // Liste des news de la classe de l'élève
                $tab_TeacherIds = $manager->getClassTeacher($idUser);

                foreach ($tab_TeacherIds as $tab_TeacherId) 
                {
                    $idUser = $tab_TeacherId->colleague_id();
                }

                $tab_newsStudent = $manager->getListNewsStudent($idUser);

                $tab_newsStudentDate = $manager->getListNewsStudentDate($idUser);


                $tab_newsStudentAutor = $manager->getListNewsStudentAutor($idUser);


                if($request->postData('filterAutor'))
                {
                    // Envoie de la variable contenant les news à afficher
                    $this->page->addVar('listNews', $tab_newsStudentAutor);
                    
                }
                elseif ($request->postData('filterDate')) 
                {

                    // Envoie de la variable contenant les news à afficher
                    $this->page->addVar('listNews', $tab_newsStudentDate);

                }
                elseif ($request->postData('filterNormal')) 
                {

                    // Envoie de la variable contenant les news à afficher
                    $this->page->addVar('listNews', $tab_newsStudent);   

                } 
                else
                {
                    // Envoie de la variable contenant les news à afficher choix, si aucun filtre n'est choisi
                    $this->page->addVar('listNews', $tab_newsStudent);
                }
                    
            }
                          

            
        }
        else
        {   
            // Si aucun utilisateur est logé, les news du groupe "tous le monde" s'affiche

            //Récupère les news avec droits -> 1
            $news = $manager->getListNewsEveryBody();

            // Liste des news triée par auteur
            $tab_newsFilterAutor = $manager->getListNewsEveryBodyFilterAutor();

            // Liste des news triée par auteur
            $tab_newsFilterDate = $manager->getListNewsEveryBodyFilterDate(); 


            if($request->postData('filterAutor'))
            {
                // Envoie de la variable contenant les news à afficher
                $this->page->addVar('listNews', $tab_newsFilterAutor);
                
            }
            elseif ($request->postData('filterDate')) 
            {

                // Envoie de la variable contenant les news à afficher
                $this->page->addVar('listNews', $tab_newsFilterDate);

            }
            elseif ($request->postData('filterNormal')) 
            {

                // Envoie de la variable contenant les news à afficher
                $this->page->addVar('listNews', $news);   

            } 
            else
            {
                // Envoie de la variable contenant les news à afficher choix, si aucun filtre n'est choisi
                $this->page->addVar('listNews', $news);
            } 
        }        
    }


    // *******************************************************************************************
    // Nom : executeView
    // But : Envoie les informations d'une news pour qu'elle soie affcihée dans la page "view"
    // Retour: -
    // Param.: HTTPRequest $request: contient les données passée en post dans la page index
    // *******************************************************************************************
    function executeView(HTTPRequest $request) 
    {    
        //Récupère les droits de l'utilisateur
        $right = $this->app->user()->getAttribute('right');

        // Récupère le manager de news
        $manager = $this->managers->getManagerOf('News');

        // Récupère la classe sélectionnée en fonction de son id
        $news = $manager->getUnique($request->getData('id'));

        
        if($news->rights() == "2")
        {
            
            //Ajout les variables à la page view
            $this->page->addVar('id', $request->getData('id'));
            $this->page->addVar('news', $news);

        }
        elseif($news->rights() == "1")
        {
            if(@$right[NEWS] & ADD)
            {
                //Ajout les variables à la page view
                $this->page->addVar('id', $request->getData('id'));
                $this->page->addVar('news', $news);
            }
            else
            {
                // Message d'erreur si l'utilisateurs n'a pas le droit de voir la news
                $this->app->user()->setFlash('Vous n\'avez pas accès à cette news', 'error');
                $this->app->httpResponse()->redirect($request->httpReferer());
            }
        }
        elseif ($news->rights() == "3") 
        {
            if(@$right[NEWS] & ADD)
            {
                //Ajout les variables à la page view
                $this->page->addVar('id', $request->getData('id'));
                $this->page->addVar('news', $news);
            }
            else
            {
                // Message d'erreur si l'utilisateurs n'a pas le droit de voir la news
                $this->app->user()->setFlash('Vous n\'avez pas accès à cette news', 'error');
                $this->app->httpResponse()->redirect($request->httpReferer());
            }
        }        
    }


    // *******************************************************************************************
    // Nom : executeAdd
    // But : Ajoute une news dans la BD
    // Retour: -
    // Param.: HTTPRequest $request: contient la news à ajouter
    // *******************************************************************************************
    function executeAdd(HTTPRequest $request) {        

        //Vérifie si la personne est authentifiée
        if($this->app->user()->isAuthenticated())            
        {

            //Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');

            //Récupère le nom d'utilisateur logé
            $idUser = $this->app->user()->getAttribute('user')->id();

            // Vérifie si la personne a les droits pour ajouter une news
            if (@$right[NEWS] & ADD) 
            {

                //Initialise le manager de News
                $manager = $this->managers->getManagerOf('News'); 

                // Vérifie si les données concernant la news ont bien été reçues
                if($request->postExists('title','content','start_date','end_date','right'))
                {

                    // Verifie si le poste reçoit des données vide
                    if (!$request->postsEmpty(array('title','content','start_date','end_date','right'))) 
                    {                         

                        //Vérifie la correspondance des dates d'affcihage (une date de fin d'affichage ne peut pas être avant la date d'affichage)
                        if ($request->postData('start_date') < $request->postData('end_date')) 
                        { 

                            // Vérifie si c'est une news attribuées à un classe
                            if ($request->postData('right') == 3)
                            {

                                // Ajoute la classe de l'enseignant à la news envoyée
                                $ResultClass = $manager->getUniqueClass($idUser);
                                $teacherClass = $ResultClass->id();
                                
                            }
                            else
                            {
                                // Si c'est un enews autre que "classe", aucune données ne sera envoyée dans le champ "class"
                                $teacherClass = '';
                            }  

                            // Création du tableau contenant les données à envoyer à la BD
                            $news = new News(array(                            
                                'title' => $request->postData('title'),
                                'content' => $request->postData('content'),
                                'start_date' => $request->postData('start_date'),
                                'end_date' => $request->postData('end_date'),
                                'right' => $request->postData('right'),
                                'colleague' => $idUser,
                                'classe' => $teacherClass                                   
                                ));

                            //Envoie le tableau dans la fonction d'ajout de news
                            $success = $manager->add($news);

                            // Vérifie si la news à bien été ajoutée
                            if ($success) 
                            {
                                // Message de réussite
                                $this->app->user()->setFlash('La news a été correctement ajoutée', 'success');
                            } 
                            else 
                            {
                                // Message d'echec
                                $this->app->user()->setFlash('Une erreur c\'est produite lors de l\'ajout de la news', 'error');
                            }                                    
                            
                        }
                        else
                        {

                            // Message d'erreur si les dates sont incorrects
                            $this->app->user()->setFlash('Il y a une erreur avec les dates', 'error');
                            $this->app->httpResponse()->redirect($request->httpReferer());

                        }                              
                        
                    }
                    else
                    {
                        // Messsage d'erreur si des champs sont incorrects
                        $this->app->user()->setFlash('<li>Tous les champs obligatoires ne sont pas renseignés.</li>', 'error');
                    }                
                    
                    // Renvoie à la page index
                    $this->app->httpResponse()->redirect($request->httpReferer());

                }
            }
            else
            {

                // Message d'erreur
                $this->app->user()->setFlash('Vous n\'avez pas le droit d\'ajouter de news', 'error');

                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());

            }
        }
        else//end if vérif
        {
            // Si une personne n'est pas logée et cherche à ajouter une news, elle sera automatiquement redirigée sur la page d'accueil
            $this->app->httpResponse()->redirect('../');                        
        }        
    }


    // *******************************************************************************************
    // Nom : executeEdit
    // But : Vérification du formulaire de modification de news et envoie des informations de modification à la requêtes de modif
    // Retour: -
    // Param.: HTTPRequest $request: contient les données de la news à modifier
    // *******************************************************************************************
    function executeEdit(HTTPRequest $request) {
        
        // Vérifie si l'utilisateur est logé
        if($this->app->user()->isAuthenticated())
        {

           //Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');

            //Récupère le nom d'utilisateur logé
            $idUser = $this->app->user()->getAttribute('user')->id();

            // Vérifie si l'utilisateur à les droits de modification
            if (@$right[NEWS] & MODIFY) 
            {

                // Initialise le manager des news
                $manager = $this->managers->getManagerOf('News');

                // Récupère la news à modifier
                $news = $manager->getUnique($request->getData('id'));

                //Envoie l'id de la news à modifier
                $this->page->addVar('id', $request->getData('id'));

                //Envoie les informations de la news
                $this->page->addVar('news', $news);

                // Vérifie si les données concernant la news ont bien été reçues
                if($request->postExists('title','content','start_date','end_date','right'))
                {
                    // Verifie si le poste reçoit des données vide
                    if (!$request->postsEmpty(array('title','content','start_date','end_date','right'))) 
                    {                      

                        //Vérifie la correspondance des dates d'affcihage (une date de fin d'affichage ne peut pas être avant la date d'affichage)
                        if ($request->postData('start_date') < $request->postData('end_date')) 
                        { 
                            // Vérifie si c'est une news attribuées à un classe
                            if ($request->postData('right') == 3)
                            {

                                // Ajoute la classe de l'enseignant à la news envoyée
                                $ResultClass = $manager->getUniqueClass($idUser);
                                $teacherClass = $ResultClass->id();
                                
                            }
                            else
                            {
                                // Si c'est un enews autre que "classe", aucune données ne sera envoyée dans le champ "class"
                                $teacherClass = '';
                            }  

                            // Création du tableau contenant les données à envoyer à la BD
                            $news = new News(array(
                                'id' => $request->getData('id'),                            
                                'title' => $request->postData('title'),
                                'add_date' => $news->add_date(),
                                'content' => $request->postData('content'),
                                'start_date' => $request->postData('start_date'),
                                'end_date' => $request->postData('end_date'),
                                'colleague' => $this->app->user()->getAttribute('user')->id(),
                                'right' => $request->postData('right'),
                                'classe' => $teacherClass
                                ));

                            //Envoie le tableau dans la fonction de modification de news
                            $success = $manager->edit($news);

                            // Vérifie si la news à bien été modifiée
                            if ($success) 
                            {
                                // Message de réussite
                                $this->app->user()->setFlash('La news a été correctement modifiée', 'success');
                            } 
                            else 
                            {
                                // Message d'echec
                                $this->app->user()->setFlash('Une erreur c\'est produite lors de la modification de la news', 'error');
                            }

                            // Renvoie à la page précedente (index)
                            $this->app->httpResponse()->redirect($request->httpReferer());
                        }
                        else
                        {
                            //Message d'erreur concernant les dates
                            $this->app->user()->setFlash('Il y a une erreur avec les dates', 'error');
                            $this->app->httpResponse()->redirect($request->httpReferer());
                        }
                        
                    }
                    else
                    {
                        // Message d'erreur conernant les champs du formulaire
                        $this->app->user()->setFlash('<li>Tous les champs obligatoires ne sont pas renseignés.</li>', 'error');
                    } 

                    // Renvoie à la page précedente (index)
                    $this->app->httpResponse()->redirect($request->httpReferer());

                }
            }
            else
            {

                // Message d'erreur
                $this->app->user()->setFlash('Vous n\'avez pas le droit de modifier de news', 'error');

                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());

            }
        }
        else
        {
            // Redirige l'utilisateur non-logé sur la page d'accueil
            $this->app->httpResponse()->redirect('../');                        
        }  
    }


    // *******************************************************************************************
    // Nom : executeDelete
    // But : Suppression définitive d'une news
    // Retour: -
    // Param.: HTTPRequest $request: contient les données de la news à supprimer
    // *******************************************************************************************
    function executeDelete(HTTPRequest $request) {

        // Vérifie si l'utilisateur est logé
        if($this->app->user()->isAuthenticated())
        {
            // Initilisation de la variable contenant les droits
            $right = $this->app->user()->getAttribute('right');

            // Vréifie si l'utilisateur à les droits de suppression
            if (@$right[NEWS] & DELETE) 
            {

                //Initialisation du manager "News"
                $manager = $this->managers->getManagerOf('News');

                // Récupération de la news
                $news = $manager->getUnique($request->getData('id'));

                // Envoie de la news à supprimer
                $success = $manager->delete($request->getData('id'));

                // Vérifie si la news à bien été supprimée
                if ($success) 
                {
                    // Message de réussite
                    $this->app->user()->setFlash('La news a été correctement supprimée', 'success');
                } 
                else 
                {
                    // Message d'echec
                    $this->app->user()->setFlash('Une erreur c\'est produite lors de la suppression de la news', 'error');
                }
                
                //Redirection sur la page d'index
                $this->app->httpResponse()->redirect($request->httpReferer());
            }
            else
            {
                // Message d'erreur
                $this->app->user()->setFlash('Vous n\'avez pas le droit de supprimer de news', 'error');

                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());

            }
        }
        else
        {
            // Redirection sur la page d'accueil
            $this->app->httpResponse()->redirect('../');                        
        }  
    }

    // *******************************************************************************************
    // Nom : executeDelete
    // But : Archivage d'une news
    // Retour: -
    // Param.: HTTPRequest $request: contient les données de la news à supprimer
    // *******************************************************************************************
    function executeArchiveNews(HTTPRequest $request) {

        // Vérifie si l'utilisateur est logé
        if($this->app->user()->isAuthenticated())
        {
            // Initilisation de la variable contenant les droits
            $right = $this->app->user()->getAttribute('right');

            // Vréifie si l'utilisateur à les droits de suppression
            if (@$right[NEWS] & DELETE) 
            {

                //Initialisation du manager "News"
                $manager = $this->managers->getManagerOf('News');

                // Récupération de la news
                $news = $manager->getUnique($request->getData('id'));

                // Envoie de la news à supprimer
                $success = $manager->archiveNews($request->getData('id'));

                // Vérifie si la news à bien été supprimée
                if ($success) 
                {
                    // Message de réussite
                    $this->app->user()->setFlash('La news a été correctement archivée', 'success');
                } 
                else 
                {
                    // Message d'echec
                    $this->app->user()->setFlash('Une erreur c\'est produite lors de l\'archivage de la news', 'error');
                }
                
                //Redirection sur la page d'index
                $this->app->httpResponse()->redirect($request->httpReferer());
            }
            else
            {
                // Message d'erreur
                $this->app->user()->setFlash('Vous n\'avez pas le droit d\'archiver de news', 'error');

                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());

            }
        }
        else
        {
            // Redirection sur la page d'accueil
            $this->app->httpResponse()->redirect('../');                        
        }  
    }
}
