﻿<script>
$(window).on("hashchange", function () {
    window.scrollTo(window.scrollX, window.scrollY - 50);
});
</script>
<div id="content">
	<div id="navi">
		<ul class="nav nav-list affix-top">
          <li><a href="#suivis"><i class="icon-chevron-right"></i> Suivis</a></li>
          <li><a href="#groupes"><i class="icon-chevron-right"></i> Groupes</a></li>
          <li><a href="#impressions"><i class="icon-chevron-right"></i> Impressions</a></li>
          <li><a href="#filter"><i class="icon-chevron-right"></i> Filtres</a></li>
        </ul>
	</div>
	
	
	<div id="suivis" class="textDiv">
		<h1 class="helpTitle">Suivis</h1>
		<div class="helpContent">
			<h3 class="HelpSubtitle">Ajout d'un suivis</h3>
			<p class="helpText">
				Tous les enseignants ont la possibilité d’ajouter un suivi à un élève. Celui-ci sera visible par le maître de classe et l'élève uniquement.<br><br>
				Pour ce faire, affichez la fiche de l’élève via les menus « Etudiants », « Classes »  ou via la fonction de recherche disponible sur la page d’accueil.<br><br>
				Une fois sur la page de l’élève, cliquez sur le bouton « Ajouter un suivi » dans la section « Suivis » située ver le bas gauche de la page.</p>
				<img id="helpFl" src="img/help/add_follow_a.png">

			<p>
				<br>Une dialogue-box apparaitra et vous demandera de saisir le texte de votre suivi. Une fois le texte saisi, cliquez sur « Ajouter » pour enregistrer le suivi.
			</p>
		</div>
		<div class="endLine"></div>
	</div>
	
	<div id="groupes" class="textDiv">
		<h1 class="helpTitle">Groupes</h1>
		<div class="helpContent">
			<h5>Les groupes permettent aux enseignants de regrouper des élèves de différentes sections et classes.</h5>
			<h3 class="HelpSubtitle">Création d'un groupe</h3>
			<p class="helpText">
				Pour en créer un, rendez-vous dans l’onglet « <a href="groups">Mes Groupes</a> » de la section « Groupes » du menu.<br>
				Une fois sur la page de vous groupes, cliquez sur le bouton « Ajouter un groupe ».<br></p>
				<img id="helpGrA" src="img/help/btn_group_a.png">
			<p>
				<br>Saisissez un nom ainsi qu’une description pour votre groupe. Pour ajouter des élèves au groupe, sélectionnez la section ainsi que la classe de l’élève puis glissez le dans le container de droite. Vous pouvez supprimer un élève du groupe en cliquant sur le bouton « X » à côté de son nom.<br>
			</p>
			<img id="helpGr" src="img/help/add_group.gif">
			<p>
				<br>Vous pouvez rendre votre groupe visible par tous les enseignants en cochant la case « Rendre le groupe public » située en bas à gauche. Cliquez ensuite sur « Ajouter » pour crée le groupe.
			</p>
		</div>
		<div class="endLine"></div>
	</div>
	
	<div id="impressions" class="textDiv">
		<h1 class="helpTitle">Impressions</h1>
		<div class="helpContent">
			<h5>Il est possible d'imprimer les détails d'un élève et la liste des suivis.</h5>
			<h3 class="HelpSubtitle">Impression d'une fiche d'élève</h3>
			<p class="helpText">
				Pour imprimer, commencez en affichant la fiche de l’élève via les menus « Etudiants », « Classes »  ou via la fonction de recherche disponible sur la page d’accueil.</br>
				Sur la page de l'élève il faut se rendre dans la barre d'outils puis cliquez sur « imprimer ».</br></br>
				<img id="helpGrA" src="img/help/print.png"></br></br>
				Une fenêtre s'ouvre. Vous pouvez changer certains paramètres. Une fois cela fait cliquez sur « imprimer ».</br>
				Ensuite vous devriez être en mesure d'imprimer le document en faisant « CTRL + P », ou de l'enregistrer au format pdf à l'aide de « CTRL + S » .
			</p>			
			<h3 class="HelpSubtitle">Impression des suivis d'un élève</h3>
			<p class="helpText">
				Il est également possible de n'imprimer que les suivis de l'élève. Pour cela il sufit de rendre sur la page de l'étudiant et de cliquer sur le bouton « imprimer » qui se situe dans la zone « Suivis »</br></br>
				<img id="helpGrA" src="img/help/print_stu_follow_i.png"></br></br>
				Vous devriez être en mesure d'imprimer le document en faisant « CTRL + P », ou de l'enregistrer au format pdf à l'aide de « CTRL + S ».
			</p>
			<h3 class="HelpSubtitle">Impression de la liste de suivis</h3>
			<p class="helpText">
				Vous devez ouvrir le menu « Suivis » puis cliquer sur « <a href="follow">Mes suivis</a> ». La liste de vos suivis s'affiche.</br>
				Sur la page de vos suivis cliquez sur « Options » puis « Imprimer ».</br></br>
				<img id="helpGrA" src="img/help/print_follow.png"></br></br>
				Vous devriez être en mesure d'imprimer le document en faisant « CTRL + P », ou de l'enregistrer au format pdf à l'aide de « CTRL + S ».
			</p>
		</div>
		<div class="endLine"></div>
	</div>
	
	<div id="filter" class="textDiv">
		<h1 class="helpTitle">Filtres</h1>
		<div class="helpContent">
			<h5>Il est possible de filtrer les résultats affichés.</h5>
			<h3 class="HelpSubtitle">Utilisation des filtres</h3>
			<p class="helpText">
				Les pages prises en compte sont les suivantes: </br>
					<ul>
						<li>« Suivis », « <a href="follow">Mes suivis</a> »</li>
						<li>« Suivis », « <a href="follow/class">Tous les suivis</a> »</li>
						<li>« Groupes », « <a href="groups/public">Liste groupes</a> »</li>
					</ul>
				Par défaut les paramètres du filtre sont cachés. Il faut commencer par afficher les filtres. Pour cela cliquer sur le bouton « Filtres <i class="icon-filter"></i> »</br></br>
				<img id="helpGrA" src="img/help/filter_i.png"></br></br>
				Les filtres disponibles s'affichent. Des listes déroulantes vous permettent de ne garder qu'une classe ou élève en particulier. Dans le cas présent tous les suivis des élèves de min3 seront affichés.</br></br>
				<img id="helpGrA" src="img/help/filtres_act.png">
			</p>
		</div>
	</div>
	
	
</div>