<?php

namespace Applications\Frontend\Modules\Absence;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class AbsenceController extends BackController
{
    public function executeIndex(HTTPRequest $request) {
        $this->app->user()->restricted($request);
        $manager = $this->managers->getManagerOf('Absence'); // Get manager (Database)

        $error = false; // Error code
        if (!empty($request->getData('class'))){
            $class = $request->getData('class'); // Get class
            $students = $manager->getStudentsFromClass($class); // Get students from the class $class
            $absences = $manager->getDailyAbsence($class, date('Y-m-d'));
            $error = empty($students[0]) ? 'NO_STUDENTS' : $error; // Set error code if empty array
        }else{
            $error = 'NO_CLASS';
        }

        if (!$error){
            $this->page()->addVar('students', $students);
            $this->page()->addVar('absences', $absences);
            $this->page()->addVar('class', $class);
        }
        $this->page()->addVar('error', $error);
    }

    public function executeValidate(HTTPRequest $request){
        $this->app->user()->restricted($request);
        $manager = $this->managers->getManagerOf('Absence'); // Get manager (Database)

        $error = false; // Error code
        if (!empty($request->getData('class'))){
            $class = $request->getData('class'); // Get class
            $students = $manager->getStudentsFromClass($class); // Get students from the class $class
            $error = empty($students[0]) ? 'NO_STUDENTS' : $error; // Set error code if empty array
        }else{
            $error = 'NO_CLASS';
        }

        if (!$error){
            if(!empty($_POST)){
                $student = htmlentities($_POST['student'], ENT_QUOTES);
                $beginDateTime = new \DateTime($_POST['dateBegin'].$_POST['timeBegin']);
                $endDateTime = new \DateTime($_POST['dateEnd'].$_POST['timeEnd']);
                $summary = htmlentities($_POST['summary'], ENT_QUOTES);
                $manager->addJustification($student, $beginDateTime->format('Y-m-d H:i:s'), $endDateTime->format('Y-m-d H:i:s'), $summary);
                header('Location:'.$request->requestURI());
                die();
            }

            // Numéro de semaine
            if(!isset($_GET['w']))
                $week =  date("W");
            else
                $week = addslashes($_GET['w']);

            // Année
            if(!isset($_GET['y']))
                $year =  date("Y");
            else
                $year = addslashes($_GET['y']);

            if(isset($_GET['date'])){
                $week = date('W', strtotime($_GET['date']));
                $year = date('Y', strtotime($_GET['date']));
            }

            if($week <= 0){
                $week = 52;
                $year--;
            }
            if($week > 52){
                $week = 1;
                $year++;
            }

            // Récupération des justification pour une semaine
            $justs = $manager->getAllJustificatifsByClassWeekYear($class,$week, $year);
            //var_dump($justs);
            //die();

            // Absences
            $abs = $manager->getAllAbsenceFromClassWeekYear($class, $week, $year);
            //var_dump($abs);
            //die();

            //Schedule
            $sch = $manager->getAllSchedules();

            $valid = $this->checkJustifications($justs, $abs, $sch);

            $this->page()->addVar('valid', $valid);
            $this->page()->addVar('justs', $justs);
            $this->page()->addVar('absences', $abs);
            $this->page()->addVar('students', $students);
            $this->page()->addVar('class', $class);
            $this->page()->addVar('week', $week);
            $this->page()->addVar('year', $year);
            $this->page()->addVar('schedules', $sch);
        }
        $this->page()->addVar('error', $error);
    }

    private function checkJustifications($justs, $absences, $schedule){
        $valid = array();
        foreach($absences as $absence){
            foreach($justs as $just){
                if($absence['absPerNumber'] >= $this->getPeriodFromTime($just['jusDateBegin'], $schedule) && $absence['absPerNumber'] <= $this->getPeriodFromTime($just['jusDateEnd'], $schedule) && $absence['fkStudent'] == $just['fkStudent']){
                    array_push($valid, $absence['idAbsence']);
                }
            }
        }
        return $valid;
    }

    private function getPeriodFromTime($time, $schedule){
        foreach($schedule as $hour){
            $time = date('H:i', strtotime($time));
            $sch = date('H:i', strtotime($hour['schHour']));
            if($time >= $sch && $time <= (new \DateTime($sch))->modify('+45 minutes')->format('H:i')){
                return $hour['schPeriod'];
            }
        }
        return false;
    }

    public function executeSet(HTTPRequest $request)
    {
        if (!(empty($request->getData('class')) || empty($_POST))) {
            $arrayAbsenceGet = $_POST;
            $arrayAbsenceParse = array();

            // Absence type
            $absenceType = $arrayAbsenceGet['absence_type'];

            // Sort absences
            // Pour chaque absence
            for ($i = 0; $i < count($arrayAbsenceGet['absences']); $i++) {
                // sépare l'élève de la période
                $arrayTemp = explode('-', $arrayAbsenceGet['absences'][$i]);
                $tempD = 0;

                /* // Cette partie permet de regrouper les absences sous un élèves, à supprimer pour plus de liberté dans la modification des absences
                // Recherche dans le tableau des absences trié si l'élève est déjà dedans
                for ($j = 0; $j < count($arrayAbsenceParse); $j++)
                {
                    if (isset($arrayAbsenceParse[$j]) && $arrayTemp[0] == $arrayAbsenceParse[$j][0]) {
                        // si oui, l'ajouter et trier le tableau
                        $arrayAbsenceParse[$j][1][count($arrayAbsenceParse[$j][1])] = $arrayTemp[1];
                        asort($arrayAbsenceParse[$j][1]);
                        $tempD = 1;
                    }
                }*/

                if ($tempD == 0) {
                    // Ajout les tableaux à arrayAbsenceParse
                    $arrayAbsenceParse[$i][0] = $arrayTemp[0];
                    $arrayAbsenceParse[$i][1][0] = $arrayTemp[1];
                }
            }

            //$this->page()->addVar('array', $arrayAbsenceGet); //DEBUG
            //$this->page()->addVar('arrayAbsenceParse', $arrayAbsenceParse); //DEBUG

            // Supprimer les doublons
            $manager = $this->managers->getManagerOf('Absence'); // Get manager (Database)
            $arrayDailyAbsence = $manager->getDailyAbsence($request->getData('class'), date('Y-m-d'));


            foreach ($arrayAbsenceParse as $studentAbsence) {
                $isSet = false;
                foreach ($arrayDailyAbsence as $dailyAbsence) {
                    foreach ($studentAbsence[1] as $absencePeriode) {
                        if ($dailyAbsence['fkStudent'] == $studentAbsence[0] && $dailyAbsence['absPerNumber'] == $absencePeriode)
                        {
                            if($absenceType == 0) {
                                $manager->deleteAbsenceType($dailyAbsence['idAbsence']);
                            } else {
                                $manager->modifyAbsenceType($dailyAbsence['idAbsence'], $absenceType);
                            }
                            $isSet = true;
                        }
                    }
                }

                if (!$isSet && $absenceType != 0){
                    $manager->setAbsence($studentAbsence[0], $absenceType, date('Y-m-d'), $studentAbsence[1]);
                }
            }

            $this->app->httpResponse()->redirect(WWW_ROOT.'absence/class/'.$_GET['class']);
        }
    }

}