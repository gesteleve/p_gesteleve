<?php

$weekDays = [
    'Lundi',
    'Mardi',
    'Mercredi',
    'Jeudi',
    'Vendredi'
];

$icons = [
    1 => '<i class="fa fa-briefcase"></i>',
    2 => '<i class="fa fa-pencil"></i>',
    3 => '<i class="fa fa-sign-out"></i>',
    4 => '<i class="fa fa-clock-o"></i>'
];
?>
<?php if (!$error): ?>
    <!-- Formulaire d'ajout d'une justification -->
    <div class="modal fade" id="justModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Nouvelle justification</h4>
                    </div>
                    <div class="modal-body">
                            <div style="text-align: center">
                                <label for="student">Élève</label>
                                <select name="student" id="student" class="form-control">
                                    <?php foreach ($students as $student): ?>
                                        <option value="<?= $student['id']; ?>"><?= $student['first_name'] . ' ' . $student['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="dateBegin">Absent du</label>
                                <input type="date" name="dateBegin" id="dateBegin" class="form-control" required>
                                <label for="timeBegin">à</label>
                                <input type="time" name="timeBegin" id="timeBegin" class="form-control" required>
                                <label for="dateEnd">jusqu'au</label>
                                <input type="date" name="dateEnd" id="dateEnd" class="form-control" required>
                                <label for="timeEnd">à</label>
                                <input type="time" name="timeEnd" id="timeEnd" class="form-control" required>
                                <label for="summary">Motif</label>
                                <textarea name="summary" id="summary" rows="15">
                            </textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Début de la page -->
    <h1>
        Validation des absences de la <?= $class ?>
        <button type="button" class="btn btn-info btn-large" data-toggle="modal" data-target="#justModal">
            <i class="fa fa-plus"></i> Ajouter une justification
        </button>
    </h1>
    <table class="table table-bordered" style="overflow: scroll">
        <tr>
            <th style="text-align: center; position: relative;" colspan="7">
                <span style="position: absolute;left: 15px;top: 14px;"><?= $year; ?></span>
                <a href="#" onclick="window.location.replace('/absence/<?= $class; ?>/validate?w=<?= ($week-1). '&y=' . $year; ?>');" class="btn btn-primary"><i class="fa fa-chevron-left"></i></a>
                <span style="cursor: pointer;margin: 0 20px;" onclick="displayWeekForm(this);">Semaine N° <?= $week; ?></span>
                <input type="date" id="weekDate" style="margin: 0 20px; display: none;" onchange="window.location.replace('/absence/<?= $class; ?>/validate?date=' + this.value);">
                <a href="#" onclick="window.location.replace('/absence/<?= $class; ?>/validate?w=<?= ($week+1). '&y=' . $year; ?>');" class="btn btn-primary"><i class="fa fa-chevron-right"></i></a>
            </th>
        </tr>
        <tr>
            <th>Élèves \ Jour</th>
            <?php foreach ($weekDays as $day): ?>
                <th><?= $day; ?></th>
            <?php endforeach; ?>
        </tr>

        <?php foreach ($students as $student): ?>
            <tr>
                <td><?= $student['first_name']." ".$student['name']; ?></td>
                <?php for ($i = 1; $i < count($weekDays) + 1; $i++): ?>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <?php
                                global $style;
                                global $icon;
                                foreach($schedules as $schedule): ?>
                                    <?php
                                    foreach($absences as $absence){
                                        if($absence['absPerNumber'] == $schedule['schPeriod'] && $absence['id'] == $student['id'] && date('N', strtotime($absence['absDate'])) == $i) {
                                            if(in_array($absence['idAbsence'], $valid)){
                                                $style = "style=\"background-color: green\"";
                                            }else{
                                                $style = "style=\"background-color: red\"";
                                            }
                                            $icon = $icons[$absence['absType']];
                                            break;
                                        }else{
                                            $style = null;
                                            $icon = null;
                                        }
                                    }
                                    ?>
                                    <td class="absence_case" title="<?= $schedule['schPeriod']; ?>" <?= $style; ?>>
                                        <?= $icon; ?>
                                    </td>
                                    <?php
                                endforeach; ?>
                            </tr>
                        </table>
                    </td>
                <?php endfor; ?>
            </tr>
        <?php endforeach; ?>
    </table>

    <div class="span12 ml0 student-summary small-top">
        <div class="widget-header">
            <i class="fa fa-paper-plane"></i>
            <h5>Justificatif(s) <span class="badge"><?= !empty($justs) ? count($justs) : '0'; ?></span></h5>
        </div>
        <?php if(!empty($justs)): ?>
            <div class="widget-body">
                <table class="table table-striped TF">
                    <thead>
                        <th>Nom de l'élève</th>
                        <th>Absent du...</th>
                        <th>Absent jusqu'au...</th>
                        <th>Motif</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        <?php foreach ($justs as $just): ?>
                            <tr>
                                <td><?= $just['first_name'] . ' ' . $just['name']; ?></td>
                                <td><?= date('d.m.Y H:i', strtotime($just['jusDateBegin'])); ?></td>
                                <td><?= date('d.m.Y H:i', strtotime($just['jusDateEnd'])); ?></td>
                                <td><?= substr($just['jusSummary'], 0, 100); ?></td>
                                <td>
                                    <a href="#" class="btn btn-warning"><i class="fa fa-edit"></i> Modifier</a> <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i> Supprimer</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div class="summary_box">
                Pas de justificatif pour cette semaine
            </div>
        <?php endif; ?>
    </div>

<?php
// Affichage des erreurs
else: ?>
    <h1>Absence</h1>
    <?php
    if ($error == 'NO_CLASS'){
        echo '<h3>Aucune classe indiqué</h3>'; // TODO page d'erreur/d'aide
    } else if ($error = 'NO_STUDENTS'){
        echo '<h3>Classe inconnue</h3>';
    } else {
        echo '<h3>Erreur inconnue</h3>';
    }

    ?>
<?php endif; ?>

<script>
    function displayWeekForm(span){
        span.style.display = "none";
        document.getElementById('weekDate').style.display = "inline-block";
        document.getElementById('weekDate').focus();
    }
</script>
