<?php
/*
echo "class : ".$class."\n";
echo "<pre>";
if (!$error){
    print_r($students);
}else{
    echo $error;
}
echo "</pre>";
die();*/
$periodesNB = 11;
?>
<?php if (!$error): ?>
    <h1>Absence <?= $class ?> - <?= date('d.m.Y') ?></h1>
    <form method="post" action="<?= WWW_ROOT."absence/class/set/".$class ?>">
        <table class="table table-bordered">
            <tr>
                <th>Élèves \ Périodes</th>
                <?php for ($i = 1; $i < $periodesNB + 1; $i++): ?>
                    <th>P<?=$i ?></th>
                <?php endfor; ?>
            </tr>

            <?php foreach ($students as $student): ?>
                <tr>
                    <td><?= $student['first_name']." ".$student['name']; ?></td>
                    <?php for ($i = 1; $i < $periodesNB + 1; $i++): ?>
                        <?php
                        $redBox = true;
                            foreach($absences as $studentAbsence){
                                if ($studentAbsence['fkStudent'] == $student['id'] && $studentAbsence['absPerNumber'] == $i){
                                    $redBox = false;
                                    ?>
                                    <td>
                                        <input style="margin: 0px; height: 100%; width: 50%;" type="checkbox" name="absences[]" value="<?= $student['id'] . '-' . $i; ?>">
                                        <?php
                                        if ($studentAbsence['absType'] == 1) {
                                            echo '<i style="margin-left: 10%; color: #8b4513;" class="fa fa-briefcase"></i>';
                                        } else if ($studentAbsence['absType'] == 2) {
                                            echo '<i style="margin-left: 10%; color: #ff8c23;" class="fa fa-pencil"></i>';
                                        } else if ($studentAbsence['absType'] == 3) {
                                            echo '<i style="margin-left: 10%; color: #ff0000;" class="fa fa-sign-out"></i>';
                                        } else if ($studentAbsence['absType'] == 4) {
                                            echo '<i style="margin-left: 10%; color: #00008b;" class="fa fa-clock-o"></i>';
                                        }
                                        ?>
                                    </td>
                                    <?php
                                }
                            }
                        if ($redBox){
                            ?>
                            <td><input style="margin: 0px; height: 100%; width: 50%;" type="checkbox" name="absences[]" value="<?= $student['id'] . '-' . $i; ?>"></td>
                            <?php
                        }
                        ?>
                    <?php endfor; ?>
                </tr>
            <?php endforeach; ?>
        </table>

        <input type="radio" name="absence_type" id="1" value="1" checked> <label for="1" style="display: inline;"><i class="fa fa-briefcase"></i> Absence pratique</label><br>
        <input type="radio" name="absence_type" id="2" value="2"> <label for="2" style="display: inline;"><i class="fa fa-pencil"></i> Absence théorique</label><br>
        <input type="radio" name="absence_type" id="3" value="3"> <label for="3" style="display: inline;"><i class="fa fa-sign-out"></i> Mise à la porte</label><br>
        <input type="radio" name="absence_type" id="4" value="4"> <label for="4" style="display: inline;"><i class="fa fa-clock-o"></i> Arrivée tardive</label><br><br>
        <input type="radio" name="absence_type" id="0" value="0"> <label for="0" style="display: inline;"><i class="fa fa-times"></i> Supprimer l'absence</label><br><br>

        <!--<a href="#"  class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Absence théorique</a> <a href="#" class="btn btn-success"><i class="fa fa-briefcase" aria-hidden="true"></i> Absence pratique</a> <a href="#" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> Mise à la porte</a> <a href="#" class="btn btn-warning"><i class="fa fa-clock-o" aria-hidden="true"></i> Arrivée tardive</a>-->
        <button type="submit" class="btn btn-primary">Mettre à jour</button>
    </form>
<?php else: ?>
    <h1>Absence</h1>
    <?php
    if ($error == 'NO_CLASS'){
        echo '<h3>Aucune classe indiqué</h3>'; // TODO page d'erreur/d'aide
    } else if ($error = 'NO_STUDENTS'){
        echo '<h3>Classe inconnue</h3>';
    } else {
        echo '<h3>Erreur inconnue</h3>';
    }

    ?>
<?php endif; ?>