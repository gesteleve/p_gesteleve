<?php
/**
 * Created by PhpStorm.
 * User: lengachelo
 * Date: 05.10.2016
 * Time: 14:29
 */

namespace Applications\Frontend\Modules\Remedial;

use Applications\Entities\Student;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class RemedialController extends BackController
{
    public function executeView(HTTPRequest $request) {

        $remedialManager = $this->managers->getManagerOf('Remedial');

        $idStudent = $request->getData('student');

        $student = $remedialManager->getStudent($idStudent); // Get manager (Database)

        $allRemdial = $remedialManager->getAllRemedials($idStudent);

        $this->page->addVar('remedials', $allRemdial);
        $this->page->addVar('student', $student);
        $this->page->addVar('title', 'Rattrapages');
    }

    public function executeDelete(HTTPRequest $request) {

        $remedialManager = $this->managers->getManagerOf('Remedial');
        $idRemedial = $request->getData('id');

        $remedialManager->deleteRemedial($idRemedial);

        header('Location: ../../remedial/beguinju');
    }


    public function executeAdd(HTTPRequest $request) {

        $remedial = $_POST;

        $remedialManager = $this->managers->getManagerOf('Remedial');

        $idStudent = $request->getData('student');
        $remedialManager->addRemedial($remedial['date'] ,$remedial['type'], $remedial['period'], $idStudent);
        header('Location: ../../remedial/beguinju');

    }

    public function executeViewModify(HTTPRequest $request) {

        $remedialManager = $this->managers->getManagerOf('Remedial');

        $idRemedial = $request->getData('id');

        $remedial = $remedialManager->getRemedialById($idRemedial);

        $student = $remedialManager->getStudent($remedial['fkStudent']);

        $this->page->addVar('student', $student);
        $this->page->addVar('remedial', $remedial);

    }

    public function executeModify(HTTPRequest $request) {

        $remedialManager = $this->managers->getManagerOf('Remedial');

        $idRemedial = $request->getData('id');

        $remedial = $remedialManager->getRemedialById($idRemedial);

        $remedialManager->modifyRemedial($idRemedial, $_POST['date'], $_POST['type'], $_POST['period']);


        header('Location: ../../remedial/'.$remedial['fkStudent'].'');


    }
}