<?php
//*********************************************************
// Societe: ETML
// Auteur : Lengacher Loïc
// Date : 05.10.16
// But : Afficher les remediations.
//*********************************************************
?>
<?php //$this->html()->js('isotope.min.js');?>
<?php //$this->html()->js('jquery.infinitescroll.min.js'); ?>

<?php
echo '<h1>Modification d\'un rattrapage de '.$student->first_name().' '.$student->name().'</h1>';
?>


<div class="mt30">
    <div id="container">
        <div id="student-picture" class="span3 ml0">
            <?php
            $photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';

            if (file_exists(WEBROOT.$photo)) {
                ?><img class="profile" src="<?php echo $this->html()->url($photo); ?>">
                <?php
            } else {
                ?><img class="profile" src="<?php echo $this->html()->url('img/nophoto.jpg'); ?>"> <?php
            }
            ?>
        </div>

        <?php //var_dump($remedial); die(); ?>

        <div>
            <form class="form-inline" action="<?= WWW_ROOT."remedial/modify/".$remedial['idRemedial'];?>" method="post">
                <h5>Dates:</h5>
                <input type="date" name="date" value="<?= $remedial['remDate'];?>">

                <h5>Pratique:</h5>
                <?php
                if($remedial['remType'] == "0"){
                    ?>
                    <input type="radio" name="type" value="0" checked="true">
                    <h5>Théorie:</h5>
                    <input type="radio" name="type" value="1">
                    <?php
                }else{
                    ?>
                    <input type="radio" name="type" value="0">
                    <h5>Théorie:</h5>
                    <input type="radio" name="type" value="1" checked="true">
                    <?php
                }
                ?>
                <h5>Nombre de périodes:</h5>
                <select name="period">
                    <option <?php if($remedial['remNbPeriod'] == "1"){echo "selected=\"selected\"";} ?>>1</option>
                    <option <?php if($remedial['remNbPeriod'] == "2"){echo "selected=\"selected\"";} ?>>2</option>
                    <option <?php if($remedial['remNbPeriod'] == "3"){echo "selected=\"selected\"";} ?>>3</option>
                    <option <?php if($remedial['remNbPeriod'] == "4"){echo "selected=\"selected\"";} ?>>4</option>
                    <option <?php if($remedial['remNbPeriod'] == "5"){echo "selected=\"selected\"";} ?>>5</option>
                </select>
                <br>
                <input type="submit" class="btn btn-primary " value="Modifier">
            </form>
        </div>
    </div>
</div>
