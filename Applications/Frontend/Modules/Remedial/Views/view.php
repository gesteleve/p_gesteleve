<?php
//*********************************************************
// Societe: ETML
// Auteur : Lengacher Loïc
// Date : 05.10.16
// But : Afficher les remediations.
//*********************************************************
?>
<?php //$this->html()->js('isotope.min.js');?>
<?php //$this->html()->js('jquery.infinitescroll.min.js'); ?>

<?php
    echo '<h1>Rattrapages de '.$student->first_name().' '.$student->name().'</h1>';
?>


<div class="mt30">
    <div id="container">
        <div id="student-picture" class="span3 ml0">
            <?php
            $photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';

            if (file_exists(WEBROOT.$photo)) {
                ?><img class="profile" src="<?php echo $this->html()->url($photo); ?>">
                <?php
            } else {
                ?><img class="profile" src="<?php echo $this->html()->url('img/nophoto.jpg'); ?>"> <?php
            }
            ?>
        </div>
        <div class="span7 student-info">
            <table class="table table-bordered">
                <tr>
                    <th><h4>Liste des rattrapages</h4></th>
                </tr>
                <tr>
                    <th>Dates</th>
                    <th>Pratique \ Théorie</th>
                    <th>Périodes</th>
                    <th>Action</th>
                    <th></th>
                </tr>
                    <?php foreach ($remedials as $remedial) { ?>
                    <tr>
                        <td><?= $remedial['remDate'] ?></td>
                        <td>
                            <?php if($remedial['remType'] == 0){
                                echo 'Pratique';
                            } else{
                                echo 'Théorie';
                            }
                            ?>
                        </td>
                        <td><?= $remedial['remNbPeriod'] ?></td>
                        <td><a href="<?= $this->html()->url('remedial/viewModify/'.$remedial['idRemedial']); ?>" class="btn btn-mini btn-primary" type="submit">Modifier<a></a></td>
                        <td><a href="<?= $this->html()->url('remedial/delete/'.$remedial['idRemedial']); ?>" class="btn btn-mini btn-danger" type="submit">Supprimer<a></a></td>
                    </tr>
                    <?php } ?>
            </table>
        </div>
        <div class="span7 student-info">
            <form class="form-inline" action="<?= WWW_ROOT."remedial/add/".$student->id();?>" method="post">

                <h4>Ajouter un rattrapage</h4>

                <h5>Dates:</h5>
                <input type="date" name="date">

                <h5>Pratique:</h5>
                <input type="radio" name="type" value="0" checked="true">
                <h5>Théorie:</h5>
                <input type="radio" name="type" value="1">

                <h5>Nombre de périodes:</h5>
                <select name="period">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>

                <h5></h5>
                <input type="submit" class="btn btn-primary" value="Ajouter">
            </form>
        </div>
    </div>
</div>
