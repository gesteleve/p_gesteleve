<?php
/****************************************************************************
Nom:        ScheduleController.php
Auteur:     Jonathan Hermann
Date:       12.05.2014
But:        Il s'agit du controleur de l'application de l'horaire
*****************************************************************************
Modifications
Date  : -
Auteur: -
Raison: -
A faire: -
*****************************************************************************/

namespace Applications\Frontend\Modules\Schedule;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class ScheduleController extends BackController
{
    public function executeIndex(HTTPRequest $request) 
    {
        //$this->page->setLayout();
		
		

        $strClassID = null;

        $strColleagueID = null;

        $blnFilter = null;

        $tab_strSchedule = null;

        $strColleagueName = "";

        // Récupère le manager du calendrier (fichier ScheduleManager.php)
        $manager = $this->managers->getManagerOf('Schedule');
        
        // Récupère le rôle de l'utilisateur connecté
        $intRole = $this->app->user()->getAttribute('role');

        if($request->postsExists(array('slcFilterClass')) && !($request->postsEmpty(array('slcFilterClass'))))  
        {
            $strClassID = $request->postData('slcFilterClass');

            // Récupère l'horaire de la classe de l'élève
            $tab_strSchedule = $manager->getClassSchedule($strClassID);

            $blnFilter = true;
        }
        elseif($request->postsExists(array('slcFilterColleague')) && !($request->postsEmpty(array('slcFilterColleague')))) 
        {
            $strColleagueID = $request->postData('slcFilterColleague');

            $strColleagueName = "";

            $tab_strSchedule = $manager->getColleagueSchedule($strColleagueID);

            $blnFilter = true;
        }
        
		
        // Enregistre l'id et le groupe de l'utilisateur connecté...
        if ($this->app->user()->isAuthenticated()) 
        { 
            $intUserId = $this->app->user()->getAttribute('user')->id();

            $intUserGroup = $this->app->user()->getAttribute('group');
        }
        // ... ou aucun si non connecté
        else
        {
            $intUserId = null;

            $intUserGroup = null;
        }

        
        if (!($blnFilter)) 
        {
            // Récupère l'horaire en fonction du type d'utilisateur : élève ou colaborateur (élèves = 7 / collaborateurs = 1 à 6)
            if ($intRole == 7) 
            {
                // Récupère le nom/id de la classe de l'élève
                $strClassID = $manager->getNameClassStudent($intUserId);

                // Récupère l'horaire de la classe de l'élève
                $tab_strSchedule = $manager->getClassSchedule($strClassID);

            }
            else
            {
                if ($intUserId != null) 
                {  
                    // Récupère l'horaire de la classe de l'élève
                    $tab_strSchedule = $manager->getColleagueSchedule($intUserId); 

                    $strColleagueID = $intUserId;

                    $strColleagueName = "";
                }
            }
        }

        // Envoie le tableau contenant l'horaire de la classe à la vue
        $this->page->addVar('tab_strSchedule', $tab_strSchedule);

		$this->page->addVar('title', 'Mon horaire');
		$this->page->addVar('description', 'Horaire personnel');
		$this->page->addVar('keywords', 'horaire');
		
        // Récupére le tableau des numéros de période
        $tab_strPeriodsNumbers = $manager->getPeriodNumber();

        // Récupére le tableau des numéros de période
        $tab_strBranchs = $manager->getBranchs();

        // Récupére le tableau des numéros de période
        $tab_strColleagues = $manager->getColleagues();

        // Récupére le tableau des numéros de période
        $tab_strClasses = $manager->getClasses();

        // Récupére le tableau des numéros de période
        $tab_strRooms = $manager->getRooms();

        // "Ajout" des variables à la page index
        $this->page->addVar('tab_strPeriodsNumbers', $tab_strPeriodsNumbers);
        $this->page->addVar('tab_strBranchs', $tab_strBranchs);
        $this->page->addVar('tab_strColleagues', $tab_strColleagues);
        $this->page->addVar('tab_strClasses', $tab_strClasses);
        $this->page->addVar('tab_strRooms', $tab_strRooms);
        $this->page->addVar('intUserId', $intUserId);
        $this->page->addVar('strClassID', $strClassID);
        $this->page->addVar('strColleagueID', $strColleagueID);
        $this->page->addVar('blnFilter', $blnFilter);
        $this->page->addVar('intUserGroup', $intUserGroup);
    }

    public function executeEdit(HTTPRequest $request) 
    {
        // Supprime le template par defaut de la page
        $this->page->setLayout();

        if($request->postsExists(array('slcBranch', 'slcColleague', 'slcClass', 'slcRooms', 'slcDays'))) 
        {
            if (!($request->postsEmpty(array('slcBranch', 'slcColleague', 'slcClass', 'slcRooms', 'slcDays')))) 
            {
                $manager = $this->managers->getManagerOf('Schedule');

                if ($request->postData('inpModif') == "true") 
                {
                    $blnResult = "true";

                    $tab_strCourse = array(
                        'id_Branch' => $request->postData('slcBranch'),
                        'id_Colleague' => $request->postData('slcColleague'),
                        'id_class' => $request->postData('slcClass'),
                        'id_Room' => $request->postData('slcRooms'),
                        'id_Day' => $request->postData('slcDays'),
                        'id_Period' => $request->postData('inpModifyPeriodId')
                    );

                    // Modifie la période
                    if (!($manager->ModifyPeriod($tab_strCourse))) 
                    {
                        $blnResult = "false";
                    }

                    // Modifie la classe de la période
                    if (!($manager->ModifyClassPeriod($tab_strCourse))) 
                    {
                        $blnResult = "false";
                    }

                    // Modifie l'enseignant de la période
                    if (!($manager->ModifyColleaguePeriod($tab_strCourse))) 
                    {
                        $blnResult = "false";
                    }

                }
                else
                {
                    // Vérifie que la date de début se situe bien après celle de fin
                    if ($request->postData('slcPeriodsStart') > $request->postData('slcPeriodsEnd')) 
                    {
                        $blnResult = "false";
                    }
                    else
                    {
                        $blnResult = "true";

                        // Parcoure le nombre de période à créer
                        for ($i=$request->postData('slcPeriodsStart'); $i <= $request->postData('slcPeriodsEnd'); $i++) 
                        { 
                            $tab_strCourse = array(
                                'id_Branch' => $request->postData('slcBranch'),
                                'id_Colleague' => $request->postData('slcColleague'),
                                'id_class' => $request->postData('slcClass'),
                                'id_Room' => $request->postData('slcRooms'),
                                'id_Day' => $request->postData('slcDays'),
                                'id_NumberPeriod' => $i
                            );

                            // Ajoute la nouvelle période
                            if (!($manager->addPeriod($tab_strCourse))) 
                            {
                                $blnResult = "false";
                            }

                            // Récupère l'id des nouvelles périodes
                            $tab_strPeriod = $manager->selectNewPeriod();

                            // Relie la période à la classe
                            if (!($manager->addClassPeriod($tab_strPeriod[0]['LAST_INSERT_ID()'], $request->postData('slcClass')))) 
                            {
                                $blnResult = "false";
                            }

                            // Relie la période à un enseignant
                            if (!($manager->addColleaguePeriod($tab_strPeriod[0]['LAST_INSERT_ID()'], $request->postData('slcColleague')))) 
                            {
                                $blnResult = "false";
                            }                    

                            $tab_strCourse = null;


                        } // for()

                    } // if()

                } // if()
                
                // Affiche le résultat dans la page
                print($blnResult);
                
            } // if()

        } // if()
    
    } // function()

    public function executeDelete(HTTPRequest $request) 
    {
        // Supprime le template par defaut de la page
        $this->page->setLayout();

        if($request->postsExists(array('inpPeriodIdDelete'))) 
        {
            if (!($request->postsEmpty(array('inpPeriodIdDelete')))) 
            {
                $manager = $this->managers->getManagerOf('Schedule');

                $blnResult = "true";

                if (!($manager->deletePeriod($request->postData('inpPeriodIdDelete')))) 
                {
                    $blnResult = "false";
                }

                if (!($manager->deleteColleaguePeriod($request->postData('inpPeriodIdDelete')))) 
                {
                    $blnResult = "false";
                }

                if (!($manager->deleteClassPeriod($request->postData('inpPeriodIdDelete')))) 
                {
                    $blnResult = "false";
                }

                print($blnResult);
            }
        }
    } // function()


}
