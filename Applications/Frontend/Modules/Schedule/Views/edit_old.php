<?php
/****************************************************************************
Nom:        index.php
Auteur:     Jonathan Hermann
Date:       12.05.2014
But:        Affiche l'horaire de l'utilisateur ou un autre selon son choix
*****************************************************************************
Modifications
Date  : -
Auteur: -
Raison: -
A faire: -
*****************************************************************************/


// Fichier CSS concernant la liste déroulante avec recherche
$this->html()->css('bootstrap-select/bootstrap-select.min.css');


// Fichier JS concernant la liste déroulante avec recherche
$this->html()->js('bootstrap-select/bootstrap-select.min.js');

?>	
<h1>Gestion de l'horaire de </h1>

<form>

	<table class="table table-striped table-bordered">
		<thead>
			<th>#</th>
			<th>Lundi <a class='icon-plus-manage' href='javascript:void(0)'><i class='icon-plus'></i></a></th>
			<th>Mardi <a class='icon-plus-manage' href='#'><i class='icon-plus'></i></a></th>
			<th>Mercredi <a class='icon-plus-manage' href='#'><i class='icon-plus'></i></a></th>
			<th>Jeudi <a class='icon-plus-manage' href='#'><i class='icon-plus'></i></a></th>
			<th>Vendredi <a class='icon-plus-manage' href='#'><i class='icon-plus'></i></a></th>
		</thead>
		<tbody>
			<?php

				for ($i=0; $i < count($tab_strPeriodsNumbers); $i++) 
				{ 
					print("<tr>");
						for ($j=0; $j < 5; $j++) 
				    	{ 
				    		
				    			if ($j==0) 
				    			{
				    				$intModalStartPeriod = substr($tab_strPeriodsNumbers[$i]['start'], 0, 5);
									$intModalEndPeriod = substr($tab_strPeriodsNumbers[$i]['end'], 0, 5);

				    				print("<td>".$tab_strPeriodsNumbers[$i]['id']." ".$intModalStartPeriod." - ".$intModalEndPeriod."</td>");
				      			}

				      			$blnEmpty = true;

				      			print("<td><div class='row-fluid div-manage'>");
					      			// Parcours l'horaire de la personne et génère les événements avec le titre, la date de début et de fin 
									for ($k=0; $k < count($tab_strSchedule); $k++) 
									{
										// Vérifie la correspondance des jours -> Lundi = Lundi
										if ($j+1 == $tab_strSchedule[$k]['day'] && $tab_strSchedule[$k]['period_id'] == $i+1) 
										{
											
											$strModalColleagueName = $tab_strSchedule[$j]['first_name']." ".$tab_strSchedule[$j]['name'];

											$strModalBranch = substr($tab_strSchedule[$k]['branch_id'], 0, 5);

											// Affiche le cours
											print("<span class='label label-info label-manage'><a href='#'>".$strModalBranch."</a> <a><!--<i class='icon-edit'></i></a>--> <a href='#'><i class='icon-remove'></i></a></span>");

											$blnEmpty = false;

											//element.attr('href', 'javascript:void(0);');
	            //element.attr('onclick', 'openModalDetail("' + event.branch + '","' + event.colleague_name + '","' + event.room + '","' + event.time_start + '", "' + event.time_end + '");');

										}

										// end if()


									} // end for()

								
								print("</div></td>");

					    	
				    	}
			    	print("</tr>");
				}
		    	
		    			
		    ?>
		</tbody>
		
	</table>
	
</form>


<!-- Modal d'affichage des détails -->
<?php
	// Affiche la 
	echo $this->html()->modalbox(
		'ModalAddModify',

		'<h4 class="modal-title" id="myModalLabel"><div id="title"></div></h4>',

		'
		
		
					

		',

		'<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>'
	);
?>




<script type="text/javascript">
        $(window).on('load', function () {

            $('.selectpicker').selectpicker({
            });

            //$('#ModalAddModify').modal('show');

        });
    </script>




