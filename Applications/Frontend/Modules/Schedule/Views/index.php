<?php
/****************************************************************************
Nom:        index.php
Auteur:     Jonathan Hermann
Date:       12.05.2014
But:        Affiche l'horaire de l'utilisateur ou un autre selon son choix
*****************************************************************************
Modifications
Date  : -
Auteur: -
Raison: -
A faire: -
*****************************************************************************/
	
	// Fichiers CSS concernant FullCalendar
	$this->html()->css('fullcalendar/fullcalendar.css');
	$this->html()->css('fullcalendar/jquery-ui.min.css');
	$this->html()->css('fullcalendar/jquery.ui.theme.css');

	// Fichier CSS concernant la liste déroulante avec recherche
	//$this->html()->css('bootstrap-select/bootstrap-select.min.css');

	// Fichier CSS concernant la liste déroulante avec recherche
	$this->html()->css('bootstrap-select-2/select2.css');

	// Fichier CSS concernant la liste déroulante avec recherche
	$this->html()->css('bootstrap-select-2/select2-bootstrap.css');

	// Fichier JS concernant FullCalendar
	$this->html()->js('fullcalendar/fullcalendar.js');

	// Fichier JS concernant la liste déroulante avec recherche
	$this->html()->js('bootstrap-select-2/select2.js');

	
	$blnAdmin = false;

	// Enregistre l'id de l'utilisateur connecté...
    if ($this->app->user()->isAuthenticated() && $intUserGroup == 8) 
    { 
        $blnAdmin =	true;
    }


    if ($blnAdmin) 
    {
    	$menu=array(
	    	"schedule", "",
   			"Ajouter un cours","Modal", "ModalAddModify"
	    );

		echo $this->html()->optionmenu($menu);
    }
    	

    print("<div class='page-header'><h1>Horaire <small>");
		if ($strColleagueID) 
		{
			print($strColleagueID);
		}
		elseif ($strClassID) 
		{
			print($strClassID);
		}
	print("</small></h1></div>");


    
?>

<?php
	##########################################################################################################################################################
	##																																						##
	##																		Filtre 																			##
	##																																						##
	##########################################################################################################################################################

	// Rempli le formulaire via javascript + subimt = Redirection avec résultat :)
?>

<?php
			
	$strListColleagues = "";
	$strListClasses = "";

	$strListColleaguesName = "";
	$strListClassesName = "";

	// Lien d'envoi du formulaire
	$strFilterLink = $this->html()->url('schedule');

	for ($i=0; $i < count($tab_strColleagues); $i++) 
	{ 
		$strListColleagues = $strListColleagues."<option value=".$tab_strColleagues[$i]["id"].">".$tab_strColleagues[$i]["first_name"]." ".$tab_strColleagues[$i]["name"]."</option>";
	}


	$strFilterListColleagues = '
				<select name="slcFilterColleague" id="slcFilterColleague" class="slcFilterColleague">
					<option value="">Enseignant</option>
			        '.$strListColleagues.'
			    </select>
	';
	
	for ($i=0; $i < count($tab_strClasses); $i++) 
	{ 
		$strListClasses = $strListClasses."<option value=".$tab_strClasses[$i]["id"].">".$tab_strClasses[$i]["id"]."</option>";
	}

	$strFilterListClasses = '
				<select name="slcFilterClass" id="slcFilterClass" class="slcFilterClass">
					<option value="">Classe</option>
					'.$strListClasses.'
				</select>
	';



?>

	<div class="well well-small well-filter">
		<form name="frmFilter" id="frmFilter" method="post" action="<?php print($strFilterLink); ?>" class="form-inline" >

			<div class="control-group">

	    		<span class="control-label">Filtre:</span> 

	    		<div class="controls form-inline">

		    		<?php
						print($strFilterListClasses.$strFilterListColleagues);    	
					?>
					

					<button type="submit" class="btn btn-primary"> <i class="icon-search icon-white"></i></button>

				</div>

			</div>

		</form>

		<form name="frmTemp" id="frmTemp" method="post" action="<?php print($strFilterLink); ?>" class="form-inline" >

			<input type="hidden" name="inpFilterClass" id="inpFilterClass" value="<?php print($strClassID); ?>">

			<input type="hidden" name="inpFilterColleague" id="inpFilterColleague" value="<?php print($strColleagueID); ?>">

		</form>
	</div>		


	<script type="text/javascript">
		$(window).on('load', function () 
		{
			$('#slcFilterColleague').select2();

			$('#slcFilterClass').select2();
		});
	</script>

<?php
	##########################################################################################################################################################
	##																																						##
	##																		AFFICHER 																		##
	##																																						##
	##########################################################################################################################################################
?>

<script>

	/* Appelle les fonction nécessaire au chargement de la page */
	$(document).ready(function() 
	{	
		initialiseCalendar();
		
	});

	/********************************************************************
	Nom :	initialise_calendar
	But :	Initialise le calendrier FullCalendar 
	Retour: -
	Param : -
	********************************************************************/
	function initialiseCalendar()
	{		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaWeek,agendaDay'
			},
			editable: false,
			selectable: false,
			

			defaultView: 'agendaWeek',
        	
        	/* Spécifie que le premier jour de la semaine est le lundi */
			firstDay: 1,

			/* Spécifie que l'heure minimale d'affichage est 08h00' */	
			minTime: 8, 

			/* Spécifie que l'heure maximale d'affichage est 19h00 */ 
			maxTime: 19, 

			/* Locales - FR */
			monthNames: ['Janvier','F\u00e9vrier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','D\u00e9cembre'], /* Noms des mois */
			monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Juin','Juil','Aout','Sep','Oct','Nov','Dec'], /* Noms raccourcis des mois */
			dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'], /* Noms des jours */
			dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'], /* Noms raccourcis des jours */
			buttonText: { /* Textes des bouttons */
				today: 'Aujour.',
				month: 'Mois',
				week: 'Semaine',
				day: 'Jour'
			},

			/* Format des colonnes */
			columnFormat: {
				/* Affiche le nom complet des jours */
				week: 'dddd' 
			},

			/* Cache le dimanche et le samedi */
			hiddenDays: [ 0, 6 ], 
			
			/* Format de la date */
			dateFormat: "dd-mm-yyyy", 

			/* Format du temps (général) */
			timeFormat: 'HH:mm {- HH:mm}' , 

			/* Format du temps dans l'axe vertical */
			axisFormat: 'HH:mm', 

			/* Appelle d'une fonction lors du clic sur un évennement/cours */
			eventRender: function (event, element) 	
			{
	            element.attr('href', 'javascript:void(0);');
	            element.attr('onclick', 'openModalDetail("' + event.period_id + '","' + event.branch + '","' + event.branch_id + '","' + event.colleague_name + '","' + event.colleague_id + '","' + event.class_id + '","' + event.room + '","' + event.day + '","' + event.number_period_id + '","' + event.time_start + '", "' + event.time_end + '");');
        	},

        	/* Affichage des events */
			events: [

				<?php
					// Date de début du calendrier
					$start_date = new DateTime("2013-01-26");

					// Récupère le Timestamp
					$start_date = $start_date->getTimestamp();

					// Date de fin du calendrier
					$end_date = new DateTime("2014-07-04");

					// Récupère le Timestamp
					$end_date = $end_date->getTimestamp();

					// Durée en seconde d'un jour
					$intDaySecondeDuration = 86400;

					// Affiche l'horaire sur une année entière -> chaque semaine de l'année
					for ($i = $start_date; $i <= $end_date; $i = $i + $intDaySecondeDuration) 
					{ 
						// Date entière
						$strDate = date('m/d/Y', $i);

						// Jour
						$strDay = date('d', $i);

						// Mois
						$strMonth = date('m', $i) - 1;
						
						// Année
						$strYear = date('Y', $i);

						// Parcours l'horaire de la personne et génère les événements avec le titre, la date de début et de fin 
						for ($j=0; $j < count($tab_strSchedule); $j++) 
						{
							// Vérifie la correspondance des jours -> Lundi = Lundi
							if (date('w', strtotime($strDate)) == $tab_strSchedule[$j]['day']) 
							{
								// Sépare les heures et les minutes pour la compatibilité avec FullCalendar
								$intStartHour = substr($tab_strSchedule[$j]['start'], 0, 2);
								$intStartMin = substr($tab_strSchedule[$j]['start'], 3, -3);
								$intEndHour = substr($tab_strSchedule[$j]['end'], 0, 2);
								$intEndMin = substr($tab_strSchedule[$j]['end'], 3, -3);

								// Récupère les chaines de caractères qui seront affichées dans le titre de la période
								$strPeriodTitle = substr($tab_strSchedule[$j]['branch_id'], 0, 5);
								$strPeriodColleague = strtoupper(substr($tab_strSchedule[$j]['colleague_id'], 3, 6));
								$strPeriodRoom = $tab_strSchedule[$j]['room_id'];
								$strPeriodClass = $tab_strSchedule[$j]['school_class_id'];

								// Récupération des heures et des minutes sans les secondes -> hh:mm
								$strTimeStart = $intStartHour.":".$intStartMin;
								$strTimeEnd = $intEndHour.":".$intEndMin;

								$strColleagueName = $tab_strSchedule[$j]['first_name']." ".$tab_strSchedule[$j]['name'];

								$strBranch = "Cours: ".$tab_strSchedule[$j]['branch_id'];

								if ($strColleagueID) 
								{
									$strTitle = $strPeriodTitle." ".$strPeriodClass." ".$strPeriodRoom;
								}
								elseif ($strClassID) 
								{
									$strTitle = $strPeriodTitle." ".$strPeriodColleague." ".$strPeriodRoom;
								}

								// Affiche le cours
								echo("
									{
										period_id:'".$tab_strSchedule[$j]['id']."',
										title: '".$strTitle."',
										start: new Date(".$strYear.", ".$strMonth.", ".$strDay.", ".$intStartHour.", ".$intStartMin."),
										end: new Date(".$strYear.", ".$strMonth.", ".$strDay.", ".$intEndHour.", ".$intEndMin."),	
										allDay: false,
										url: '-',
										branch: '".$strBranch."',
										branch_id: '".$tab_strSchedule[$j]['branch_id']."',
										colleague_name: '".$strColleagueName."',
										colleague_id: '".$tab_strSchedule[$j]['colleague_id']."',
										class_id: '".$tab_strSchedule[$j]['school_class_id']."',
										room: '".$tab_strSchedule[$j]['room_id']."',
										day: '".$tab_strSchedule[$j]['day']."',
										number_period_id: '".$tab_strSchedule[$j]['period_id']."',
										time_start:'".$strTimeStart."',
										time_end:'".$strTimeEnd."'

									},
								");

							} // end if()

						} // end for()

					} // end for() 
					
				?>

			] /* end events[] */

		});

	} /* end initialiseCalendar{} */

	/********************************************************************
	Nom :	openModalDetail
	But :	Envoie des valeurs à la modal et l'affiche
	Retour: -
	Param : nom du cours, nom de l'enseignant, salle,...
	********************************************************************/
	function openModalDetail(period_id, branch, branch_id, colleague_name, colleague_id, class_id, room, day, number_period_id, time_start, time_end) 
	{

	    $('#ModalDetail #title').text(branch);

	    $('#ModalDetail #ModalColleagueName').text(colleague_name);

	    $('#ModalDetail #ModalClassName').text(class_id);

	    $('#ModalDetail #ModalRoom').text(room);

	    $('#ModalDetail #ModalStart').text(time_start);

	    $('#ModalDetail #ModalEnd').text(time_end);

	    $('#ModalDetail #inpBranchId').val(branch_id);

	    $('#ModalDetail #inpColleagueId').val(colleague_id);

	    $('#ModalDetail #inpClassId').val(class_id);

	    $('#ModalDetail #inpNumberPeriodId').val(number_period_id);

	    $('#ModalDetail #inpDayId').val(day);

	    $('#ModalDetail #inpPeriodId').val(period_id);

	    $('#ModalDetail').modal('show');
	} // end openModalDetail{}

</script>

<div class="mt30">
  <div id="container">
	<div id="search-box">
		<?php // La div ci-dessous est la div qui permet la création du calendrier FullCalendar. ?>
		<div id="calendar" class="fc fc-ltr"></div>
	</div>
  </div>
</div>


<!-- Modal d'affichage des détails -->
<?php
	
	$strAdminButtons = "";

	if ($blnAdmin) 
	{
		$strAdminButtons = '

			<a class="btn btn-danger btnDelete" data-toggle="modal" data-target="#ModalDeleteConfirm" onclick="SetModalDelete()">Supprimer</a>
		
			<a class="btn btn-primary btnModify" data-toggle="modal" data-target="#ModalAddModify" onclick="SetModalModify()">Modifier</a>
			
		';
	}
	

	// Affiche la modal d'affichage des détails
	echo $this->html()->modalbox(
		'ModalDetail',

		'<h4 class="modal-title" id="myModalLabel"><div id="title"></div></h4>',

		'
		<dl class="dl-horizontal">

			<dt>Professeur:</dt>
			<dd><div id="ModalColleagueName"></div></dd>

			<dt>Classe:</dt>
			<dd><div id="ModalClassName"></div></dd>

			<dt>Salle:</dt>
			<dd><div id="ModalRoom"></div></dd>

			<dt>Début:</dt>
			<dd><div id="ModalStart"></div></dd>

			<dt>Fin:</dt>
			<dd><div id="ModalEnd"></div></dd>

			<input type="hidden" name="inpBranchId" id="inpBranchId" >

			<input type="hidden" name="inpColleagueId" id="inpColleagueId" >

			<input type="hidden" name="inpClassId" id="inpClassId" >

			<input type="hidden" name="inpDayId" id="inpDayId" >

			<input type="hidden" name="inpNumberPeriodId" id="inpNumberPeriodId" >

			<input type="hidden" name="inpPeriodId" id="inpPeriodId" >

		</dl>
		',

		$strAdminButtons.'
			<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
		'
	);
?>



<?php
	##########################################################################################################################################################
	##																																						##
	##																		AJOUTER 																		##
	##																																						##
	##########################################################################################################################################################
?>

<?php
	
	// Préparation des variables qui contiendront la liste des branches, enseignants,...
    $strListBranchs = "";
	$strFormListColleagues = "";
	$strFormListClasses = "";
	$strListRooms = "";
	$strListPeriodsStart = "";
	$strListPeriodsEnd = "";

	// Liste des branches
	for ($i=0; $i < count($tab_strBranchs); $i++) 
	{ 
		$strListBranchs = $strListBranchs."<option value=".$tab_strBranchs[$i]["id"].">".$tab_strBranchs[$i]["id"]."</option>";
	}

	// Liste des salles
	for ($i=0; $i < count($tab_strRooms); $i++) 
	{ 
		$strListRooms = $strListRooms."<option value=".$tab_strRooms[$i]["id"].">".$tab_strRooms[$i]["id"]."</option>";
	}

	// Liste des périodes avec heure de début
	for ($i=0; $i < count($tab_strPeriodsNumbers); $i++) 
	{ 
		$strPeriodStart = substr($tab_strPeriodsNumbers[$i]["start"], 0, 5);

		$strListPeriodsStart = $strListPeriodsStart."<option value=".$tab_strPeriodsNumbers[$i]["id"].">".$strPeriodStart."</option>";
	}

	// Liste des périodes avec heure de fin
	for ($i=0; $i < count($tab_strPeriodsNumbers); $i++) 
	{ 
		$strPeriodEnd = substr($tab_strPeriodsNumbers[$i]["end"], 0, 5);

		$strListPeriodsEnd = $strListPeriodsEnd."<option value=".$tab_strPeriodsNumbers[$i]["id"].">".$strPeriodEnd."</option>";
	}

	$strFormLink = "";

	if ($blnAdmin) 
	{
		// Lien d'envoi du formulaire
		$strFormLink = $this->html()->url('schedule/edit');
	}
	

	if (!($blnFilter)) 
	{
		if ($this->app->user()->getAttribute('group') != 8) 
		{
			$strFormListColleagues = '
				<div class="control-group">
				    <label class="control-label">Professeur</label>
				    <div class="controls">
						<select name="slcColleague" id="slcColleague" class="form-control span7">
							<option value="">-</option>
					        '.$strListColleagues.'
					    </select>
					</div>
				</div>

				<input name="slcClass" type="hidden" value="'.$strClassID.'">
			';
		}
		else
		{
			$strFormListClasses = '
				<div class="control-group">
					<label class="control-label">Classe</label>
					<div class="controls">
						<select name="slcClass" id="slcClass" class="form-control span7">
							<option value="">-</option>
							'.$strListClasses.'
						</select>
					</div>
				</div>

				<input name="slcColleague" type="hidden" value="'.$intUserId.'">
			';		
		}
	}


	if ($blnFilter) 
	{
		if ($strClassID) 
		{
			$strFormListColleagues = '
				<div class="control-group">
				    <label class="control-label">Professeur</label>
				    <div class="controls">
						<select name="slcColleague" id="slcColleague" class="form-control span7">
							<option value="">-</option>
					        '.$strListColleagues.'
					    </select>
					</div>
				</div>

				<input name="slcClass" type="hidden" value="'.$strClassID.'">
			';
		}
		else
		{
			$strFormListClasses = '
				<div class="control-group">
					<label class="control-label">Classe</label>
					<div class="controls">
						<select name="slcClass" id="slcClass" class="form-control span7">
							<option value="">-</option>
							'.$strListClasses.'
						</select>
					</div>
				</div>

				<input name="slcColleague" type="hidden" value="'.$strColleagueID.'">
			';		
		}
	}

	// Affiche la 
	echo $this->html()->modalbox(
		'ModalAddModify',

		'<h4 class="modal-title" id="myModalLabel"><div id="title">Ajouter un cours</div></h4>',

		'
			<div class="alert alert-error fade in" id="add-error" style="display:none;">
			    Une erreur est survenue lors de l\'ajout. Veuillez vérifier que tous les champs soient remplis et que la période de début soit inférieur ou égal à celle de fin.
			</div>

			<form name="frmScheduleAdd" id="frmScheduleAdd" class="form-horizontal" action="'.$strFormLink.'" method="post">
				<div class="control-group">
					<label class="control-label">Cours</label>
					<div class="controls">
						<select name="slcBranch" id="slcBranch" class="form-control span7" >
							<option value="">-</option>
							'.$strListBranchs.'
					    </select>
					</div>
				</div>
				'.$strFormListColleagues.'
				'.$strFormListClasses.'				
				<div class="control-group">
				    <label class="control-label">Salle</label>
				    <div class="controls">
						<select name="slcRooms" id="slcRooms" class="form-control span7">
							<option value="">-</option>
							'.$strListRooms.'
					    </select>
					</div>
				</div>
				<div class="control-group">
				    <label class="control-label">Jour</label>
				    <div class="controls">
						<select name="slcDays" id="slcDays" class="form-control span7">
							<option value="">-</option>
							<option value="1">Lundi</option>
							<option value="2">Mardi</option>
							<option value="3">Mercredi</option>
							<option value="4">Jeudi</option>
							<option value="5">Vendredi</option>
					    </select>
					</div>
				</div>
				<div class="control-group">
				    <label class="control-label">Début</label>
				    <div class="controls">
						<select name="slcPeriodsStart" id="slcPeriodsStart" class="form-control span7">
							<option value="">-</option>
							'.$strListPeriodsStart.'
					    </select>
					</div>
				</div>
				<div class="control-group">
				    <label class="control-label">Fin</label>
				    <div class="controls">
						<select name="slcPeriodsEnd" id="slcPeriodsEnd" class="form-control span7">
							<option value="">-</option>
							'.$strListPeriodsEnd.'
					    </select>
					</div>
				</div>

				<input type="hidden" name="inpModifyPeriodId" id="inpModifyPeriodId">

				<input type="hidden" name="inpModif" id="inpModif">

		',

		'	<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn">Enregistrer</button>
				</div>
			</div>
		</form>'
	);
?>


<script type="text/javascript">
    $(window).on('load', function () {

       	$('#slcBranch').select2();

       	$('#slcColleague').select2();

       	$('#slcClass').select2();

       	$('#slcRooms').select2();

       	$('#slcDays').select2();

       	$('#slcPeriodsStart').select2();

       	$('#slcPeriodsEnd').select2();

       	

   		$('#ModalAddModify').on('hidden', function () 
   		{
		    $("#ModalAddModify #title").text("Ajouter un cours");

		    $('#slcBranch').select2("val", "");

		    $('#slcColleague').select2("val", "");

		    $('#slcClass').select2("val", "");

		    $('#slcRooms').select2("val", "");
		    
		    $('#slcDays').select2("val", "");

		    $('#slcPeriodsStart').select2("val", "");

		    $('#slcPeriodsEnd').select2("val", "");

		    $("#inpModifyPeriodId").val("");

		    $("#inpModif").val("");

		    $("#add-error").hide();

		})

    });

</script>

<script type="text/javascript">
	$(document).ready(function() {
    $('#frmScheduleAdd').on('submit', function() {
        
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'text',
            success: function(result) 
            {
               	if(result == "true")
               	{
					$('#ModalAddModify').modal('hide');

					$('#slcFilterClass').select2("val", "<?php print($strClassID); ?>");

	    			$('#slcFilterColleague').select2("val", "<?php print($strColleagueID); ?>");

					$( "#frmFilter" ).submit();
				}
				else
				{
					$("#add-error").show();

				}
            }
        });

        return false;
    });	
});
</script>

<?php 
	##########################################################################################################################################################
	##																																						##
	##																		MODIFIER 																		##
	##																																						##
	##########################################################################################################################################################
	
	
?>

	<script type="text/javascript">


	
		function SetModalModify()
		{
			var strBranchId = $('#inpBranchId').val();

			var strColleagueId = $('#inpColleagueId').val();

			var strRoomId = $('#ModalRoom').text();

			var strDayId = $('#inpDayId').val();

			var intNumberPeriodId = $('#inpNumberPeriodId').val();

			var intPeriodId = $('#inpPeriodId').val();

			var strClassID = $('#inpClassId').val();


			$("#ModalAddModify #title").text("Modifier un cours");

			$("#slcBranch").select2("val", strBranchId);

			$("#slcColleague").select2("val", strColleagueId);

			$("#slcClass").select2("val", strClassID);

			$("#slcRooms").select2("val", strRoomId);

			$("#slcDays").select2("val", strDayId);

			$("#slcPeriodsStart").select2("val", intNumberPeriodId);

			$("#slcPeriodsEnd").select2("val", intNumberPeriodId);

			$("#slcPeriodsStart").select2("enable", false);

			$("#slcPeriodsEnd").select2("enable", false);

			$("#inpModifyPeriodId").val(intPeriodId);

			$("#inpModif").val(true);

			

			$('#ModalDetail').modal('hide');

		}
	</script>

<?php
##########################################################################################################################################################
##																																						##
##																SUPPRIMER 																				##
##																																						##
##########################################################################################################################################################
	$strFormLink = "";

	if ($blnAdmin) 
	{
		$strFormLink = $this->html()->url('schedule/delete');
	}	

	// Affiche la modal d'affichage des détails
	echo $this->html()->modalbox(
		'ModalDeleteConfirm',

		'<h4 class="modal-title" id="myModalLabel"><div id="title"></div></h4>',

		'
		Voulez-vous vraiment supprimer ce cours ?

		<form name="frmScheduleDelete" id="frmScheduleDelete" class="form-horizontal" action="'.$strFormLink.'" method="post">

			<input type="hidden" name="inpPeriodIdDelete" id="inpPeriodIdDelete">
		
		',

		'
			<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button><button type="submit" class="btn btn-danger" >Confirmer</button></form>
		'
	);
?>

<script type="text/javascript">
	
	function SetModalDelete()
	{
		var strBranchId = $('#inpBranchId').val();

		var intPeriodId = $('#inpPeriodId').val();


		$("#ModalDeleteConfirm #title").text("Supprimer le cours: " + strBranchId);

		

		$("#inpPeriodIdDelete").val(intPeriodId);

		

		$('#ModalDetail').modal('hide');

	}
</script>


<script type="text/javascript">
	$(document).ready(function() {
    $('#frmScheduleDelete').on('submit', function() {
        
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'text',
            success: function(result) 
            {
               	if(result == "true")
               	{
					$('#ModalDeleteConfirm').modal('hide');

					$('#slcFilterClass').select2("val", "<?php print($strClassID); ?>");

	    			$('#slcFilterColleague').select2("val", "<?php print($strColleagueID); ?>");

					$( "#frmFilter" ).submit();
				}
				else
				{
					$("#add-error").show();

				}
            }
        });

        return false;
    });	
});
</script>