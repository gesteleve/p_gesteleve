<?php
/****************************************************************************
Nom:		view.php
Auteur:		Ludovic Delafontaine - MIN3 2011-2015
Date:		20.01.2014
But:		Permet de récupérer et traiter les événements du calendrier
Sources:	- https://github.com/phurni/fullcalendar
*****************************************************************************
Modifications
Date  : XX.XX.XXXX
Auteur: -
Raison: -
A faire: -
****************************************************************************/
?>
<?php foreach ($calendar as $period): ?>
    <?php echo $period->number(); ?>e période<br>
    <?php echo $period->branch(); ?><br>
    Donné par : <?php echo $period->colleague_id(); ?>
    <hr>
<?php endforeach ?>
