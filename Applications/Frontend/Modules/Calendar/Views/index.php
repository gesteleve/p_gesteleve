<?php
/****************************************************************************
Nom:		index.php
Auteur:		Ludovic Delafontaine - MIN3 2011-2015
Date:		13.01.2014
But:		Page d'index du calendrier/horaire de l'ETML
Sources:	- https://github.com/phurni/fullcalendar
			- http://arshaw.com/fullcalendar/docs/agenda/allDaySlot/
*****************************************************************************
Modifications
Date  : 22.01.2014
Auteur: Ludovic Delafontaine - MIN3 2011-2015
Raison: Création de la page complète
A faire: Gérer les jours de la semaine
*****************************************************************************
Modifications
Date  : 23.01.2014
Auteur: Ludovic Delafontaine - MIN3 2011-2015
Raison: Création de la page totale
A faire: Gérer les jours de la semaine
****************************************************************************/

	if ($user->isAuthenticated()) 
	{ 

		$id = $this->app->user()->getAttribute('user')->id();

		print('

				<div id="wrap">
				    <div class="navbar navbar-fixed-top">
				        <div class="navbar-inner">
				            <div class="container-fluid">
				                 <div class="logo">
				                <a href="/">ETML - Gestion Elèves</a>
				            </div>
				            <?php // Bouton en haut à droit de l\'écran  ?>
				            <a class="btn btn-navbar visible-phone visible-tablet" data-toggle="collapse" data-target=".nav-collapse">
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </a>
				                    <ul class="pull-right nav">
				                      
				                     <li class="dropdown">
				                            <a class="dropdown-toggle connect" data-toggle="dropdown" href="#">
				                                <i class="icon-wrench icon-white"></i>
				                                Options
				                            </a>
				                   <ul class="dropdown-menu p10">
				                            
				                                <li>
				                                    <a href="/calendar/add_lesson/'.$id.'">Ajouter un cours</a>
				                                </li>

				     </ul></li>
				                        <li>
				                            <a href="???/logout">
				                                <i class="icon-off icon-white"></i>
				                                Déconnexion
				                            </a>
				                        </li>
				     
				                </ul>
				            </div>
				        </div>
				    </div>
				</div>

			');

		//$id = $user->getAttribute('user')->id();
	
		// Fichiers CSS concernant FullCalendar
		$this->html()->css('fullcalendar/fullcalendar.css');

		$this->html()->css('fullcalendar/jquery-ui.min.css');
		$this->html()->css('fullcalendar/jquery.ui.theme.css');

		//$this->html()->css('fullcalendar/bootstrap-datetimepicker.min.css');
		//$this->html()->css('fullcalendar/fullcalendar.print.css');
		// Fichiers javascript concernant FullCalendar
		//$this->html()->js('fullcalendar/jquery.min.js');
		$this->html()->js('fullcalendar/fullcalendar.js');
		
		//$this->html()->js('fullcalendar/bootstrap-datetimepicker.min.js');
		//$this->html()->js('fullcalendar/bootstrap-datetimepicker.fr.js');
		

		$this->html()->js('fullcalendar/jquery-ui-1.10.0.custom.min.js');
		$this->html()->js('fullcalendar/timepicker-test.js');
		
	} 
	

	// Défini l'horaire par rapport aux périodes
	$tab_Horaire = array(
		1 => array(
							// "8:00-8:45"
							"intBeginHour" => 8,
							"intBeginMin" => 00,
							"intEndHour" => 8,
							"intEndMin" => 45,
						),
		2 =>  array(
							// "8:50-9:35"
							"intBeginHour" => 8,
							"intBeginMin" => 50,
							"intEndHour" => 9,
							"intEndMin" => 35,
						),
		3 =>  array(
							// "9:50-10:35"
							"intBeginHour" => 9,
							"intBeginMin" => 50,
							"intEndHour" => 10,
							"intEndMin" => 35,
						),
		4 =>  array(
							// "10:40-11:25"
							"intBeginHour" => 10,
							"intBeginMin" => 40,
							"intEndHour" => 11,
							"intEndMin" => 25,
						),
		5 =>  array(
							// "11:30-12:15"
							"intBeginHour" => 11,
							"intBeginMin" => 30,
							"intEndHour" => 12,
							"intEndMin" => 15,
						),
		6 =>  array(
							// "12:20-13:05"
							"intBeginHour" => 12,
							"intBeginMin" => 20,
							"intEndHour" => 13,
							"intEndMin" => 05,
						),
		7 =>  array(
							// "13:10-13:55"
							"intBeginHour" => 13,
							"intBeginMin" => 10,
							"intEndHour" => 13,
							"intEndMin" => 55,
						),
		8 =>  array(
							// "14:00-14:45"
							"intBeginHour" => 14,
							"intBeginMin" => 00,
							"intEndHour" => 14,
							"intEndMin" => 45,
						),
		9 =>  array(
							// "14:55-15:40"
							"intBeginHour" => 14,
							"intBeginMin" => 55,
							"intEndHour" => 15,
							"intEndMin" => 40,
						),
		10 =>  array(
							// "15:50-16:35"
							"intBeginHour" => 15,
							"intBeginMin" => 50,
							"intEndHour" => 16,
							"intEndMin" => 35,
						),
		11 =>  array(
							// "16:40-17:25"
							"intBeginHour" => 16,
							"intBeginMin" => 40,
							"intEndHour" => 17,
							"intEndMin" => 25,
						),
		12 =>  array(
							// "17:30-18:15"
							"intBeginHour" => 17,
							"intBeginMin" => 30,
							"intEndHour" => 18,
							"intEndMin" => 15,
						),
	);
		
?>	


<script>

	$(document).ready(function() 
	{	
		initialise_calendar();
		
	});

	function initialise_calendar()
	{

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaWeek,agendaDay'
			},
			editable: false,
			selectable: true,
	
			defaultView: 'agendaWeek',
        
			firstDay: 1,	/* Spécifie que le premier jour de la semaine est le lundi (0=dimanche) */
			minTime: 8,  
			maxTime: 19, 

			/*slotEventOverlap: false,*/

			/* Locales - FR */
			monthNames: ['Janvier','F\u00e9vrier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','D\u00e9cembre'], /* Noms des mois */
			monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Juin','Juil','Aout','Sep','Oct','Nov','Dec'], /* Noms raccourcis des mois */
			dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'], /* Noms des jours */
			dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'], /* Noms raccourcis des jours */
			buttonText: { /* Textes des bouttons */
				today: 'Aujour.',
				month: 'Mois',
				week: 'Semaine',
				day: 'Jour'
			},

			/* Format des colonnes */
			columnFormat: {
				week: 'dddd' /* Affiche le nom complet des jours */
			},

			hiddenDays: [ 0 ], /* Cache le dimanche */
			
			dateFormat: "dd-mm-yyyy", /* Format de la date */
			timeFormat: 'HH:mm {- HH:mm}' , /* Format du temps (général) */
			axisFormat: 'HH:mm', /* Format du temps dans l'axe vertical */

			/*allDayText: 'tout les jours',*/

			eventRender: function (event, element) 	
			{
	            element.attr('href', 'javascript:void(0);');
	            element.attr('onclick', 'openModal("' + event.title + '","' + event.description + '","' + event.url + '","' + event.id + '", "' + event.start + '", "' + event.end + '", "' + event.date_start + '", "' + event.date_end + '");');
        	},

			events: [

				<?php

					$start_date = new DateTime("2014-01-01");

					$start_date = $start_date->getTimestamp();

					$end_date = new DateTime("2014-12-31");

					$end_date = $end_date->getTimestamp();

					//echo("<br/>".$start_date."<br/>".$end_date);
					
					//$monday = 1;

					$one_day = 86400;

					for ($i = $start_date; $i <= $end_date; $i = $i + $one_day) 
					{ 
						$column_date = date('m/d/Y', $i);

						$d = date('d', $i);

						$m = date('m', $i) - 1;
						
						$y = date('Y', $i);				

						/* Parcours l'horaire de la personne et génère les événements avec le titre, la date de début et de fin */
						for ($j=0; $j < count($calendar); $j++) 
						{
							if (date('w', strtotime($column_date)) == $calendar[$j]['day']) 
							{
								echo("
									{
										id: '".$calendar[$j]['id']."',
										title: '".$calendar[$j]['branch']."',
										start: new Date(".$y.", ".$m.", ".$d.", ".$tab_Horaire[$calendar[$j]['number']]['intBeginHour'].", ".$tab_Horaire[$calendar[$j]['number']]['intBeginMin']."),
										end: new Date(".$y.", ".$m.", ".$d.", ".$tab_Horaire[$calendar[$j]['number']]['intEndHour'].", ".$tab_Horaire[$calendar[$j]['number']]['intEndMin']."),	
										allDay: false,
										url: '-'
									},
								");
							}
						}
					}

					/* Affiche les events de l'utilisateur */
					for ($i=0; $i < count($events); $i++) 
					{
						echo("
							{
								id: '".$events[$i]['id']."',
								title: '".$events[$i]['title']."',
								start: '".$events[$i]['start']."',
								end: '".$events[$i]['end']."',	
								date_start: '".$events[$i]['start']."',
								date_end: '".$events[$i]['end']."',
								allDay: false,
								url: '-'
							},
						");
					}

					/* Affiche les events d'une classe */
					for ($i=0; $i < count($Classeevents); $i++) 
					{
						echo("
							{
								id: '".$Classeevents[$i]['id']."',
								title: '".$Classeevents[$i]['title']."',
								start: '".$Classeevents[$i]['start']."',
								end: '".$Classeevents[$i]['end']."',
								date_start: '".$events[$i]['start']."',
								date_end: '".$events[$i]['end']."',	
								allDay: false
							},
						");
					}
					
				?>

			]
		});
	}


	function openModal(title, info, url, id, start, end, date_start, date_end) 
	{

		$('form').find("input[name=StartDate]").each(function(ev)
		{
			if(!$(this).val()) 
			{ 
				$(this).attr("placeholder", date_start);
			}		
		});

		$('form').find("input[name=EndDate]").each(function(ev)
		{
			if(!$(this).val()) 
			{ 
				$(this).attr("placeholder", date_end);
			}		
		});

	    $('#myModal #title').text(date_start);

	    $('#StartDate').val(date_start);

	    $('#myModal #id').val(id);

	    /*$('#StartDate').datetimepicker({
	    	defaultDate: start,
			pickSeconds: false,
	 		language: 'fr'
	    }); 
	    
	    $('#EndDate').datetimepicker({
			pickSeconds: false,
	 		language: 'fr'
	    });*/

	    $('#myModal').modal('show');

	    $.datepicker.regional.fr = {
	        closeText: "Fermer",
	        prevText: "<Préc",
	        nextText: "Sui>",
	        currentText: "Aujourd'hui",
	        monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
	        monthNamesShort: ["Jan", "Fév", "Ma", "Avr", "Mai", "Jui", "Jul", "Aoû", "Sep", "Oct", "Nov", "Déc"],
	        dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
	        dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
	        dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
	        weekHeader: "Semaine",
	        dateFormat: "dd.mm.yy",
	        firstDay: 1
	    };
	    $.datepicker.setDefaults($.datepicker.regional.fr);

	    $('.date').datetimepicker({
	    	dateFormat: "dd-mm-yy",
	        timeFormat: "HH:mm",
	        minDateTime: new Date(),
	        stepMinute: 5,
	        hourMin: 8,
	        hourMax: 18,
	        timeText: "Temps",
	        hourText: "Heures",
	        minuteText: "Minutes",
	        currentText: "Aujourd'hui",
	        closeText: "Fermer",
	        showOtherMonths: true,
	        selectOtherMonths: true
	    });

	}



</script>

<div class="alert alert-error fade in" id="login-error" style="display:none;">
    <button type="button" class="close">×</button>
    Une erreur est survenue.
</div>

<div class="mt30">
  <div id="container">
	<div id="search-box">
		<?php // La div ci-dessous est la div qui permet la création du calendrier FullCalendar. ?>
		<div id="calendar" class="fc fc-ltr"></div>
	</div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><div id="title"></div></h4>
      </div>
      <div class="modal-body">
      
        <form id="monForm" action="<?php echo $this->html()->url('calendar/add'); ?>" method="post">
        	<fieldset>
        		<!--<label for="StartDate">Date de début</label>

        		<div id="StartDate" class="input-append date">
					<input id="StartDate" class="date" name="StartDate" data-format="yyyy-MM-dd hh:mm" type="text" disabled="true"></input>
					<span class="add-on">
						<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>
				</div>

        		<label for="EndDate">Date de fin</label>

        		<div id="EndDate" class="input-append date">
					<input id="EndDate" class="date" name="EndDate" data-format="yyyy-MM-dd hh:mm" type="text" disabled="true"></input>
					<span class="add-on">
						<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>
				</div>-->

				<input  type="text" name="CreatebeginDate" class="date" placeholder="Commencement..." required />
				<input type="text" name="CreateendDate" class="date" placeholder="Fin..." required/> </td>

				<!--
					<label for="Description">Description</label>
					<textarea name="Description" class="form-control" rows="3"></textarea>
				-->


				<input id="id" name="id" type="hidden">

				
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>

        </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">


	$(document).ready(function() {
    $('#monForm').on('submit', function() {
  
        var EndDate = $('#EndDate').attr("placeholder");

        $('#StartDate').attr("value", StartDate);
        $('#EndDate').attr("value", EndDate);
       
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'text',
            success: function(result) 
            {
               	if(result.match(/Success/g))
               	{
					$('#myModal').modal('hide');

					$('#calendar').fullCalendar('refetchEvents');
				}
				else
				{
					$("#login-error").show();

					$('#myModal').modal('hide');

					$('#calendar').fullCalendar('refetchEvents');
				}
            }
        });

        return false;
    });	
});
</script>









