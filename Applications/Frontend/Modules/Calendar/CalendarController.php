<?php

namespace Applications\Frontend\Modules\Calendar;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class CalendarController extends BackController
{
    public function executeIndex() 
    {
		
		// Récupère le manager du calendrier (fichier CalendarManager.php)
		$manager = $this->managers->getManagerOf('Calendar');
		
		$role = $this->app->user()->getAttribute('role');

        if ($role == 7) 
        {
            $calendar = $manager->getStudentCalendar($this->app->user()->getAttribute('user')->id());

            $events = $manager->getStudentEvents($this->app->user()->getAttribute('user')->id());
			
			$Classeevents = $manager->getSchoolClasseEvents($this->app->user()->getAttribute('user')->id());
        }
        else
        {
            $calendar = $manager->getColleagueCalendar($this->app->user()->getAttribute('user')->id());
        }

		$this->page->addVar('title', 'Mon horaire');
		$this->page->addVar('description', 'Horaire personnel');
		$this->page->addVar('keywords', 'horaire');

		$this->page->addVar('calendar', $calendar);
        $this->page->addVar('events', $events);
		$this->page->addVar('Classeevents', $Classeevents);
    }

    public function executeAdd(HTTPRequest $request) 
    {
        if($request->postsExists(array('StartDate', 'EndDate', 'id'))) 
        {
            if (!($request->postsEmpty(array('StartDate', 'EndDate', 'id')))) 
            {
                // Supprime le template par defaut de la page
                //$this->page->setLayout();

                $id = htmlentities($_POST['id']);
                $start = htmlentities($_POST['StartDate']);
                $end = htmlentities($_POST['EndDate']);

                // Récupère le manager du calendrier (fichier CalendarManager.php)
                $manager = $this->managers->getManagerOf('Calendar');


                if ($manager->updateEvent($id, $start, $end)) 
                {
                    echo "Success";
                }

                
            }
        }
    }

    public function executeView(HTTPRequest $request) 
    {
        if ($request->getExists('class')) {
            $this->page->addVar('title', 'Horaire - '.$request->getData('class'));
            $this->page->addVar('description', 'Horaire de la classe '.$request->getData('class'));
            $this->page->addVar('keywords', 'horaire '.$request->getData('class'));

            $manager = $this->managers->getManagerOf('Calendar');
            $calendar = $manager->getColleagueCalendar($this->app->user()->getAttribute('user')->id());


            $this->page->addVar('calendar', $calendar);
        }
    }
}
