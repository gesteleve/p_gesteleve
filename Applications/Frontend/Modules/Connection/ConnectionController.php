<?php


namespace Applications\Frontend\Modules\Connection;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class ConnectionController extends BackController
{
    protected $link;
    protected $login;
    protected $password;
    protected $ip = LDAP_IP;
    protected $domain = LDAP_DOMAIN;
    protected $root = LDAP_ROOT;

    public function executeLogin(HTTPRequest $request)
    {
		if ($request->postsExists(array('login', 'password')))
        {

            if ($this->app->user()->isAuthenticated())
            {
                $this->app->user()->setFlash('Vous êtes déjà connecté');
            }
            elseif ($request->postsEmpty(array('login', 'password')))
            {
                $this->app->user()->setFlash('Tous les champs ne sont pas renseignés');
            }
            else
            {
                $this->login = $request->postData('login');
                $this->password = $request->postData('password');

                $in_ldap = $this->checkLDAP();

                if ($in_ldap == null)
                {
                    $this->app->user()->setFlash('La connexion LDAP a échouée', 'error');
                    $this->app->httpResponse()->redirect($request->httpReferer());
                }
                elseif (!$in_ldap)
                {
                    $this->app->user()->setFlash('Vos identifiants sont incorrects', 'error');
                    $this->app->httpResponse()->redirect($request->httpReferer());
                }
                else
                {
                    /*
                    $val = @ldap_get_values($this->link, @ldap_first_entry($this->link, @ldap_search ($this->link, $this->root, 'sAMAccountName=' . $this->login)), "distinguishedName");
                    $tmp = explode(',', $val[0]);
                    $role = substr($tmp[1], strpos($tmp[1], '=') + 1);
                    */

                    $manager = $this->managers->getManagerOf('Colleague');

                    $user = $manager->getUnique($this->login);

					$role = 2;

					$group = $manager->getGroup($this->login);

					$right = $manager->getRight($this->login);


					if (!$user)
					{
						$manager = $this->managers->getManagerOf('Student');
						$user = $manager->getUnique($this->login);
						$role = 7;
						$group = null;
						$right = $manager->getRight($this->login);

						if (!$user)
						{
							$this->app->user()->setFlash('Vous n\'avez pas accès à cette application', 'error');
							$this->app->httpResponse()->redirect($this->page->html()->url($request->httpReferer()));
						}
					}


					$this->app->user()->setAttribute('user', $user);
					$this->app->user()->setAttribute('role', $role);
					$this->app->user()->setAttribute('group', $group);
					$this->app->user()->setAttribute('right', $right);
					$this->app->user()->setAuthenticated();





                }
            }

        }

        $this->app->httpResponse()->redirect($request->httpReferer());
    }

    public function executeLogout(HTTPRequest $request)
    {
        $this->app->user()->setAuthenticated(false);
        $this->app->user()->unsetAttribute('right');
        $this->app->httpResponse()->redirect($request->httpReferer());
    }

    private function checkLDAP()
    {
        // remove when using ldap srv
        return true;

        $this->link = ldap_connect($this->ip);

        if (!$this->link)
        {
            return null;
        }

        ldap_set_option($this->link, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->link, LDAP_OPT_REFERRALS, 0);

        $handle = ldap_bind($this->link, $this->login.$this->domain, $this->password);

        if (!$handle)
        {
            return false;
        }

        return true;
    }
}
