<?php

namespace Applications\Frontend\Modules\Group;

use Applications\Entities\Group;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class GroupController extends BackController
{
     function executeIndex(HTTPRequest $request) {
        // On vérifie que l'utilisateur est authentifié
        if ($this->app->user()->isAuthenticated()) {

            // Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');
            // On Vérifie que ce soit quelqu'un qui ait les droits d'ajout
            if (@$right[FOLLOW] & ADD) {
                $manager = $this->managers->getManagerOf('Group');
				$smanager = $this->managers->getManagerOf('Student');
                // Récupère la liste de suivis concernant l'élève
                $groups = $manager->getListByColleague($this->app->user()->getAttribute('user')->id());
				$students = $manager->getStudents();
				$studentsAll = $smanager->getList();
				$userid = $this->app->user()->getAttribute('user')->id();
                $this->page->addVar('title', 'Mes groupes');
                $this->page->addVar('description', 'Liste des groupes d\'un collaborateur de l\'ETML');
                $this->page->addVar('keywords', 'liste groupes');
				$this->page->addVar('userid', $userid);	
                $this->page->addVar('groups', $groups);
				$this->page->addVar('students', $students);
				$this->page->addVar('studentsAll', $studentsAll);
            }
            else {
                $this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                // Redirige l'utilisateur vers la page précédente
                $this->app->httpResponse()->redirect($request->httpReferer());
            }
        }
        else {
            $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            // Redirige l'utilisateur vers la page précédente
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
     }
	 
	 function executeAdd(HTTPRequest $request) {

            if ($this->app->user()->isAuthenticated()) {

                // Récupère les droits de l'utilisateur
                $right = $this->app->user()->getAttribute('right');
                // Vérifie que l'utilisateur ait les droits d'ajout
                if(@$right[FOLLOW] & ADD) {
					$manager = $this->managers->getManagerOf('Group');
					
                    $students = json_decode($_POST['students']);
			
					$colleague_id = $_POST['colleague_id'];
					$name = $_POST['name'];
					$content = $_POST['content'];
					$isPublic = $_POST['isPublic'];
					
					$manager->createGroup($colleague_id, $name, $content, $isPublic);
					$lastGroup = $manager->getLastIdByColleague($colleague_id);
					$groupID = $lastGroup[0]->id();
					foreach($students as $student){
						$manager->addUserGroup($groupID, $student);
					}

					//$this->app->httpResponse()->redirect($request->httpReferer());
                }
                else {
                    //$this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
                }
            }
            else {
                //$this->app->user()->setFlash('Vous devez vous authentifier', 'error');
            }
            // Redirige l'utilisateur vers la page précédente
            //$this->app->httpResponse()->redirect($request->httpReferer());
    }
	 
	function executeView(HTTPRequest $request) {
		if ($request->getExists('id')) {
			// On vérifie que l'utilisateur est authentifié
			if ($this->app->user()->isAuthenticated()) {

            // Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');
            // On Vérifie que ce soit quelqu'un qui ait les droits d'ajout
				if (@$right[FOLLOW] & ADD) {
					$right = $this->app->user()->getAttribute('right');
					$manager = $this->managers->getManagerOf('Group');
					$smanager = $this->managers->getManagerOf('Student');
					
					$group = $manager->getGroupById($request->getData('id'));
					$studentsInGroup = $manager->getStudentsByGroup($request->getData('id'));
					
					$this->page->addVar('group', $group[0]);
					$this->page->addVar('students', $studentsInGroup);
				}
			}
			else {
				$this->app->user()->setFlash('Vous devez vous authentifier', 'error');
				// Redirige l'utilisateur vers la page précédente
				$this->app->httpResponse()->redirect($request->httpReferer());
			}
		}
    }
	function executeDelete(HTTPRequest $request) {
		if ($request->getExists('id')) {
			// On vérifie que l'utilisateur est authentifié
			if ($this->app->user()->isAuthenticated()) {
					// Récupère les droits de l'utilisateur
					$right = $this->app->user()->getAttribute('right');
					if (@$right[FOLLOW] & ADD) {
					$manager = $this->managers->getManagerOf('Group');
					
					$group = $manager->getGroupById($request->getData('id'));
					$is_author = ($this->app->user()->getAttribute('user')->id() == $group[0]->colleague_id()) ? 1 : 0;
					if($is_author)
					{
						$manager->removeAllGroup($request->getData('id'));
						$manager->removeGroup($request->getData('id'));	
					}
					$this->app->httpResponse()->redirect($request->httpReferer());
				}
				
			}
			else {
				$this->app->user()->setFlash('Vous devez vous authentifier', 'error');
				// Redirige l'utilisateur vers la page précédente
				$this->app->httpResponse()->redirect($request->httpReferer());
			}
		}
		
    }
	function executeEdit(HTTPRequest $request)
	{
		if ($this->app->user()->isAuthenticated()) {

            // Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');
            // Vérifie que l'utilisateur ait les droits d'ajout
            if(@$right[FOLLOW] & ADD) {
				$manager = $this->managers->getManagerOf('Group');
				
                $students = json_decode($_POST['students']);
				$gId = $_POST['id'];
				$name = $_POST['name'];
				$content = $_POST['content'];
				$isPublic = $_POST['isPublic'];
		
				$manager->updateGroup($gId, $name, $content, $isPublic);
				$manager->removeAllGroup($gId);
				foreach($students as $student){
					$manager->addUserGroup($gId, $student);
				}

				//$this->app->httpResponse()->redirect($request->httpReferer());
            }
            else {
                //$this->app->user()->setFlash('Vous n\'avez pas le droit de faire ceci', 'error');
            }
        }
        else {
            $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
        }
	}
	
	function executePublic(HTTPRequest $request)
	{
		if ($this->app->user()->isAuthenticated()) {
			// Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');
            // On Vérifie que ce soit quelqu'un qui ait les droits d'ajout
            if (@$right[FOLLOW] & ADD) {
				$manager = $this->managers->getManagerOf('Group');
				$smanager = $this->managers->getManagerOf('Student');
				
				$groups = $manager->getListPublic();
				
				$students = $manager->getStudents();
				$studentsAll = $smanager->getList();
				$userid = $this->app->user()->getAttribute('user')->id();
                $this->page->addVar('title', 'Mes groupes');
                $this->page->addVar('description', 'Liste des groupes d\'un collaborateur de l\'ETML');
                $this->page->addVar('keywords', 'liste groupes');
				$this->page->addVar('userid', $userid);	
                $this->page->addVar('groups', $groups);
				$this->page->addVar('students', $students);
				$this->page->addVar('studentsAll', $studentsAll);
			}
		}
        else {
            $this->app->user()->setFlash('Vous devez vous authentifier', 'error');
        }
	}
	
}
