﻿<?php
//*********************************************************
// Societe: ETML
// Auteur : Diego Villagrasa
// Date : inconnue
// But : Afficher les groupes
//*********************************************************

 if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); 


//Cr?tion du menu option pour le controller student avec comme parametre l'id du student
//avec les options imprimer
$menu=array(
    "follow","","",@FOLLOW,$right,
    '<i class="icon-print"></i> Imprimer',"","Modal","PrintModal",@VIEW_ALL
    );

echo $this->html()->optionmenu($menu);
} 

?>  
       <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui-1.10.0.custom.min.js"></script>                     
  <!-- 
Modal pour impression 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur imprimer dans le menu option
-->
<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

 <!-- Titre de la modal box -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Impression</h4>
      </div>

      <div class="modal-body">
    

 <!-- Liste d?oulante format police -->
        <div class="btn-group">
          <select id="tailleList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('tailleList').value);">
            <option selected="selected">Format de texte</option>
            <option value="x-small">Très petit</option>
            <option value="small">Petit</option>
            <option value="medium">Moyen</option>
            <option value="large">Grand</option>
            <option value="x-large">Très Grand</option>
          </select>
        </div>

<!-- Liste d?oulante police -->
        <div class="btn-group">
          <select id="policeList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('policeList').value);">
            <option selected="selected">Police</option>
            <option value="TNR">Times New Roman</option>
            <option value="arial">Arial</option> 
            <option value="CenturyGothic">Century Gothic</option>
            <option value="EcoFont">EcoFont</option>    
          </select>
        </div>
<br/><br/>
<!-- Liste d?oulante en-tete et pied de page -->
        <div class="btn-group">
          <select id="enteteList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('enteteList').value);">
            <option selected="selected">En-tete</option>
            <option value="ETPPyes">oui</option>
            <option value="ETPPno">non</option>  
          </select>
        </div>


        <br/><br/>
      </div>
 <!-- Bouton modal box (fermer,imprimer) -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrint();">Imprimer</button>
      </div>

    </div>
  </div>
</div>
<script src="/css/jquery-ui/jquery-ui.js"></script>     
<!-- Fin de la modal box d'impression -->           
<div class="m10">
    <div class="widget-header"><i class="icon-user"></i>
        <h5><?php echo $this->h('Mes Groupes'); ?></h5>
		<a id="addbtn" href="#addGroup" role="button" class="pull-right btn btn-small" style="margin: 10px 5px 0 8px;" data-toggle="modal"><i class="icon-plus"></i>&nbsp;Ajouter un groupe</a>
    </div>

    <div class="widget-body clearfix">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><?php echo $this->h('Nom'); ?></th>
                    <th><?php echo $this->h('Description'); ?></th>
					<th><?php echo $this->h('Eleves'); ?></th>
					<th style="width: 40px;"><?php echo $this->h('Publique'); ?></th>
                    <th class="print-hidden"></th>
                </tr>
            </thead>
            <tbody>
			
                <?php foreach($groups as $j=>$group): $i= 0;?>
                <tr>
                    <td style="width: 150px;"><a href="<?php echo $this->html()->url('group/'.$group->id()); ?>"><?php echo $group->name(); ?></a></td>
					<td><?php echo $group->content(); ?></td>
					<td style="width: 300px;">
					
					<?php foreach($students as $k=>$student){
						
						if($student->group_id() == $group->id()){ 
						$photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';
						?>
						
						<?php
							if($i < 3){
							$i++;?>
							<a href="/student/<?php echo $student->id();?>">
							
							<?php if (file_exists(WEBROOT.$photo)) {
									?><img style="width: 50px; margin-left: 10px;" src="<?php echo $this->html()->url($photo); ?>"><?php
								} else {
									?><img style="width: 50px; margin-left: 10px;" src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
								}?>
								
							</a>
							<?php }else
							{
								
								$i++;
							}				?>
						<?php }} if($i > 3){?><a href="/group/<?php echo $group->id();?>"> <?php echo ($i-3)." autres";?></a><?php } ?>
					</td>
					<td>
						<p style="text-align: center;margin-top: 40%;"><?php if($group->isPublic() == 0)
						{
							echo "Non";
						}
						else
						{
							echo "Oui";
						}?>
						</p>
					</td>
                    <td style="width: 100px;padding-top: 25px;" class="print-hidden">
                    	<a role="button" data-toggle="modal" href="#addGroup" data-id="<?php echo $group->id(); ?>" class="btn mod mod-edit"><i class="icon-pencil"></i></a>
                    	<a href="<?php echo $this->html()->url('group/delete/'.$group->id()); ?>" class="btn mod" onclick="return confirm('Voulez ous vraiment supprimer ce groupe ?')"><i class="icon-trash"></i></a>
                    </td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
	<script>
	var groups = new Array();
	<?php foreach($groups as $group)
	{
		?>
		groups[<?php echo $group->id();?>] = new Array();
		groups[<?php echo $group->id();?>][0] = "<?php echo $group->name();?>";
		groups[<?php echo $group->id();?>][1] = "<?php echo $group->content();?>";
		groups[<?php echo $group->id();?>][3] = "<?php echo $group->isPublic();?>";
		groups[<?php echo $group->id();?>][2] = new Array();
		<?php foreach($students as $stud)
		{
			if($stud->group_id() == $group->id()){
			?>
			groups[<?php echo $group->id();?>][2].push("<?php echo $stud->id();?>");
			<?php
			}
		}
	}
	?>
	 console.log(groups);
	</script>
    <div id="addGroup" class="modal modal-group hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Ajout d'un groupe</h3>
            </div>
            <div class="modal-body modal-body-group" id="main-zone">
				<div>
					<label>Nom</label>
					<input id="name" type="text" name="name" tabindex="1" class="input-block-level" value="" required="">
					<label>Description</label>
					<textarea id="content" type="text" style="resize: none;" name="content" tabindex="1" class="input-block-level"></textarea>
				</div>
				<div style="width: 330px; float: left;">
					<h4 style="text-align: center;">Selectionnez les eleves</h4>
					
					<div class="filter-group">
						<p style="float: left; margin-left: 10px; margin-top: 4px;">Section:</p>
						<select id="sectionSelect" onchange="selectSection();">
							<option value="1">Automaticien-ne</option>
							<option value="2">Ebéniste</option>
							<option value="3">Ebéniste - Menusier-ère</option>
							<option value="4">Electronicien-ne</option>
							<option value="5">ES - Ecole supérieur</option>
							<option value="6">Informaticien-ne</option>
							<option value="7">Mécatronicien-ne d'automobiles</option>
							<option value="8">Menuisier-ère</option>
							<option value="9">MPT post CFC</option>
							<option value="10">Polymécanicien-ne</option>
							<option value="11">Préapprentissage</option>
						</select>
						<p style="float: left; margin-left: 10px; margin-top: 9px;">Classe:</p>
						<select id="classSelect" onchange="selectSection2();">
							<option value="1">CAM1</option>
							<option value="2">CAM2</option>
							<option value="3">CAM3</option>
							<option value="4">CAM4</option>
							<option value="5">MAM1</option>
							<option value="6">MAM2</option>
							<option value="7">MAM3</option>
							<option value="8">MAM4</option>
						</select>
					</div>
					
					<div class="group-selector" id="selector">
					<?php foreach($studentsAll as $student){ ?>
						<div id="<?php echo $student->id(); ?>" class="group-student <?php echo $student->school_class_id() ?>">
						<?php
										$imageUrl = "img/nophoto.jpg";
									
										if (file_exists("img/students/".$student->entry_date()."/".$student->id().".jpg")) { 
											$imageUrl = "img/students/".$student->entry_date()."/".$student->id().".jpg";
										}
						?>
							<div class="group-student-picture" style="background-image: url(<?php print($imageUrl);?>)">
								
							</div>
							
							<div style="max-width: 150px;" class="group-student-name">
								<p><?php echo $student->first_name()." ".$student->name() ?></p>
							</div>
							<div class="delete-group" style="float: right; margin-top: 20px; margin-right: 10px; display: none;">
								<a class="btn mod" onclick="removeStudent('<?php echo addslashes($student->id()); ?>');"><i class="icon-remove"></i></a>
							</div>
						</div>
						<div class="group-border <?php echo $student->school_class_id() ?>"></div><?php
					} ?>
					</div>
				</div>
				<img src="/img/arrow.png"/ style="width: 100px; margin-left: 85px; margin-top: 150px;">
				<div class="group-final">
					<h4 style="text-align: center;">Deposez les eleves</h4>
					<div id="receiver" class="group-selector" style="height: 330px;">
					
						
					</div>
				</div>
                <input type="hidden" id="colleague_id" value="<?php echo $userid; ?>"/>
            </div>
            <div class="modal-footer modal-footer-group">
				<div style="float: left; margin-top: 5px;" class="checkbox"><input type="checkbox" name="public" id="public" value="0"><label for="public">Rendre le groupe public</label></div>
				<p id="counter">Aucun élève dans le groupe</p>
                <button id="addBtn" class="btn btn-primary" onclick="createGroup();" type="submit">Ajouter</button>
            </div>
        </div>
</div>



<script>
$(document).on("click", "#addbtn", function () {
	$(".modal-header #myModalLabel").text("Ajouter un groupe");
	 $(".modal-footer .btn").remove();
	 $(".modal-footer").append("<button id='addBtn' class='btn btn-primary' onclick='createGroup();' type='submit'>Ajouter</button>");
     $(".modal-body #name").val("");
     $(".modal-body #content").val("");
	 $('#receiver').empty();
		 $('#selector').children('div').each(function () {
			$(this).css("opacity", 1);
			$(this).draggable({helper: 'clone',
				appendTo: '#main-zone'
			});
			$(this).draggable("enable");
			
		 });
		counter = 0;
		$("#counter").text("Aucun élève dans le groupe");
});
var done = false;
$(document).on("click", ".mod-edit", function () {
     var Id = $(this).data('id');
	 console.log(Id);
	 $(".modal-header #myModalLabel").text("Modifier un groupe");
	 $(".modal-footer .btn").remove();
	 $(".modal-footer").append("<button id='addBtn' class='btn btn-primary' onclick='editGroup(" + Id +");' type='submit'>Modifier</button>");
     $(".modal-body #name").val(groups[Id][0]);
     $(".modal-body #content").val(groups[Id][1]);
	 if(groups[Id][3] == 1)
	 {
		$('#public').prop('checked', true);
	 }
	 else
	 {
		 $('#public').prop('checked', false);
	 }
	 
	 $(".modal-footer #public").val(groups[Id][1]);
	 var j = 0;
	 if(done)
	 {
		 $('#receiver').empty();
		 $('#selector').children('div').each(function () {
			$(this).css("opacity", 1);
			$(this).draggable({helper: 'clone',
				appendTo: '#main-zone'
			});
			$(this).draggable("enable");
			counter = 0;
		 });
	 
	 }
	 done = true;
	 for (j = 0; j < groups[Id][2].length; j++)
	 {
		 
	 	$("#selector #" + groups[Id][2][j]).clone().appendTo("#receiver");
		$("#receiver #" + groups[Id][2][j]).css("display", "block");
	 	var received = document.getElementById("receiver").getElementsByClassName("delete-group");
	 	var i;
	 	for (i = 0; i < received.length; i++)
	 	{
	 		received[i].style.display = "block";
	 	}
	 	updateCount(true);
	 	
	 	document.getElementById(groups[Id][2][j]).style.opacity = 0.2;
	 	$('#' + groups[Id][2][j]).draggable( "disable" );
	 }
		
	 
	 
});
</script>

<script>

function selectSection()
{
	
	console.log($( "#sectionSelect" ).val());
	switch($( "#sectionSelect" ).val()) {
    case "1":
		var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
        $('#classSelect').append($('<option>', {
			value: 1,
			text: 'CAM1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CAM2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CAM3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CAM4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MAM1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MAM2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MAM3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MAM4'
		}));

        break;
    case "2":
        var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CEB3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CEB4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MEB3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MEB4'
		}));
		break;
	case "3":
       var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CEM1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CEM2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CEM3'
		}));
		
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MEM1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MEM2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MEM3'
		}));
        break;
    case "4":
        var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
			$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CLO1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CLO2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CLO3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CLO4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MLO1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MLO2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MLO3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MLO4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'FLO1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'FLO2'
		}));
        break;
	case "5":
        var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
			$('#classSelect').append($('<option>', {
			value: 1,
			text: 'SIG1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'SIG2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'SLO1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'SLO2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'SME1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'SME2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'STE1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'STE2'
		}));
        break;
    case "6":
        var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
			$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN1A'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN1B'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN-CID2A'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN-CID2B'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN3A'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN3B'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN4A'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CIN4B'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MIN1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MIN-MID2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MIN3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MIN4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'FIN1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'FIN2'
		}));
        break;
	case "7":
        var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CMA1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CMA2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CMA3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CMA4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MMA1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MMA2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MMA3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MMA4'
		}));
        break;
    case "8":
        var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CMN3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CMN4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MMN3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MMN4'
		}));
        break;
	case "9":
        var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MTU1A'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MTU1B'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MTU1C'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MTU1D'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MTU1E'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MTU2E'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MT151'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MT152'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MT251'
		}));
        break;
    case "10":
		var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CPM1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CPM2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CPM3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'CPM4'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MPM1'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MPM2'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MPM3'
		}));
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'MPM4'
		}));
        break;
	case "11":
		var Node = document.getElementById("classSelect");
		while (Node.firstChild) {
			Node.removeChild(Node.firstChild);
		}
		$('#classSelect').append($('<option>', {
			value: 1,
			text: 'TPA1'
		}));
        break;
	}
			selectSection2();
}
	
</script>

<script>
$('#selector').addEventListener('mousedown',function(e) {
    
});
$('#selector').addEventListener('mouseup',function(e) {
	
});
$('#selector').addEventListener('mouseenter',function(e) {
	$('#selector').css( 'cursor', '-webkit-grab');
});
$('#selector').addEventListener('mouseleave',function(e) {
	
});
</script>

<script>

 $(function() {
    $( ".group-student" ).draggable({helper: 'clone',
		appendTo: '#main-zone'
	});

    $( "#receiver" ).droppable({
      drop: function( event, ui ) {
		
	    ui.draggable.clone().appendTo("#receiver");
		var received = document.getElementById("receiver").getElementsByClassName("delete-group");
		var i;
		for (i = 0; i < received.length; i++)
		{
			received[i].style.display = "block";
		}
		updateCount(true);
		var draggableId = ui.draggable.attr("id");
		document.getElementById(draggableId).style.opacity = 0.2;
		$('#' + draggableId).draggable( "disable" );
      }
    });
  });
  </script>

<script>
function selectSection2()
{
	
	var sel = $("#classSelect option:selected").html();
	var x = document.getElementById("selector").childNodes;
	var i;
	
	for (i = 0; i < x.length; i++) {
		var found = false;
		var classList = x[i].className.split(/\s+/);
		for (var y = 0; y < classList.length; y++) {
			if (classList[y] === sel) {
				x[i].style.display = "block";
				found = true;
			}
			else if(!found)
			{
				x[i].style.display = "none";
			}
		}
		
	}
	console.log(sel);
	$("#selector").slider('value',0);
	$('#selector').slider('refresh');
	
}
selectSection2();
</script>

<script>
var counter = 0;
function updateCount(add)
{
	if(add)
	{
		counter++;
	}
	else
	{
		counter--;
	}
	
	if(counter === 1)
	{
		$("#counter").text(counter + " élève dans le groupe");
	}
	else
	{
		$("#counter").text(counter + " élèves dans le groupe");
	}
	
}
</script>

<script>
function removeStudent(id)
{
	$('#receiver #' + id).remove();
	document.getElementById(id).style.opacity = 1;
	$('#' + id).draggable( "enable" );
	updateCount(false);
}
</script>

<script>
function createGroup()
{
	
	var name = $('#name').val();
	if(name != "")
	{
		$("#addBtn").off('click');
		var content = $('#content').val();
		var students = [];
		var colleague_id = $('#colleague_id').val();
		var is_public = $("#public").is(':checked');
		var isPublicB = 0;
		if(is_public === true)
		{
			isPublicB = 1;
		}
		$('#receiver').children('div').each(function () {
			students.push($(this).attr('id'));
		});
		
		var data = {'colleague_id':colleague_id, 'name':name, 'content':content,'isPublic':isPublicB, 'students':JSON.stringify(students)};
		
		$.ajax({
			type:"POST",
			url:"./groups/add",
			data: data,
			dataType : 'text',

		}).done(function(back){
			location.reload();
		});
		
	}
	else
	{
		$('#name').focus();
	}
	
	
}

function editGroup(id)
{
	var name = $('#name').val();
	if(name != "")
	{
		$("#addBtn").off('click');
		var is_public = $("#public").is(':checked');
		var isPublicB = 0;
		if(is_public === true)
		{
			isPublicB = 1;
		}
		var content = $('#content').val();
		var students = [];
		$('#receiver').children('div').each(function () {
			students.push($(this).attr('id'));
		});
		
		var data = {'id':id, 'name':name, 'content':content, 'isPublic':isPublicB, 'students':JSON.stringify(students)};
		
		$.post('./groups/edit', data, function(returnedData) {
			location.reload();
		});
			
	}
	else
	{
		$('#name').focus();
	}
}
</script>
