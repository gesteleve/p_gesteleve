<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); } 
?>

<div id="content">
    <div id="group">
		<div>
			<h1><?php  echo $group->name(); ?></h1>
        </div>
		
		<p id="description"><?php echo $group->content(); ?></p>
		
		<div id="students_list">
			<?php
				foreach($students as $student){
					$photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';
					
					?>
					<div class="post">
					<a href="<?php echo $this->html()->url('student/'.$student->id()); ?>">
						<div class="picture">
							<?php
								if (file_exists(WEBROOT.$photo)) {
									?><img src="<?php echo $this->html()->url($photo); ?>" alt=""><?php
								} else {
									?><img src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
								}
							?>
						</div>
						</a>
						<div class="information">
							
							<h5><a class="info-link" href="<?php echo $this->html()->url('student/'.$student->id()); ?>"><?php echo $student->name(); ?> <?php echo $student->first_name(); ?></a></h5>
							
							<p><a class="info-link" href="<?php echo $this->html()->url('student/'.$student->id()); ?>"><?php echo $student->id(); ?></a></p>
							<p><a class="info-link" href="<?php echo $this->html()->url('class/'.$student->school_class_id()); ?>"><?php echo $student->school_class_id(); ?></a></p>
							<p><a class="info-link" href="<?php echo $this->html()->url('profession/'.$student->profession_id()); ?>"><?php echo $student->profession_name(); ?></a></p>
						</div>
					</div>
					
					<?php
				}
			?>
			
			
			
			
			
		</div>
		
	</div>
</div>