<?php
//*********************************************************
// Societe: ETML
// Auteur : Lanz Romain
// Date : inconnue
// But : Afficher les suivis
//*********************************************************
// Modifications:
// Date : 20.05.2014
// Auteur : Lukyantsev  Vladislav
// Raison : Ajout du lien imprimer dans le menu option + ajouter la modalbox d'impression
//          ainsi que les class css "print-hidden" afin de masquer les elements non voulu lors d el'impression
//          Ajout de la gestion des droits dans le menu option
//*********************************************************

 if ($user->isAuthenticated()) { $right = $user->getAttribute('right'); 


//Cr�ation du menu option pour le controller student avec comme parametre l'id du student
//avec les options imprimer
$menu=array(
    "follow","","",@FOLLOW,$right,
    '<i class="icon-print"></i> Imprimer',"","Modal","PrintModal",@VIEW_ALL
    );

echo $this->html()->optionmenu($menu);
} 

?>  
       <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>  		
  <!-- 
Modal pour impression 
Cette modal box s'ouvrira lorsque l'utilisateur cliquera sur imprimer dans le menu option
-->
<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

 <!-- Titre de la modal box -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Impression</h4>
      </div>

      <div class="modal-body">
    

 <!-- Liste d�roulante format police -->
        <div class="btn-group">
          <select id="tailleList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('tailleList').value);">
            <option selected="selected">Format de texte</option>
            <option value="x-small">Très petit</option>
            <option value="small">Petit</option>
            <option value="medium">Moyen</option>
            <option value="large">Grand</option>
            <option value="x-large">Très Grand</option>
          </select>
        </div>

<!-- Liste d�roulante police -->
        <div class="btn-group">
          <select id="policeList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('policeList').value);">
            <option selected="selected">Police</option>
            <option value="TNR">Times New Roman</option>
            <option value="arial">Arial</option> 
            <option value="CenturyGothic">Century Gothic</option>
            <option value="EcoFont">EcoFont</option>    
          </select>
        </div>
<br/><br/>
<!-- Liste d�roulante en-tete et pied de page -->
        <div class="btn-group">
          <select id="enteteList" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" onchange="changeMediaStyle(document.getElementById('enteteList').value);">
            <option selected="selected">En-tete</option>
            <option value="ETPPyes">oui</option>
            <option value="ETPPno">non</option>  
          </select>
        </div>


        <br/><br/>
      </div>
 <!-- Bouton modal box (fermer,imprimer) -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary" onclick="optionPrint();">Imprimer</button>
      </div>

    </div>
  </div>
</div>
<script src="/css/jquery-ui/jquery-ui.js"></script>     
<!-- Fin de la modal box d'impression -->           
<div class="m10">
    <div class="widget-header"><i class="icon-user"></i>
        <h5><?php echo $this->h('Groupes Publics'); ?></h5>
		<h5 style="float:right;margin-right:10px;"><a onclick="toggleFilter();"><?php echo $this->h('Filtres '); ?><i class="icon-filter"></i></a></h5>
	</div>

    <div class="widget-body clearfix">
        <table id="public_table" class="table table-striped">
            <thead>
                <tr>
                    <th><?php echo $this->h('Nom'); ?></th>
                    <th><?php echo $this->h('Description'); ?></th>
					<th><?php echo $this->h('Eleves'); ?></th>
					<th><?php echo $this->h('Créateur'); ?></th>
                </tr>
            </thead>
            <tbody>
			
                <?php foreach($groups as $j=>$group): $i= 0;?>
                <tr>
                    <td style="width: 150px;"><a href="<?php echo $this->html()->url('group/'.$group->id()); ?>"><?php echo $group->name(); ?></a></td>
					<td><?php echo $group->content(); ?></td>
					<td style="width: 300px;">
					
					<?php foreach($students as $k=>$student){
						
						if($student->group_id() == $group->id()){ 
						$photo = 'img/students/'.$student->entry_date().'/'.$student->id().'.jpg';
						?>
						
						<?php
							if($i < 3){
							$i++;?>
							<a href="/student/<?php echo $student->id();?>">
							
							<?php if (file_exists(WEBROOT.$photo)) {
									?><img style="width: 50px; margin-left: 10px;" src="<?php echo $this->html()->url($photo); ?>"><?php
								} else {
									?><img style="width: 50px; margin-left: 10px;" src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
								}?>
								
							</a>
							<?php }else
							{
								
								$i++;
							}				?>
						<?php }} if($i > 3){?><a href="/group/<?php echo $group->id();?>"> <?php echo ($i-3)." autres";?></a><?php } ?>
					</td>
                    <td style="width: 100px;">
						<a href="/colleague/<?php echo $group->colleague_id();?>"> <?php echo $group->colleague_id(); ?></a>
                    </td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>

<script>
var isFilterVisible = false;

function showFilters()
{
	var table_Props = {
		col_0: "none",
		col_1: "none",
		col_2: "none",
		col_3: "Select",
		display_all_text: "[Tout]",
		sort_select: true
	};
	var tf2 = setFilterGrid("public_table", table_Props);
	$('.fltrow').hide();
}

function toggleFilter(){
	if(isFilterVisible){
		$('.fltrow').slideUp( 0, "swing", function() {
			isFilterVisible = false;
		});
		
	}else{
		$('.fltrow').slideDown( 0, "swing", function() {
			isFilterVisible = true;
		});
	} 
}

window.onload = showFilters;
</script>