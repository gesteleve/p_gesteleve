<?php

namespace Applications\Frontend\Modules\Profession;

use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class ProfessionController extends BackController
{
    function executeIndex() 
    {
        $this->page->addVar('title', 'Liste des professions');
        $this->page->addVar('description', 'Liste des profession de l\'ETML');
        $this->page->addVar('keywords', 'liste, profession');

        $professions = $this->managers->getManagerOf('Profession')->getListSection();

        $this->page->addVar('professions', $professions);
    }

    function executeView(HTTPRequest $request) 
    {
        if ($request->getExists('id')) 
        {
            $professions = $this->managers->getManagerOf('Profession')->getUnique($request->getData('id'));
            $this->page->addVar('professions', $professions);      
        

            $schoolClasses = $this->managers->getManagerOf('Profession')->getListSchoolClass($professions->id()); 
            $this->page->addVar('schoolClasses', $schoolClasses);

            $colleagues = $this->managers->getManagerOf('Colleague')->getList();
            $this->page->addVar('colleagues', $colleagues);

            $students = $this->managers->getManagerOf('Profession')->getStudents();
            $this->page->addVar('students', $students);    
        }
    }

    function executeAdd(HTTPRequest $request) {

    }

    function executeEdit(HTTPRequest $request) {

    }

    function executeDelete(HTTPRequest $request) {

    }
}
