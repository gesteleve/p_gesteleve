<div class="m10">
    <div class="widget-header"><i class="icon-th-list"></i>
        <h5><?php echo $this->h('Profession'); ?></h5>
    </div>

    <div class="widget-body clearfix">
        <table class="table table-striped">
            
            <tbody>

                <?php foreach ($professions as $profession): ?>
                <tr>
                    <td><a href="<?php echo $this->html()->url('profession/'.$profession->id()); ?>"><?php echo $profession->name(); ?></a></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
