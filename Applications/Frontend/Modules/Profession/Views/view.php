<?php if ($user->isAuthenticated()) { $right = $user->getAttribute('right');

}?>

<div id="content">
    <?php 

        echo "<h2>".$professions->name()."</h2>";
        //print_r($schoolClasses);
        //print_r($colleagues);

        foreach ($schoolClasses as $schoolClasse) 
        {
            ?>

            <div class="span">

                <?php                   

                /*foreach ($colleagues as $colleague) 
                {
                    print($schoolClasse->colleague_id()." ");
                    if($colleague->id() == $schoolClasse->colleague_id())
                    {
                        $ColleagueFirstName = $colleague->first_name();
                        $ColleagueName = $colleague->name();
                    }
                }*/

                // Variable -> compte les élèves
                $x=0;

                foreach ($students as $student)
                {
                    if($student->school_class_id() == $schoolClasse->id())
                    { 
                        $x++;
                        
                    }
                }
            
                

                // Si la classe existe (il y ades élèves dedans) -> affiche l'entête
                if($x != 0)
                {
                    print("<h3>".$schoolClasse->id()." </h3>");
                    ?>
                    <div class="second">
                        <table>
                            <tr>
                                <td class="grey w150p">                            
                                    <?php
                                    print("<p class='span'>Nb d'élève:</p>");
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    print("<p class='span'>".$x."</p>");
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="grey w150p">
                                    <?php
                                    print("<p class='span'>Maître de classe: </p>");
                                    ?>
                                </td>
                                <td>
                                <a class="info-link" href="<?php echo $this->html()->url('colleague/'.$schoolClasse->colleague_id()); ?>"</a>    
                                    
                                <?php 
                                    print("<p class='span'>".$schoolClasse->colleague_id()."</p>");
                                    
                                ?>
                                    
                                </td>
                            </tr>

                        </table>
                    </div>
                    <?php                
                }            
            
                foreach ($students as $student)
                {
                    if($student->school_class_id() == $schoolClasse->id())
                    { 
                        ?>
                        <div class="post <?php echo $student->school_class_id().' '.$student->profession_id(); ?>">
                            <a href="<?php echo $this->html()->url('student/'.$student->id()); ?>" data-toggle="modal">
                        
                                <div class="picture">
                                    <?php
                                    $photo = '/img/students/'.$student->entry_date().'/'.$student->id().'.jpg';

                                    if (file_exists(WEBROOT.$photo)) 
                                    {
                                        ?><img src="<?php echo $this->html()->url($photo); ?>" alt=""><?php
                                    } 
                                    else 
                                    {
                                        ?><img src="<?php echo $this->html()->url('/img/nophoto.jpg'); ?>" alt="No photo"><?php
                                    }?>
                                </div>

                                <div class="information">
                                    <h5>
                                        <a class="info-link" href="<?php echo $this->html()->url('student/'.$student->id()); ?>"><?php echo $student->first_name(); ?> <?php echo $student->name(); ?></a>
                                    </h5>

                                    <p><?php echo $student->id(); ?></p>
                                </div>
                            </a>
                        </div><?php
                    }  
                }?>            
            </div><?php            
        }?>	
</div>