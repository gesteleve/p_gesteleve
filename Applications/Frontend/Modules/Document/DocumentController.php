<?php

namespace Applications\Frontend\Modules\Document;

use Applications\Entities\Document;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class DocumentController extends BackController
{
    private function is_master(&$student) {
        if (!$this->app->user()->isAuthenticated()) 
        { 
            return false; 
        }

        $tmp = @$this->managers->getManagerOf('schoolClass')->getUniqueByColleague($this->app->user()->getAttribute('user')->id());
        if ($tmp == null) 
        {
            return false;
        } 
        else 
        {
            
            foreach ($tmp as $class){

                if ($student->school_class_id() == $class->id()){
                    return true;
                }
            }
            return false;
        }
    }

    public function executeIndex(HTTPRequest $request) {
        $student = $this->managers->getManagerOf('Student')->getUnique($request->getData('id'));
		
		$manager = $this->managers->getManagerOf('Colleague');
		$colleaguesRoles = $manager->getColleagueRoles($request->getData('id'));

        $is_administrator  = ($this->app->user()->getAttribute('group')== ADMINISTRATOR);

        if ($is_administrator || $this->is_master($student) || $colleaguesRoles <= 2) {
            $this->page->addVar('student', $student);

            $this->page->addVar('title', 'Téléversement de document');
            $this->page->addVar('description', 'Téléversement de document pour l\'élèves '.$student->first_name().' '.$student->name());
            $this->page->addVar('keywords', 'téléversment, upload, document, '.$student->first_name().', '.$student->name());
        } else {
            $this->app->user()->setFlash('Vous n\'avez pas accès à cette page', 'error');
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
    }

    public function executeUpload(HTTPRequest $request) {
        $student = $this->managers->getManagerOf('Student')->getUnique($request->getData('id'));
		$manager = $this->managers->getManagerOf('Colleague');
		$colleagueRoles = $manager->getColleagueRoles($request->getData('id'));

        $is_administrator  = ($this->app->user()->getAttribute('group')== ADMINISTRATOR);
		
		if(!file_exists(ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date()))
		{
			mkdir(ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date(), 0777, true);
			mkdir(ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id(), 0777, true);
		}
		elseif(!file_exists(ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id()))
		{
			mkdir(ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id(), 0777, true);
		}
		
        if ($is_administrator || $this->is_master($student) || $colleaguesRoles <= 2) {
            if (isset($_FILES['upl'])) {
                $this->page->setLayout();

                $file_id = $request->postData('file-id');

                if (!$request->postsEmpty(array('security-'.$file_id, 'description-'.$file_id, 'type-'.$file_id, 'student'))) {
                    $allowed_types = array ('pdf');
                    $max_size = 8388608; // 8Mb

                    $file_name = $_FILES['upl']['name'];
                    $file_type = $_FILES['upl']['type'];
                    $file_size = $_FILES['upl']['size'];
                    $file_tmp = $_FILES['upl']['tmp_name'];
                    $drive = ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id();

                    if (!in_array(substr(strrchr($file_name,'.'),1), $allowed_types)) {
                        echo '{"status":"error", "msg":"Le fichier n\'est pas au format PDF"}';
                        die;
                    }

                    // Vérification de la taille imposée - 8Mb
                    if ($file_size > $max_size) {
                        echo '{"status":"error", "msg":"La taille du fichier dépasse la limite imposée de 8Mb"}';
                        die;
                    }

                    $file  = basename($file_name, substr($file_name, strrpos($file_name, '.')));

                    // Remplacement des caractères  accentué
                    $file_name = strtr($file_name,
                                'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                                'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

                    $i = 1;
                    while (file_exists($drive.DS.$file_name)) {
                        $file_name = $file.'_'.$i.'.pdf';
                        $i++;
                    }
                    unset($i);
					error_log($file_tmp, 0);
					
                    if (move_uploaded_file($file_tmp, $drive.DS.$file_name)) {
                        $document = new Document(array(
                        'title' => $file_name,
                        'description' => $request->postData('description-'.$file_id),
                        'add_date' => date('Y-m-d H:i:s'),
                        'size' => $file_size,
                        'right' => $request->postData('security-'.$file_id),
                        'student_id' => $request->postData('student'),
                        'colleague_id' => $this->app->user()->getAttribute('user')->id(),
                        'types_document_id' => $request->postData('type-'.$file_id),
                        ));
                        $this->managers->getManagerOf('Document')->save($document);

                        echo '{"status":"success"}';
                    }

                    die;
                }

                echo '{"status":"error"}';
            } else {
                $this->app->httpResponse()->redirect404();
            }
        } else {
            $this->app->user()->setFlash('Vous n\'avez pas accès à cette page', 'error');
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
    }

    public function executeEdit(HTTPRequest $request) {
        $document = $this->managers->getManagerOf('Document')->getUnique($request->getData('id'));
        $student = $this->managers->getManagerOf('Student')->getUnique($document->student_id());

        $is_administrator  = ($this->app->user()->getAttribute('group')== ADMINISTRATOR);

        if ($is_administrator  || $this->is_master($student) || $document->right() <= 2) {

            if ($request->postsExists(array('name', 'description', 'security', 'type'))) {

                try {
                    $student = $this->managers->getManagerOf('Student')->getUnique($document->student_id());

                    if ($document) {
                        $new_document = new Document(array(
                            'id' => $document->id(),
                            'title' => $request->postData('name'),
                            'description' => $request->postData('description'),
                            'right' => $request->postData('security'),
                            'types_document_id' => $request->postData('type')
                        ));

                        $drive = ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id().DS.$new_document->title();
                        $file  = basename($new_document->title(), substr($new_document->title(), strrpos($new_document->title(), '.')));
                        $new_document->setTitle(strtr($new_document->title(), 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy'));

                        $i = 1;
                        while (file_exists($drive.DS.$new_document->title())) {
                            $new_document->setTitle($file.'_'.$i.'.pdf');
                            $i++;
                        }
                        unset($i);


                        $this->managers->getManagerOf('Document')->save($new_document);
                        rename(ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id().DS.$document->title(), ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id().DS.$new_document->title());

                        $this->app->user()->setFlash('Document modifié avec succès', 'success');
                        $this->app->httpResponse()->redirect($request->httpReferer());
                    } else {
                        $this->app->httpResponse()->redirect404();
                    }
                } catch (\Exception $e) {
                    $this->app->user()->setFlash('Une erreur c\'est produite', 'error');
                    $this->app->httpResponse()->redirect($request->httpReferer());
                }
            } else {
                $this->page->setLayout();
                $this->page->addVar('types', $this->managers->getManagerOf('documentType')->getList());
                $this->page->addVar('document', $this->managers->getManagerOf('Document')->getUnique($request->getData('id')));
            }
        } else {
            $this->app->user()->setFlash('Vous n\'avez pas accès à cette page', 'error');
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
    }

    public function executeDelete(HTTPRequest $request) {
        $document = $this->managers->getManagerOf('Document')->getUnique($request->getData('id'));
        $student = $this->managers->getManagerOf('Student')->getUnique($document->student_id());

        $is_administrator  = ($this->app->user()->getAttribute('group')== ADMINISTRATOR);

        if ( $is_administrator  ||  $this->is_master($student) || $document->right() <= 2) {

            try {
                $this->managers->getManagerOf('Document')->delete($request->getData('id'));
                unlink(ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id().DS.$document->title());
            } catch (\Exception $e) {
                $this->app->user()->setFlash('Une erreur c\'est produite', 'error');
                $this->app->httpResponse()->redirect($request->httpReferer());
            }

            $this->app->user()->setFlash('Document supprimé avec succès', 'success');
            $this->app->httpResponse()->redirect($request->httpReferer());
        } else {
            $this->app->user()->setFlash('Vous n\'avez pas accès à cette page', 'error');
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
    }

    public function executeAction(HTTPRequest $request) {
        $this->page->setLayout();
        $document = $this->managers->getManagerOf('Document')->getUnique($request->getData('id'));
        $this->page->addVar('document', $this->managers->getManagerOf('Document')->getUnique($request->getData('id')));
    }

    public function executeDownload(HTTPRequest $request) {
        $document = $this->managers->getManagerOf('Document')->getUnique($request->getData('id'));
        $student = $this->managers->getManagerOf('Student')->getUnique($document->student_id());

        $is_administrator  = ($this->app->user()->getAttribute('group')== ADMINISTRATOR);
        $is_student = ($document->student_id()   == $this->app->user()->getAttribute('user')->id());
        $is_author  = ($document->colleague_id() == $this->app->user()->getAttribute('user')->id());
        $is_teacher  = ($document->right()==2) ;
        $is_student_visible  = ($is_student && ($document->right()==1)) ;
        $_isMainMaster = ($this->app->user()->getAttribute('group')== 10);
        $_isDirection = ($this->app->user()->getAttribute('group')== 3);
        $_isDoyen = ($this->app->user()->getAttribute('group')== 9);

        $is_ok = $is_administrator || ($is_student_visible || $this->is_master($student) || $is_author || $_isMainMaster || $_isDirection || $_isDoyen || ($is_teacher and !$is_student )) ;


        if ( $is_ok) {
            $this->page->setLayout();
            $drive = ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id().DS.$document->title();
            header("Content-type: application/force-download");
            header("Content-Length: ".filesize($drive));
            header("Content-Disposition: attachment; filename=".basename($drive));
            readfile($drive);
        } else {
            $this->app->user()->setFlash('Vous n\'avez pas accès à cette page', 'error');
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
    }

    public function executeView(HTTPRequest $request) {
        $document = $this->managers->getManagerOf('Document')->getUnique($request->getData('id'));
        $student = $this->managers->getManagerOf('Student')->getUnique($document->student_id());

        $is_administrator  = ($this->app->user()->getAttribute('group')== ADMINISTRATOR);
        $is_student = ($document->student_id()   == $this->app->user()->getAttribute('user')->id());
        $is_author  = ($document->colleague_id() == $this->app->user()->getAttribute('user')->id());
        $is_teacher  = ($document->right()==2) ;
        $is_student_visible  = ($is_student && ($document->right()==1)) ;
        $_isMainMaster = ($this->app->user()->getAttribute('group')== 10);
        $_isDirection = ($this->app->user()->getAttribute('group')== 3);
        $_isDoyen = ($this->app->user()->getAttribute('group')== 9);

        $is_ok = $is_administrator || ($is_student_visible || $this->is_master($student) || $is_author || $_isMainMaster || $_isDirection || $_isDoyen || ($is_teacher and !$is_student )) ;

        if ($is_ok) {
            $drive = ROOT.DS.'Drive'.DS.'students'.DS.$student->entry_date().DS.$student->id().DS.$document->title();
            header("Content-type: application/pdf");
            header("Content-Length: ".filesize($drive));
            readfile($drive);
        } else {
            $this->app->user()->setFlash('Vous n\'avez pas accès à cette page', 'error');
            $this->app->httpResponse()->redirect($request->httpReferer());
        }
    }
}
