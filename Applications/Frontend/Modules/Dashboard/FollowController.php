<?php

namespace Applications\Frontend\Modules\Dashboard;

use Applications\Entities\Dashboard;
use Library\Sly\Controller\BackController;
use Library\Sly\Network\HTTPRequest;

class DashboardController extends BackController
{
     function executeIndex(HTTPRequest $request) 
     {
        
            // Récupère les droits de l'utilisateur
            $right = $this->app->user()->getAttribute('right');

                $this->page->addVar('title', 'Tableau de bord');
                $this->page->addVar('description', 'Tableau de bord');
                $this->page->addVar('keywords', 'Tableau de bord');

                $this->page->addVar('Dashboard', $Dashboard);
            // On vérifie que l'utilisateur est authentifié
            if ($this->app->user()->isAuthenticated()) 
            {

            }

     }

    
}
