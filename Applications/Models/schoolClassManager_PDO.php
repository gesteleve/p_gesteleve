<?php

namespace Applications\Models;

use Applications\Entities\schoolClass;
use Applications\Entities\Student;

class schoolClassManager_PDO extends schoolClassManager
{
    public function getList() {
        $req = $this->dao->query('SELECT school_classes.*, professions.name AS profession_name
                                  FROM school_classes, professions
                                  WHERE school_classes.profession_id = professions.id
                                  ORDER BY school_classes.id');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $classes = $req->fetchAll();
        $req->closeCursor();

        return $classes;
    }

    //listes des étudiants d'un maître de classe
     public function getListStudentByMC($id) {
        $req = $this->dao->prepare('SELECT students.*, school_classes.colleague_id
                                  FROM students LEFT JOIN school_classes 
                                  ON students.school_class_id = school_classes.id
								  where students.active = 1
                                  and school_classes.colleague_id=:id');

        
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $ListStudentByMC = $req->fetchAll();
        $req->closeCursor();

        return $ListStudentByMC;

    }


    public function getListSectionClass() {
        $req = $this->dao->query('SELECT DISTINCT professions.name AS profession_name
                                  FROM school_classes, professions 
                                  WHERE school_classes.profession_id = professions.id
                                  ');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $classes = $req->fetchAll();
        $req->closeCursor();

        return $classes;
    }

    public function getListClass() {
        $req = $this->dao->query('SELECT school_classes.*, professions.name AS profession_name, 
                                    (
                                    SELECT count(*) 
                                    FROM students 
                                    WHERE students.school_class_id=school_classes.id and students.active=1
                                    ) 
                                  AS student_nbre
                                  FROM school_classes 
                                  INNER JOIN professions 
                                  ON school_classes.profession_id = professions.id;
                                  ');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $classes = $req->fetchAll();
        $req->closeCursor();

        return $classes;
    }

    public function getUnique($id) {
        $req = $this->dao->prepare('SELECT school_classes.*, professions.name AS profession_name
                                    FROM school_classes, professions
                                    WHERE school_classes.id = :id AND school_classes.profession_id = professions.id');
        
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $classes = $req->fetch();
        $req->closeCursor();

        return $classes;
    }

    public function getUniqueByColleague($id) {
        $req = $this->dao->prepare('SELECT school_classes.*, professions.name AS profession_name,
                                    (
                                    SELECT count(*) 
                                    FROM students 
                                    WHERE students.school_class_id=school_classes.id and students.active=1
                                    ) AS student_nbre
                                    FROM school_classes LEFT JOIN professions ON school_classes.profession_id = professions.id
                                    WHERE school_classes.colleague_id = :id');
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $classes = $req->fetchAll();
        $req->closeCursor();

        return $classes;
    }

    public function getStudents($id) {
        $req = $this->dao->prepare('SELECT students.*
                                   FROM students
								   where students.active = 1
                                   and students.school_class_id = :id');
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }

    public function search($pattern) {
        $req = $this->dao->prepare('SELECT school_classes.*, professions.name AS profession_name
                                    FROM school_classes, professions, students
                                    WHERE school_classes.profession_id = professions.id
                                    AND (school_classes.id LIKE :pattern
                                        OR school_classes.colleague_id LIKE :pattern
                                        OR (school_classes.id = students.school_class_id
                                            AND students.active = 1
                                            AND (students.id LIKE :pattern
                                                OR students.name LIKE :pattern
                                                OR students.first_name LIKE :pattern
                                                OR students.email LIKE :pattern
                                                OR students.school_class_id LIKE :pattern
                                                )
                                            )
                                        )
                                    GROUP BY school_classes.id
                                    ORDER BY school_classes.id');
        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $classes = $req->fetchAll();
        $req->closeCursor();

        return $classes;
    }

    
   public function loadData( $nom,$prenom,$BirthDate,$GetLocalite,$adresse,$telephonemobile,$telephonemaison,$GetDateEntry,$GetDateExit,$classe,$GetNpa)
     {
        

        /*
            $req = $this->dao->prepare("INSERT INTO test(Nom_id, Prenom_id, Age_id, Classe_id) VALUES (:name, :first_name, :old, :classe)");
        
            $req->bindValue(':name', $nom);
            $req->bindValue(':first_name', $prenom);
            $req->bindValue(':old', $age);
            $req->bindValue(':classe', $classe);
       
        

            $req->execute();
            $req->closeCursor();*/
        



        $req = $this->dao->prepare('INSERT INTO test (name, first_name, birth_date, zip, city, address, mobile, phone, email, entry_date, exit_date, comment, school_class_id)
                                    VALUES ( :name, :first_name, :birth_date, :zip, :city, :address, :mobile, :phone, "-", :entry_date, :exit_date, "-", :school_class_id)');
       


            $req->bindValue(':name', $nom);
            $req->bindValue(':first_name', $prenom);
            $req->bindValue(':birth_date', $BirthDate);
            $req->bindValue(':zip', $GetNpa);
            $req->bindValue(':city', $GetLocalite);
            $req->bindValue(':address', $adresse);
            $req->bindValue(':mobile', $telephonemobile);
            $req->bindValue(':phone', $telephonemaison);
            $req->bindValue(':entry_date', $GetDateEntry);
            $req->bindValue(':exit_date', $GetDateExit);
            $req->bindValue(':school_class_id', $classe);









       /* $req->bindValue(':id', $student->id());
        $req->bindValue(':name', $student->name());
        $req->bindValue(':first_name', $student->first_name());
        $req->bindValue(':birth_date', $student->birth_date());
        $req->bindValue(':zip', $student->zip());
        $req->bindValue(':city', $student->city());
        $req->bindValue(':address', $student->address());
        $req->bindValue(':mobile', $student->mobile());
        $req->bindValue(':phone', $student->phone());
        $req->bindValue(':email', $student->email());
        $req->bindValue(':entry_date', $student->entry_date());
        $req->bindValue(':exit_date', $student->exit_date());
        $req->bindValue(':comment', $student->comment());
        $req->bindValue(':school_class_id', $student->school_class_id());*/
        $req->execute();
        $req->closeCursor();
                    

                       
               
        



      
            
      
        

        
                    

                       
           
    }

    public function add(schoolClass $schoolClass) {
         $req = $this->dao->prepare('INSERT INTO school_classes (`id`, `profession_id`, `colleague_id`)
                                    VALUES (:schoolClass_Id, :profession_id, :colleague_id)');
        $req->bindValue(':schoolClass_Id', $schoolClass->id());
        $req->bindValue(':profession_id', $schoolClass->profession_id());
        $req->bindValue(':colleague_id', $schoolClass->colleague_id());
        $success = $req->execute();
        $req->closeCursor();

        return $success;

    }

    public function modify(schoolClass $schoolClass) {
        $req = $this->dao->prepare('UPDATE school_classes SET id = :id, profession_id = :profession_id, 
                                                              colleague_id = :colleague_id WHERE id = :id');
        $req->bindValue(':id', $schoolClass->id());
        $req->bindValue(':profession_id', $schoolClass->profession_id());
        $req->bindValue(':colleague_id', $schoolClass->colleague_id());
        $success = $req->execute();
        $req->closeCursor();

        return $success;

    }

    public function delete() {

    }
}
