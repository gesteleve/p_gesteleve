<?php

namespace Applications\Models;

use Applications\Entities\schoolClass;
use Library\Sly\Database\Manager;

abstract class schoolClassManager extends Manager
{
  abstract function getList();
  abstract function getListClass();
  abstract function getUnique($id);
  abstract function getUniqueByColleague($id);
  abstract function getStudents($id);
  abstract function search($pattern);
  abstract function add(schoolClass $schoolClass);
  abstract function modify(schoolClass $schoolClass);
  abstract function delete();


  public function save(schoolClass $class) {
    if ($class->isValid()) {
      $class->isNew() ? $this->modify($class) : $this->add($class);
    } else {
      throw new \RuntimeException('La classe doit être valide pour être enregistrée');
    }
  }
}
