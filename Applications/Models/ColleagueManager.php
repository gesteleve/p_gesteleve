<?php

namespace Applications\Models;

use Applications\Entities\Colleague;
use Library\Sly\Database\Manager;

abstract class ColleagueManager extends Manager
{
  abstract function getList();
  abstract function getListSectionColleague();
  abstract function getUnique($id);
  abstract function getRight($id);
  abstract function getGroup($id); 
  abstract function search($pattern);
  abstract function add();
  abstract function loadData($id,$Name,$First_Name,$email,$inter_phone,$phone);
  abstract function modify();
  abstract function delete();

  public function save(Colleague $colleague) {
    if ($colleague->isValid()) {
      $colleague->isNew() ? $this->modify($colleague) : $this->add($colleague);
    } else {
      throw new \RuntimeException('Le collaborateur doit être valide pour être enregistré');
    }
  }
}
