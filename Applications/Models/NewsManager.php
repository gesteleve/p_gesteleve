<?php
//*********************************************************
// Societe: ETML
// Auteur : Alexis Gonzalez
// Date : 26.05.2014
// But : Fichier manager du module news, il indique les fonction utilisée dans le module, entre les page, et si elles contiennent quelques chose
//*********************************************************
// Modifications:
// Date : 
// Auteur : 
// Raison : 
//*********************************************************
// Date :
// Auteur :
// Raison :
//*********************************************************

namespace Applications\Models;

use Applications\Entities\News;
use Library\Sly\Database\Manager;

abstract class NewsManager extends Manager
{
	abstract function getList();
  	abstract function getUnique($id);
  	abstract function add(News $news);
  	abstract function edit(News $news);
  	abstract function delete($id);

}
