<?php

namespace Applications\Models;

use Applications\Entities\Profession;
use Library\Sly\Database\Manager;

abstract class ProfessionManager extends Manager
{
  abstract function getList();
  abstract function getUnique($id);
  abstract function search($pattern);
  abstract function add();
  abstract function modify();
  abstract function delete();
  abstract function getStudentsBySection();

  public function save(Profession $profession) {
    if ($profession->isValid()) {
      $profession->isNew() ? $this->modify($profession) : $this->add($profession);
    } else {
      throw new \RuntimeException('La profession doit être valide pour être enregistré');
    }
  }
}
