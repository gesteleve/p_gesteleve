<?php
//*********************************************************
// Societe: ETML
// Auteur : Vladislav Lukyantsev
// Date : 23.06.2014
// But : Permet d'afficher un texte de bienvenue ou le modifier
//*********************************************************
// Modifications:
// Date : 
// Auteur : 
// Raison : 
//*********************************************************
// Date :
// Auteur :
// Raison :
//*********************************************************
namespace Applications\Models;

use Applications\Entities\Search;

class SearchManager_PDO extends SearchManager
{
     

    public function getedit($text) 
    {
       //Requete faite à la BD
        $req = $this->dao->prepare('UPDATE searchtitle SET  `SearchContent` =:content');

        // Insert les donnnées concernant la news dans la requete
        $req->bindValue(':content', $text);   

        // Execute la requete
        $success = $req->execute();

        //Ferme la connexion à la BD
        $req->closeCursor();

        // Retourne le resultat lorsqu'on execute la requete
        return $success;
    }
}
