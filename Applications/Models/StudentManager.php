<?php

namespace Applications\Models;

use Applications\Entities\Student;
use Library\Sly\Database\Manager;

abstract class StudentManager extends Manager
{
  abstract function getList();
  abstract function getUnique($id);
  abstract function search($pattern);
  abstract function add(Student $student);
  abstract function loadData( Student $student);
  abstract function modify(Student $student);
  abstract function delete($id);

  public function save(Student $student) {
    if ($student->isValid()) {
      $student->isNew() ? $this->modify($student) : $this->add($student);
    } else {
      throw new \RuntimeException('L\'élève doit être valide pour être enregistré');
    }
  }
}
