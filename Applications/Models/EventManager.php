<?php

namespace Applications\Models;

use Applications\Entities\Event;
use Library\Sly\Database\Manager;

abstract class EventManager extends Manager
{
  abstract function getList();
  abstract function getUnique($id);
  abstract function search($pattern);
  abstract function add();
  abstract function modify();
  abstract function delete();

  public function save(Event $event) {
    if ($event->isValid()) {
      $event->isNew() ? $this->modify($event) : $this->add($event);
    } else {
      throw new \RuntimeException('L\'événement doit être valide pour être enregistré');
    }
  }
}
