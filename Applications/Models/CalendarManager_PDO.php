<?php
/****************************************************************************
Nom:		CalendarManager_PDO.php
Auteur:		????
Date:		??.??.????
But:		Il s'agit du modèle de l'application du calendrier
Sources:	http://stackoverflow.com/questions/15759223/cannot-use-object-of-type-pdostatement-as-array
*****************************************************************************
Modifications
Date  : 20.01.2014
Auteur: Ludovic Delafontaine - MIN 2011-2015
Raison: - Ajout de cet entête
		- Modification de la requête SQL de base pour checher une classe
A faire: - Ajouter des fonctions de recherches pour une personne (ex. delafontlu), une salle de classe (N508a) ou une classe (MIN3)
*****************************************************************************
Modifications
Date  : 21.01.2014
Auteur: Ludovic Delafontaine - MIN 2011-2015
Raison: - Création des fonctions getSchoolClassCalendar, getColleagueCalendar et getClassRoomCalendar
A faire: -
*****************************************************************************
Modifications
Date  : 22.01.2014
Auteur: Ludovic Delafontaine - MIN 2011-2015
Raison: - Changement du setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Calendar'); en setFetchMode(\PDO::FETCH_ASSOC); afin de convertir automatiquement les données reçue en tableau associatif pour PHP (Je ne sais pas si c'est la méthode la plus propre...)
A faire: -
****************************************************************************/

namespace Applications\Models;

use Applications\Entities\Calendar;

class CalendarManager_PDO extends CalendarManager
{
	/********************************************************************
	Nom :	getSchoolClassCalendar
	But :	Récupère le calendrier de la classe demandée
	Retour: $calendar:	- Il s'agit du calendrier correspondant à la classe
	Param : $class_id:	- Il s'agit de l'identifiant de la classe
	********************************************************************/
    public function getSchoolClassCalendar($class_id) {
        $req = $this->dao->prepare('SELECT periods.day, periods.number, branchs.id AS branch, rooms.id AS room, colleagues_periods.colleague_id AS colleague_id
									FROM school_classes_periods, periods, colleagues_periods, branchs, rooms, colleagues
									WHERE colleagues.id = :class_id
										AND branchs.id = periods.branch_id
										AND rooms.id = periods.room_id
										AND colleagues_periods.colleague_id = colleagues.id
										AND colleagues_periods.period_id = periods.id
									GROUP BY colleagues_periods.period_id
									ORDER BY periods.day, periods.number');
		// Remplace la variable dans le code SQL par la variable de la fonction
        $req->bindValue(':class_id', $class_id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $calendar = $req->fetchAll();
        $req->closeCursor();

        return $calendar;
    }// end getSchoolClassCalendar
	
	/********************************************************************
	Nom :	getColleagueCalendar
	But :	Récupère le calendrier de la personne demandée
	Retour: $calendar:		- Il s'agit du calendrier correspondant à la personne
	Param : $colleague_id:	- Il s'agit de l'identifiant du collègue
	********************************************************************/
	public function getColleagueCalendar($colleague_id) {
        $req = $this->dao->prepare('SELECT periods.day, periods.number, branchs.id AS branch, rooms.id AS room, colleagues_periods.colleague_id AS colleague_id
									FROM school_classes_periods, periods, colleagues_periods, branchs, rooms, colleagues
									WHERE colleagues.id = :colleague_id
										AND branchs.id = periods.branch_id
										AND rooms.id = periods.room_id
										AND colleagues_periods.colleague_id = colleagues.id
										AND colleagues_periods.period_id = periods.id
									GROUP BY colleagues_periods.period_id
									ORDER BY periods.day, periods.number');
		// Remplace la variable dans le code SQL par la variable de la fonction
        $req->bindValue(':colleague_id', $colleague_id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $calendar = $req->fetchAll();
        $req->closeCursor();

        return $calendar;
    }// end getColleagueCalendar

    /********************************************************************
	Nom :	getStudentCalendar
	But :	Récupère le calendrier de la personne demandée
	Retour: $calendar:		- Il s'agit du calendrier correspondant à la personne
	Param : $student_id:	- Il s'agit de l'identifiant de l'élève
	********************************************************************/
	public function getStudentCalendar($student_id) {
        $req = $this->dao->prepare('SELECT periods.day, periods.number, branchs.id AS branch, rooms.id AS room, students_periods.student_id AS student_id
									FROM school_classes_periods, periods, students_periods, branchs, rooms, students
									WHERE students.id = :student_id
										AND branchs.id = periods.branch_id
										AND rooms.id = periods.room_id
										AND students_periods.student_id = students.id
										AND students_periods.period_id = periods.id
									GROUP BY students_periods.period_id
									ORDER BY periods.day, periods.number');
		// Remplace la variable dans le code SQL par la variable de la fonction
        $req->bindValue(':student_id', $student_id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $calendar = $req->fetchAll();
        $req->closeCursor();

        return $calendar;
    }// end getStudentCalendar
	
    /********************************************************************
	Nom :	getStudentEvents
	But :	Récupère les évennements de la personne demandée
	Retour: $events:		- Il s'agit des évennements correspondant à la personne
	Param : $student_id:	- Il s'agit de l'identifiant de l'élève
	********************************************************************/
	public function getStudentEvents($student_id) {
        $req = $this->dao->prepare('SELECT events.id, events.title, events.start, events.end
									FROM students_events, students, events
									WHERE students.id = :student_id
										AND events.id = students_events.event_id
										AND students_events.student_id = students.id
										AND students_events.event_id = events.id');
		// Remplace la variable dans le code SQL par la variable de la fonction
        $req->bindValue(':student_id', $student_id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $events = $req->fetchAll();
        $req->closeCursor();

        return $events;
    }// end getStudentEvents

	/********************************************************************
	Nom :	getClassRoomCalendar
	But :	Récupère le calendrier de la salle demandée
	Retour: $calendar:	- Il s'agit du calendrier correspondant à la salle de classe
	Param : $room_id:	- Il s'agit de l'identifiant de la salle de classe
	********************************************************************/
	public function getClassRoomCalendar($room_id) {
        $req = $this->dao->prepare('SELECT periods.day, periods.number, branchs.id AS branch, rooms.id AS room, colleagues_periods.colleague_id AS colleague_id
									FROM school_classes_periods, periods, colleagues_periods, branchs, rooms, colleagues
									WHERE rooms.id = :room_id
										AND branchs.id = periods.branch_id
										AND rooms.id = periods.room_id
										AND colleagues_periods.colleague_id = colleagues.id
										AND colleagues_periods.period_id = periods.id
									GROUP BY colleagues_periods.period_id
									ORDER BY periods.day, periods.number');
		// Remplace la variable dans le code SQL par la variable de la fonction
        $req->bindValue(':room_id', $room_id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $calendar = $req->fetchAll();
        $req->closeCursor();

        return $calendar;
    }// end getClassRoomCalendar
	
	/********************************************************************
	Nom :	getSchoolClasseEvents
	But :	Récupère les évennements de la classe de l'élève connecté
	Retour: $events:		- Il s'agit des évennements correspondant à la personne
	Param : $student_id:	- Il s'agit de l'identifiant de l'élève
	********************************************************************/
	public function getSchoolClasseEvents($student_id) {
        $req = $this->dao->prepare('SELECT events.id AS id, events.title, events.start, events.end
									FROM school_classes_events, school_classes, students, events
									WHERE students.id = :student_id
										AND students.school_class_id = school_classes_events.school_classes_id
										AND events.id = school_classes_events.event_id
										AND school_classes_events.school_classes_id = school_classes.id
										AND school_classes_events.event_id = events.id');
		// Remplace la variable dans le code SQL par la variable de la fonction
        $req->bindValue(':student_id', $student_id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_ASSOC);

       	$Classeevents = $req->fetchAll();
        $req->closeCursor();

        return $Classeevents;
    }// end getStudentEvents

    /********************************************************************
	Nom :	updateEvent
	But :	Permet de modifier un évennement
	Retour: $Result:		- Résultat de la requête
	Param : $id,...:		
	********************************************************************/
    public function updateEvent($id, $start, $end)
    {
    	$req = $this->dao->prepare('UPDATE events SET start = :start, end = :end WHERE id = :id');

    	$req->bindValue(':id',$id);
        $req->bindValue(':start', $start);
        $req->bindValue(':end', $end);

        $result = $req->execute();

        $req->closeCursor();

        return $result;
	}
}