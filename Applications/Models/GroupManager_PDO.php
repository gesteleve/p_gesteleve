<?php

namespace Applications\Models;

use Applications\Entities\Group;
use Applications\Entities\Student;

class GroupManager_PDO extends GroupManager
{
	public function createGroup($colleague_id, $name, $content,$isPublic)
	{
		$req = $this->dao->prepare('Insert INTO gest_groups(colleague_id, name, content, isPublic) VALUES (:id, :name, :content, :isPublic)');
        $req->bindValue(':id', $colleague_id);
		$req->bindValue(':name', $name);
		$req->bindValue(':content', $content);
		$req->bindValue(':isPublic', $isPublic);
        $req->execute();
		$req->closeCursor();
	}
    public function getListByColleague($colleague_id) {
        $req = $this->dao->prepare('SELECT gest_groups.id, gest_groups.name, gest_groups.colleague_id, gest_groups.content, gest_groups.isPublic, colleagues.name as colleague_name, colleagues.first_name as colleague_first_name, professions.name as profession
                                  FROM gest_groups
								  INNER JOIN colleagues ON gest_groups.colleague_id = colleagues.id
								  INNER JOIN professions ON colleagues.profession_id = professions.id
                                  WHERE gest_groups.colleague_id = :id
                                  ORDER BY id DESC');
        $req->bindValue(':id', $colleague_id);
        $req->execute();
        
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Group');

        $groups = $req->fetchAll();
        $req->closeCursor();

        return $groups;
    }
	public function getGroupById($id) {
        $req = $this->dao->prepare('SELECT id, name,colleague_id,content
                                  FROM gest_groups
                                  WHERE gest_groups.id = :id
                                  ORDER BY id DESC');
        $req->bindValue(':id', $id);
        $req->execute();
        
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Group');

        $groups = $req->fetchAll();
        $req->closeCursor();

        return $groups;
    }
	public function addUserGroup($id, $user)
	{
		$req = $this->dao->prepare('Insert INTO gest_students(group_id, student_id) VALUES (:id, :user)');
        $req->bindValue(':id', $id);
		$req->bindValue(':user', $user);
        $req->execute();
		$req->closeCursor();
	}
	public function removeUserGroup($id, $user)
	{
		$req = $this->dao->prepare('DELETE FROM gest_students
									WHERE group_id = :id
									AND WHERE student_id = :user');
        $req->bindValue(':id', $id);
		$req->bindValue(':user', $user);
        $req->execute();
		$req->closeCursor();
	}
	public function updateGroup($id, $name, $content, $isPublic)
	{
		$req = $this->dao->prepare('UPDATE gest_groups
									SET name = :name, content = :content, isPublic = :isPublic
									WHERE id = :id');
        $req->bindValue(':id', $id);
		$req->bindValue(':name', $name);
		$req->bindValue(':content', $content);
		$req->bindValue(':isPublic', $isPublic);
        $req->execute();
		$req->closeCursor();
	}
	public function removeAllGroup($id)
	{
		$req = $this->dao->prepare('DELETE FROM gest_students
									WHERE group_id = :id');
        $req->bindValue(':id', $id);
        $req->execute();
		$req->closeCursor();
	}
	public function removeGroup($id)
	{
		$req = $this->dao->prepare('DELETE FROM gest_groups
									WHERE id = :id');
        $req->bindValue(':id', $id);
        $req->execute();
		$req->closeCursor();
	}
	public function getLastIdByColleague($colleague_id) {
        $req = $this->dao->prepare('SELECT id
                                  FROM gest_groups
                                  WHERE colleague_id = :id
                                  ORDER BY id DESC
								  LIMIT 1');
        $req->bindValue(':id', $colleague_id);
        $req->execute();
        
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Group');

        $groups = $req->fetchAll();
        $req->closeCursor();

        return $groups;
    }
	public function getStudentsByGroup($group_id) {
        $req = $this->dao->prepare('SELECT students.*
                                  FROM gest_students
								  INNER JOIN students ON gest_students.student_id = students.id
                                  WHERE gest_students.group_id = :id
                                  ');
        $req->bindValue(':id', $group_id);
        $req->execute();
        
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }
	public function getStudents() {
        $req = $this->dao->prepare('SELECT students.*, group_id
                                  FROM gest_students
								  INNER JOIN students ON gest_students.student_id = students.id
                                  ');
        $req->execute();
        
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }
	
	public function getListPublic(){
		$req = $this->dao->prepare('SELECT gest_groups.id, gest_groups.name, gest_groups.colleague_id, gest_groups.content, gest_groups.isPublic, colleagues.name as colleague_name, colleagues.first_name as colleague_first_name, professions.name as profession
                                  FROM gest_groups
								  INNER JOIN colleagues ON gest_groups.colleague_id = colleagues.id
								  INNER JOIN professions ON colleagues.profession_id = professions.id
                                  WHERE gest_groups.isPublic = 1
                                  ORDER BY id DESC');
        $req->execute();
        
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Group');

        $groups = $req->fetchAll();
        $req->closeCursor();

        return $groups;
	}
}