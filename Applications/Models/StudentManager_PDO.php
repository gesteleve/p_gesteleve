<?php

namespace Applications\Models;

use Applications\Entities\Student;

class StudentManager_PDO extends StudentManager
{
    public function getList() {
        $req = $this->dao->query('SELECT students.*, professions.id AS profession_id, professions.name AS profession_name
                                  FROM students, professions, school_classes
                                  WHERE students.active = 1
                                  AND (school_classes.profession_id = professions.id
                                         AND students.school_class_id = school_classes.id)');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }
	//retourne les élèves qui ont leur anniversaire à la date
	public function getBirthList($date){
        $req = $this->dao->query("SELECT students.*, professions.id AS profession_id, professions.name AS profession_name
                                  FROM students, professions, school_classes
                                  WHERE students.active = 1 and MONTH(students.birth_date)= MONTH('$date') and DAY(students.birth_date)= DAY('$date')
                                  AND (school_classes.profession_id = professions.id
                                         AND students.school_class_id = school_classes.id) AND UPPER(RIGHT(students.school_class_id,2)) <> 'XX'");
        
		
		$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');
        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }
    // retourne les infos du maitre de classe
    public function getMasterClass($idStudent) {
        $req = $this->dao->prepare('SELECT colleagues.*
                                    FROM (students INNER JOIN school_classes 
                                    ON students.school_class_id = school_classes.id) 
                                    INNER JOIN colleagues ON school_classes.colleague_id = colleagues.id 
                                    WHERE students.id=:id');
        $req->bindValue(':id', $idStudent);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $MasterClass = $req->fetch();
        $req->closeCursor();

        return $MasterClass;
    }

    public function getUnique($id) {
        $req = $this->dao->prepare('SELECT students.*, professions.name AS profession_name
                                    FROM students, professions, school_classes
                                    WHERE students.id = :id
                                    AND students.active = 1
                                    AND (school_classes.profession_id = professions.id
                                         AND students.school_class_id = school_classes.id)');
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $student = $req->fetch();
        $req->closeCursor();

        return $student;
    }

    public function getRight($id) {
        $rights_of_student = array();
        $req = $this->dao->prepare('SELECT BIT_OR(groups_modules.rights), modules.id FROM groups, groups_modules, modules
                                   WHERE
                                        groups_modules.group_id = 7
                                        AND groups_modules.module_id = modules.id
                                    GROUP BY module_id');
        $req->execute();

        $right = $req->fetchAll();

        $j = count($right);
        for ($i = 0; $j > $i; $i++) {
            $rights_of_student[$right[$i]['id']] = $right[$i]['BIT_OR(groups_modules.rights)'];
        }

        return $rights_of_student;
    }

      public function search($pattern) {
        $req = $this->dao->prepare('SELECT students. * , professions.name AS profession_name
                                        FROM students, professions, school_classes
                                        WHERE(
                                            school_classes.profession_id = professions.id
                                            AND students.school_class_id = school_classes.id
                                        )
                                        AND (
                                            students.id LIKE :pattern
                                            OR students.name LIKE :pattern
                                            OR students.first_name LIKE :pattern
                                            OR CONCAT( students.name, " ", students.first_name ) LIKE :pattern
                                            OR CONCAT( students.first_name, " ", students.name ) LIKE :pattern
                                            OR students.email LIKE :pattern
                                            OR students.school_class_id LIKE :pattern
                                        )'
                                );
        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }

    public function filterStudent($pattern) {
       $req = $this->dao->prepare('SELECT students.*, professions.name AS profession_name
                                    FROM students, professions, school_classes
                                    WHERE students.active = 1
                                    AND (school_classes.profession_id = professions.id
                                         AND students.school_class_id = school_classes.id)
                                    AND (students.id LIKE :pattern
                                         OR students.name LIKE :pattern
                                         OR students.first_name LIKE :pattern
                                         OR students.email LIKE :pattern
                                        )'
                                );

        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }

   public function loadData(Student $student)
    {  
        $req = $this->dao->query('SELECT students.* FROM students');
        $students = $req->fetchAll();
        $req->closeCursor();
        
        $idData = $this->dao->query('SELECT id FROM students');
        $id = $idData ->fetchAll();
        $idData ->closeCursor(); 
     
        if(empty($students))
        {
            $req = $this->dao->prepare('INSERT INTO students (id, name, first_name, birth_date, zip, city, address, mobile, phone, email, entry_date, exit_date, comment, school_class_id)
                                    VALUES (:id, :name, :first_name, :birth_date, :zip, :city, :address, :mobile, :phone, :email, :entry_date, :exit_date, :comment, :school_class_id)');

            $req->bindValue(':id', $student->id());
            $req->bindValue(':name', $student->name());
            $req->bindValue(':first_name', $student->first_name());
            $req->bindValue(':birth_date', $student->birth_date());
            $req->bindValue(':zip', $student->zip());
            $req->bindValue(':city', $student->city());
            $req->bindValue(':address', $student->address());
            $req->bindValue(':mobile', $student->mobile());
            $req->bindValue(':phone', $student->phone());
            $req->bindValue(':email', $student->email());
            $req->bindValue(':entry_date', $student->entry_date());
            $req->bindValue(':exit_date', $student->exit_date());
            $req->bindValue(':comment', $student->comment());
            $req->bindValue(':school_class_id', $student->school_class_id());

            try
            {
                $req->execute();
            }
        
            catch(\PDOException $e)
            {
            
                $req = $this->dao->prepare('INSERT INTO temp_import (login, name, first_name, birth_date, zip, city, address, mobile, phone, email, entry_date, exit_date, comment, school_class_id)
                                        VALUES (:id, :name, :first_name, :birth_date, :zip, :city, :address, :mobile, :phone, :email, :entry_date, :exit_date, :comment, :school_class_id)');
           

                $req->bindValue(':id', $student->id());
                $req->bindValue(':name', $student->name());
                $req->bindValue(':first_name', $student->first_name());
                $req->bindValue(':birth_date', $student->birth_date());
                $req->bindValue(':zip', $student->zip());
                $req->bindValue(':city', $student->city());
                $req->bindValue(':address', $student->address());
                $req->bindValue(':mobile', $student->mobile());
                $req->bindValue(':phone', $student->phone());
                $req->bindValue(':email', $student->email());
                $req->bindValue(':entry_date', $student->entry_date());
                $req->bindValue(':exit_date', $student->exit_date());
                $req->bindValue(':comment', $student->comment());
                $req->bindValue(':school_class_id', $student->school_class_id());

                $req->execute();          
                $req->closeCursor();
            }

            $req->closeCursor();
        }
        else
        {   
            $NewAdd = 0;
            for($x=0;$x<count($id);$x++)
            {
                if($id[$x][0] == $student->id())
                {
                    $Birth = $this->dao->query('SELECT birth_date FROM students WHERE id = "'.$id[$x][0].'"');
                         
                    $birth_date = $Birth->fetchAll();
                    $Birth->closeCursor();
                    if($birth_date[0][0]==$student->birth_date())
                    {
                        $req = $this->dao->prepare('UPDATE students SET name = :name, first_name = :first_name, birth_date = :birth_date, zip = :zip, city = :city, address = :address, mobile = :mobile,
                               phone = :phone, email = :email, entry_date= :entry_date,exit_date = :exit_date, comment = :comment,  school_class_id = :school_class_id WHERE id = :id');

                        $req->bindValue(':id', $student->id());
                        $req->bindValue(':name', $student->name());
                        $req->bindValue(':first_name', $student->first_name());
                        $req->bindValue(':birth_date', $student->birth_date());
                        $req->bindValue(':zip', $student->zip());
                        $req->bindValue(':city', $student->city());
                        $req->bindValue(':address', $student->address());
                        $req->bindValue(':mobile', $student->mobile());
                        $req->bindValue(':phone', $student->phone());
                        $req->bindValue(':email', $student->email());
                        $req->bindValue(':entry_date', $student->entry_date());
                        $req->bindValue(':exit_date', $student->exit_date());
                        $req->bindValue(':comment', $student->comment());
                        $req->bindValue(':school_class_id', $student->school_class_id());

                        $req->execute();
                        $req->closeCursor();
                           
                    }
                    else
                    {
                        $idString=$id[$x][0];
                           
                        $idEditString=substr($idString, 0, -1);
                            
                        $idEditString.= "n";
                        $req = $this->dao->prepare('INSERT INTO students (id, name, first_name, birth_date, zip, city, address, mobile, phone, email, entry_date, exit_date, comment, school_class_id)
                                        VALUES (:id, :name, :first_name, :birth_date, :zip, :city, :address, :mobile, :phone, :email, :entry_date, :exit_date, :comment, :school_class_id)');
               

                        $req->bindValue(':id', $idEditString);
                        $req->bindValue(':name', $student->name());
                        $req->bindValue(':first_name', $student->first_name());
                        $req->bindValue(':birth_date', $student->birth_date());
                        $req->bindValue(':zip', $student->zip());
                        $req->bindValue(':city', $student->city());
                        $req->bindValue(':address', $student->address());
                        $req->bindValue(':mobile', $student->mobile());
                        $req->bindValue(':phone', $student->phone());
                        $req->bindValue(':email', $student->email());
                        $req->bindValue(':entry_date', $student->entry_date());
                        $req->bindValue(':exit_date', $student->exit_date());
                        $req->bindValue(':comment', $student->comment());
                        $req->bindValue(':school_class_id', $student->school_class_id());
           

    
              
                        try 
                        {
                            $req->execute();
                        } 
                        catch (\PDOException $e) 
                        {
                            $req = $this->dao->prepare('UPDATE students SET name = :name, first_name = :first_name, birth_date = :birth_date, zip = :zip, city = :city, address = :address, mobile = :mobile,
                                   phone = :phone, email = :email, entry_date= :entry_date,exit_date = :exit_date, comment = :comment,  school_class_id = :school_class_id WHERE id = :id');

                            $req->bindValue(':id', $idEditString);
                            $req->bindValue(':name', $student->name());
                            $req->bindValue(':first_name', $student->first_name());
                            $req->bindValue(':birth_date', $student->birth_date());
                            $req->bindValue(':zip', $student->zip());
                            $req->bindValue(':city', $student->city());
                            $req->bindValue(':address', $student->address());
                            $req->bindValue(':mobile', $student->mobile());
                            $req->bindValue(':phone', $student->phone());
                            $req->bindValue(':email', $student->email());
                            $req->bindValue(':entry_date', $student->entry_date());
                            $req->bindValue(':exit_date', $student->exit_date());
                            $req->bindValue(':comment', $student->comment());
                            $req->bindValue(':school_class_id', $student->school_class_id());
                        }
                        $req->closeCursor();
                    }        
                    $NewAdd=1;
                    break;
                }
                   
            }
            if($NewAdd == 0)
            {
                $req = $this->dao->prepare('INSERT INTO students (id, name, first_name, birth_date, zip, city, address, mobile, phone, email, entry_date, exit_date, comment, school_class_id)
                                        VALUES (:id, :name, :first_name, :birth_date, :zip, :city, :address, :mobile, :phone, :email, :entry_date, :exit_date, :comment, :school_class_id)');
               

                $req->bindValue(':id', $student->id());
                $req->bindValue(':name', $student->name());
                $req->bindValue(':first_name', $student->first_name());
                $req->bindValue(':birth_date', $student->birth_date());
                $req->bindValue(':zip', $student->zip());
                $req->bindValue(':city', $student->city());
                $req->bindValue(':address', $student->address());
                $req->bindValue(':mobile', $student->mobile());
                $req->bindValue(':phone', $student->phone());
                $req->bindValue(':email', $student->email());
                $req->bindValue(':entry_date', $student->entry_date());
                $req->bindValue(':exit_date', $student->exit_date());
                $req->bindValue(':comment', $student->comment());
                $req->bindValue(':school_class_id', $student->school_class_id());
           
                $req->execute();
                $req->closeCursor();
            }
        }
    }
    

    public function add(Student $student) {
        $req = $this->dao->prepare('INSERT INTO students (id, name, first_name, birth_date, zip, city, address, mobile, phone, email, entry_date, exit_date, comment, school_class_id)
                                    VALUES (:id, :name, :first_name, :birth_date, :zip, :city, :address, :mobile, :phone, :email, :entry_date, :exit_date, :comment, :school_class_id)');
        $req->bindValue(':id', $student->id());
        $req->bindValue(':name', $student->name());
        $req->bindValue(':first_name', $student->first_name());
        $req->bindValue(':birth_date', $student->birth_date());
        $req->bindValue(':zip', $student->zip());
        $req->bindValue(':city', $student->city());
        $req->bindValue(':address', $student->address());
        $req->bindValue(':mobile', $student->mobile());
        $req->bindValue(':phone', $student->phone());
        $req->bindValue(':email', $student->email());
        $req->bindValue(':entry_date', $student->entry_date());
        $req->bindValue(':exit_date', $student->exit_date());
        $req->bindValue(':comment', $student->comment());
        $req->bindValue(':school_class_id', $student->school_class_id());
        $req->execute();
        $req->closeCursor();
    }

    public function modify(Student $student) {
    // On vérifie si du contenu à étét envoyé pour le résumé
    if($student->summary() != null){
        // Prépare la requête
        $req = $this->dao->prepare('UPDATE students SET name = :name, first_name = :first_name, birth_date = :birth_date, zip = :zip, city = :city, address = :address, mobile = :mobile,
                               phone = :phone, email = :email, exit_date = :exit_date, comment = :comment, summary = :summary, school_class_id = :school_class_id WHERE id = :id');
        // Lie les données à la requête
        $req->bindValue(':summary', $student->summary());
    } else{
        $req = $this->dao->prepare('UPDATE students SET name = :name, first_name = :first_name, birth_date = :birth_date, zip = :zip, city = :city, address = :address, mobile = :mobile,
                               phone = :phone, email = :email, exit_date = :exit_date, comment = :comment, school_class_id = :school_class_id WHERE id = :id');
    }
        $req->bindValue(':name', $student->name());
        $req->bindValue(':first_name', $student->first_name());
        $req->bindValue(':birth_date', $student->birth_date());
        $req->bindValue(':zip', $student->zip());
        $req->bindValue(':city', $student->city());
        $req->bindValue(':address', $student->address());
        $req->bindValue(':mobile', $student->mobile());
        $req->bindValue(':phone', $student->phone());
        $req->bindValue(':email', $student->email());
        $req->bindValue(':exit_date', $student->exit_date());
        $req->bindValue(':comment', $student->comment());
        $req->bindValue(':school_class_id', $student->school_class_id());
        $req->bindValue(':id', $student->id());
        $req->execute();
        $req->closeCursor();
    }

    public function delete($id) {
        $req = $this->dao->prepare('UPDATE students SET active = 0 WHERE id = :id');
        $req->bindValue(':id', $id);
        $success = $req->execute();
        $req->closeCursor();

        return $success;
    }

    public function filterClass($pattern) {
        $req = $this->dao->prepare('SELECT students.*, professions.name AS profession_name
                                    FROM students INNER JOIN (school_classes 
                                        INNER JOIN professions 
                                        ON school_classes.profession_id = professions.id) 
                                        ON students.school_class_id = school_classes.id
                                    WHERE students.active = 1
                                    AND students.school_class_id LIKE :pattern 
                                    ');
        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $classes = $req->fetchAll();
        $req->closeCursor();

        return $classes;
    }
    public function filterSection($pattern) {
        $req = $this->dao->prepare('SELECT students.*, professions.name AS profession_name
                                    FROM students, school_classes, professions     
                                    WHERE students.active = 1
                                    AND (school_classes.profession_id = professions.id
                                         AND students.school_class_id = school_classes.id)
                                    AND  professions.name LIKE :pattern
                                    ');
        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Profession');

        $professions = $req->fetchAll();
        $req->closeCursor();

        return $professions;
    }

    public function getAllAbsencesPrat($idStudent){
        $req = $this->dao->prepare('SELECT COUNT(*) FROM t_absence WHERE fkStudent = ? AND absType = 1');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbAbsences = $req->fetch();
        $req->closeCursor();

        return $nbAbsences['COUNT(*)'];
    }

    public function getAllJustifiedAbsencesPrat($idStudent){
        $req = $this->dao->prepare('SELECT COUNT(*) FROM t_absence WHERE (fkStudent = ?) AND (absJustified = "1") AND absType = 1');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbJustiAbsences = $req->fetch();
        $req->closeCursor();

        return $nbJustiAbsences['COUNT(*)'];
    }

    public function getAllRemedialPrat($idStudent){
        $req = $this->dao->prepare('SELECT SUM(remNbPeriod) FROM t_remedial WHERE fkStudent = ? AND remType = 0');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbRemedial = $req->fetch();
        $req->closeCursor();

        return $nbRemedial['SUM(remNbPeriod)'];
    }


    public function getAllAbsencesTheo($idStudent){
        $req = $this->dao->prepare('SELECT COUNT(*) FROM t_absence WHERE fkStudent = ? AND absType = 2');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbAbsences = $req->fetch();
        $req->closeCursor();

        return $nbAbsences['COUNT(*)'];
    }

    public function getAllJustifiedAbsencesTheo($idStudent){
        $req = $this->dao->prepare('SELECT COUNT(*) FROM t_absence WHERE (fkStudent = ?) AND (absJustified = "1") AND absType = 2');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbJustiAbsences = $req->fetch();
        $req->closeCursor();

        return $nbJustiAbsences['COUNT(*)'];
    }

    public function getAllRemedialTheo($idStudent){
        $req = $this->dao->prepare('SELECT SUM(remNbPeriod) FROM t_remedial WHERE fkStudent = ? AND remType = 1');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbRemedial = $req->fetch();
        $req->closeCursor();

        return $nbRemedial['SUM(remNbPeriod)'];
    }


    public function getAllDoors($idStudent){
        $req = $this->dao->prepare('SELECT COUNT(*) FROM t_absence WHERE fkStudent = ? AND absType = 3');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbDoors = $req->fetch();
        $req->closeCursor();

        return $nbDoors['COUNT(*)'];
    }

    public function getAllLates($idStudent){
        $req = $this->dao->prepare('SELECT COUNT(*) FROM t_absence WHERE fkStudent = ? AND absType = 4');
        $req->execute([$idStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $nbDoors = $req->fetch();
        $req->closeCursor();

        return $nbDoors['COUNT(*)'];
    }
}
