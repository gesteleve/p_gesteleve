<?php
/****************************************************************************
Nom:        ScheduleManager_PDO.php
Auteur:     Jonathan Hermann
Date:       12.05.2014
But:        Fonctions liées à la base de données
*****************************************************************************
Modifications
Date  : -
Auteur: -
Raison: -
A faire: -
*****************************************************************************/

namespace Applications\Models;

use Applications\Entities\Schedule;

class ScheduleManager_PDO extends ScheduleManager
{	

	/********************************************************************
	Nom :	getNameClassStudent
	But :	Récupère le nom de la classe d'un élève
	Retour: $strNameClass: Il s'agit du nom de la classe
	Param : $intId: Il s'agit de l'identifiant de l'élève
	********************************************************************/
	public function getNameClassStudent($intStudentId)
	{
		// Requête
		$Request = $this->dao->prepare('SELECT students.school_class_id AS NameClass FROM students WHERE students.id = :intStudentId');

		// Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':intStudentId', $intStudentId);

        // Exécute la requête
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strNameClass = $Request->fetchAll();

        $Request->closeCursor();

        // Enregistre une valeur null si aucune classe n'est retournée
        if (empty($tab_strNameClass[0]['NameClass']))
        {
        	$strNameClass = null;
        }
        else
        {
			$strNameClass = $tab_strNameClass[0]['NameClass'];
        }

        return $strNameClass;
	}// end getNameClassStudent

	/********************************************************************
	Nom :	getClassSchedule
	But :	Récupère le calendrier de la classe demandée
	Retour: $calendar:	- Il s'agit du calendrier correspondant à la classe
	Param : $strClassID:	- Il s'agit de l'identifiant de la classe
	********************************************************************/
    public function getClassSchedule($strClassID) 
    {
        $Request = $this->dao->prepare('SELECT periods.id, periods.day, periods.room_id, periods.branch_id, number_period.id AS "period_id", number_period.start, number_period.end, colleagues_periods.colleague_id, colleagues.name, colleagues.first_name, school_classes_periods.school_class_id 
			FROM school_classes_periods, periods, number_period, colleagues_periods, colleagues
			WHERE school_classes_periods.school_class_id = :strClassID
			AND school_classes_periods.period_id = periods.id
			AND periods.number_period_id = number_period.id
			AND school_classes_periods.period_id = colleagues_periods.period_id 
			AND colleagues_periods.colleague_id = colleagues.id
		');
		
		// Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':strClassID', $strClassID);
        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strClassSchedule = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_strClassSchedule;
    }// end getClassSchedule

    /********************************************************************
	Nom :	getColleagueSchedule
	But :	Récupère le calendrier d'un enseignant
	Retour: $calendar:	- Il s'agit du calendrier correspondant à la classe
	Param : $strColleagueID - Il s'agit de l'identifiant de la classe
	********************************************************************/
    public function getColleagueSchedule($strColleagueID) 
    {
        $Request = $this->dao->prepare('SELECT periods.id, periods.day, periods.room_id, periods.branch_id, number_period.id AS "period_id", number_period.start, number_period.end, colleagues_periods.colleague_id, colleagues.name, colleagues.first_name, school_classes_periods.school_class_id  
			FROM periods, number_period, colleagues_periods, colleagues, school_classes_periods
			WHERE colleagues_periods.colleague_id = :strColleagueID
			AND colleagues_periods.period_id = periods.id
			AND periods.number_period_id = number_period.id
			AND colleagues_periods.colleague_id = colleagues.id
            AND school_classes_periods.period_id = periods.id
			
		');
		
		// Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':strColleagueID', $strColleagueID);
        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strColleagueSchedule = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_strColleagueSchedule;
    }// end getColleagueSchedule

    /********************************************************************
    Nom :   getNameColleague
    But :   Récupère le nom de d'un enseignant
    Retour: $strNameClass: Il s'agit du nom de l'enseignant
    Param : $strId: Il s'agit de l'identifiant de l'enseignant
    ********************************************************************/
    public function getNameColleague($strId)
    {
        // Requête
        $Request = $this->dao->prepare('SELECT students.school_class_id AS NameClass FROM students WHERE students.id = :intStudentId');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':intStudentId', $intStudentId);

        // Exécute la requête
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strNameClass = $Request->fetchAll();

        $Request->closeCursor();

        // Enregistre une valeur null si aucune classe n'est retournée
        if (empty($tab_strNameClass[0]['NameClass']))
        {
            $strNameClass = null;
        }
        else
        {
            $strNameClass = $tab_strNameClass[0]['NameClass'];
        }

        return $strNameClass;
    }// end getNameColleague

    /********************************************************************
	Nom :	getPeriodsNumbers
	But :	Récupère les numéros de périodes
	Retour: $calendar:	Tableau des numéros de périodes
	Param : -
	********************************************************************/
    public function getPeriodNumber() 
    {
        $Request = $this->dao->prepare('SELECT * FROM number_period');
		        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strPeriodNumber = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_strPeriodNumber;
    }// end getPeriodNumber

    /********************************************************************
	Nom :	getBranch
	But :	Récupère la liste de tous les cours
	Retour: $tab_Branch:	Tableau des numéros de périodes
	Param : -
	********************************************************************/
    public function getBranchs() 
    {
        $Request = $this->dao->prepare('SELECT * FROM branchs');
		        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strBranchs = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_strBranchs;
    }// end getBranch

    /********************************************************************
	Nom :	getColleagues
	But :	Récupère la liste de tous les professeurs
	Retour: $tab_Colleagues:	Tableau des collaborateurs/profs
	Param : -
	********************************************************************/
    public function getColleagues() 
    {
        $Request = $this->dao->prepare('SELECT * FROM colleagues order by name');
		        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strColleagues = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_strColleagues;
    }// end getColleagues

    /********************************************************************
    Nom :   getClasses
    But :   Récupère la liste de toutes les classes
    Retour: $tab_Classes:    Tableau des classes
    Param : -
    ********************************************************************/
    public function getClasses() 
    {
        $Request = $this->dao->prepare('SELECT * FROM school_classes');
                
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_Classes = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_Classes;
    }// end getClasses()

    /********************************************************************
	Nom :	getRooms
	But :	Récupère la liste de toutes les salles
	Retour: $tab_strRooms:	Tableau des salles
	Param : -
	********************************************************************/
    public function getRooms() 
    {
        $Request = $this->dao->prepare('SELECT * FROM rooms');
		        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strRooms = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_strRooms;
    }// end getRooms

    /********************************************************************
	Nom :	isClass
	But :	Test si l'ID correspond à celui d'une classe
	Retour: $blnResult:	1 -> Oui / 0 -> Non 
	Param : $intId -> Identifiant à tester
	********************************************************************/
    public function isClass($intId)
    {
    	$Request = $this->dao->prepare('SELECT * FROM school_classes WHERE id=:intID');

    	// Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':intID', $intID);
		        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strClass = $Request->fetchAll();
        
        $Request->closeCursor();

        if (empty($tab_strClass)) 
        {
        	$blnResult = false;
        }
        else
        {
        	$blnResult = true;
        }

        return $blnResult;

    }

    /********************************************************************
	Nom :	isColleague
	But :	Test si l'ID correspond à celui d'un enseignant
	Retour: $blnResult:	1 -> Oui / 0 -> Non 
	Param : $intId -> Identifiant à tester
	********************************************************************/
    public function isColleague($intId)
    {
    	$Request = $this->dao->prepare('SELECT * FROM colleagues WHERE id=:intId');

    	// Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':intId', $intId);
		        
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strColleague = $Request->fetchAll();
        
        $Request->closeCursor();

        if (empty($tab_strColleague)) 
        {
        	$blnResult = false;
        }
        else
        {
        	$blnResult = true;
        }

        return $blnResult;

    }

    /********************************************************************
    Nom :   addPeriod
    But :   Ajoute un cours (dans l'horaire) dans la base de donnée
    Retour: $blnResult -> Résultat de l'ajout
    Param : $tab_strCourse -> Tableau contenant le cours à ajouter
    ********************************************************************/
    public function addPeriod($tab_strCourse)
    {
        $Request = $this->dao->prepare('INSERT INTO periods (day, room_id, branch_id, number_period_id)
                                    VALUES (:day, :room_id, :branch_id, :number_period_id)');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':day', $tab_strCourse['id_Day']);
        $Request->bindValue(':room_id', $tab_strCourse['id_Room']);
        $Request->bindValue(':branch_id', $tab_strCourse['id_Branch']);
        $Request->bindValue(':number_period_id', $tab_strCourse['id_NumberPeriod']);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   selectNewPeriods
    But :   Récupère les périodes qui viennent d'être enregistrées
    Retour: $tab_strPeriods -> Tableau de(s) période(s)
    Param : $strId -> Identifiant de la classe
    ********************************************************************/
    public function selectNewPeriod()
    {
        $Request = $this->dao->prepare('SELECT LAST_INSERT_ID() FROM periods');
                
        $Request->execute();

        $Request->setFetchMode(\PDO::FETCH_ASSOC);

        $tab_strPeriods = $Request->fetchAll();
        
        $Request->closeCursor();

        return $tab_strPeriods;

    } // selectNewPeriods()

    /********************************************************************
    Nom :   addClassPeriod
    But :   Ajoute une période à une classe
    Retour: $blnResult -> Résultat de l'ajout
    Param : $str_PeriodId, $str_ClassId -> Identifiant de la période et 
    #                                      de la classe
    ********************************************************************/
    public function addClassPeriod($str_PeriodId, $str_ClassId)
    {
        $Request = $this->dao->prepare('INSERT INTO school_classes_periods (school_class_id, period_id)
                                    VALUES (:school_class_id, :period_id)');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':school_class_id', $str_ClassId);
        $Request->bindValue(':period_id', $str_PeriodId);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   addColleaguePeriod
    But :   Ajoute une période à un enseignant
    Retour: $blnResult -> Résultat de l'ajout
    Param : $strId -> Identifiant de la classe
    ********************************************************************/
    public function addColleaguePeriod($str_PeriodId, $str_ColleagueId)
    {
        $Request = $this->dao->prepare('INSERT INTO colleagues_periods (colleague_id, period_id)
                                    VALUES (:colleague_id, :period_id)');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':colleague_id', $str_ColleagueId);
        $Request->bindValue(':period_id', $str_PeriodId);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   modifyPeriod
    But :   Modifie un cours (dans l'horaire) 
    Retour: $blnResult -> Résultat de la modification
    Param : $strId -> Identifiant de la classe
    ********************************************************************/
    public function modifyPeriod($tab_strCourse)
    {
        $Request = $this->dao->prepare('UPDATE periods SET day = :day, room_id = :room_id, branch_id = :branch_id
                                        WHERE id = :period_id');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':day', $tab_strCourse['id_Day']);
        $Request->bindValue(':room_id', $tab_strCourse['id_Room']);
        $Request->bindValue(':branch_id', $tab_strCourse['id_Branch']);
        $Request->bindValue(':period_id', $tab_strCourse['id_Period']);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   modifyClassPeriod
    But :   Modifie la classe d'une période (dans l'horaire) 
    Retour: $blnResult -> Résultat de la modification
    Param : $strId -> Identifiant de la classe
    ********************************************************************/
    public function modifyClassPeriod($tab_strCourse)
    {
        $Request = $this->dao->prepare('UPDATE school_classes_periods SET school_class_id = :school_class_id
                                        WHERE period_id = :period_id');


        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':school_class_id', $tab_strCourse['id_class']);
        $Request->bindValue(':period_id', $tab_strCourse['id_Period']);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   modifyColleaguePeriod
    But :   Modifie la classe d'une période (dans l'horaire) 
    Retour: $blnResult -> Résultat de la modification
    Param : $strId -> Identifiant de la classe
    ********************************************************************/
    public function modifyColleaguePeriod($tab_strCourse)
    {
        $Request = $this->dao->prepare('UPDATE colleagues_periods SET colleague_id = :colleague_id
                                        WHERE period_id = :period_id');


        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':colleague_id', $tab_strCourse['id_Colleague']);
        $Request->bindValue(':period_id', $tab_strCourse['id_Period']);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   deletePeriod
    But :   Supprime une période (dans l'horaire) 
    Retour: $blnResult -> Résultat de la modification
    Param : $strId -> Identifiant de la période
    ********************************************************************/
    public function deletePeriod($strId)
    {
        $Request = $this->dao->prepare('DELETE FROM periods WHERE id = :period_id');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':period_id', $strId);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   deleteColleaguePeriod
    But :   Supprime le collégue d'une période (dans l'horaire) 
    Retour: $blnResult -> Résultat de la modification
    Param : $strId -> Identifiant de la période
    ********************************************************************/
    public function deleteColleaguePeriod($strId)
    {
        $Request = $this->dao->prepare('DELETE FROM colleagues_periods WHERE period_id = :period_id');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':period_id', $strId);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }

    /********************************************************************
    Nom :   deleteClassPeriod
    But :   Supprime l'enseignant d'une période (dans l'horaire) 
    Retour: $blnResult -> Résultat de la modification
    Param : $strId -> Identifiant de la période
    ********************************************************************/
    public function deleteClassPeriod($strId)
    {
        $Request = $this->dao->prepare('DELETE FROM school_classes_periods WHERE period_id = :period_id');

        // Remplace la variable dans le code SQL par la variable de la fonction
        $Request->bindValue(':period_id', $strId);
                
        $Request->execute();

        if ($Request) 
        {
            $blnResult = true;
        }
        else
        {
            $blnResult = false;
        }
        
        $Request->closeCursor();

        return $blnResult;

    }
	
}