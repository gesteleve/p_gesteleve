<?php

namespace Applications\Models;

use Applications\Entities\Event;

class EventManager_PDO extends EventManager
{
    public function getList() {
        $req = $this->dao->query('SELECT * FROM happenings');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Event');

        $events = $req->fetchAll();
        $req->closeCursor();

        return $events;
    }

    public function getUnique($id) {
        $req = $this->dao->prepare('SELECT * FROM happenings WHERE id = :id');
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Event');

        $event = $req->fetch();
        $req->closeCursor();

        return $event;
    }

    public function search($pattern) {
        $req = $this->dao->prepare('SELECT * FROM happenings WHERE id = :pattern');
        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Event');

        $events = $req->fetchAll();
        $req->closeCursor();

        return $events;
    }


    public function add() {

    }

    public function modify() {

    }

    public function delete() {

    }
}
