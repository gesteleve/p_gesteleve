<?php
namespace Applications\Models;

//use Applications\Entities\Absence;

use Library\Sly\Database\Manager;

class AbsenceManager_PDO extends Manager
{
    public function getStudentsFromClass($class_id)
    {
        $req = $this->dao->prepare('SELECT id, first_name, name FROM students WHERE school_class_id = ? AND active = 1');
        $req->execute([$class_id]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $absence = $req->fetchAll();
        $req->closeCursor();

        return $absence;
    }

    public function setAbsence($idStudent, $absType , $date, $arrayPeriode)
    {
        /*
        // Récupère le dernier id
        $req = $this->dao->prepare('SELECT idMissing FROM t_missing ORDER BY idMissing DESC LIMIT 1');
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_ASSOC);
        $idMissing = $req->fetch();
        $idMissing = $idMissing['idMissing']+1;

        $req = $this->dao->prepare('INSERT INTO t_missing (idMissing, misType, misJustified, misSummary, fkStudent) VALUES (?, ?, ?, ?, ?)');
        $req->execute([$idMissing, $misType, $misJustified, $misSummary, $idStudent]);
        $req->closeCursor();


        $req = $this->dao->prepare('SELECT idPeriodMissed FROM t_periodmissed ORDER BY idPeriodMissed DESC LIMIT 1');
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_ASSOC);
        $idPeriodMissed = $req->fetch();
        $idPeriodMissed = $idPeriodMissed['idPeriodMissed']+1;

        $i = 0;
        foreach ($arrayPeriode as $periode)
        {
            $req = $this->dao->prepare('INSERT INTO t_periodmissed (idPeriodMissed, perHour, perNumber, perDate, perClasse, fkMissing) VALUES (?, ?, ?, ?, ?, ?)'); // TODO ajouter la classe
            $req->execute([$idPeriodMissed + $i, $periode, 1, date('Y-m-d'), $class, $idMissing]);
            $req->closeCursor();
            $i++;
        }*/

        $j = 0;
        foreach ($arrayPeriode as $periode)
        {
            $req = $this->dao->prepare('INSERT INTO t_absence (fkStudent, absJustified, absType, absPerNumber, absDate) VALUES (?, 0, ?, ?, ?)');
            $req->execute([$idStudent, $absType, $periode, $date]);
            $req->closeCursor();
            $j++;
        }
    }

    public function getDailyAbsence($class, $date)
    {
        //$req = $this->dao->prepare('SELECT idMissing, fkStudent, perHour, perNumber, misType FROM t_periodmissed INNER JOIN t_missing ON fkMissing = idMissing WHERE perDate = ? AND perClasse = ?');
        $req = $this->dao->prepare('SELECT idAbsence, fkStudent, absPerNumber, absType FROM t_absence INNER JOIN students ON id = fkStudent WHERE absDate = ? AND school_class_id = ?');
        $req->execute([$date, $class]);
        $req->setFetchMode(\PDO::FETCH_ASSOC);
        $absence = $req->fetchAll();
        $req->closeCursor();

        return $absence;
    }

    public function modifyAbsenceType($idAbsence, $type)
    {
        $req = $this->dao->prepare('UPDATE t_absence SET absType=? WHERE idAbsence=?;');
        $req->execute([$type, $idAbsence]);
        $req->closeCursor();
    }


    public function deleteAbsenceType($idAbsence)
    {
        $req = $this->dao->prepare('DELETE FROM t_absence WHERE idAbsence =?;');
        $req->execute([$idAbsence]);
        $req->closeCursor();
    }


    /**
     * @param $student string Élève concerné
     * @param $beginDateTime Date et heure du début (Y-m-d H:i:s)
     * @param $endDateTime Date et heure du fin (Y-m-d H:i:s)
     * @param $summary Modif de l'absence
     * @return bool True si la justification a bien été ajoutée
     * Permet d'ajouter une justification
     */
    public function addJustification($student, $beginDateTime, $endDateTime, $summary){
        $req = $this->dao->prepare('INSERT INTO t_justification (fkStudent, jusDateBegin, jusDateEnd, jusSummary) VALUES (?, ?, ?, ?)');
        $res = $req->execute([
            $student,
            $beginDateTime,
            $endDateTime,
            $summary
        ]);
        $req->closeCursor();
        return $res;
    }

    public function getAllJustificatifsByWeekYear($week, $year){
        $req = $this->dao->prepare('SELECT * FROM t_justification JOIN students on t_justification.fkStudent = students.id WHERE WEEK(jusDateEnd) = ? AND YEAR(jusDateEnd) = ?');
        $req->execute([$week, $year]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $docs = $req->fetchAll();
        $req->closeCursor();

        return $docs;
    }

    public function getAllJustificatifsByClassWeekYear($class,$week, $year){
        $req = $this->dao->prepare('SELECT * FROM t_justification JOIN students on t_justification.fkStudent = students.id WHERE school_class_id = ? AND WEEK(jusDateEnd) = ? AND YEAR(jusDateEnd) = ?');
        $req->execute([$class,$week, $year]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $docs = $req->fetchAll();
        $req->closeCursor();

        return $docs;
    }

    public function getAllAbsenceFromClassWeekYear($class, $week, $year){
        $req = $this->dao->prepare('SELECT * FROM t_absence JOIN students on t_absence.fkStudent = students.id WHERE school_class_id = ? AND WEEK(absDate) = ? AND YEAR(absDate) = ?');
        $req->execute([$class, $week, $year]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $docs = $req->fetchAll();
        $req->closeCursor();

        return $docs;
    }

    public function getAllSchedules(){
        $req = $this->dao->prepare('SELECT * FROM t_schedule');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $docs = $req->fetchAll();
        $req->closeCursor();

        return $docs;
    }


}