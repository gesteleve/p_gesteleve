<?php
namespace Applications\Models;

//use Applications\Entities\Absence;
use Applications\Entities\Student;

use Applications\Entities\Remedial;
use Library\Sly\Database\Manager;

class RemedialManager_PDO extends Manager
{
    /**
     * Cette fonction renvoit tous les rattrapages relatifs à un élèves
     * @param $fkStudent
     * @return mixed
     */
    public function getAllRemedials($fkStudent){
        $req = $this->dao->prepare('SELECT * FROM t_remedial where fkStudent = ?');
        $req->execute([$fkStudent]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $remedial = $req->fetchAll();
        $req->closeCursor();

        return $remedial;
    }

    /**
     * Cette fonction ajoute une remédiation à la BDD en récupérant les variable dans l'Entity
     * @param $idRemedial
     * @param $remDate
     * @param $remType
     * @param $remNbPeriod
     * @param $fkStudent
     */
    public function addRemedial($remDate, $remType, $remNbPeriod, $fkStudent){
        $req = $this->dao->prepare('INSERT INTO t_remedial (remDate, remType, remNbPeriod, fkStudent) VALUES (:remDate, :remType, :remNbPeriod, :fkStudent)');

        //Association des valeurs aux paramètres
        $req->bindValue(':remDate', $remDate);
        $req->bindValue(':remType', $remType);
        $req->bindValue(':remNbPeriod', $remNbPeriod);
        $req->bindValue(':fkStudent', $fkStudent);

        $req->execute();
        $req->closeCursor();
    }

    public function deleteRemedial($id){
        $req = $this->dao->prepare('DELETE FROM t_Remedial WHERE idRemedial = ?');
        $req->execute([$id]);
        $req->closeCursor();
    }

    public function getStudent($id){
        $req = $this->dao->prepare('SELECT * FROM students WHERE id = ?');
        $req->execute([$id]);

        $req->setFetchMode(\PDO::FETCH_CLASS, '\Applications\Entities\Student');

        $student = $req->fetch();
        $req->closeCursor();

        return $student;
    }


    public function getRemedialById($id){
        $req = $this->dao->prepare('SELECT * FROM t_remedial where idRemedial = ?');
        $req->execute([$id]);

        $req->setFetchMode(\PDO::FETCH_ASSOC);

        $remedial = $req->fetch();
        $req->closeCursor();

        return $remedial;
    }

    public function modifyRemedial($idRemedial,$remDate, $remType, $remNbPeriod){
        $req = $this->dao->prepare('UPDATE t_remedial SET remDate = :remDate, remType = :remType, remNbPeriod = :remNbPeriod where idRemedial = :idRemedial');
        $req->bindValue(':remDate',$remDate);
        $req->bindValue(':remType', $remType);
        $req->bindValue(':remNbPeriod', $remNbPeriod);
        $req->bindValue(':idRemedial', $idRemedial);
        $req->execute();

        $req->closeCursor();
    }
}