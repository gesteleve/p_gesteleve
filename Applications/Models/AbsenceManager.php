<?php
namespace Applications\Models;

use Applications\Entities\Absence;
use Library\Sly\Database\Manager;

abstract class AbsenceManager extends Manager
{
    abstract function getStudentsFromClass($class_id);
    abstract function setAbsence($idStudent, $absType, $date, $arrayPeriode);
    abstract function getDailyAbsence($class, $date);
    abstract function modifyAbsenceType($idAbsence, $type);
    abstract function deleteAbsenceType($idAbsence);
}
