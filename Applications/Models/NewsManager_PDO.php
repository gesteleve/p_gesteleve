<?php

namespace Applications\Models;

use Applications\Entities\News;

class NewsManager_PDO extends NewsManager
{
    // *******************************************************************
    // Nom : getList
    // But : Avoir la lsite de toute les news
    // Retour: $news: contient toutes les news de la base de donnée
    // Param.: -
    // *******************************************************************
    public function getSearchTitle() 
    {

        // Requête
        $req = $this->dao->query('SELECT SearchContent
                                  FROM searchtitle');

        // Déclaration de l'entities "News"
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');

        //Création du tableau
        $Title = $req->fetchAll();
        $req->closeCursor();

        //Envoie du tableau contenant les news
        return $Title;

    }  


    // *******************************************************************
    // Nom : getList
    // But : Avoir la lsite de toute les news
    // Retour: $news: contient toutes les news de la base de donnée
    // Param.: -
    // *******************************************************************
    public function getList() 
    {

        // Requête
        $req = $this->dao->query('SELECT *
                                  FROM news');

        // Déclaration de l'entities "News"
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');

        //Création du tableau
        $news = $req->fetchAll();
        $req->closeCursor();

        //Envoie du tableau contenant les news
        return $news;

    }    


    // *******************************************************************
    // Nom : getListNewsEveryBody
    // But : Récupère la liste des news que tous le monde peut voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsEveryBody() 
    {
        //Requête faite à la BD
        $req = $this->dao->query('SELECT *
                                  FROM news
                                  WHERE rights = 2 AND archive = 0
                                  ORDER BY start_date DESC');

        // Passe les données dans le fichier entities de news
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');

        //Crée un tableau
        $news = $req->fetchAll();
        $req->closeCursor();

        //Retourne le tableau contenant les news
        return $news;

    }

    // *******************************************************************
    // Nom : getListNewsEveryBody
    // But : Récupère la liste des news que tous le monde peut voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getClassTeacher($id) 
    {
        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT school_classes.*
                                  FROM school_classes, students
                                  WHERE :id = students.id AND students.school_class_id = school_classes.id');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetchAll();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;

    }

    // *******************************************************************
    // Nom : getListNewsEveryBody
    // But : Récupère la liste des news que tous le monde peut voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsStudent($id) 
    {
        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT news.*
                                  FROM news
                                  WHERE idx_colleague = :id AND archive = 0 OR rights = 2 AND archive = 0
                                  ORDER BY start_date DESC');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetchAll();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;

    }

    // *******************************************************************
    // Nom : getListNewsEveryBody
    // But : Récupère la liste des news que tous le monde peut voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsStudentDate($id) 
    {
        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT news.*
                                  FROM news
                                  WHERE idx_colleague = :id AND archive = 0 OR rights = 2 AND archive = 0
                                  ORDER BY start_date ASC');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetchAll();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;

    }

    // *******************************************************************
    // Nom : getListNewsEveryBody
    // But : Récupère la liste des news que tous le monde peut voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsStudentAutor($id) 
    {
        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT news.*
                                  FROM news
                                  WHERE idx_colleague = :id AND archive = 0 OR rights = 2 AND archive = 0
                                  ORDER BY idx_colleague DESC');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetchAll();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;

    }
    

    // *******************************************************************
    // Nom : getListNewsEveryBodyFilterDate
    // But : Récupère la liste des news que tous le monde peut voir, et qui ne sont pas archivée et trie le tableau par date
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsEveryBodyFilterDate() 
    {
        //Requête faite à la BD
        $req = $this->dao->query('SELECT *
                                  FROM news
                                  WHERE rights = 2 AND archive = 0
                                  ORDER BY start_date ASC');

        // Passe les données dans le fichier entities de news
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');

        //Crée un tableau
        $news = $req->fetchAll();
        $req->closeCursor();

        //Retourne le tableau contenant les news
        return $news;

    }

    // *******************************************************************
    // Nom : getListNewsEveryBodyFilterAutor
    // But : Récupère la liste des news que tous le monde peut voir, et qui ne sont pas archivée et trie les entrée par auteur
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsEveryBodyFilterAutor() 
    {
        //Requête faite à la BD
        $req = $this->dao->query('SELECT *
                                  FROM news
                                  WHERE rights = 2 AND archive = 0
                                  ORDER BY idx_colleague');

        // Passe les données dans le fichier entities de news
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');

        //Crée un tableau
        $news = $req->fetchAll();
        $req->closeCursor();

        //Retourne le tableau contenant les news
        return $news;

    }


    // *******************************************************************
    // Nom : getListNewsTeacher
    // But : Récupère la liste des news que les enseignants peuvent voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsTeacher($id) 
    {

        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT news.*
                                  FROM news
                                  WHERE idx_colleague = :id AND archive = 0 OR rights = 1 AND archive = 0 OR rights = 2 AND archive = 0
                                  ORDER BY start_date DESC');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetchAll();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;

    }    

    // *******************************************************************
    // Nom : getListNewsTeacher
    // But : Récupère la liste des news que les enseignants peuvent voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsTeacherByDate($id) 
    {

        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT news.*
                                  FROM news
                                  WHERE idx_colleague = :id AND archive = 0 OR rights = 1 AND archive = 0 OR rights = 2 AND archive = 0
                                  ORDER BY start_date ASC');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetchAll();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;

    }    

    // *******************************************************************
    // Nom : getListNewsTeacher
    // But : Récupère la liste des news que les enseignants peuvent voir, et qui ne sont pas archivée
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getListNewsTeacherByAutor($id) 
    {

        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT news.*
                                  FROM news
                                  WHERE idx_colleague = :id AND archive = 0 OR rights = 1 AND archive = 0 OR rights = 2 AND archive = 0
                                  ORDER BY idx_colleague DESC');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetchAll();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;

    }    

    

    

    // *******************************************************************
    // Nom : getNewsArchived
    // But : Récupère la liste des news archivées
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: -
    // *******************************************************************
    public function getNewsArchived() 
    {

        //Requête faite à la BD
        $req = $this->dao->query('SELECT *
                                  FROM news
                                  WHERE archive = 1;
                                  ORDER BY start_date DESC');

        // Passe les données dans le fichier entities de news
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');

        //Crée un tableau
        $news = $req->fetchAll();
        $req->closeCursor();

        //Retourne le tableau contenant les news
        return $news;

    }    


    // *******************************************************************
    // Nom : getUnique
    // But : Récupère une news spécifique
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: $id: contient l'id de la news à connaitre
    // *******************************************************************
    public function getUnique($id) 
    {

        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT *
                                  FROM news 
                                  WHERE id = :id');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        // Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetch();

        //Ferme la connexion à la BD        
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;
        
    }

    // **********************************************************************************************
    // Nom : getUniqueClass
    // But : Récupère la classe d'un collègue, permet d'ajouter et de modifier une news par la suite
    // Retour: $news: contient les news retournée par la requêtes
    // Param.: $id: contient l'id de la news à connaitre
    // **********************************************************************************************
    public function getUniqueClass($id) 
    {

        //Requête faite à la BD
        $req = $this->dao->prepare('SELECT id
                                  FROM school_classes 
                                  WHERE colleague_id = :id');

        // Récupère l'id passé en paramètre
        $req->bindValue(':id', $id);

        //Execute la requete
        $req->execute();

        //Crée un tableau
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\News');
        $news = $req->fetch();

        //Ferme la connexion à la BD
        $req->closeCursor();

        //Retourne le tableau contenant la news
        return $news;
        
    }   


    // **********************************************************************************************
    // Nom : add
    // But : Ajoute une news à la BD
    // Retour: $success: Vérifie si la news à bien été ajoutée
    // Param.: $news: contient les données de la news à ajouter
    // **********************************************************************************************
    public function add(News $news) 
    {
        
        //Date d'aujourd'hui
        $today = date("Y-m-d");

        // Envoie de la requete à la base de données
        $req = $this->dao->prepare('INSERT INTO `news`(`id`, `add_date`, `mod_date`, `start_date`, `end_date`, `title`, `content`, `rights`, `archive`, `idx_colleague`, `idx_schoolClasse`) 
                                    VALUES (NULL, :add_date, NULL, :start_date, :end_date, :title, :content, :rights, :archive, :idx_colleague, :idx_schoolClasse)');
      
        // Insert les donnnées concernant la news dans la requete
        $req->bindValue(':add_date', $today);       
        $req->bindValue(':start_date', $news->start_date());        
        $req->bindValue(':end_date', $news->end_date());
        $req->bindValue(':title', $news->title());
        $req->bindValue(':content', $news->content()); 
        $req->bindValue(':rights', $news->right());
        $req->bindValue(':archive', '0');
        $req->bindValue(':idx_colleague', $news->colleague());
        $req->bindValue(':idx_schoolClasse', $news->classe()); 

        // Execute la requetes
        $success = $req->execute();

        //Ferme la connexion à la BD
        $req->closeCursor();

        // Retourne le resultat lorsqu'on execute la requete
        return $success;

    }


    // **********************************************************************************************
    // Nom : edit
    // But : Modifie une news
    // Retour: $success: Vérifie si la news à bien été modifiée
    // Param.: $news: contient les données de la news à modifier
    // **********************************************************************************************
    public function edit(News $news) 
    {

        //Date d'aujourd'hui
        $today = date("Y-m-d");

        //Requete faite à la BD
        $req = $this->dao->prepare('UPDATE news SET  `add_date` =:add_date,
        `mod_date` =:mod_date,
        `start_date` =:start_date,
        `end_date` =:end_date,
        `title` =  :title,
        `content` =  :content,
        `rights` =:rights,
        `archive` =:archive,
        `idx_colleague` =  :idx_colleague,
        `idx_schoolClasse` =  :idx_schoolClasse WHERE  `id` =:id');

        // Insert les donnnées concernant la news dans la requete
        $req->bindValue(':id', $news->id());
        $req->bindValue(':add_date', $news->add_date());
        $req->bindValue(':mod_date', $today);
        $req->bindValue(':start_date', $news->start_date());        
        $req->bindValue(':end_date', $news->end_date());
        $req->bindValue(':title', $news->title());
        $req->bindValue(':content', $news->content()); 
        $req->bindValue(':rights', $news->right());
        $req->bindValue(':archive', '0');
        $req->bindValue(':idx_colleague', $news->colleague());
        $req->bindValue(':idx_schoolClasse', $news->classe());        

        // Execute la requete
        $success = $req->execute();

        //Ferme la connexion à la BD
        $req->closeCursor();

        // Retourne le resultat lorsqu'on execute la requete
        return $success;

    }


    // **********************************************************************************************
    // Nom : delete
    // But : Supprime la news définitivement
    // Retour: $success: Vérifie si la news à bien été supprimée
    // Param.: $news: contient les données de la news à supprimer
    // **********************************************************************************************
    public function delete($id) 
    {
        
        //Requete faite à la BD
        $req = $this->dao->prepare('DELETE FROM news WHERE id = :id;');

        // Insert les donnnées concernant la news dans la requete
        $req->bindValue(':id', $id);

        //Execute la requete
        $success = $req->execute();

        //Ferme la connexion à la BD
        $req->closeCursor();

        // Retourne le resultat lorsqu'on execute la requete
        return $success;

    }


    // **********************************************************************************************
    // Nom : archiveNews
    // But : Archive une news
    // Retour: $success: Envoie le résultat de l'execution de la requete
    // Param.: $id: contient l'id de la news à archiver
    // **********************************************************************************************
    public function archiveNews($id) 
    {
        
        //Requete faite à la BD
        $req = $this->dao->prepare('UPDATE news SET `archive` = 1 WHERE `id` = :id;');

        // Insert les donnnées concernant la news dans la requete
        $req->bindValue(':id', $id);

        //Execute la requete
        $success = $req->execute();

        //Ferme la connexion à la BD
        $req->closeCursor();

        // Retourne le resultat lorsqu'on execute la requete
        return $success;

    }
    

    public function search() {
       
    }
}
