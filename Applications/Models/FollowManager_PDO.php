<?php

namespace Applications\Models;

use Applications\Entities\Follow;

class FollowManager_PDO extends FollowManager
{
    public function getListByStudent($student_id) {
        $req = $this->dao->prepare('SELECT follows.*, colleagues.name AS colleague_name, colleagues.first_name AS colleague_first_name, mod_colleagues.name AS mod_colleague_name, mod_colleagues.first_name AS mod_colleague_first_name
                                  FROM follows 
                                  INNER JOIN colleagues ON follows.colleague_id = colleagues.id 
                                  LEFT OUTER JOIN colleagues AS mod_colleagues ON follows.mod_colleague_id = mod_colleagues.id
                                  WHERE follows.student_id = :id
                                  ORDER BY add_date DESC');
        $req->bindValue(':id', $student_id);
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Follow');

        $follows = $req->fetchAll();
        $req->closeCursor();

        return $follows;
    }

    public function getListByColleague($colleague_id) {
        $req = $this->dao->prepare('SELECT follows.*,  students.school_class_id as student_class,
                                  students.name AS student_name, students.first_name AS student_first_name,
                                  mod_colleagues.name AS mod_colleague_name,
                                  mod_colleagues.first_name AS mod_colleague_first_name
                                  FROM follows
                                  INNER JOIN students ON follows.student_id = students.id
                                  LEFT OUTER JOIN colleagues AS mod_colleagues ON follows.mod_colleague_id = mod_colleagues.id
                                  WHERE follows.colleague_id = :id and students.active=1
                                  ORDER BY add_date DESC');
        $req->bindValue(':id', $colleague_id);
        $req->execute();
        
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Follow');

        $follows = $req->fetchAll();
        $req->closeCursor();

        return $follows;
    }

    public function getListByMaster($colleague_id) {

        /* $req = $this->dao->prepare('SELECT follows.*, colleagues.name AS colleague_name, colleagues.first_name AS colleague_first_name, mod_colleagues.name AS mod_colleague_name, mod_colleagues.first_name AS mod_colleague_first_name, students.name AS student_name, students.first_name AS student_first_name
                                  FROM follows 
                                  INNER JOIN colleagues ON follows.colleague_id = colleagues.id
                                  INNER JOIN students ON follows.student_id = students.id 
                                  LEFT OUTER JOIN colleagues AS mod_colleagues ON follows.mod_colleague_id = mod_colleagues.id
                                  WHERE follows.student_id IN (SELECT students.id
                                                                FROM students
                                                                WHERE students.school_class_id = (SELECT school_classes.id
                                                                                                    FROM school_classes
                                                                                                    WHERE school_classes.colleague_id = :id))
                                  ORDER BY add_date DESC');
        */

        $query = 'SELECT follows.*, students.school_class_id as student_class, colleagues.name AS colleague_name,
                                    colleagues.first_name AS colleague_first_name,
                                    mod_colleagues.name AS mod_colleague_name,
                                    mod_colleagues.first_name AS mod_colleague_first_name,
                                    students.name AS student_name, students.first_name AS student_first_name
                                    FROM ((follows INNER JOIN students ON follows.student_id = students.id)
                                    INNER JOIN colleagues ON follows.colleague_id = colleagues.id)
                                    LEFT  JOIN colleagues AS mod_colleagues ON follows.mod_colleague_id = mod_colleagues.id
                                    LEFT JOIN school_classes ON students.school_class_id = school_classes.id
                                    WHERE school_classes.colleague_id=:id and students.active=1 ORDER BY add_date DESC';

        $req = $this->dao->prepare($query);



        $req->bindValue(':id', $colleague_id);
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Follow');
        
        $follows = $req->fetchAll();
        $req->closeCursor();

        return $follows;
    }

    public function getListByMainMaster($profession_id) {

        $query = "SELECT follows.*, students.school_class_id as student_class, colleagues.name AS colleague_name,
                                    colleagues.first_name AS colleague_first_name,
                                    students.name AS student_name, students.first_name AS student_first_name
                    FROM school_classes
                    INNER JOIN students
                    ON students.school_class_id = school_classes.id
                    INNER JOIN follows
                    ON follows.student_id = students.id
                    INNER JOIN colleagues
                    ON colleagues.id = follows.colleague_id
                    WHERE school_classes.profession_id = :id
                    ORDER BY add_date DESC";

        $req = $this->dao->prepare($query);

        $req->bindValue(':id', $profession_id);
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Follow');

        $follows = $req->fetchAll();
        $req->closeCursor();

        return $follows;


    }

    public function getUnique($id) {
        $req = $this->dao->prepare('SELECT follows.*
                                  FROM follows 
                                  WHERE follows.id = :id');
        $req->bindValue(':id', $id);
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Follow');

        $follow = $req->fetch();
        $req->closeCursor();

        return $follow;
    }

    public function add(Follow $follow) {
        $req = $this->dao->prepare('INSERT INTO follows (`content`, `add_date`, `right`, `student_id`, `colleague_id`)
                                    VALUES (:content, NOW(), :right, :student_id, :colleague_id)');
        $req->bindValue(':content', $follow->content());
        $req->bindValue(':right', 0, \PDO::PARAM_INT);
        $req->bindValue(':student_id', $follow->student_id());
        $req->bindValue(':colleague_id', $follow->colleague_id());
        $success = $req->execute();
        $req->closeCursor();

        return $success;
    }

    public function delete($id) {
        $req = $this->dao->prepare('DELETE FROM follows WHERE id = :id;
                                    DELETE FROM follows_updates WHERE follow_id = :id');
        $req->bindValue(':id', $id);
        $success = $req->execute();
        $req->closeCursor();

        return $success;
    }

    public function update(Follow $follow, Follow $followUpdate) {
        $req = $this->dao->prepare('INSERT INTO follows_updates (`content`, `add_date`, `right`, `follow_id`, `colleague_id`)
                                    VALUES (:content, :add_date, :right, :follow_id, :colleague_id);
                                    UPDATE follows
                                    SET `content` = :update_content, `right` = :update_right, `mod_date` = NOW(), `mod_colleague_id` = :update_mod_colleague_id
                                    WHERE id = :follow_id');

        $req->bindValue(':content', $follow->content());
        $req->bindValue(':add_date', ($follow->mod_date() != null) ? $follow->mod_date()->format('Y-m-d H:i:s') : $follow->add_date()->format('Y-m-d H:i:s'));
        $req->bindValue(':right', 0, \PDO::PARAM_INT);
        $req->bindValue(':colleague_id', ($follow->mod_colleague_id() != null) ? $follow->mod_colleague_id() : $follow->colleague_id());

        $req->bindValue(':follow_id', $followUpdate->id());

        $req->bindValue(':update_content', $followUpdate->content());
        $req->bindValue(':update_right', ($followUpdate->right() == 1) ? 1 : 0, \PDO::PARAM_INT);
        $req->bindValue(':update_mod_colleague_id', $followUpdate->mod_colleague_id());
  

        $success = $req->execute();
        $req->closeCursor();

        return $success;
    }
}