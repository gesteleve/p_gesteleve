<?php
/****************************************************************************
Nom:        ScheduleManager.php
Auteur:     Jonathan Hermann
Date:       12.05.2014
But:		Il s'agit du modèle de l'application du calendrier
Sources:	-
****************************************************************************/
namespace Applications\Models;

use Applications\Entities\Schedule;
use Library\Sly\Database\Manager;

abstract class ScheduleManager extends Manager
{
	abstract function getNameClassStudent($intStudentId); // Permet de récupérer le nom d'une classe
	abstract function getClassSchedule($strClassID); // Permet de récupérer l'horaire d'une classe (MIN3a, CIN2b)
	abstract function getPeriodNumber(); // Permet de récupérer la liste des périodes
	abstract function getBranchs(); // Permet de récupérer la liste des cours
	abstract function getColleagues();
	abstract function getRooms();
	abstract function isClass($intId);
	abstract function isColleague($intId);
	abstract function addPeriod($tab_strCourse);
	
}
