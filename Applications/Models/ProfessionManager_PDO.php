<?php

namespace Applications\Models;

use Applications\Entities\Profession;

class ProfessionManager_PDO extends ProfessionManager
{
    public function getList() {
        $req = $this->dao->query('SELECT * FROM professions ORDER BY name');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Profession');

        $professions = $req->fetchAll();
        $req->closeCursor();

        return $professions;
    }

    // retourne que la liste des professions
    public function getListSection() {
        $req = $this->dao->query('SELECT * FROM professions where filter=1 ORDER BY name');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Profession');

        $professions = $req->fetchAll();
        $req->closeCursor();

        return $professions;
    }

    public function getUnique($id) {
        $req = $this->dao->prepare('SELECT * FROM professions WHERE id = :id');
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Profession');

        $profession = $req->fetch();
        $req->closeCursor();

        return $profession;
    }

    
    // Cherche les classes en fonction de la profession
    public function getListSchoolClass($id) {
        $req = $this->dao->prepare('SELECT * FROM school_classes WHERE profession_id = :id');
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\schoolClass');

        $schoolClasses = $req->fetchAll();
        $req->closeCursor();

        return $schoolClasses;
    } 

    // Cherche tous les élèves
    public function getStudents() {
        $req = $this->dao->query('SELECT * FROM students where students.active = 1');        

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Student');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }

    // Cherche tous les élèves
    public function getStudentsBySection() {
        $req = $this->dao->query('SELECT professions.name, professions.id as profession_id, Count(students.id) AS student_nbre
                                  FROM (students INNER JOIN school_classes ON students.school_class_id = school_classes.id) INNER JOIN professions ON school_classes.profession_id = professions.id
                                  where students.active=1
                                  GROUP BY professions.name');        

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Profession');

        $StudentsBySection = $req->fetchAll();
        $req->closeCursor();

        return $StudentsBySection;
    }
    


    public function search($pattern) {
        $req = $this->dao->prepare('SELECT * FROM professions WHERE name = :pattern ORDER BY name');
        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Profession');

        $professions = $req->fetchAll();
        $req->closeCursor();

        return $professions;
    }

     

    public function add() {

    }

    public function modify() {

    }

    public function delete() {

    }
}
