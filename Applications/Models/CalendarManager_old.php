<?php
/****************************************************************************
Nom:		CalendarManager.php
Auteur:		????
Date:		??.??.????
But:		Il s'agit du modèle de l'application du calendrier
Sources:	-
*****************************************************************************
Modifications
Date  : 21.01.2014
Auteur: Ludovic Delafontaine - MIN 2011-2015
Raison: - Ajout des fonctions créées dans le fichier CalendarManager_PDO.php
A faire: -
*****************************************************************************
Modifications
Date  : 21.01.2014
Auteur: Ludovic Delafontaine - MIN 2011-2015
Raison: - Création des fonctions getSchoolClassCalendar, getColleagueCalendar et getClassRoomCalendar
A faire: -
****************************************************************************/
namespace Applications\Models;

use Applications\Entities\Calendar;
use Library\Sly\Database\Manager;

abstract class CalendarManager extends Manager
{
    abstract function getSchoolClassCalendar($class_id); // Permet de récupérer l'horaire d'une classe (MIN3a, CIN2b)
    abstract function getColleagueCalendar($colleague_id); // Permet de récupérer l'horaire d'une personne (delafontlu, zfpdls)
    abstract function getClassRoomCalendar($room_id); // Permet de récupérer l'horaire d'une salle de classe (N101, N512b)
	
}
