<?php

namespace Applications\Models;

use Applications\Entities\Historic;

class HistoricManager_PDO extends HistoricManager
{

    public function getList() {
        $req = $this->dao->query('SELECT * FROM historics');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Historic');

        $historics = $req->fetchAll();
        $req->closeCursor();

        return $historics;
    }

    public function add(Historic $historic) {
        $req = $this->dao->prepare('INSERT INTO historics (add_date, type, text, colleague_id)
                                   VALUES (:add_date, :type, :text, :colleague_id)');

        $req->bindValue(':add_date', date("Y-m-d H:i:s"));
        $req->bindvalue(':type', $historic->type());
        $req->bindValue(':text', $historic->text());
        $req->bindValue(':colleague_id', $historic->colleague_id());
        $req->execute();
        $req->closeCursor();
    }
}
