<?php

namespace Applications\Models;

use Applications\Entities\Historic;
use Library\Sly\Database\Manager;


abstract class HistoricManager extends Manager
{
    abstract function add(Historic $historic);
}
