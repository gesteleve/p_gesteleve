<?php

namespace Applications\Models;

use Applications\Entities\Student;

class ColleagueManager_PDO extends ColleagueManager
{
    public function getList() {
        $req = $this->dao->query('SELECT colleagues.*, professions.name as profession_name FROM professions 
                                  right join colleagues on professions.id=colleagues.profession_id 
                                  where colleagues.Actif=1 ORDER BY colleagues.name');
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }

    public function getListMasterClassColleague($id) {
        $req = $this->dao->query('SELECT school_classes.*, professions.name AS profession_name 
                                  FROM school_classes LEFT JOIN professions 
                                  ON school_classes.profession_id = professions.id 
                                  WHERE school_classes.colleague_id = :id');
        
        $req->bindValue(':id', $id);
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\SchoolClasse');

        $ListMasterClassColleague = $req->fetchAll();
        $req->closeCursor();

        return $ListMasterClassColleague;
    }

    public function getListSectionColleague() {
        $req = $this->dao->query('SELECT DISTINCT professions.name AS profession_name
                                  FROM colleagues, professions 
                                  WHERE colleagues.profession_id = professions.id
								  AND colleagues.Actif=1
                                  order by professions.order');
        
        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $sectionsColleagues = $req->fetchAll();
        $req->closeCursor();

        return $sectionsColleagues;
    }

    public function getAllRoom () {
        $req = $this->dao->query('SELECT DISTINCT room_id FROM colleagues_rooms ORDER BY room_id ASC');

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $AllRoom = $req->fetchAll();
        $req->closeCursor();

        return $AllRoom;
    }
	
    public function getColleagueRoom ($colleagueId) {
        $req = $this->dao->query('SELECT room_id FROM colleagues_rooms WHERE colleague_id =\''.$colleagueId.'\'');

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $colleagueRoom = $req->fetchAll();
        $req->closeCursor();

        return $colleagueRoom;
    }
	
    public function getColleagueRoles($colleague_id) {
        $req = $this->dao->query('SELECT roles.name FROM roles, colleagues_roles 
                                    WHERE roles.id = colleagues_roles.role_id 
                                    AND colleagues_roles.colleague_id = \''.$colleague_id.'\';'
        );

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $colleagueRoles = $req->fetchAll();
        $req->closeCursor();

        return $colleagueRoles;
    }

    public function getColleaguesRoles() {
        $req = $this->dao->query('SELECT * FROM roles, colleagues_roles 
                                    WHERE roles.id = colleagues_roles.role_id' 
        );

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $colleagueRoles = $req->fetchAll();
        $req->closeCursor();

        return $colleagueRoles;
    }

    public function getUnique($id) {
        $req = $this->dao->prepare('SELECT * FROM colleagues WHERE id = :id');
        $req->bindValue(':id', $id);
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $student = $req->fetch();
        $req->closeCursor();

        return $student;
    }

    public function getRight($id) {
        $rights_of_colleague = array();
        $req = $this->dao->prepare('SELECT BIT_OR(groups_modules.rights), modules.id FROM colleagues_groups, groups, groups_modules, modules
                                   WHERE
                                        colleagues_groups.colleague_id = :id
                                        AND colleagues_groups.group_id = groups.id
                                        AND groups_modules.group_id = groups.id
                                        AND groups_modules.module_id = modules.id
                                    GROUP BY module_id');
        $req->bindValue(':id', $id);
        $req->execute();

        $right = $req->fetchAll();

        $j = count($right);
        for ($i = 0; $j > $i; $i++) {
            $rights_of_colleague[$right[$i]['id']] = $right[$i]['BIT_OR(groups_modules.rights)'];
        }

        return $rights_of_colleague;
    }
	
	   public function getAllRoles() {
        $req = $this->dao->query('SELECT roles.name FROM roles ORDER BY roles.name ASC'
        );

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $colleagueRoles = $req->fetchAll();
        $req->closeCursor();

        return $colleagueRoles;
    }
	
	public function getGroup($id) 
    {
        $rights_of_colleague = array();
        $req = $this->dao->prepare('SELECT colleagues_groups.group_id FROM colleagues_groups
                                   WHERE
                                        colleagues_groups.colleague_id = :id');
        $req->bindValue(':id', $id);

        $req->execute();

        $group = $req->fetch();

        return $group['group_id'];
    }

	
    public function search($pattern) {
        $req = $this->dao->prepare('SELECT * FROM colleagues WHERE id LIKE :pattern OR name LIKE :pattern OR first_name LIKE :pattern OR email LIKE :pattern');
        $req->bindValue(':pattern', '%'.$pattern.'%');
        $req->execute();

        $req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Applications\Entities\Colleague');

        $students = $req->fetchAll();
        $req->closeCursor();

        return $students;
    }

     public function loadData($id,$Name,$First_Name,$email,$inter_phone,$phone)
    {
        
        $req = $this->dao->query('SELECT colleagues.* FROM colleagues');
        $colleagues = $req->fetchAll();
        $req->closeCursor();
        
        $idData = $this->dao->query('SELECT id FROM colleagues');
        $idDataColleagues = $idData ->fetchAll();
        $idData ->closeCursor(); 
     
        if(empty($colleagues))
        {

            $req = $this->dao->prepare('INSERT INTO colleagues (id, name, first_name,inter_phone, phone, email)
                                    VALUES (:id, :name, :first_name, :inter_phone, :phone, :email)');

            
            $req->bindValue(':id',$id);
            $req->bindValue(':name', $Name);
            $req->bindValue(':first_name', $First_Name);
            $req->bindValue(':inter_phone', $inter_phone);
            $req->bindValue(':phone', $phone);
            $req->bindValue(':email', $email);
            $req->execute();
            $req->closeCursor();

        }    
        else
        {   
            $NewAdd = 0;
            for($x=0;$x<count($idDataColleagues);$x++)
            {
                if($idDataColleagues[$x][0] == $id)
                {
                    
                        $req = $this->dao->prepare('UPDATE colleagues SET name = :name, first_name = :first_name, inter_phone = :inter_phone,phone = :phone, email = :email WHERE id = :id');

                        $req->bindValue(':id',$id);
                        $req->bindValue(':name', $Name);
                        $req->bindValue(':first_name', $First_Name);
                        $req->bindValue(':inter_phone', $inter_phone);
                        $req->bindValue(':phone', $phone);
                        $req->bindValue(':email', $email);

                        $req->execute();
                        $req->closeCursor();
  
                    $NewAdd=1;
                    break;
                }
                   
            }
            if($NewAdd == 0)
            {
                $req = $this->dao->prepare('INSERT INTO colleagues (id, name, first_name,inter_phone, phone, email)
                                    VALUES (:id, :name, :first_name, :inter_phone, :phone, :email)');

            
                $req->bindValue(':id',$id);
                $req->bindValue(':name', $Name);
                $req->bindValue(':first_name', $First_Name);
                $req->bindValue(':inter_phone', $inter_phone);
                $req->bindValue(':phone', $phone);
                $req->bindValue(':email', $email);
           
                try 
                {
                    $req->execute();
                } 
                catch (\PDOException $e) 
                {
                }
                
                $req->closeCursor();
            }
        }
        
    }

    public function add() {

    }

    public function modify() {

    }

    public function delete() {

    }
}
