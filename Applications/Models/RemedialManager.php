<?php
namespace Applications\Models;

use Applications\Entities\Remedial;
use Library\Sly\Database\Manager;

abstract class RemedialManager extends Manager
{
    abstract function getAllRemedials($fkStudent);// Permet de récupérer tous les rattrapages d'un élève

}
