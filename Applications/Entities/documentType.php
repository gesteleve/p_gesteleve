<?php

namespace Applications\Entities;

use Library\Sly\Database\Entity;

class documentType extends Entity
{
    protected $name;
    protected $description;

    public function setName() {
        if (is_string($name) && !empty($name)) {
            $this->name = $name;
        }
    }

    public function setDescription() {
        if (is_string($description) && !empty($description)) {
            $this->description = $description;
        }
    }

    public function name() { return $this->name; }
    public function description() { return $this->description; }
}
