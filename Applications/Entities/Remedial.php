<?php

namespace Applications\Entities;

use Library\Sly\Database\Entity;

class Remedial extends Entity
{
    /**
     * Id du rattrapage
     *
     * @var int
     * @access protected
     */
    protected $id;

    /**
     * Date du rattrapage
     *
     * @var int
     * @access protected
     */
    protected $date;

    /**
     * Type du rattrapage (théorique/pratique)
     *
     * @var int
     * @access protected
     */
    protected $type;

    /**
     * Nombres de périodes de la remédiation
     *
     * @var int
     * @access protected
     */
    protected $nbPeriodes;

    /**
     * Nom de l'élève
     *
     * @var string
     * @access protected
     */
    protected $fkStudent;

    ///////////////////////// GET & SET ///////////////////////////

    /**
     * Cette fonction définit l'id de la rattrapage
     * @param $id
     */
    public function setId($id)
    {
        if (is_string($id) && !empty($id)) {
            $this->id = $id;
        }
    }

    //Cette fonction retourne l'attribut id
    public function getId(){
        return $this->id;
    }


    /**
     * Cette fonction définit la date de le rattrapage
     * @param $date
     */
    public function setDate($date)
    {
        if (is_string($date) && !empty($date)) {
            $this->date = $date;
        }
    }

    //Cette fonction retourne l'attribut date
    public function getDate(){
        return $this->date;
    }


    /**
     * Cette fonction définit le type de le rattrapage
     * @param $type
     */
    public function setType($type)
    {
        if (is_string($type) && !empty($type)) {
            $this->type = $type;
        }
    }

    //Cette fonction retourne l'attribut type
    public function getType(){
        return $this->date;
    }


    /**
     * Cette fonction définit la nombre de périodes de le rattrapage
     * @param $nbPeriod
     */
    public function setNbPeriod($nbPeriod)
    {
        if (is_string($nbPeriod) && !empty($nbPeriod)) {
            $this->nbPeriodes = $nbPeriod;
        }
    }

    //Cette fonction retourne le nombre de oériodes du rattrapage
    public function getNbPeriod(){
        return $this->nbPeriodes;
    }


    /**
     * Cette fonction définit la nombre de périodes de le rattrapage
     * @param $fkStudent
     */
    public function setfkStudent($fkStudent)
    {
        if (is_string($fkStudent) && !empty($fkStudent)) {
            $this->fkStudent = $fkStudent;
        }
    }

    //Cette fonction retourne le nombre de périodes du rattrapage
    public function getfkStudent(){
        return $this->fkStudent;
    }

}
