<?php

namespace Applications\Entities;

use Library\Sly\Database\Entity;

class schoolClass extends Entity
{
    /**
     * Id de la classe
     *
     * @var string
     * @access protected
     */
    protected $id;

    protected $profession_name;
    protected $profession_id;
    protected $colleague_id;
    protected $student_nbre;
 

    public function setProfession_name($name) {
        if (is_string($name) && !empty($name)) {
            $this->profession_name = $name;
        }
    }

    public function setProfession_id($id) {
        if (is_int($id) && !empty($id)) {
            $this->profession_id = (int) $id;
        }
    }

    public function setColleague_id($id) {
        if (is_string($id) && !empty($id)) {
            $this->colleague_id = $id;
        }
    }

    public function setStudent_nbre($nbre) {
        if (is_string($nbre) && !empty($nbre)) {
            $this->student_nbre = $nbre;
        }
    }

/**
     * Gets l'id de la classe
     *
     * @return string
     */
    public function id()
    {
        return ($this->id);
    }

    public function profession_name() { return $this->profession_name; }
    public function profession_id() { return $this->profession_id; }
    public function colleague_id() { return $this->colleague_id; }
    public function student_nbre() { return $this->student_nbre; }
 
 
}
