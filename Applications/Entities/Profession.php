<?php

namespace Applications\Entities;

use Library\Sly\Database\Entity;

class Profession extends Entity
{

    
    protected $name;
    protected $filter;
    protected $first_name;
    protected $profession_id;
  
    protected $student_nbre;

    public function setName($name) {
        if (is_string($name) && !empty($name)) {
            $this->name = $name;
        }
    }

    public function setFirst_name($name) {
        if (is_string($name) && !empty($name)) {
            $this->first_name = $name;
        }
    }

    
    public function setProfession_id($id) {
        if (is_int($id) && !empty($id)) {
            $this->profession_id = (int) $id;
        }
    }

    public function setFilter($bit) {
        if (!empty($bit)) {
            $this->filter = $bit;
        }
    }

  
    public function setStudent_nbre($nbre) {
        if (is_string($nbre) && !empty($nbre)) {
            $this->student_nbre = $nbre;
        }
    }

    public function name() { return $this->name; }
    public function first_name() { return $this->first_name; }
    public function profession_id() { return $this->profession_id; }
    public function filter() { return $this->filter; }
    public function student_nbre() { return $this->student_nbre; }

}
