$(document).ready(function(){

	/* Datetimepicker */
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '<Préc',
		nextText: 'Sui>',
		currentText: 'Aujourd\'hui',
		monthNames: [
			'Janvier','Février','Mars','Avril','Mai','Juin',
			'Juillet','Août','Septembre','Octobre','Novembre', 'Décembre'
		],
		monthNamesShort: [
			'Jan','Fév','Ma','Avr','Mai','Jui',
			'Jul','Aoû','Sep','Oct','Nov','Déc'
		],
		dayNames: [
			'Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'
		],
		dayNamesShort: [
			'Dim','Lun','Mar','Mer','Jeu','Ven','Sam'
		],
		dayNamesMin: [
			'Di','Lu','Ma','Me','Je','Ve','Sa'
		],
		weekHeader: 'Semaine',
		dateFormat: 'dd.mm.yy',
		firstDay: 1
	};
	$.datepicker.setDefaults($.datepicker.regional[ "fr"]);
	$('.date').datetimepicker({
		dateFormat 	: "dd-mm-yy", /*Format jour-mois-année. ex: 03.01.2013 */
		timeFormat 	: "HH:mm", /* Format 24h. */
		minDateTime	: new Date(), /* Date minimale [aujourd'hui] */
		stepMinute	: 30, /* Intervale de 15minutes */
		hourMin		: 8, /* Heure minimale */
		hourMax		: 18, /* Heure maximale */
		timeText	: "Temps", /*Texte 'temps' */
		hourText	: "Heures", /* Texte 'heures' */
		minuteText	: "Minutes", /* Texte 'minutes' */
		currentText	: "Aujourd'hui", /* Texte 'aujourd'hui' */
		closeText	: "Fermer", /* Texte 'fermer' */
		showOtherMonths: true, /* voir les autres mois */
     	selectOtherMonths: true /* Selection des autres mois. */
	});

	/* Change input date debut */
	$('input[name=ModifybeginDate]').change(function(){
		if($('input[name=ModifyendDate]').val() < $('input[name=ModifybeginDate]').val()){
			$('input[name=ModifyendDate]').val($('input[name=ModifybeginDate]').val());
		}
	});

	/* Change input date fin */
	$('input[name=ModifyendDate]').change(function(){
		if($('input[name=ModifyendDate]').val() < $('input[name=ModifybeginDate]').val()){
			$('input[name=ModifybeginDate]').val($('input[name=ModifyendDate]').val());
		}
	});

	/* Déconnexion */
	$('#logout').click(function(){
		if (confirm("Voulez-vous vraiment vous déconnecter ?")) { /* Clic sur OK*/
           $(location).attr('href','index.php?page=logout'); /*Redirige vers la page logout*/
       }
	});// end Logout

	/* Annulation d'une réservation */
	$('a.cancelReservation').click(function(){
		if (confirm("Voulez-vous vraiment supprimer cette réservation ?")) { /* Clic sur OK*/
           var iID = $(this).attr('id').replace('CancelReservation_',''); /*Recupère l'id de la réservation à supprimer.*/
           $.ajax({
			   	type: "POST",
			   	url: "./inc/ajax.inc.php",
			   	data: {
			   		sFunction 	: 'CancelReservation',
			   		iID			: iID
			   	},
			   	success: function(bResult){
			   		if(bResult == true){
			   			location.reload(); /*refresh*/
			   		}
			   	}
			});
       }
	}); // end Annulation d'une reservation

	/* Suppression d'un véhicule */
	$('.deleteVehicule').click(function(){
		if (confirm("Voulez-vous vraiment supprimer ce véhicule ?")) { /* Clic sur OK */
           var iID = $(this).attr('id'); /* Recupère l'ID du véhicule.*/
           $.ajax({
			   	type: "POST",
			   	url: "./inc/ajax.inc.php",
			   	data: {
			   		sFunction 	: 'DeleteVehicule',
			   		iID			: iID
			   	},
			   	success: function(bResult){
					location.reload(); /*refresh*/
			   	}
			});
       	}
	}); // end Suppression vehicule

	/* Modifications d'un véhicule */
	$('.modifyVehicule').click(function(){
		$('[href="#collapseTwo"]').click(); //Simulation d'un clique sur le collapse d'ajout/modification des véhicules
       	var iID = $(this).attr('id'); // recupère l'id du véhicule
      	$.ajax({
		   	type: "POST",
		   	url: "./inc/ajax.inc.php",
		   	data: {
		   		sFunction 	: 'getVehicule',
		   		iID			: iID
		   	},
		   	success: function(aValues){
		   		aValues = $.parseJSON(aValues);
		   		$('input[name=ID]').val(aValues[0].ID);
		   		$('select[name=LstType] option[value="'+aValues[0].TypeID+'"]').prop('selected', true);
		   		$('input[name=Marque]').val(aValues[0].Marque);
		   		$('input[name=Modele]').val(aValues[0].Modele);
		   		$('input[name=Plaque]').val(aValues[0].Plaque);
		   		$('input[name=Action]').val('edit');
		   		$('[href="#collapseTwo"]').text('Modifier un véhicule');
		   		$('input[name=ReloadFormVehicule]').removeClass('hidden');
		   	}
		});
	});// end Modification vehicule

	/* Bouton de "vidage" du formulaire pour les véhicules */
	$('input[name=ReloadFormVehicule]').click(function(){
		$('input[name=ID]').val('');
   		$('select[name=LstType] option[value=""]').prop('selected', true);
   		$('input[name=Marque]').val('');
   		$('input[name=Modele]').val('');
   		$('input[name=Plaque]').val('');
   		$('input[name=Action]').val('create');
   		$('[href="#collapseTwo"]').text('Ajouter un véhicule');
   		$('input[name=ReloadFormVehicule]').addClass('hidden');
	}); //end reload form vehicule

	/* Calendrier */
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today', // Gauche de l'en-tete
			center: 'title', // Centre de l'en-tete
			right: 'month,agendaWeek,agendaDay' // droite de l'en-tete
		},
		firstDay: 1, // Premier jours du calendrier => 1 = Lundi
		minTime: '8:00', // Heure minimale
		maxTime: '18:00', // Heure maximale
		axisFormat : 'HH:mm', // Format 24H
		timeFormat: 'HH:mm{ - HH:mm}', // Format des heures pour dans l'en-tete des évenements
		columnFormat : {
			week: 'ddd dd/MM' //Format des colonnes pour la semaine
		},
		titleFormat :{
			week: "dd[ MMMM][ yyyy]{ '&#8212;' dd MMMM yyyy}", // Format du titre
			day: "ddd. dd MMMM yyyy" // Format du titre
		},
		defaultView: 'agendaWeek', // Affichage par défaut => Semaine
		selectable : true, // Peut-être selectionne
		select: function(start, end, allDay) {
			if(start < new Date()){
				alert('Vous ne pouvez pas selectionner une date ultérieure à l\'heure que nous vivons aujourd\'hui.');	
			}
			else{
				//Recupère les infos de la date de début pour la reformater
				var beginDate 		= start.getDate();
				if(beginDate < 10){ beginDate = "0"+beginDate;}
				var beginMonth 		= start.getMonth()+1;
				if(beginMonth < 10){ beginMonth = "0"+beginMonth;}
				var beginYear 		= start.getFullYear();
				var beginHours		= start.getHours();
				if(beginHours < 10){ beginHours = "0"+beginHours;}
				var beginMinutes	= start.getMinutes();
				if(beginMinutes < 10){ beginMinutes = "0"+beginMinutes;}

				//Recupère les infos de la date de fin pour la reformater
				var endDate 		= end.getDate();
				if(endDate < 10){ endDate = "0"+endDate;}
				var endMonth 		= end.getMonth()+1;
				if(endMonth < 10){ endMonth = "0"+endMonth;}
				var endYear 		= end.getFullYear();
				var endHours		= end.getHours();
				if(endHours < 10){ endHours = "0"+endHours;}
				var endMinutes		= end.getMinutes();
				if(endMinutes < 10){ endMinutes = "0"+endMinutes;}
					
				if(allDay == true){
					//Si l'evenement dure tout le jour, il commence à 8h00 et fini à 18h
					beginHours = "08";
					beginMinutes = "00"; 

					endHours = "18";
					endMinutes = "00";
				}
				var begin 	= beginDate+"-"+beginMonth+"-"+beginYear+" "+beginHours+":"+beginMinutes; //Reformate la date de début
				var end 	= endDate+"-"+endMonth+"-"+endYear+" "+endHours+":"+endMinutes; //Reformate la date de fin

				$('input[name=CreatebeginDate]').val(begin);
				$('input[name=CreateendDate]').val(end);
				$('div#errorCreateReservation').empty();
				$('#ModalCreateReservation').modal();
			}
			$('#calendar').fullCalendar('unselect');
		},
		selectHelper : true,
		editable: false, // Pas de modification en drag/drop
		allDayDefault : false, // Les événements ne durent pas tout le jour par défaut.
		events: {
	        url: './inc/ajax.inc.php',
	        type: 'POST',
	        data: {
	            sFunction: 'getAllReservations'
	        },
	        error: function() {
	            alert('Erreur lors de la récupération des événements!');
	        }
	    },
		eventClick: function(calEvent){
			//Recupère les infos de la date de fin pour la reformater
			var beginDate 		= calEvent.start.getDate();
			if(beginDate < 10){ beginDate = "0"+beginDate;}
			var beginMonth 		= calEvent.start.getMonth()+1;
			if(beginMonth < 10){ beginMonth = "0"+beginMonth;}
			var beginYear 		= calEvent.start.getFullYear();
			var beginHours		= calEvent.start.getHours();
			if(beginHours < 10){ beginHours = "0"+beginHours;}
			var beginMinutes	= calEvent.start.getMinutes();
			if(beginMinutes < 10){ beginMinutes = "0"+beginMinutes;}
			var begin 			= beginDate+"-"+beginMonth+"-"+beginYear+" "+beginHours+":"+beginMinutes;

			//Recupère les infos de la date de fin pour la reformater
			var endDate 		= calEvent.end.getDate();
			if(endDate < 10){ endDate = "0"+endDate;}
			var endMonth 		= calEvent.end.getMonth()+1;
			if(endMonth < 10){ endMonth = "0"+endMonth;}
			var endYear 		= calEvent.end.getFullYear();
			var endHours		= calEvent.end.getHours();
			if(endHours < 10){ endHours = "0"+endHours;}
			var endMinutes		= calEvent.end.getMinutes();
			if(endMinutes < 10){ endMinutes = "0"+endMinutes;}
			var end 			= endDate+"-"+endMonth+"-"+endYear+" "+endHours+":"+endMinutes;

			$.ajax({
				type: 'POST',
				url:'./inc/ajax.inc.php',
				data : {
					sFunction : 'getSpecificReservation',
					iID : calEvent.id
				},
				success: function(aValues){
					aValues = $.parseJSON(aValues);
					if(aValues != ''){
						$.post('./inc/ajax.inc.php', {
							sFunction 	: 'getDateReservations',
							sBeginDate 	: begin,
							sEndDate 	: end,
							sVehicule	: calEvent.vehicule
						}).done(function (data){
							data = $.parseJSON(data);
							$('table#SameReservation').empty();
							if(data != ''){
								$('table#SameReservation').append('<tr><th>Utilisateur</th><th>Véhicule</th><th>Date</th></tr>');
								$.each(data, function(i, val){
									var startHour = val.DateBegin;
									var endHour = val.DateEnd;

									if(i == 0){
										$('table#SameReservation').append('<tr style="background-color:#97FF94;"><td>'+val.Name+'</td><td>'+val.vehicule+'</td><td>'+startHour+' / '+endHour+'</td></tr>');
									}
									else if(i > 0 && i <= 2){
										$('table#SameReservation').append('<tr style="background-color:#FFC794;"><td>'+val.Name+'</td><td>'+val.vehicule+'</td><td>'+startHour+' / '+endHour+'</td></tr>');
									}
									else if(i > 2){
										$('table#SameReservation').append('<tr style="background-color:#FF949A;"><td>'+val.Name+'</td><td>'+val.vehicule+'</td><td>'+startHour+' / '+endHour+'</td></tr>');
									}		
								});
								$.post('./inc/ajax.inc.php',{
									sFunction	: 'getUserLogin'
								}).done(function(user){
									user = $.parseJSON(user);
									bgetEventofUser = false;

									$('input[name=id]').val('');
									$('input[name=ModifybeginDate]').val('');
									$('input[name=ModifyendDate]').val('');
									$('select[name=ModifyLstVehicules] option:selected').prop('selected', false);
									$('textarea[name=ModifytxtDescription]').val(''); 
					
									$.each(data, function(index, value){
										$.each(value, function(i,v){
											if(i == 'User' && bgetEventofUser == false && v == user){
												bgetEventofUser = true;
												$('input[name=id]').val(data[index].id);
												$('input[name=ModifybeginDate]').val(data[index].DateBegin);
												$('input[name=ModifyendDate]').val(data[index].DateEnd);
												$('select[name=ModifyLstVehicules] option[value="'+data[index].VehiculeID+'"]').prop('selected', true);
												$('textarea[name=ModifytxtDescription]').val(data[index].Description); 
											}// if user
										});// each value
									}); // each data
									$('#ModalModifyReservation').modal();

									if($('input[name=id]').val() != ""){
										$('a[name=btnDeleteMineReservation]').show();
										$('a[name=btnModifyReservation]').show();
										$('#ModalModifyReservation .modal-body form').show();
									}
									else{
										$('a[name=btnDeleteMineReservation]').hide();
										$('a[name=btnModifyReservation]').hide();
										$('#ModalModifyReservation .modal-body form').hide();
									}
								});// done post getUserLogin
							}// if data
						});// done post getDateReservation
					}// if aValues
				}// success
			});//done ajax getSpecificReservation
		},// eventClick
		loading: function(bool) {
			if (bool) $('#loading').show();
			else $('#loading').hide();
		}// durant le loading du calendrier.	
	}); //end calendrier

	/* Emettre une reservation */
	$('a[name=btnReserve]').click(function(){
		$('div#errorCreateReservation').empty();
		sBeginDate 	= $('input[name=CreatebeginDate]').val(); // Recuperation de la date de debut
		sEndDate	= $('input[name=CreateendDate]').val(); // Recuperation de la date de fin
		sDescription= $('textarea[name=CreatetxtDescription]').val(); // Recuperation de la description
		sVehicule	= $('select[name=CreateLstVehicules] option:selected').val(); // Recuperation du vehicule

		if(sBeginDate == '' || sEndDate == '' || sVehicule == ''){
			$('div#errorCreateReservation').append('<div class="alert alert-error">Veuillez renseigné correctement les champs.</div>'); //Message d'erreur si manque champ.
		}
		else{
			$.ajax({
				type: 'POST',
				url: './inc/ajax.inc.php',
				data:{
					sFunction 		: 'addReservation',
					sBeginDate 		: sBeginDate,
					sEndDate		: sEndDate,
					sDescription 	: sDescription,
					sVehicule		: sVehicule
				},
				success: function(data){
					if(data == true){
						location.reload(); //refresh
					}
					else{
						$('div#errorCreateReservation').append('<div class="alert alert-error">Erreur lors de l\'ajout dans la base de données</div>'); //message d'erreur
					}
					
				},
				error: function(){
					alert('Erreur de communication avec la base de données lors de l\'ajout de votre réservation');
				}
			});
		}
	}); // Click du bouton de reservation

	/* Annulation de reservation */
	$('a[name=btnDeleteMineReservation]').click(function(){
		iID = $('input[name=id]').val(); //Recupere l'id de la réservation.
		
		$.ajax({
			type: 'POST',
			url:'./inc/ajax.inc.php',
			data : {
				sFunction 	: 'CancelReservation',
				iID 		: iID
			},
			success : function(data){
				if(data == true){
					location.reload();
				}
				else{
					alert('Erreur lors de l\'annulation de la réservation.');
				}
			},
			error: function(){
				alert('Erreur lors de l\'annulation de la réservation.');
			}
		});
	}); //end delete mine reservation

	/* Modification de reservation */
	$('a[name=btnModifyReservation]').click(function(){
		iID = $('input[name=id]').val(); //Recupere l'id de la réservation.
		sBeginDate 	= $('input[name=ModifybeginDate]').val(); // Recuperation de la date de debut
		sEndDate	= $('input[name=ModifyendDate]').val(); // Recuperation de la date de fin
		sDescription= $('textarea[name=ModifytxtDescription]').val(); // Recuperation de la description
		sVehicule	= $('select[name=ModifyLstVehicules] option:selected').val(); // Recuperation du vehicule
		$.ajax({
			type: 'POST',
			url:'./inc/ajax.inc.php',
			data : {
				sFunction		: 'ModifyReservation',
				iID 			: iID,
				sBeginDate 		: sBeginDate,
				sEndDate		: sEndDate,
				sDescription 	: sDescription,
				sVehicule		: sVehicule
			},
			success : function(data){
				if(data == true){
					location.reload();
				}
				else{
					alert('Erreur lors de la modification de la réservation.');
				}
			},
			error: function(){
				alert('Erreur lors de la modification de la réservation.');
			}
		});
	}); //end modify reservation
	
});