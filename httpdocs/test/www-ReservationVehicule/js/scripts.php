<?php 

header('Content-type: text/javascript'); // Precise le language du fichier.
header("Vary: Accept-Encoding"); // Règle les problèmes de proxies
header("Cache-control: public"); // Accepte l'utilisation du cache.
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

$jsscripts = '';

$aJS = array();
// scripts persos
array_push($aJS, 'scripts.js');

foreach ($aJS as $sJs) {
	//Parcours les fichiers css à compresser ensemble.
	$lines = file($sJs); //ouvre le fichier
	foreach ($lines as $line_num => $line){
		//Parcours les lignes du fichier.
		$jsscripts .= preg_replace("/(?:\/\/.*)/", "",trim($line)); // Remplace les commentaires sur une seule ligne.
	}
}

//Remplace retour à la lignes, tabulations et les blocs de commentaires.
echo preg_replace('/\/\*(.+?)\*\/|[\t\r\n]/','$2',$jsscripts);
?>