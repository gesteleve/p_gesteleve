<?php
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : lundi 04.02.2013
** Version : 0.1
** Description : Fichier de redirection par rapport au GET
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/
	$oDesign->setConfig(array(
		'title' => 'Gestion des véhicules',
		'css' => $aDefaultCSS,
		'js' => $aDefaultJS,
		'menu' => $aMenu, 
		'subtitle' => 'Gérer les véhicules disponibles'
	));

	$oDesign->makeHeader();
	$oDesign->makeBody();

	//Obtient les véhicules
	$oSql = new cMySql();
	$aVehicules = $oSql->getAllVehicules();
	unset($oSql);

	if(isset($_GET['a'])){
		if($_GET['a'] == true){
			echo '<div class="alert alert-success">
				Action effectuée correctement !
			</div>';
		}
		else{
			echo'<div class="alert alert-error">
			L\'action n\'est pas valide
			</div>';
		}
	}

echo '
<div class="accordion" id="accordion2">
	<div class="accordion-group">
   		<div class="accordion-heading">
      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        	Véhicules disponibles
      		</a>
    	</div>
    	<div id="collapseOne" class="accordion-body collapse in">
	      	<div class="accordion-inner">
				<table class="table">
					<tr>
						<th>Type</th>
						<th>Marque</th>
						<th>Modèle</th>
						<th>Plaque</th>
						<th></th>
					</tr>';
			if(count($aVehicules) < 1){
					echo'
					<tr>
						<td class="table_NoData" colspan="5">Aucun Vehicule disponible</td>
					</tr>';
			}
			else{
				for($i = 0; $i < count($aVehicules); $i++){
					echo '
					<tr>
						<td>'.$aVehicules[$i]['Type'].'</td>
						<td>'.$aVehicules[$i]['Marque'].'</td>
						<td>'.$aVehicules[$i]['Modele'].'</td>
						<td>'.$aVehicules[$i]['Plaque'].'</td>
						';if($_SESSION['admin'] == true) echo '
						<td>
							<div class="btn-toolbar">
  								<div class="btn-group"> 
  									<a class="btn modifyVehicule" id="EditVehicule_'.$aVehicules[$i]['ID'].'" title="Modifier..." href="#"><i class="icon-uniF47C icon-black"></i></a>
  									<a class="btn deleteVehicule" id="DeleteVehicule_'.$aVehicules[$i]['ID'].'" title="Supprimer..." href="#"><i class="icon-uniF470 icon-black"></i></a>
  								</div>
  							</div>
  						</td>
						'; echo '
					</tr>';
				}
			}
echo'			</table>
			</div>
    	</div>
  	</div>
	';if($_SESSION['admin'] == true) { echo'
  	<div class="accordion-group">
   		<div class="accordion-heading">
      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        	Ajouter un véhicule...
      		</a>
    	</div>
    	<div id="collapseTwo" class="accordion-body collapse">
	      	<div class="accordion-inner">
	      	'; include(FOLDER_INC . 'vehicule.inc.php'); //Inclus le formulaire pour l'ajout de vehicule
	    echo'
	      	</div>
    	</div>
  	</div>
	'; } echo '
  </div>';


  	if(DEBUG){
  		echo '<pre>';print_r($aVehicules);echo'</pre>';
  	}
	

	$oDesign->makeFooter();
?>