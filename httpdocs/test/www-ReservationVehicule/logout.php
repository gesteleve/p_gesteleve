<?php
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : lundi 13 février 2012
** Version : 0.1
** Description : Page de déconnexion
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/

$oDesign->setConfig(array(
	'title' => 'Déconnexion...', 
	'css' => $aDefaultCSS, 
	'js' => $aDefaultJS,
	'show_menu' => false
));
$oDesign->makeHeader();
$oDesign->makeBody();

session_destroy();

fRedirect($_SERVER['PHP_SELF']."?logout=ok", 0);

$oDesign->makeFooter();
?>