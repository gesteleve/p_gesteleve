<?php
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : mercredi 06 février 2013
** Version : 0.1
** Description : Page des informations
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/
$oDesign->setConfig(array(
	'title' => 'Informations',
	'css' => $aDefaultCSS,
	'js' => $aDefaultJS,
	'menu' => $aMenu, 
	'subtitle' => 'Découvrez et modifiez vos informations'
));

$oDesign->makeHeader();
$oDesign->makeBody();

if(!empty($_POST['txtEmail'])){
	$sEmail = $_POST['txtEmail'];

	if($sEmail != $_SESSION['mail']){
		//Mise à jour de l'utilisateur
		$oSql = new cMySql();
		$bUpdateUser = $oSql->updateUser($_SESSION['id'], $sEmail);
		unset($oSql);

		if($bUpdateUser){
			fSetAlert('Votre email a été modifié. Nouvel email : <strong>'.$sEmail.'</strong>.','success');
			$_SESSION['mail'] = $sEmail;
		}
		else{
			fSetAlert('Erreur lors de la mise à jour de votre email.','error');
		}
	}
	else{
		fSetAlert('Votre ancien et nouvel email sont identiques.','error');
	}
		
	fRedirect($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'],0);
}

echo '
<form method="POST" action="#">
	<table class="table" style="width: 50%;">
		<thead></thead>
		<tbody>
			<tr>
				<th>Nom</th>
				<td><input type="text" value="'.$_SESSION['name'].'" disabled /></td>
				<th>Prénom</th>
				<td><input type="text" value="'.$_SESSION['firstname'].'" disabled /></td>
			</tr>
			<tr>
				<th>Login</th>
				<td><input type="text" value="'.$_SESSION['login'].'" disabled /></td>
				<th>Section</th>
				<td><input type="text" value="'.$_SESSION['section'].'" disabled /></td>
			</tr>
			<tr>
				<th colspan="2" style="text-align: right;">Email</th>
				<td colspan="2">
					<div class="input-prepend">
					  	<span class="add-on"><i class="icon-at icon-black"></i></span>
						<input id="prependedInput" name="txtEmail" type="email" placeholder="Email..." value="'.$_SESSION['mail'].'" required/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><input type="submit" class="btn btn-primary" value="Actualiser mes informations"/></td>
			</tr>
		</tbody>
	</table>
</form>';

if(DEBUG){
	$oldDate 			= '';
	$oldBeginDate 		= '';
	$oldEndDate			= '';
	$oldVehicule 		= '';
	$aSendReservation 	= array();
	$iDesincrement 		= 0;

	$oSql = new cMySql();
	$aReservations = $oSql->getAllReservations();
	unset($oSql);

	foreach ($aReservations as $key => $val) {
		//Parcours les réservations et enlève les réservations ou les heures se situent en même temps que la précédente.
		$bvallAdd = false;


		if($val['login'] == $_SESSION['login']){
			$val['className'] = 'mineEvent';
		}
		else{
			$val['className'] = 'defaultEvent';
		}
		if(date('d-m-Y H:i:s',strtotime($val['end'])) < date('d-m-Y H:i:s')){
			$val['className'] = 'pastEvent';
		}

		if( (!($aReservations[$key]['start'] > $oldBeginDate && $aReservations[$key]['start'] < $oldEndDate) &&
			!($aReservations[$key]['end'] > $oldBeginDate && $aReservations[$key]['end'] < $oldEndDate)) ||
			$aReservations[$key]['vehicule'] != $oldVehicule
			)
		{

			array_push($aSendReservation, $val);
			$bvallAdd = true;
		}
		foreach ($val as $k => $v) {
			if($k == 'start' && in_array($bvallAdd, $aSendReservation)){
				$oldBeginDate = $v;
			}
			if($k == 'end' && in_array($bvallAdd, $aSendReservation)){
				$oldEndDate = $v;
			}
			if($k == 'vehicule'){
				$oldVehicule = $v;
			}
		}
	}


	echo '<pre>'; print_r($aSendReservation); echo '</pre>';
}

$oDesign->makeFooter();
?>