﻿<?php 
/*
** ********************************************
**
** Auteur: Jordan Michael
** Date de création : jeudi 31 janvier 2013
** Version : 0.1
** Description : classe de connexion au LDAP
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/

class cLdap {
	
	// Déclaration des options
	var $sLogin = '';
	var $sPassword = '';
	var $sIp = LDAP_IP;
	var $sDomain = LDAP_DOMAIN;
	var $rLink = '';
	
	var $sLdapRoot = LDAP_ROOT;
	
	var $bDebug = true;
	
	var $bError = false;
	
	
	/*
	 * But: configure les options de l'objet
	 * input: tableau de configuration
	 * output: none
	 */
	public function setConfig($aConfig) {
		(isset($aConfig['login'])		? $this->sLogin = $aConfig['login'] : null);
		(isset($aConfig['password'])	? $this->sPassword = $aConfig['password'] : null);
		(isset($aConfig['ip'])			? $this->sIp = $aConfig['ip'] : null);
		(isset($aConfig['root'])		? $this->sLdapRoot = $aConfig['root'] : null);
		(isset($aConfig['debug'])		? $this->bDebug = $aConfig['debug'] : null);
	}
	
	
	/*
	 * But: connection au LDAP
	 * input: none
	 * output: none
	 */
	public function connect() {
		
		return true;
		// Connection
		$this->rLink = ldap_connect($this->sIp);
		if (!$this->rLink && $this->bDebug) {
			die('Impossible de se connecter au LDAP');
		}
		
		// Modification de la version du protocole
		ldap_set_option($this->rLink, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($this->rLink, LDAP_OPT_REFERRALS, 0);
		
		// Authentification
		$bConnection = @ldap_bind($this->rLink, $this->sLogin . $this->sDomain, $this->sPassword);
		
		if (!$bConnection) {
			$this->bError = true;
		}
		return $this->bError;
	}
	
	
	/*
	 * But: Retourne les information du user connecté
	 * input: none
	 * output: tableau d'information
	 */
	public function getInfo() {
		if (!$this->bError) {
			$sFilter = 'sAMAccountName=' . $this->sLogin;
			
			$rSearch = @ldap_search ($this->rLink, $this->sLdapRoot, $sFilter);
	
			// Retourne la première entrée du résultat
			$rEntry = @ldap_first_entry($this->rLink, $rSearch);
			// Le résultat que l'on veut est distinguishedName
			$aValues = @ldap_get_values($this->rLink, $rEntry, "distinguishedName");
			
			// MemberOf afin de vérifier l'appartenance au groupe mobilite
			$aMemberOf = @ldap_get_values($this->rLink, $rEntry, "memberOf");
			
			// Stock chaque informations dans une case d'un tableau
			$sInfo = $aValues[0];
			$aInfos = explode(',', $sInfo);
			
			// Stock chaque informations groupes dans une case d'un tableau
			$sGroups = $aMemberOf[0];
			$aGroups = explode(',', $sGroups);
			
			// Supprime les caractères juqu'au =
			$iSize = count($aInfos);
			for ($i=0; $i<$iSize; $i++) {
				$iPos = strpos($aInfos[$i], '=')+1;
				$aInfos[$i] = substr($aInfos[$i], $iPos);
			}
			
			// Supprime les caractères juqu'au =
			$iSize = count($aGroups);
			for ($i=0; $i<$iSize; $i++) {
				$iPos = strpos($aGroups[$i], '=')+1;
				$aGroups[$i] = substr($aGroups[$i], $iPos);
			}
			
			$aGroupsInfos = array_merge($aInfos, $aGroups);
			
			// Test si dans le groupe mobilite
			$bConnected = false;
			$_SESSION['admin'] = false;
			foreach ($aGroupsInfos as $iKey=>$sVal) {
//				if ($sVal == "MobiliteAdmin" || $sVal == 'Mobilite') {
				if ($sVal == "Maitres" || $sVal == 'Eleves') {
				 
					$oSql = new cMySql();
					$aUser = $oSql->getUser($this->sLogin);
					unset($oSql);

					if(count($aUser) < 1){
						$sName = $aGroupsInfos[0];
						$aName = explode(' ', $sName);

						$oSql = new cMySql();
						$bUserAdd = $oSql->addUser($this->sLogin, $aName[1], $aName[0], $aGroupsInfos[2]);
						
						$aUser = $oSql->getUser($this->sLogin);
						unset($oSql);
					}
					
					$bConnected = true;
					$_SESSION['id']			= $aUser[0]['ID']; // ID
					$_SESSION['name'] 		= $aUser[0]['Nom']; // Nom
					$_SESSION['firstname'] 	= $aUser[0]['Prenom']; // Prenom
					$_SESSION['connected'] 	= $bConnected;
					$_SESSION['login'] 		= $aUser[0]['Login']; // Login
					$_SESSION['section'] 	= $aUser[0]['Section']; // Section
					$_SESSION['mail']		= $aUser[0]['Mail']; //email
				}
				if ($sVal == "MobiliteAdmin") {
					$_SESSION['admin'] = true;
				}
				
			}
			
			return $bConnected;
		}
		ldap_close($this->rLink);
	}
	
}

?>