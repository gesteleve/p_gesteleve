<?php 
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : jeudi 31 janvier 2013
** Version : 0.1
** Description : Classe de connexion mysql
**
** ********************************************
*/

class cMySql {
	
	// constantes pour la BD
	const STR_HOST    = SQL_SERVER;
	const STR_USER    = SQL_USER;
	const STR_PWD     = SQL_PASSWORD;
	const STR_DB_NAME = SQL_DATABASE;
	
	var $rLink = '';
	var $bDebug = true;
	
	// l'objet de connexion
	private $objConnexion = null;
	
	// *******************************************************************
	// Nom   :	__construct
	// But   :	Constructeur. Ouvre la connexion a la BD
	// Param.: 	-
	// *******************************************************************
	public function __construct()
	{
		// appel de la fonction de connexion
		$this->dbConnect();

	} // end __construct()
	
	/*
	 * But: configure les options de l'objet
	 * input: tableau de configuration
	 * output: none
	 */
	public function setConfig($aConfig) {
		(isset($aConfig['server'])		? $this->sServer = $aConfig['server'] : null);
		(isset($aConfig['user'])		? $this->sUser = $aConfig['user'] : null);
		(isset($aConfig['password'])	? $this->sPassword = $aConfig['password'] : null);
		(isset($aConfig['database'])	? $this->sDataBase = $aConfig['database'] : null);
		(isset($aConfig['debug'])		? $this->bDebug = $aConfig['debug'] : null);
	}
	
	
	
	/*
	 * But: connection à la base de données mysql
	 * input: none
	 * output: none
	 */
	public function dbConnect() {
			
		try
		{
			// Connection
			$this->objConnexion = new mysqli(self::STR_HOST,
										   self::STR_USER,
										   self::STR_PWD,
										   self::STR_DB_NAME)
								  or die (mysqli_connect_error());
		}
		catch(Exception $erreur)
		{
			print("Probleme de connexion BD");
		}

		
		
		// Défini l'encodage de la base de données
		$this->executeSqlRequest("SET NAMES 'utf8'");
	}
	
	// *******************************************************************
	// Nom :	executeSqlRequest
	// But :	Exécute une requête SQL. 
	// Retour:	le recordSet de la requete (null en cas d'erreur)
	// Param.: 	$strSQLRequest = requete SQL a executer
	// *******************************************************************
	private function executeSqlRequest($strSQLRequest)
	{
		// Messaged'erreur
		$strErrorMsg = "";
	
		// connection à la BD
		if ($this->objConnexion == null)
		{
			$this->dbConnect();
		}
	
		// Execution de la requete et recuperation du recordset
		$orsRslt = $this->objConnexion->query($strSQLRequest) or die ($this->objConnexion->error);		
				
		// renvoie le recordSet
		return $orsRslt;
			
	} // executeSqlRequest()

	// *******************************************************************
	// Nom :	getRequest
	// But :	realise une requete de type "Select" dans la BD
	// Retour:	le resultat de la requete sous forme de tableau associatif
	// Param.: 	$strSqlRequest -> la requete a realiser
	// *******************************************************************
	private function getRequest($strSqlRequest)
	{		
		// le tableau de retour
		$tab_values = array();
		
		// lancement de la requête
		$orsResult = $this->executeSqlRequest($strSqlRequest);		
			
		// pour savoir si la requete renvoie au moins un record
		if($orsResult->num_rows > 0) // ATTENTION DANS un ADD retourne un BOOLEEN
		{
			// transformer le tout en tableau associatif
			$tab_values[] = $orsResult->fetch_all(MYSQLI_ASSOC);
		
			// liberer le recordSet
			$orsResult->free_result();
			
			// renvoi du tableau
			return $tab_values[0];
		} // if au moins un record
		
		// renvoi du tableau vide
		return $tab_values;
		
	} // end getRequest()
		
	// *******************************************************************
	// Nom :	blnRequest
	// But :	realise une requete de type "INSERT" ou "UPDATE" ou "DELETE"
	//			dans la BD
	// Retour:	un booleen pour savoir si la requete a bien passé
	// Param.: 	$strSqlRequest -> la requete a realiser
	// *******************************************************************
	private function blnRequest($strSqlRequest)
	{		
		
		// lancement de la requête
		$orsResult = $this->executeSqlRequest($strSqlRequest);		
					
		// renvoi le resultat de la requete (booleen)
		return $orsResult;
		
	} // end blnRequest()

	// *******************************************************************
	// Nom :	getUser
	// But :	Verifier l'existence de l'utilisateur
	//			
	// Retour:	tableau des informations de l'utilisateur
	// Param.: 	login de l'user
	// *******************************************************************
	public function getUser($sLogin){
		$sSelectedCols = "id_user as ID, usr_login as Login, usr_name as Nom, usr_firstname as Prenom, usr_email as Mail, usr_Section as Section";
		
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_USERS." WHERE usr_login = '".$sLogin."'";

		return $this->getRequest($strSqlRequest);
	} // end getUser()

	// *******************************************************************
	// Nom :	addUser
	// But :	Verifier l'existence de l'utilisateur
	//			
	// Retour:	tableau des informations de l'utilisateur
	// Param.: 	login de l'user, nom, prénom, section
	// *******************************************************************
	public function addUser($sLogin, $sName, $sFirstName, $sSection){
		$strSqlRequest = "INSERT INTO ".T_USERS." VALUES ('','".$sLogin."','".$sName."','".$sFirstName."','".$sSection."','');";

		return $this->executeSqlRequest($strSqlRequest);
	} // end addUser()

	// *******************************************************************
	// Nom :	updateUser
	// But :	Metttre a jour les infos de l'utilisateur
	//			
	// Retour:	boolean de réussite/echec
	// Param.: 	id, email
	// *******************************************************************
	public function updateUser($iID, $sEmail){
		$strSqlRequest = "UPDATE ".T_USERS." SET usr_email = '".$sEmail."' WHERE id_user = '". $iID. "'";

		return $this->executeSqlRequest($strSqlRequest);
	} // end updateUser()






	// *******************************************************************
	// Nom :	getUserReservation
	// But :	Obtient les réservation d'un User
	//			
	// Retour:	tableau des reservations
	// Param.: 	id de l'utilisateur
	// *******************************************************************
	public function getUserReservations($iIDUser){
		$sSelectedCols = "id_reservation as ID, DATE_FORMAT(CONCAT_WS(' ', res_datebegin,res_hourbegin), '%d-%m-%Y %H:%i') as DateBegin, DATE_FORMAT(CONCAT_WS(' ', res_dateend,res_hourend), '%d-%m-%Y %H:%i') as DateEnd, vtyp_name as Type, veh_marque as Marque, veh_modele as Modele, res_description as Description";
		
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_RESERVATIONS." LEFT JOIN ".T_VEHICULES." ON ".T_RESERVATIONS.".idx_vehicule = ".T_VEHICULES.".id_vehicule
		LEFT JOIN ".T_VEHICULETYPES." ON ".T_VEHICULES.".idx_vehiculetype = ".T_VEHICULETYPES.".id_vehiculetype LEFT JOIN ".T_USERS." ON ".T_RESERVATIONS.".idx_user = ".T_USERS.".id_user
		WHERE id_user = '".$iIDUser."' AND res_state = 1 AND (res_datebegin >= '".date('Y-m-d')."' OR res_dateend >= '".date('Y-m-d')."') ORDER BY res_datebegin";

		return $this->getRequest($strSqlRequest);
	} // end getUserReservation()

	// *******************************************************************
	// Nom :	getAllReservations
	// But :	Obtient toutes les réservation
	//			
	// Retour:	tableau des reservations
	// Param.:	-
	// *******************************************************************
	public function getAllReservations(){
		$sSelectedCols = "id_reservation as id, CONCAT_WS(' - ', CONCAT_WS(' ',usr_name, usr_firstname), CONCAT_WS(' ',veh_marque, veh_modele)) as title, idx_vehicule as vehicule, CONCAT_WS(' ', res_datebegin, res_hourbegin) as start, CONCAT_WS(' ', res_dateend, res_hourend) as end, res_description as Description, usr_login as login";
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_RESERVATIONS." LEFT JOIN ".T_VEHICULES." ON ".T_RESERVATIONS.".idx_vehicule = ".T_VEHICULES.".id_vehicule
		LEFT JOIN ".T_VEHICULETYPES." ON ".T_VEHICULES.".idx_vehiculetype = ".T_VEHICULETYPES.".id_vehiculetype LEFT JOIN ".T_USERS." ON ".T_RESERVATIONS.".idx_user = ".T_USERS.".id_user 
		WHERE res_state = 1 ORDER BY res_datedemande,res_datebegin,res_hourbegin,id_vehicule ASC";

		return $this->getRequest($strSqlRequest);
	} // end getAllReservations()

	// *******************************************************************
	// Nom :	getDateReservations
	// But :	Obtient les réservations se trouvant dans la même plage qu'une autre
	//			
	// Retour:	tableau des reservations
	// Param.:	date de début, date de fin, heure de début, heure de fin, id du vehicule
	// *******************************************************************
	public function getDateReservations($sBeginDate, $sBeginHour, $sEndDate,$sEndHour,$sVehicule){
		$sSelectedCols = "id_reservation as id, usr_login as User, usr_email as Email, CONCAT_WS(' ',usr_name, usr_firstname) AS Name, CONCAT_WS(' ',veh_marque, veh_modele) as vehicule, id_vehicule AS VehiculeID, DATE_FORMAT(CONCAT_WS(' ', res_datebegin,res_hourbegin), '%d-%m-%Y %H:%i') as DateBegin, DATE_FORMAT(CONCAT_WS(' ', res_dateend,res_hourend), '%d-%m-%Y %H:%i') as DateEnd, res_description as Description";
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_RESERVATIONS." LEFT JOIN ".T_VEHICULES." ON ".T_RESERVATIONS.".idx_vehicule = ".T_VEHICULES.".id_vehicule
		LEFT JOIN ".T_VEHICULETYPES." ON ".T_VEHICULES.".idx_vehiculetype = ".T_VEHICULETYPES.".id_vehiculetype LEFT JOIN ".T_USERS." ON ".T_RESERVATIONS.".idx_user = ".T_USERS.".id_user 
		WHERE res_datebegin = '".$sBeginDate."'
			AND (
				 (res_hourbegin >= '".$sBeginHour."' AND res_hourbegin < '".$sEndHour."') OR (res_hourend > '".$sBeginHour."' AND res_hourend <= '".$sEndHour."')
				)
			AND id_vehicule = '".$sVehicule."'
		ORDER BY res_datedemande ASC";

		return $this->getRequest($strSqlRequest);
	} // end getDateReservations()

	// *******************************************************************
	// Nom :	getSpecificReservation
	// But :	Obtient une réservation
	//			
	// Retour:	la reservations correspondant à l'ID
	// Param.:	l'id de la reservation
	// *******************************************************************
	public function getSpecificReservation($iID){
		$sSelectedCols = "id_reservation as ID, res_datebegin as DateBegin, res_hourbegin as HourBegin, res_dateend as DateEnd, res_hourend as HourEnd, idx_vehicule as VehiculeID, res_description as Description";
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_RESERVATIONS." LEFT JOIN ".T_VEHICULES." ON ".T_RESERVATIONS.".idx_vehicule = ".T_VEHICULES.".id_vehicule
		LEFT JOIN ".T_VEHICULETYPES." ON ".T_VEHICULES.".idx_vehiculetype = ".T_VEHICULETYPES.".id_vehiculetype
		WHERE id_reservation = ".$iID." AND (
			res_datebegin > '".date('Y-m-d')."' OR (res_datebegin = '".date('Y-m-d')."' AND (res_hourbegin > '".date('H:i:s')."' OR res_hourend > '".date('H:i:s')."'))
			)
		ORDER BY res_datedemande,res_datebegin LIMIT 1";

		return $this->getRequest($strSqlRequest);
	} // end getSpecificReservation()

	// *******************************************************************
	// Nom :	addReservation
	// But :	Ajoute une réservation
	//			
	// Retour:	booléen de réussite/echec
	// Param.:	la date de début, la date de fin, le véhicule(, la description)
	// *******************************************************************
	public function addReservation($sBeginDate, $sBeginHour, $sEndDate, $sEndHour, $sVehicule, $sDescription, $idUser){
		$strSqlRequest = "INSERT INTO ".T_RESERVATIONS." VALUES (NULL ,'".date('Y-m-d H:i:s')."','".$sBeginDate."','".$sBeginHour."','".$sEndDate."','".$sEndHour."','".$sDescription."','1','".$idUser."','".$sVehicule."')";

		return $this->executeSqlRequest($strSqlRequest);
	}//end addReservation

	// *******************************************************************
	// Nom :	ModifyReservation
	// But :	Modifier une réservation
	//			
	// Retour:	booléen de réussite/echec
	// Param.:	l'id de la reservation, date/heure début, date/heure fin, vehicule, description
	// *******************************************************************
	public function ModifyReservation($iID, $sBeginDate, $sBeginHour, $sEndDate, $sEndHour, $sVehicule, $sDescription){
		$strSqlRequest = "UPDATE " . T_RESERVATIONS . " SET res_datedemande = '".date('Y-m-d H:i:s')."', res_datebegin = '".$sBeginDate."', res_hourbegin = '".$sBeginHour."', 
		res_dateend = '".$sEndDate."', res_hourend = '".$sEndHour."', idx_vehicule = ".$sVehicule.", res_description = '".$sDescription."'
		WHERE id_reservation = '". $iID. "';";

		return $this->executeSqlRequest($strSqlRequest);
	}//end ModifyReservation

	// *******************************************************************
	// Nom :	CancelReservation
	// But :	Annuler une réservation
	//			
	// Retour:	booléen de réussite/echec
	// Param.:	l'id de la reservation
	// *******************************************************************
	public function CancelReservation($iID, $iUserID){
		$strSqlRequest = "UPDATE " . T_RESERVATIONS . " SET res_state = 0 WHERE id_reservation = '". $iID. "' AND idx_user = '".$iUserID."' ;";

		return $this->executeSqlRequest($strSqlRequest);
	}//end CancelReservation
	
	// *******************************************************************
	// Nom :	DeleteReservation
	// But :	Supprimer une réservation de la base de données
	//			
	// Retour:	booléen de réussite/echec
	// Param.:	l'id de la reservation
	// *******************************************************************
	public function DeleteReservation($iID, $iUserID){
		$strSqlRequest = "DELETE FROM " . T_RESERVATIONS . " WHERE id_reservation = '". $iID. "' AND idx_user = '".$iUserID."' ;";

		return $this->executeSqlRequest($strSqlRequest);
	}//end DeleteReservation






	// *******************************************************************
	// Nom :	getVehicule
	// But :	Obtient un vehicule par rapport à son ID
	//			
	// Retour:	info d'un vehicule
	// Param.:	l'ID du vehicule
	// *******************************************************************
	public function getVehicule($iID){
		$sSelectedCols = "id_vehicule as ID, id_vehiculetype as TypeID, vtyp_Name as Type, veh_marque as Marque, veh_modele as Modele, veh_plaque as Plaque";
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_VEHICULES." LEFT JOIN ".T_VEHICULETYPES." ON ".T_VEHICULES.".idx_vehiculetype = ".T_VEHICULETYPES.".id_vehiculetype 
		WHERE veh_state = 1 AND id_vehicule = ".$iID." ORDER BY vtyp_Name ASC";

		return $this->getRequest($strSqlRequest);
	}//end getVehicule

	// *******************************************************************
	// Nom :	getAllVehicules
	// But :	Obtient les vehicules disponibles
	//			
	// Retour:	tableau des vehicules
	// Param.:	-
	// *******************************************************************
	public function getAllVehicules(){
		$sSelectedCols = "id_vehicule as ID, vtyp_Name as Type, veh_marque as Marque, veh_modele as Modele, veh_plaque as Plaque, id_vehiculetype as TypeID";
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_VEHICULES." LEFT JOIN ".T_VEHICULETYPES." ON ".T_VEHICULES.".idx_vehiculetype = ".T_VEHICULETYPES.".id_vehiculetype 
		WHERE veh_state = 1 ORDER BY vtyp_Name ASC";

		return $this->getRequest($strSqlRequest);
	}//end getAllVehicules

	// *******************************************************************
	// Nom :	getAllType
	// But :	Obtient tous les types de véhicules
	//			
	// Retour:	tableau des types
	// Param.:	-
	// *******************************************************************
	public function getAllType(){
		$sSelectedCols = "id_vehiculetype as ID, vtyp_name as Name";
		$strSqlRequest = "SELECT ".$sSelectedCols." FROM ".T_VEHICULETYPES." ORDER BY vtyp_name ASC";

		return $this->getRequest($strSqlRequest);
	}//end getAllType

	// *******************************************************************
	// Nom :	addVehicule
	// But :	Ajoute un véhicule
	//			
	// Retour:	booléen de réussite/echec
	// Param.:	le type, la marque, le modèle, la plaque
	// *******************************************************************
	public function addVehicule($iType, $sMarque, $sModele,$sPlaque){
		$strSqlRequest = "INSERT INTO ".T_VEHICULES." VALUES ('','$sMarque','$sModele', '$sPlaque','1','$iType') ";

		return $this->executeSqlRequest($strSqlRequest);
	}//end addVehicule

	// *******************************************************************
	// Nom :	editVehicule
	// But :	Modifier un Véhicule
	//			
	// Retour:	booléen de réussite/echec
	// Param.:	l'id du véhicule, le type, la marque, le modele, la plaque
	// *******************************************************************
	public function editVehicule($iID, $iType, $sMarque, $sModele, $sPlaque){
		$strSqlRequest = "UPDATE " . T_VEHICULES . " SET idx_vehiculetype = '$iType', veh_marque = '$sMarque', veh_modele = '$sModele', veh_plaque = '$sPlaque' WHERE id_vehicule = '". $iID. "'";

		return $this->executeSqlRequest($strSqlRequest);
	}//end editVehicule

	// *******************************************************************
	// Nom :	DeleteVehicule
	// But :	Supprimer un Véhicule
	//			
	// Retour:	booléen de réussite/echec
	// Param.:	l'id du véhicule
	// *******************************************************************
	public function DeleteVehicule($iID){
		$strSqlRequest = "UPDATE " . T_VEHICULES . " SET veh_state = 0 WHERE id_vehicule = '". $iID. "'";

		return $this->executeSqlRequest($strSqlRequest);
	}//end DeleteVehicule
}


?>
