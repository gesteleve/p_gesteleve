<?php
	session_start();
	// Appelle les fichiers objet et fonctions
	require('conf.inc.php');
	require('../class/mysql.class.php');
	require('func.inc.php');

	/* Ajout réservation */
	function addReservation($sBeginDate, $sEndDate, $sVehicule, $sDescription){
		$baddReservation = false;

		if(strtotime($sEndDate) > strtotime($sBeginDate) && strtotime($sBeginDate) > time() ){
			$sBeginHour = date('H:i', strtotime($sBeginDate));
			$sBeginDate = date('Y-m-d', strtotime($sBeginDate));

			$sEndHour = date('H:i', strtotime($sEndDate));
			$sEndDate = date('Y-m-d', strtotime($sEndDate));


			$oSql = new cMySql();
			$aVehicules = $oSql->getVehicule($sVehicule);
			/*$usrReservations = $oSql->getUserReservations($_SESSION['id']);
			foreach($usrReservations as $x=>$y) {
				if($y['Marque'] == $aVehicules[0]['Marque'] && $y['Modele'] == $aVehicules[0]['Modele']) {
					if($y['DateBegin'] <= $sBeginDate && $y['DateEnd'] >= $sBeginDate) {
						fSetAlert('Vous avez déjà réservé ce véhicule à ce moment-ci','error');
						$sError = true;
					}
					if($y['DateBegin'] <= $sEndDate && $y['DateEnd'] >= $sEndDate) {
						fSetAlert('Vous avez déjà réservé ce véhicule à ce moment-ci','error');
						$sError = true;
					}
				}
			}*/
			$baddReservation = $oSql->addReservation($sBeginDate, $sBeginHour, $sEndDate, $sEndHour, $sVehicule, $sDescription, $_SESSION['id']);
			unset($oSql);
			
			
			if($baddReservation){
				fInformUser($_SESSION['mail'], $sBeginDate, $sBeginHour, $sEndDate, $sEndHour, ($aVehicules[0]["Marque"]." ".$aVehicules[0]["Modele"]),'add');
				fSetAlert('Réservation effectuée avec succès','success');
			}
			
		}
		
		echo $baddReservation;
	} //addReservation

	/* Annulation de reservation */
	function CancelReservation($iID){

		$oSql = new cMySql();
		$aThisReservation = $oSql->getSpecificReservation($iID);
		$bCancelReservation = $oSql->CancelReservation($iID, $_SESSION['id']);
		$aNextReservation = $oSql->getDateReservations($aThisReservation[0]['DateBegin'],$aThisReservation[0]['HourBegin'],$aThisReservation[0]['DateEnd'],$aThisReservation[0]['HourEnd'],$aThisReservation[0]['VehiculeID']);
		if(!empty($aNextReservation[0])){
			fInformUser($aNextReservation[0]['Email'], $aNextReservation[0]['DateBegin'],0,$aNextReservation[0]['DateEnd'],0,$aNextReservation[0]['vehicule'],'cancel');
		}
		fSetAlert('Annulation de la réservation effectuée avec succès','success');
		$bDeleteReservation = $oSql->DeleteReservation($iID, $_SESSION['id']);
		unset($oSql);

		echo $bCancelReservation;
	} //CancelReservation

	/* Suppression de vehicule */
	function DeleteVehicule($iID){

		$iID = str_replace('DeleteVehicule_','',$iID);

		$oSql = new cMySql();
		$bDeleteVehicule = $oSql->DeleteVehicule($iID);
		unset($oSql);

		echo $bDeleteVehicule;
	} //DeleteVehicule

	/* Recuperer les reservations */
	function getAllReservations(){
		$oldDate 			= '';
		$oldBeginDate 		= '';
		$oldEndDate			= '';
		$oldVehicule 		= '';
		$aSendReservation 	= array();
		$iDesincrement 		= 0;

		$oSql = new cMySql();
		$aReservations = $oSql->getAllReservations();
		unset($oSql);

		foreach ($aReservations as $key => $val) {
			//Parcours les réservations et enlève les réservations ou les heures se situent en même temps que la précédente.
			$bvallAdd = false;


			if($val['login'] == $_SESSION['login']){
				$val['className'] = 'mineEvent';
			}
			else{
				$val['className'] = 'defaultEvent';
			}
			if(date('Y-m-d H:i:s',strtotime($val['end'])) < date('Y-m-d H:i:s')){
				$val['className'] = 'pastEvent';
			}

				//Si date de debut n'est pas entre l'ancienne date de début et la date de fin n'est pas entre l'ancienne date de début et date de fin.
			if((
				!($aReservations[$key]['start'] >= $oldBeginDate && $aReservations[$key]['start'] <= $oldEndDate) 
				&& !($aReservations[$key]['end'] >= $oldBeginDate && $aReservations[$key]['end'] <= $oldEndDate)
				) 
				// OU le véhicule est différent de l'ancien véhicule
				|| $aReservations[$key]['vehicule'] != $oldVehicule
			){

				array_push($aSendReservation, $val);
				$bvallAdd = true;
			}
			foreach ($val as $k => $v) {
				if($k == 'start' && in_array($bvallAdd, $aSendReservation)){
					$oldBeginDate = $v;
				}
				if($k == 'end' && in_array($bvallAdd, $aSendReservation)){
					$oldEndDate = $v;
				}
				if($k == 'vehicule'){
					$oldVehicule = $v;
				}

			}
		}

		echo json_encode($aSendReservation);
	} //getAllReservations

	/* Recuperer les reservations par rapport au date de début/fin et vehicule */
	function getDateReservations($sBeginDate, $sEndDate,$sVehicule){

		$sBeginHour = date('H:i:s',strtotime($sBeginDate));
		$sBeginDate = date('Y-m-d',strtotime($sBeginDate));

		$sEndHour = date('H:i:s',strtotime($sEndDate));
		$sEndDate = date('Y-m-d',strtotime($sEndDate));

		$oSql = new cMySql();
		$aReservations = $oSql->getDateReservations($sBeginDate, $sBeginHour, $sEndDate,$sEndHour,$sVehicule);
		unset($oSql);
		 
		echo json_encode($aReservations);
	} //getDateReservations

	/* Recuperer une reservation par son ID */
	function getSpecificReservation($iID){

		$oSql = new cMySql();
		$aReservations = $oSql->getSpecificReservation($iID);
		unset($oSql);

		echo json_encode($aReservations);
	} //getSpecificReservation

	/* Recuperer un vehicule par rapport a son ID */
	function getVehicule($iID){

		$iID = str_replace('EditVehicule_','',$iID);

		$oSql = new cMySql();
		$aVehicule = $oSql->getVehicule($iID);
		unset($oSql);

		echo json_encode($aVehicule);
	} //getVehicule

	/* Modifier une reservation */
	function ModifyReservation($iID, $sBeginDate, $sEndDate, $sVehicule, $sDescription){

		$sBeginHour = date('H:i', strtotime($sBeginDate));
		$sBeginDate = date('Y-m-d', strtotime($sBeginDate));

		$sEndHour = date('H:i', strtotime($sEndDate));
		$sEndDate = date('Y-m-d', strtotime($sEndDate));

		$oSql = new cMySql();
		$bModifyReservation = $oSql->ModifyReservation($iID, $sBeginDate, $sBeginHour, $sEndDate, $sEndHour, $sVehicule, $sDescription);
		$aVehicules = $oSql->getVehicule($sVehicule);
		unset($oSql);
		if($bModifyReservation){
			fInformUser($_SESSION['mail'],$sBeginDate,$sBeginHour,$sEndDate,$sEndHour,($aVehicules[0]["Marque"]." ".$aVehicules[0]["Modele"]),'modify');
			fSetAlert('Modiciation de la réservation effectuée avec succès','success');
		}

		echo $bModifyReservation;
	} //ModifyReservation



	switch($_POST['sFunction']){
		case 'addReservation' :
			$sBeginDate 	= $_POST['sBeginDate'];
			$sEndDate 		= $_POST['sEndDate'];
			$sVehicule 		= $_POST['sVehicule'];
			$sDescription 	= $_POST['sDescription'];
			addReservation($sBeginDate, $sEndDate, $sVehicule, $sDescription);
			break;

		case 'CancelReservation' :
			$iID = $_POST['iID'];
			CancelReservation($iID);
			break;

		case 'DeleteVehicule' :
			$iID = $_POST['iID'];
			DeleteVehicule($iID);
			break;

		case 'getAllReservations' :
			getAllReservations();
			break;

		case 'getDateReservations' :
			$sBeginDate 	= $_POST['sBeginDate'];
			$sEndDate 		= $_POST['sEndDate'];
			$sVehicule 		= $_POST['sVehicule'];
			getDateReservations ($sBeginDate, $sEndDate,$sVehicule);
			break;

		case 'getSpecificReservation' :
			$iID = $_POST['iID'];
			getSpecificReservation($iID);
			break;

		case 'getUserLogin' :
			echo json_encode($_SESSION['login']);
			break;

		case 'getVehicule' :
			$iID = $_POST['iID'];
			getVehicule($iID);
			break;

		case 'ModifyReservation' :
			$iID 			= $_POST['iID'];
			$sBeginDate 	= $_POST['sBeginDate'];
			$sEndDate 		= $_POST['sEndDate'];
			$sVehicule 		= $_POST['sVehicule'];
			$sDescription 	= $_POST['sDescription'];
			ModifyReservation($iID, $sBeginDate, $sEndDate, $sVehicule, $sDescription);
			break;

		default :
			break;
	} //switch de fonction

?>