<?php
	$sVehiculesOptions = "";

	// Obtient les vehicules
	$oSql = new cMySql();
	$aVehicules = $oSql->getAllVehicules();
	unset($oSql);
	
	//Cree les options pour les vehicules
	for($i = 0; $i < count($aVehicules); $i++){
		$sVehiculesOptions .= "<option value='".$aVehicules[$i]['ID']."'>".$aVehicules[$i]['Type']." - ".$aVehicules[$i]['Marque']." ".$aVehicules[$i]['Modele']. "</option>";
	}
?>
<div class="alert alert-info">
	Attention, si la plage voulue est deja occupée, vous recevrez un mail si la/les personne(s) situées devant vous dans la file d'attente auront annulé.
</div>
<form action="#" method="POST">
	<div id="errorCreateReservation"></div>
	<table >
		<tbody>
				<tr>
					<td><input  type="text" name="CreatebeginDate" class="date" placeholder="Commencement..." required /> </td>
					<td><input type="text" name="CreateendDate" class="date" placeholder="Fin..." required/> </td>
				</tr>
				<tr>
					<td><select name="CreateLstVehicules" required><option value="">Sélectionnez un véhicule</option><?php echo $sVehiculesOptions; ?></select></td>
				</tr>
				<tr>
					<td colspan="2"><textarea name="CreatetxtDescription" style="width: 430px;resize: none;" placeholder="Description..."></textarea> </td>
				</tr>
				<tr>
					<td><input name="btnReservationSend" type="submit" class="btn btn-primary hidden" /></td>
				</tr>
		</tbody>
	</table>
</form>