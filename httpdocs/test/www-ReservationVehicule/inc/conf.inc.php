﻿<?php
	/* **********************************************
	** **********************************************
	**
	** Auteur : Michael Jordan
	** Date de Creation : Jeudi, 31 janvier 2013
	** Version : 1.0
	**
	** **********************************************
	**
	** Date de Modification :
	** Modification(s) :
	**
	**
	** ******************************************* */

	// Page a visiter
	if (isset($_GET['page']) && $_GET['page'] != '') {
		define('PAGE', $_GET['page']);
	}
	else {
		define('PAGE', 'home');
	}

	/* GENERAL */
	define('VERSION','1.6.0');
	define('DEBUG',false);

	/* DOSSIERS */
	define('FOLDER_CLASS','./class/');
	define('FOLDER_INC','./inc/');
	define('FOLDER_CSS','./css/');
	define('FOLDER_JS','./js/');
	define('FOLDER_IMG','./img/');

	/* PARAMS SQL */
	define('SQL_SERVER','localhost');
	define('SQL_USER','web_intranetmobi');
	define('SQL_PASSWORD','FtzTTrDbPxfkKmLuczpK');
	define('SQL_DATABASE','web_intranetmobi');

	/* TABLES SQL */
	define('T_USERS','t_users');
	define('T_RESERVATIONS','t_reservations');
	define('T_VEHICULES','t_vehicules');
	define('T_VEHICULETYPES','t_vehiculetypes');

	/* LDAP */
//	define('LDAP_DOMAIN', '@test-inf.local');
//	define('LDAP_IP', '172.16.9.6');
//	define('LDAP_ROOT', 'DC=test-inf, DC=local');

	define('LDAP_DOMAIN', '@etmlnet.local');
	define('LDAP_IP', '10.224.8.68');
	define('LDAP_ROOT', 'DC=etmlnet, DC=local');

	/* Menu */
	$aMenu = array();
	$aMenu['home'] 			= "<i class='icon-home'></i> Accueil";
	$aMenu['reservations'] 	= "<i class='icon-calendar2'></i> Réservations";
	$aMenu['vehicules'] 	= "<i class='icon-automobile'></i> Véhicules";
	$aMenu['userinfos'] 	= "<i class='icon-contact'></i> Vos Infos";

	/* CSS */
	$aDefaultCSS = array();
	array_push($aDefaultCSS, 'style.php'); //CSS compressé

	/* JavaScript */
	$aDefaultJS = array();
	// bibliothèque jquery
	array_push($aDefaultJS, 'jquery.js');
	// bibliothèque bootstrap
	array_push($aDefaultJS, 'bootstrap.js');
	// bibliothèque jQueryUI
	array_push($aDefaultJS, 'jquery-ui-1.10.0.custom.min.js');
	array_push($aDefaultJS, 'timepicker.js');
	array_push($aDefaultJS, 'fullcalendar.js');
	// autres scripts compressé
	array_push($aDefaultJS, 'scripts.php');
?>