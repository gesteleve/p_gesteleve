<?php

	$sTypeOptions = "";

	// Obtient les vehicules
	$oSql = new cMySql();
	$aType = $oSql->getAllType();
	unset($oSql);

	//Cree les options pour les vehicules
	for($i = 0; $i < count($aType); $i++){
		$sTypeOptions .= "<option value='".$aType[$i]['ID']."'>".$aType[$i]['Name']."</option>";
	}

	if(!empty($_POST['LstType'])){
		$iID		= $_POST['ID'];
		$sMarque 	= $_POST['Marque'];
		$sModele 	= $_POST['Modele'];
		$sPlaque 	= $_POST['Plaque'];
		$iType 		= $_POST['LstType'];
		$sAction	= $_POST['Action'];

		if($sAction == 'create'){
			// Si l'action est de creer alors cree un nouveau vehicule.
			$oSql = new cMySql();
			$bAddVehicule = $oSql->addVehicule($iType, $sMarque, $sModele,$sPlaque);
			unset($oSql);

			if($bAddVehicule){
				fSetAlert('Le véhicule "'.$sMarque.'" créé avec succès !','success');
			}
			else{
				fSetAlert('Erreur lors de la création du véhicule','error');
			}

			fRedirect($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'],0);

		}
		if($sAction == 'edit'){
			// Si l'action est de modifier alors modifie un vehicule.
			$oSql = new cMySql();
			$beditVehicule = $oSql->editVehicule($iID, $iType, $sMarque, $sModele,$sPlaque);
			unset($oSql);

			if($beditVehicule){
				fSetAlert('Modification du véhicule "'.$sMarque.'" faite avec succès !','success');
			}
			else{
				fSetAlert('Erreur lors de la modification du véhicule','error');
			}
			
			fRedirect($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'],0);
		}
	}

?>

<form method="POST" action="#">
	<table>
		<tbody>
				<tr>
					<td>
						<input type="hidden" name="ID" value="" />
						<select name="LstType" id="LstType" required><option value="">Sélectionnez un type</option><?php echo $sTypeOptions; ?></select>
					</td>
				</tr>
				<tr>
					<td><input  type="text" id="Marque" name="Marque" placeholder="Marque..." required /> </td>
					<td><input type="text" id="Modele" name="Modele" placeholder="Modèle..." /> </td>
					<td><input type="text" id="Plaque" name="Plaque" placeholder="Plaque..."/> </td>
				</tr>
				<tr>
					<td>
						<input type="submit" class="btn btn-primary" />
						<input type="button" name="ReloadFormVehicule" class="btn hidden" value="Annuler Modification" />
						<input type="hidden" name="Action" value="create" />
					</td>
				</tr>
		</tbody>
	</table>
</form>