<?php
	/*
	 * But: Effectue une redirection en PHP après x secondes
	 * input: url de la page, le temps avant la redirection
	 * output: none
	 */
	function fRedirect($sUrl, $iTime=3) {
		// On vérifie si aucune en-tête n'à été envoyée
		if (!headers_sent()) {
			header('refresh: ' . $iTime . ';url=' . $sUrl);
		}
		else {
			echo '<meta http-equiv="refresh" content="' . $iTime . ';url=' . $sUrl . '">'; 
		}
	}//end fRedirect()

	/*
	 * But: Creer une abréviation
	 * input: l'abréviation
	 * output: none
	 */
	function fMakeAbbr($sAbbr, $aAbbr) {
		foreach ($aAbbr as $iKey=>$sVal){
			echo '<abbr title="'.$sVal.'">'.$sAbbr.'</abbr>';
		}
	}//end fMakeAbbr()

	/*
	 * But: Modifie une date pour l'afficher dans un affichage commun
	 * input: la date
	 * output: Date formatée
	 */
	function fMakeCommonDate($sDate) {
		$sNewDate = date('d-m-Y H:i',strtotime($sDate));
		return $sNewDate;
	}//end fMakeCommonDate()

	/*
	 * But: Parametrer l'alert
	 * input: le message, le type d'alert
	 * output: -
	 */
	function fSetAlert($message, $type = null){
	     $_SESSION['Alert'] = array(
          'message' => $message,
          'type' => $type
	     );
	}// end fSetAlert()

	/*
	 * But: Affiche une alert
	 * input: -
	 * output: l'alert
	 */
	function fMakeAlert(){
	    if(isset($_SESSION['Alert']['message'])){
	        $html = '<div class="alert alert-'.$_SESSION['Alert']['type'].'">
			'.$_SESSION['Alert']['message'].'
			</div>';
	        $_SESSION['Alert'] = array();
	        return $html;
		}
	}// end fMakeAlert()

	/*
	 * But: Avertir utilisateur d'une reservation
	 * input: email, date de debut, date de fin, vehicule, action
	 * output: -
	 */
	function fInformUser($sEmail,$sBeginDate,$sBeginHour,$sEndDate,$sEndHour,$sVehicule,$sAction){
		$sSubject = '';
		$sMessage = '';

	    switch ($sAction) {
	    	case 'add':
	    		$sSubject = 'Réservation du véhicule "'.$sVehicule.'"';
	    		$sMessage = utf8_decode('La réservation pour le véhicule "'.$sVehicule.'" pour les dates du '.$sBeginDate.' à '.$sBeginHour.' au '.$sEndDate.' à '.$sEndHour.' a été prise en compte.');
	    		# code...
	    		break;

	    	case 'modify':
	    		$sSubject = 'Modification de réservation';
	    		$sMessage = utf8_decode('La modification de votre réservation pour le véhicule "'.$sVehicule.'" du '.$sBeginDate.' à '.$sBeginHour.' au '.$sEndDate.' à '.$sEndHour.' à été prise en compte.');
	    		# code...
	    		break;

	    	case 'cancel':
	    		$sSubject = 'Passage en prioritaire pour une réservation';
	    		$sMessage = utf8_decode('Une ou plusieurs personnes ayant annulé leur réservation, vous passez prioritaire pour la réservation du véhicule "'.$sVehicule.'" pour les dates du '.$sBeginDate.' au '.$sEndDate);
	    		# code...
	    		break;
	    	
	    	default:
	    		# code...
	    		break;
	    }
	    if(!empty($sSubject)){
	    	mail($sEmail,$sSubject,$sMessage,"From:IntraMobi");
			mail("vanessa.gaberthuel@vd.ch",$sSubject,$sMessage,"From:".$_SESSION['firstname'].$_SESSION['name']);
			mail("sonia.de-oliveira-azevedo@vd.ch",$sSubject,$sMessage,"From:".$_SESSION['firstname'].$_SESSION['name']);
			mail("mireille.mestroni@vd.ch",$sSubject,$sMessage,"From:".$_SESSION['firstname'].$_SESSION['name']);
	    }
	}// end fInformUser()
?>