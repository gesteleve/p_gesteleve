<div class="alert alert-info">
	Attention, la modification de la réservation aura pour effet de vous mettre dans les "viennent ensuites" si la plage voulue est deja occupée.
</div>
<form action="#" method="POST">
	<table >
		<tbody>
				<tr>
					<th colspan="2">Ma réservation :</th>
				</tr>
				<tr>
					<td>
						<input type="hidden" id="id" name="id" />
						<input  type="text" name="ModifybeginDate" class="date" placeholder="Commencement..." required />
					</td>
					<td><input type="text" name="ModifyendDate" class="date" placeholder="Fin..." required /> </td>
				</tr>
				<tr>
					<td><select name="ModifyLstVehicules" required><option value="">Sélectionnez un véhicule</option><?php echo $sVehiculesOptions; ?></select></td>
				</tr>
				<tr>
					<td colspan="2"><textarea name="ModifytxtDescription" style="resize:none;width:430px;" placeholder="Description..."></textarea></td>
				</tr>
				<tr>
					<td><input name="btnDeleteReservation" type="submit" class="btn btn-primary hidden" /></td>
				</tr>
		</tbody>
	</table>
</form>
<table class="table" id="SameReservation">

</table>
