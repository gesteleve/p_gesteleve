<?php 
header('Content-type: text/css'); // Precise le language du fichier.
header("Vary: Accept-Encoding"); // Règle les problèmes de proxies
header("Cache-control: public"); // Accepte l'utilisation du cache.
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

$cssstyles = '';

$aCSS = array();
array_push($aCSS, 'bootstrap.css');
array_push($aCSS, 'bootstrap-responsive.css');
// css jqueryUI
array_push($aCSS, 'blitzer/jquery-ui-1.10.0.custom.min.css');
array_push($aCSS, 'timepicker.css');
array_push($aCSS, 'fullcalendar.css');
// css de base
array_push($aCSS, 'sprites.css');
array_push($aCSS, 'design.css');

foreach ($aCSS as $sCss) {
	//Parcours les fichiers css à compresser ensemble.
	$lines = file($sCss); //ouvre le fichier
	foreach ($lines as $line_num => $line){
		//Parcours les lignes du fichier.
		$cssstyles .= preg_replace("/(?:\/\/.*)/", "",trim($line)); // Remplace les commentaires sur une seule ligne.
	}
}

//Remplace retour à la lignes, tabulations et les blocs de commentaires.
echo preg_replace('/\/\*(.+?)\*\/|[\s]*([:;{},>])[\s]*/','$2',$cssstyles);
?>