<?php
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : jeudi 31 janvier 2013
** Version : 0.1
** Description : Fichier de redirection par rapport au GET
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/
$oDesign->setConfig(array(
	'title' => 'Bienvenue',
	'css' => $aDefaultCSS,
	'js' => $aDefaultJS,
	'menu' => $aMenu, 
	'subtitle' => 'sur le site de réservation de véhicule de l\'ETML'
));

$oDesign->makeHeader();
$oDesign->makeBody();

$oSql = new cMySql();
$aUserReservations = $oSql->getUserReservations($_SESSION['id']);
unset($oSql);

echo '
<div class="accordion" id="accordion2">
	<div class="accordion-group">
   		<div class="accordion-heading">
      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        	Mes Réservations
      		</a>
    	</div>
    	<div id="collapseOne" class="accordion-body collapse in">
	      	<div class="accordion-inner">
				<table id="MyReservations" class="table">
					<tr>
						<th>Date de début</th>
						<th>Date de fin</th>
						<th>Véhicule</th>
						<th>Description</th>
						<th></th>
					</tr>';
			if(count($aUserReservations) < 1){
					echo'
					<tr>
						<td class="table_NoData" colspan="5">Vous n\'avez effecté aucune réservation</td>
					</tr>';
			}
			else{
				for($i = 0; $i < count($aUserReservations); $i++){
					echo '
					<tr>
						<td>'.fMakeCommonDate($aUserReservations[$i]['DateBegin']).'</td>
						<td>'.fMakeCommonDate($aUserReservations[$i]['DateEnd']).'</td>
						<td>'.$aUserReservations[$i]['Type'].' - '.$aUserReservations[$i]['Marque'].' '.$aUserReservations[$i]['Modele'].'</td>
						<td>'.$aUserReservations[$i]['Description'].'</td>
						<td>
							<div class="btn-toolbar">
  								<div class="btn-group">
  									<a class="btn cancelReservation" id=CancelReservation_'.$aUserReservations[$i]['ID'].' title="Supprimer..." href="#"><i class="icon-uniF470 icon-black"></i></a>
  								</div>
  							</div>
  						</td>
					</tr>';
				}
			}
echo'			</table>
			</div>
    	</div>
  	</div>
  <a href="guide.pdf">Guide d\'utilisation</a>
  </div>';
$oDesign->makeFooter();
?>