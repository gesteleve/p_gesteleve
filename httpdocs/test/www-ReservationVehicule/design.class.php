<?php 
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : jeudi 31 janvier 2013
** Version : 0.1
** Description : Classe de création de design
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/

class cDesign {
	
	// Déclaration des options
	var $sTitle = 'ETML';
	var $sFavicon = 'favicon.png';
	var $aDefaultCss = array();
	var $aDefaultJs = array();
	var $bShowDesign = true;
	var $bShowMenu = true;
	var $aMenu = array();
	var $sSubTitle = '';
	
	var $bShowFooter = true;
	
	
	
	/*
	 * But: configure les options de l'objet
	 * input: tableau de configuration
	 * output: none
	 */
	public function setConfig($aConfig) {
		(isset($aConfig['title'])		? $this->sTitle = $aConfig['title'] : null);
		(isset($aConfig['favicon'])		? $this->sFavicon = $aConfig['favicon'] : null);
		(isset($aConfig['show_design'])	? $this->bShowDesign = $aConfig['show_design'] : null);
		(isset($aConfig['menu'])		? $this->aMenu = $aConfig['menu'] : null);
		(isset($aConfig['show_menu'])	? $this->bShowMenu = $aConfig['show_menu'] : null);
		(isset($aConfig['subtitle'])	? $this->sSubTitle = $aConfig['subtitle'] : null);
		(isset($aConfig['show_footer'])	? $this->bShowFooter = $aConfig['show_footer'] : null);
	
		// Traite les css si elle vienne d'un tableau ou d'une chaine de caractères
		if (isset($aConfig['css']) && !empty($aConfig['css'])) {
			if (is_array($aConfig['css'])) {
				foreach ($aConfig['css'] as $sCss) {
					$this->aDefaultCss[] = $sCss;
				}
			}
			else {
				$this->aDefaultCss = $aConfig['css'];
			}
		}
		
		// Traite les feuilles js si elle vienne d'un tableau ou d'une chaine de caractères
		if (isset($aConfig['js']) && !empty($aConfig['js'])) {
			if (is_array($aConfig['js'])) {
				foreach ($aConfig['js'] as $sJs) {
					$this->aDefaultJs[] = $sJs;
				}
			}
			else {
				$this->aDefaultJs = $aConfig['js'];
			}
		}
		
	}
	
	
	
	
	/*
	 * But: Ajoute une feuille CSS
	 * input: nom de la feuille CSS
	 * output: none
	 */
	public function addCss($sCss) {
		$this->aDefaultCss[] = $sCss;
	}
	
	/*
	 * But: Ajoute une feuille JS
	 * input: nom de la feuille JS
	 * output: none
	 */
	public function addJs($sJs) {
		$this->aDefaultJs[] = $sJs;
	}
	
	
	/*
	 * But: Crée l'en-tête CSS
	 * input: none
	 * output: chaine de caractères
	 */
	private function makeCss() {
		$aCss = $this->aDefaultCss;
		
		$sReturn = '';
		foreach ($aCss as $sCss) {
			$sReturn .= "<link rel='stylesheet' href='" . FOLDER_CSS . $sCss . "' media='all' />\n";
		}
		
		return $sReturn;
	}
	
	
	
	/*
	 * But: Crée l'en-tête JS
	 * input: none
	 * output: chaine de caractères
	 */
	private function makeJs() {
		$aJs = $this->aDefaultJs;
		
		$sReturn = '';
		foreach ($aJs as $sJs) {
			$sReturn .= "<script src='" . FOLDER_JS . $sJs . "'></script>\n";
		}
		
		return $sReturn;
	}
	
	/*
	 * But: Génère le menu déroulant
	 * input: none
	 * output: chaine de caractères
	 */
	private function makeMenu() {
		$aMenu = $this->aMenu;

		$sReturn = '<div class="navbar navbar-inverse navbar-fixed-top">
		  <div class="navbar-inner">
		  	<div class="container">
		  		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    	<span class="icon-bar"></span>
			    </a>
		    	<a style="float:right;" class="brand" id="logout" href="#">Déconnexion...</a>
		    	<div class="nav-collapse collapse">
		    	<ul class="nav">';

		foreach ($aMenu as $sLink => $sSubItem) {
			$sActive  = '';
			// Création des onglets
			if(isset($_GET['page'])) {
				if($sLink == $_GET['page']){
					$sActive = 'active';
				}
			}
			else{
				if ($sLink == 'home'){
					$sActive = 'active';
				}
			}
			$sReturn .= '<li class="'.$sActive.'"><a id="'.$sLink.'" href="' . $_SERVER['PHP_SELF'] . '?page=' . $sLink . '">' . $sSubItem . '</a>';
		}

		$sReturn .= '</ul></div>
		  </div>
		  </div>
		</div>';
		
		return $sReturn;
	}
	
	
	
	/*
	 * But: crée l'en-tête html
	 * input: none
	 * output: chaine de caractères
	 */
	public function makeHeader() {
		
		// Création des configuration
		$sCss = $this->makeCss();
		$sMenu = $this->makeMenu();
		
		// Crée le contenu de la balise head
		$sReturn = '
<!DOCTYPE html >
<html lang="fr">
<head>
	<meta name="author" content="Michael Jordan" />
	<meta name="description" content="Reservation de véhicule ETML" />
	<meta charset="utf-8" /> 
	<link rel="icon" type="image/png" href="' . FOLDER_IMG . $this->sFavicon . '" />
	<title>' . $this->sTitle . '</title>
	' . $sCss . '
</head>
<body>';
		
		// Affichage de l'en-tête du body
		if ($this->bShowDesign) {
			$sReturn .= ($this->bShowMenu ? $sMenu : null) . '
<div class="jumbotron subhead">
  <div class="container">
    <h1>'. $this->sTitle.'</h1>
    <p class="lead">'.$this->sSubTitle.'</p></div>
</div><div id="container">';
		}
		
		echo $sReturn;
		
	}
	
	
	
	/*
	 * But: crée le contenu
	 * input: none
	 * output: chaine de caractères
	 */
	public function makeBody() {
		$sAlert = fMakeAlert();

		$sReturn = 
'
<div class="content">'.
	$sAlert
	.'<div class="content_text">';
		
		
		echo $sReturn;
	}

	
	/*
	 * But: crée le pied de page
	 * input: none
	 * output: chaine de caractères
	 */
	public function makeFooter() {
		$sJs = $this->makeJs();

		$sReturn = ($this->bShowFooter ? '
	</div>
</div>
</div>
<div class="footer">
	<div class="container">
		<p>Version : v'.VERSION.'</p>
		<p>&copy; Site développé à l\'<abbr title="Ecole Technique des Métiers de Lausanne">ETML</abbr> par Michael Jordan  - 2013.</p>
		<p>Design basé sur <a href="http://twitter.github.com/bootstrap/" title="Bootstrap">Bootstrap</a>.</p>
	</div>
</div>' : null) . '

'. $sJs .'
</body>
</html>';
		
		echo $sReturn;
	}
	
}

?>