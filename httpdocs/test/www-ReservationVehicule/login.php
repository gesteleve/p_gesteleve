<?php
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : Jeudi 31 janvier 2013
** Version : 0.1
** Description : Page de traitement de login
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/

$oLdap = new cLdap();

// Récupération des données et traitement du login
if (isset($_POST['login']) && isset($_POST['password'])) {
	$sLogin = $_POST['login'];
	$sPassword = $_POST['password'];

	$bInfo = false;
	$bConnect = false;

	$oLdap->setConfig(array(
		'login' => $sLogin, 
		'password' => $sPassword
	));
	
	
	$bConnect = $oLdap->connect();
	$bInfo = $oLdap->getInfo();
	
	fRedirect($_SERVER['PHP_SELF'].(!$bInfo ?"?error=login" : null),0);

}
else {
	
	$oDesign->setConfig(array(
	'title' => 'Connexion...',
	'css' => $aDefaultCSS,
	'js' => $aDefaultJS,
	'show_menu' => false,
	'subtitle' => 'Connectez-vous pour pouvoir réserver un véhicule !'
	));
	$oDesign->makeHeader();
	$oDesign->makeBody();

	echo '
	<form method="POST" action="#">
		<table>
			<tr>
				<td>
					<input type="text" name="login" placeholder="Identifiant..." required  />
				</td>
				<td>
					<input type="password" name="password" placeholder="Mot de passe..." required />
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" class="btn btn-primary" value="Se connecter" />
				</td>
			</tr>
		</table>
	</form>';

	if (isset($_GET['error']) && $_GET['error'] == 'login'){
		echo '
		<div class="alert alert-error">
			Oups! Votre identifiant ou votre mot de passe n\'est pas valide !
		</div>';
	}
	if (isset($_GET['logout']) && $_GET['logout'] == 'ok'){
		echo '
		<div class="alert alert-success">
			Vous avez été correctement déconnecté.
		</div>';
	}

	$oDesign->makeFooter();
}


?>