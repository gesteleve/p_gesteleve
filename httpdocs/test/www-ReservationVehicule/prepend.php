<?php 
	/*
	** ********************************************
	**
	** Auteur: Michael Jordan
	** Date de création : jeudi 31 janvier 2013
	** Version : 0.1
	** Description : incorpore les fichiers des classes et fonctions, et instancie les objets
	**
	** ********************************************
	**
	** Date de modification : 
	** Modification(s) :
	** 
	** ********************************************
	*/
?>
<?php 
	// Appelle les fichiers objet et fonctions
	require(FOLDER_CLASS . 'mysql.class.php');
	require(FOLDER_CLASS . 'ldap.class.php');
	require(FOLDER_CLASS . 'design.class.php');
	require(FOLDER_INC . 'func.inc.php');
	
	// Instancie les objets
	$oDesign = new cDesign();
?>