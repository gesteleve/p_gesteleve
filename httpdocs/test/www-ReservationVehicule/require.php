<?php 
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : jeudi 31 janvier 2013
** Version : 0.1
** Description : Fichier de redirection par rapport au GET
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/
if(isset($_SESSION['name'])){
	if ($_SESSION['mail'] == "" && PAGE != 'logout') {
		fSetAlert('Veuillez entrer votre e-mail','error');
		require('./userinfos.php');
	}
	elseif (defined('PAGE')) {
		if (PAGE == 'home') {
			require('./home.php');
		}
		elseif (PAGE == 'login') {
			require('./login.php');
		}
		elseif (PAGE == 'reservations') {
			require('./reservations.php');
		}
		elseif (PAGE == 'logout') {
			require('./logout.php');
		}
		elseif (PAGE == 'userinfos') {
			require('./userinfos.php');
		}
		elseif (PAGE == 'vehicules') {
			require('./vehicules.php');
		}
		else {
			require('./home.php');
		}
	}
	else {
		require('./home.php');
	}
}
else{
	require('./login.php');
}

?>