<?php
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : jeudi 2 février 2012
** Version : 0.1
** Description : Fichier central, il sera toujours appelé en premier
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/
session_start();

// privisoirement pour afficher les erreur sur le serveur
error_reporting(E_ALL);
// on précise la timezone sinon DateTime ne fonctionne pas !
date_default_timezone_set('Europe/Berlin');

require('./inc/conf.inc.php');
require('./prepend.php');
require('./require.php');

?>