<?php
/*
** ********************************************
**
** Auteur: Michael Jordan
** Date de création : jeudi 31 janvier 2013
** Version : 0.1
** Description : Fichier de redirection par rapport au GET
**
** ********************************************
**
** Date de modification : 
** Modification(s) :
** 
** ********************************************
*/
$oDesign->setConfig(array(
	'title' => 'Réservation véhicule',
	'css' => $aDefaultCSS,
	'js' => $aDefaultJS,
	'menu' => $aMenu, 
	'subtitle' => 'Calendrier des réservations'
));

$oDesign->makeHeader();
$oDesign->makeBody();

//Obtient toutes les réservations
$oSql = new cMySql();
$aAllReservations = $oSql->getAllReservations();
unset($oSql);


echo '
<div id="loading" style="display:none;">loading...</div>
<div id="calendar"></div>

<div id="lstColorsCode">
	<table>
		<tr>
			<td><span class="label" style="background-color:#ad001d;">Mes évenements</span></td>
			<td><span class="label" style="background-color:#005E0C;">Autres évenements</span></td>
			<td><span class="label" style="background-color:black;">Evenements passés</span></td>
		</tr>
	</table>
</div>';

echo '<div id="ModalCreateReservation" class="modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Réservation de véhicule</h3>
  </div>
  <div class="modal-body">';
      	include(FOLDER_INC . 'create_reservation.inc.php');//Inclus le formulaire de création.
echo '
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Fermer</a>
    <a href="#" name="btnReserve" id="btnReserve" class="btn btn-primary">Réserver</a>
  </div>
</div>';

echo '<div id="ModalModifyReservation" class="modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Information sur la réservation</h3>
  </div>
  <div class="modal-body">';
      	include(FOLDER_INC . 'modify_reservation.inc.php'); //Inclus le formulaire de modification.
echo '
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Fermer</a>
    <a href="#" name="btnDeleteMineReservation" class="btn btn-danger">Supprimer ma réservation</a>
    <a href="#" name="btnModifyReservation" class="btn btn-success">Modifier</a>
  </div>
</div>';

$oDesign->makeFooter();
?>