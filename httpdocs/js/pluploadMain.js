function $(id) {
	return document.getElementById(id);	
}
	
var testTab = new Object();

jQuery(function($){
var uploader = new plupload.Uploader({
	runtimes :'html5,flash',
	//containaire principale bouton de séléction des fichiers et div de drag and drop
	container: 'plupload',
	browse_button : 'browse',
	drop_element : "droparea",
	//liens  vers le fichiers de traitement
	url : 'upload',
	//emplacement du fichier flash en cas de non compatibilité avec l'html 5
	flash_swf_url : '../js/plupload/plupload.flash.swf',
	multipart: true,
	urlstream_upload: true
});

//reforme l'interface pour le flash
uploader.bind('Init', function(up, params) {
if(params.runtime == 'flash')
{
		
		$('#DropHelptext').remove();
		$('#dorpSpan').remove();
		$('#droparea').removeClass('notHover');
		$('#droparea').addClass('DropFlash');
		$('#browse').addClass('browseFlash');
		$('#droparea').removeClass('browse');
}
});
//avancement de la progressebar

			var nameLbl = $('#nameLbl');
			var commentLBL = $("#commentLbl");
			var studentLbl = $("#studentLbl");
			var typeLbl = $("#typeLbl");

			var nameTXT = $('#nameTxt');
			var extTXT = $('#extTxt');
			var commentTXT = $("#commentTxt");

			var progressbar = $("#progressbar");
			var student = $("#student");
			var typeDocument = $("#typeDocument");
			var model = $("#model");

			var data = $('#data');
			var label = $('#label');
			var fileliste = $('#filelist');


/*uploader.bind('UploadProgress',function(up,files){
	$('#'+file.id).find('.progress').css('width',file.percent+'%');
});*/

//crée la liste des fichiers uploader
uploader.bind('FilesAdded', function(up, files) {
	for (var i in files) {
			var file =files[i];
			var fileName = file.name.split('.')[0];
			var ext = file.name.split('.')[1];

			testTab[i] = file.name;

			var CloneData = data.clone();
			var CloneLabel = label.clone();
			var CloneFile = $("#file").clone();

			fileliste.prepend (CloneFile);
			CloneFile.prepend (CloneData);
			CloneFile.prepend (CloneLabel);

			CloneFile.removeAttr("id");
			CloneFile.attr('id', fileName);
			$('#'+fileName+' #label #span #nameLbl').append(' (' + plupload.formatSize(file.size) + ')');
			$('#'+fileName+' #label #span #nameLbl').attr('value', fileName);

			$('#'+fileName+' #data #span #nameTxt').attr('value', fileName);
			$('#'+fileName+' #data #extTxt').attr('value', ext);

			$('#'+fileName+' #data #span #nameTxt').attr('id', 'nameTxt'+i);
			$('#'+fileName+' #data #span #commentTxt').attr('id', 'commentTxt'+i);
			$('#'+fileName+' #data #extTxt').attr('id', 'extTxt'+i);
			$('#'+fileName+' #data #span #student').attr('id', 'student'+i);
			$('#'+fileName+' #data #span #typeDocument').attr('id', 'typeDocument'+i);

		//enlève la classe css qui met en surbriance la drop area
		$('#droparea').removeClass('hover');
		$('#droparea').addClass('notHover');

		}
	});
//lance l'upload quand on click sur le bouton uploadfiles

	$('#uploadfiles').click(function(){

		//création de la liste conteant les éléments lier à 1 seul upload
		var cpt = 0;
		var comment = '';
		var name = '';
		var ext = '';
		var strValue ='';

		//crée la chaine de caractère qui contiendra toute les donnéess a envoyer a la page PHP
		for(var i in testTab)
        {
            comment = $('#commentTxt'+cpt).val();
            name = $('#nameTxt'+cpt).val();
            ext = $('#extTxt'+cpt).val();
            student = $('#student'+cpt).val();
            typeDocument = $('#typeDocument'+cpt).val();

            if(i == 0){
            	strValue = testTab[i]+';'+new Array(comment,name,ext,student,typeDocument);
            }
            else{
            	strValue = strValue+';'+testTab[i]+';'+new Array(comment,name,ext,student,typeDocument) ;
            }
             cpt = cpt +1 ;
        }
		//met a jours l'objet uploader avec les valeur a envoyer a la page PHP
        uploader.settings.multipart_params = {
            'tabvalue': strValue
        };
        uploader.start();
    })

	//ajoute un effet de drag en drop sur la div qui resoi les documents
	$('#droparea').bind({
		
		dragover: function(e){
			$(this).addClass('hover');
			$(this).removeClass('notHover');
		},
		dragleave: function(e){
			$(this).addClass('notHover');
			$(this).removeClass('hover');
		}
	})
	uploader.init();
});