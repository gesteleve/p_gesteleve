var files = new Array();
var student_id = $('#student').text();

function submit_form(id) {
    if (document.getElementsByName('description-'+id)[0].value == '') {
        alert('Vous êtes obligé de renseigner une description !');
    } else {
        document.getElementById('file-id').value = id;
        var jqxHR = files[id].submit();
    }
}

$(function() {
    var dropbox = $('#files-box');
    var i = 0;

    // Création de l'objet jQuery File Upload Plugin
    $('#upload').fileupload({

        dropZone: $('#dropbox'),

        add: function (e, data) {
            if (getExt(data.files[0].name) != 'pdf') {
                alert('Le fichier "'+data.files[0].name+'" n\'est pas un PDF');
            } else if (data.files[0].size > 8388608) {
                alert('Le fichier "'+data.files[0].name+'" est trop lourd, la limite est fixée à 8Mb');
            } else {
                i += 1;
                var tpl= $(
				'<div class="file">'+
					//bouton exit
					'<div class="exit-button">'+
						'<img class="status" src="'+window.location.origin+'/img/cross_circle.png" alt="X" />'+
					'</div>'+
					'<div class="file-componement">'+
						//***********************************
						//Les infos sur le fichier à uploader
						//***********************************
						'<div class="file-info">'+
							
							//Image Du type de fichier
							'<div class="file-icon">'+ //Créé style !
								'<img class="img-file" src="'+window.location.origin+'/img/pdf.png" alt="PDF"/>'+
							'</div>'+
							//Nom + Bar de chargement
							'<div class="file-upload">'+ //créé style!
								'<div class="file-name"></div>'+
								'<progress class="progress-bar progress-striped" value="0" min="0" max="100"></progress>'+
							'</div>'+
						'</div>'+ //Fin des infos sur le fichier à uploader
						//***********************************
						//Choix des propriétés du fichier
						//***********************************
						'<div class="file-property">'+
							//Choix du type de fichier
							'<div class="control-group">'+
								'<label class="control-label">Type :</label>'+
								'<div class="controls">'+
									'<select name="type-'+i+'" class="big-input">'+
										'<option value="1">Bulettin trimestriel</option>'+
										'<option value="2">Bulletin semestriel</option>'+
										'<option value="3">Certificat</option>'+
										'<option value="4">Contrat de travail (stage)</option>'+
										'<option value="5">Certificat de travail (stage)</option>'+
										'<option value="6">Rapport de stage</option>'+
										'<option value="7">Evaluation de stage</option>'+
										'<option value="8">Dossier d\'admission</option>'+
										'<option value="9">Lettre de passage</option>'+
										'<option value="10">Convocation</option>'+
										'<option value="11">Autre</option>'+
									'</select>'+
								'</div>'+
							'</div>'+ //fin Choix du type de fichier
							//Description
							'<div class="control-group">'+
								'<label class="control-label">Description : </label>'+
								'<div class="controls">'+
									'<textarea class="Description-textarea" name="description-'+i+'" cols="30" rows="10" required></textarea>'+
								'</div>'+
							'</div>'+ //fin Description
							//Choix de la Sécurité
							'<div class="control-group">'+
								'<label class="control-label">Visible par : </label>'+
								'<div class="controls">'+
									'<select name="security-'+i+'" class="big-input">'+
										'<option value="1">Elève</option>'+
										'<option value="2">Enseignant</option>'+
										'<option value="3">Maître de classe</option>'+
									'</select>'+
								'</div>'+
							'</div>'+ //Fin Sécurité
							//Bouton confirmer
							'<div class="control-group">'+
								'<div class="controls">'+
									'<input type="button" class="btn big-input" onClick="javascript:submit_form('+i+');" value="Confirmer">'+
								'</div>'+
							'</div>'+//Bouton confimer
						'</div>'+ //fin des choix des propriétés du fichier	
					'</div>'+
				'</div>'		
				);
                tpl.find('div.file-name').text(data.files[0].name);
                tpl.find('span.file-size').text(formatFileSize(data.files[0].size));

                files[i] = data;

                data.context = tpl.appendTo(dropbox);

                tpl.find('img.status').click(function() {
                    if (tpl.hasClass('working')) {
                        jqxHR.abort();
                    }

                    tpl.fadeOut(function() {
                        tpl.remove();
                    });
                });

            }
        },

        progress: function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('progress').attr('value', progress);

            if (progress == 100) {
                data.context.removeClass('working');
                data.context.addClass('success');
                data.context.delay(1000).fadeOut(1000);
            }

        },

        fail:function(e, data) {
            data.context.addClass('error');
        }
    });

    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    function getExt(filename) {
        var t = filename.split(".");
        return (t[(t.length-1)]);
    }
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
});
