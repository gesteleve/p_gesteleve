/*function eventCady(){
    $('div.caddy-number').html($('div.isotope-item.ui-selected').not('.isotope-hidden').length+' Elève(s) séléctionné(s)');
}*/

jQuery(function($) {
    $('#edit-document').on('hidden', function() {
        $(this).data('modal').$element.removeData();
        $('#edit-document .modal-body').html('');
    });

    $('#edit-document').on('show', function() {
        $('#btn-edit-document').on('click', function() {
            $('#form-edit-document').submit();
        });

        $('#btn-delete-document').on('click', function() {
            window.location = $('#link-delete-document').attr('href');
        });
    });

    $('#dl-vw-document').on('hidden', function() {
        $(this).data('modal').$element.removeData();
        $('#dl-vw-document .modal-body').html('');
    });

    $('#dl-vw-document').on('show', function() {
        $('#btn-view-document').on('click', function() {
            window.location = $('#link-view-document').attr('href');
        });

        $('#btn-download-document').on('click', function() {
            window.location = $('#link-download-document').attr('href');
        });
    });

    $('#modFollow').on('hidden', function() {
        $(this).data('modal').$element.removeData();
        $('#modFollow .modal-body').html('');
    });

	    $('#viewFollow').on('hidden', function() {
        $(this).data('modal').$element.removeData();
        $('#viewFollow .modal-body').html('');
    });
	
    $('#modFollow').on('show', function() {
        $('#btn-delete-follow').click(function() {
            window.location = $('#deleteFollow').attr('href');
        });

        $('#btn-edit-follow').click(function() {
            $('#update-follow').submit();
        });
    });

    $('#doc-filter').on('change', function() {
        v = $('#doc-filter option:selected').val();
        if (v == 0) {
            $('.file').show();
        } else {
            $('.file').hide();
            $('.'+v).show();
        }
    });



    $('.search').focus();

    // scrollbar custom
    $('.slimscroll').slimScroll({height:'310px'});

    // init knob
    // Circle Canevas HTML5
    /*$(".knob").knob();*/

    // footer developpeur
    $('#dev').click(function() {
        $('span#name').toggle("slow");
    });

    /*$('div#container').selectable({
        selected: function( event, ui ) {
            $('#second-step').show();
            eventCady();
        },
        unselected: function( event, ui ) {
            $('#second-step').hide();
            eventCady();
        }
    });*/

    /*var $container = $('#widget-document');
    var $filter    = $('#document-filters').find('a:not(.cat)');
    var $category  = $('#document-filters').find('a.cat');

    // Isotope Filter
    $filter.click(function() {
        if($(this).not($(this).attr('data-filter')).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $(this).addClass('active');
            $('#filters').find('a.first').removeClass('active');
        }

        if($(this).attr('data-filter')=='*'){
            $filter.removeClass('active');
            $(this).addClass('active');
        }

        if($('#filters').find('.active').length < 2){
            $('#filters').find('.first').addClass('active');
        }

        var filters = [];

        $filter.each(function(){
            if($(this).hasClass('active')){
                filters.push( $(this).attr('data-filter') );
            }
        });

        // ['.red', '.blue'] -> '.red, .blue'
        filters = filters.join(', ');

        try {
            $container.isotope({
                filter: filters,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: true,
                }
            });
            return false;
        } catch (err) {}
    });

    $category.click(function() {
        if($(this).hasClass('active')){
            $category.removeClass('active');

            $(this).parent().find('ul.second').css({'display' : 'none'});

        }else{
            $category.parent().find('ul.second').css({'display' : 'none'});

            var widthdemi = $('div.main_container').width()/2;
            $(this).parent().find('ul.second').css({'display' : 'block','min-width' : widthdemi});

            $category.removeClass('active');
            $(this).addClass('active');
        }
    });

    // Run Isotope
    $(window).load(function() {
        try {
            $container.isotope({
                filter: '*',
                layoutMode: 'fitRows',
                animationOptions: {
                    duration: 750,
                    easing: 'linear'
                }
            });
            // touche
            // pas touche
        } catch (err) {}
    });
    /*$container.infinitescroll({
        navSelector: ".paging",
        nextSelector: ".paging .next a",
        itemSelector: ".post",
        debug: false,
        dataType: 'html',
        loadingImg: '',
        loadingText: '',
        bufferPx: 750,
        donetext: '',
    },  function(newElements) {
            $container.isotope('appended', $(newElements), function() {
                $container.isotope('reLayout');
            });
        });*/

	/****************************/
	/*
	/*------- Fullscreen -------*/
	/*
	/****************************/

    /*$('#full-close .close').on({
        mouseenter: function(){
            $(this).animate({   backgroundColor: 'black',
                                color: 'white',
                            }, 400);
        },
        mouseleave: function(){
            $(this).animate({   backgroundColor: 'white',
                                color: 'black',
                            }, 400);
        }
    });

	$(document).on('click.modal.data-api', '[data-toggle="modal"]', function (e) {
        var src = $(this).find('img').attr('src');
        $('.full-preview-picture-holder').html('<img src="'+src+'">');

       	// Calcul la taille du fullscreen
	    var width = $(window).width();
	    // pourcentage
	    width = width*0.4;
	    $('#full-preview-picture').width(width);
	});

    $(window).resize(function() {
        var width = $(window).width();
        width -= $('#full-preview-info').width();
        $('#full-preview-picture').width(width);
    });


    $('#full-preview').on('show', function(){
        $('body').css("overflow","hidden");
    });

    $('#full-preview').on('hidden', function(){
        $('body').css("overflow","auto");
    });*/

    /* event paginate */
    /*$(document).ready(function(){

        $('.data-table').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',

        });

        $('input[type=checkbox],input[type=radio],input[type=file]').uniform();

        $('select').select2();

        $("span.icon input:checkbox, th input:checkbox").click(function() {
            var checkedStatus = this.checked;
            var checkbox = $(this).parents('.widget-box').find('tr td:first-child input:checkbox');
            checkbox.each(function() {
                this.checked = checkedStatus;
                if (checkedStatus == this.checked) {
                    $(this).closest('.checker > span').removeClass('checked');
                }
                if (this.checked) {
                    $(this).closest('.checker > span').addClass('checked');
                }
            });
        });
    });*/ /* End event paginate */
   /* $(document).ready(function() {
        $('.data-table').dataTable({
            "bJQueryUI": true,
            "sDom": '<""l>t<"F"fp>',
            "sPaginationType": "full_numbers"
        });
    });*/


    /* event adding form */
    /*$(document).ready( function() {
        $('#stype').change( function() {
            $('.hform').html(''); // On vide le formulaire

            var v = $(this).val(); // Stock la valeur du select
            if (v <= 3 && v >= 1) {
                $.ajax({
                    type : 'POST',
                    url : '/happenings/hform/',
                    data : 'value='+v,
                    success : function (data) {
                        $('.hform').html(data); // Affiche le formulaire
                    }
                });
            }
        });
    });*/
    /* End event adding form*/

});


/*

    Loader

*/
/*var cSpeed=9;
var cWidth=130;
var cHeight=130;
var cTotalFrames=75;
var cFrameWidth=130;
var cImageSrc='img/sprites-preload.gif';

var cImageTimeout=false;
var cIndex=0;
var cXpos=0;
var SECONDS_BETWEEN_FRAMES=0;

function startAnimation(){

    document.getElementById('loaderImage').style.backgroundImage='url('+cImageSrc+')';
    document.getElementById('loaderImage').style.width=cWidth+'px';
    document.getElementById('loaderImage').style.height=cHeight+'px';

    //FPS = Math.round(100/(maxSpeed+2-speed));
    FPS = Math.round(100/cSpeed);
    SECONDS_BETWEEN_FRAMES = 1 / FPS;

    setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES/1000);

}

function continueAnimation(){

    cXpos += cFrameWidth;
    //increase the index so we know which frame of our animation we are currently on
    cIndex += 1;

    //if our cIndex is higher than our total number of frames, we're at the end and should restart
    if (cIndex >= cTotalFrames) {
        cXpos =0;
        cIndex=0;
    }

    document.getElementById('loaderImage').style.backgroundPosition=(-cXpos)+'px 0';

    setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES*1000);
}

function imageLoader(s, fun)//Pre-loads the sprites image
{
    clearTimeout(cImageTimeout)
    cImageTimeout=0;
    genImage = new Image();
    genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
    genImage.src=s;
}

//The following code starts the animation
new imageLoader(cImageSrc, 'startAnimation()');*/

/* Timer pour la recherche */
// Déclare le timer
var searchTimer;
$(document).ready( function() {
    // détection de la saisie dans le champ de recherche
    $('#pattern').keyup( function(){
        // Supprime le timout s'il existe
        clearTimeout(searchTimer);
        // Déclare le temps à attendre en ms et la fonction à appeler
        searchTimer = setTimeout('search()', 300);
    });
	
	// détection de la modif de la checkbox
    $('#active').click( function(){
        // Supprime le timout s'il existe
        clearTimeout(searchTimer);
        // Déclare le temps à attendre en ms et la fonction à appeler
        searchTimer = setTimeout('search()', 300);
    });
	
}); /* Fin du timer */

/* Recherche */
function search() {
    $field = $('#pattern');
	
    // on commence à traiter à partir du 2ème caractère saisie
    if( $field.val().length > 1 ) {
        // on envoie la valeur recherché en GET au fichier de traitement
        $.ajax({
            type : 'POST', // envoi des données en GET ou POST
            url : window.location.pathname, // url du fichier de traitement
            data : 'pattern='+$('#pattern').val()+'&active='+document.getElementById('active').checked, // données à envoyer en  GET ou POST
            beforeSend : function() { // traitements JS à faire AVANT l'envoi
                $('#ajax-loader').show();
            },
            success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
                $('#ajax-loader').hide();
                $('#results').html(data); // affichage des résultats dans le bloc
            }
        });
    } else {
        $('#results').html('');
    }
}/* End search function */
