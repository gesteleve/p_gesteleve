//*********************************************************
// Societe: ETML
// Auteur : Lukyantsev Vladislav
// Date : 12.05.2014
// But : Gêrer les impressions
//*********************************************************

//créer une feuille css et une class css "media print"
var inlineMediaStyle = null;
var head = document.getElementsByTagName('head')[0];
var newStyle = document.createElement('style');
newStyle.setAttribute('type', 'text/css');
newStyle.setAttribute('media', 'print');


// *******************************************************************
// Nom : optionPrintPDF
// But : Generer un pdf automatique depuis la page en cours
// *******************************************************************
function optionPrintPDF() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#content')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('Test.pdf');
    }, margins);

}

// *******************************************************************
// Nom : changeMediaStyle
// But : Changer le css de type media print 
// Retour: 
// Param.: code qui la valeur à changer (image,text,police)
// *******************************************************************
    function changeMediaStyle (code)
    {
        

        //verifie si l'on doit afficher ou cacher les images
        if(code == "imgOff")
        {
          newStyle.appendChild(document.createTextNode('div.picture { display:none;} div.information{padding: 1px 0 0 0px;} div.post {width:130px;}'));
         newStyle.appendChild(document.createTextNode('#student-picture { display:none;}'));
        }
        else if(code == "imgOn")
        {
          newStyle.appendChild(document.createTextNode('div.picture { display:block;} div.information{padding: 1px 0 0 120px;} div.post {width:300px;}'));
                 newStyle.appendChild(document.createTextNode('#student-picture { display:block;}'));
        }

        //Verifie quelle taille de police appliquer
        if(code == "x-small")
        {
          newStyle.appendChild(document.createTextNode('div{font-size:x-small;} a{font-size:x-small;}'));
        }
        else if(code == "small")
        {
          newStyle.appendChild(document.createTextNode('div{font-size:small;}  a{font-size:small;}'));
        }
        else if(code == "medium")
        {
          newStyle.appendChild(document.createTextNode('div{font-size:medium;}  a{font-size:medium;}'));
        }
        else if(code == "large")
        {
          newStyle.appendChild(document.createTextNode('div{font-size:large;}  a{font-size:large;}'));
        }
        else if(code == "x-large")
        {
          newStyle.appendChild(document.createTextNode('div{font-size:x-large;}  a{font-size:x-large;}'));
        }
        
         //Verifie quelle police appliquer
        if(code == "TNR")
        {
          newStyle.appendChild(document.createTextNode('body{font-family: "Times New Roman";}'));
        }
        else if(code == "arial")
        {
          newStyle.appendChild(document.createTextNode('body{font-family: arial;}'));
        }
        else if(code == "EcoFont")
        {
          newStyle.appendChild(document.createTextNode('body{font-family: "EcoFont";}'));
        }
        else if(code == "CenturyGothic")
        {
          newStyle.appendChild(document.createTextNode('body{font-family: CenturyGothic;}'));
        }

         //Verifie si l'on doit afficher ou pas l'entete
        if(code == "ETPPyes")
        {
          newStyle.appendChild(document.createTextNode('div.header-print{display: block;}'));
        
        }
        else if(code == "ETPPno")
        {
          newStyle.appendChild(document.createTextNode('div.header-print{display: none;}'));
          
        }

        //Verifie si l'on doit afficher ou pas les détails de l'élève
         if(code == "Detailyes")
        {
          newStyle.appendChild(document.createTextNode('.printDetail{display: block;}'));
        }
        else if(code == "Detailno")
        {
         newStyle.appendChild(document.createTextNode('.printDetail{display: none;}'));
        }

        //Verifie si l'on doit afficher ou pas les suivis de l'élève
        if(code == "Followyes")
        {
          newStyle.appendChild(document.createTextNode('#widget-follow{display: block;}'));
        }
        else if(code == "Followno")
        {
         newStyle.appendChild(document.createTextNode('#widget-follow{display: none;}'));
        }

        //Verifie si le style existe déjà et si il doit etre remplacé
        if (inlineMediaStyle != null)
        {
            head.replaceChild(newStyle, inlineMediaStyle)
        }
        else
        {
            head.appendChild(newStyle);
        }

        inlineMediaStyle = newStyle;
    }
 

// *******************************************************************
// Nom : optionPrint
// But : Lance l'impression
// Retour: aucun
// Param.: aucun
// *******************************************************************
function optionPrint()
{
  //cache la modal box et l'ombrage d'arriere plan
  document.getElementById('PrintModal').style.display = "none";
  document.getElementById('modal-backdrop').style.display = "none";

  //lance l'impression
  window.print();

  //affiche la modal box et l'ombrage d'arriere plan
  document.getElementById('modal-backdrop').style.display = "block";
  document.getElementById('PrintModal').style.display = "block";
  

}   