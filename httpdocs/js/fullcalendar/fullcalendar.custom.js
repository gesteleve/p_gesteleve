/****************************************************************************
Nom:		fullcalendar.custom.js
Auteur:		Ludovic Delafontaine - MIN3 2011-2015
Date:		15.01.2014
But:		Configuration personnalis�e de fullcalendar
Sources:	- http://arshaw.com/fullcalendar/docs/
			- http://code.google.com/p/jsfcal/source/browse/trunk/jsfcal-core/src/main/resources/META-INF/assets/js/month/fullcalendar.locale.fr.js?r=27
*****************************************************************************
Modifications
Date  :		20.01.2014
Auteur:		Ludovic Delafontaine - MIN3 2011-2015
Raison:		- Modification de l'ent�te du calendrier
A faire:	
****************************************************************************/
$(document).ready(function() {

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			// Vue
			defaultView: 'agendaWeek', // Vue par d�faut
			firstDay: 1,	// Sp�cifie que le premier jour de la semaine est le lundi (0=dimanche)
			minTime: 8, // Heure de d�but
			maxTime: 20, // Heure de fin
			slotMinutes: 60,
			weekends: true, // D�sactive les weekends
			hiddenDays: [ 0 ], // Cache le dimanche
			
			// Locales - FR
			monthNames: ['Janvier','F\u00e9vrier','Mars','Avril','Mai','Juin','Juillet','Ao�t','Septembre','Octobre','Novembre','D\u00e9cembre'], // Noms des mois
			monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Juin','Juil','Aout','Sep','Oct','Nov','Dec'], // Noms raccourcis des mois
			dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'], // Noms des jours
			dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'], // Noms raccourcis des jours
			buttonText: { // Textes des bouttons
				today: 'Aujour.',
				month: 'Mois',
				week: 'Semaine',
				day: 'Jour'
			},
			dateFormat: "dd-mm-yyyy", // Format de la date
			timeFormat: 'HH:mm' , // Format du temps (g�n�ral)
			axisFormat: 'HH:mm', // Format du temps dans l'axe vertical

			// Ent�te du calendrier
			header: {
				left: 'prev,today,next', // Elements sur la gauche du calendrier
				center: 'title', // Elements sur le centre du calendrier
				right: 'month,agendaWeek,agendaDay' // Element sur la droite du calendrier
			},
			
			editable: false, // Permet de d�finir si le calendrier est �ditable, c'est-�-dire que l'on peut d�placer les �v�nements
			
			events: [
				{
					title: 'All Day Event',
					start: new Date(y, m, 1)
				},
				{
					title: 'Long Event',
					start: new Date(y, m, d-5),
					end: new Date(y, m, d-2)
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d-3, 16, 0),
					allDay: false
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d+4, 16, 0),
					allDay: false
				},
				{
					title: 'Meeting',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				},
				{
					title: 'Lunch',
					start: new Date(y, m, d, 12, 0),
					end: new Date(y, m, d, 14, 0),
					allDay: false
				},
				{
					title: 'Birthday Party',
					start: new Date(y, m, d+1, 19, 0),
					end: new Date(y, m, d+1, 22, 30),
					allDay: false
				},
				{
					title: 'Click for Google',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				}
			]
		});
		
	});