<?php
/**
 * PHP version 5
 *
 * This file is part of SlyWork.
 *
 * SlyWork is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SlyWork is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SlyWork. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Lanz Romain <lanz.romain@inosly.com>
 * @copyright  Copyright 2013 (c) InoSly - Lanz Romain <support@slywork.inosly.ch>
 * @version    13.3.20
 * @link       http://slywork.inosly.ch
 * @license    http://www.gnu.org/licenses/gpl.html
 */

namespace Library\Sly\Core;

use Library\Sly\Config\Config;
use Library\Sly\Network\HTTPRequest;
use Library\Sly\Network\HTTPResponse;
use Library\Sly\Routing\Route;
use Library\Sly\Routing\Router;
use Library\Sly\User\User;

abstract class Application
{
    /**
     * User request
     *
     * @var HTTPRequest
     * @access protected
     */
    protected $httpRequest;

    /**
     * Server response
     *
     * @var HTTPResponse
     * @access protected
     */
    protected $httpResponse;

    /**
     * Web application's user
     *
     * @var User
     * @access protected
     */
    protected $user;


    /**
     * Application's configuration
     *
     * @var Config
     * @access protected
     */
    protected $config;

    /**
     * Application's name (Frontend/Backend)
     *
     * @var string
     * @access protected
     */
    protected $name;

    /**
     * Run the application
     *
     * @access public
     * @return void
     */
    abstract public function run();

    /**
     * Prepare the application to run
     *
     * @access public
     * @return void
     */
    public function __construct() {
        $this->config = new Config($this);
        $this->httpRequest = new HTTPRequest($this);
        $this->httpResponse = new HTTPResponse($this);
        $this->user = new User($this);
        $this->name = '';
    }

    /**
     * Search and load controller
     *
     * @access public
     * @return Controller
     */
    public function getController() {
        $router = new Router;

        //$xml = \DOMDocument::load(APP_DIR.DS.$this->name.'/Config/routes.xml');
        //$routes = $xml->getElementsByTagName('route');

        $xml = simplexml_load_file(APP_DIR.DS.$this->name.DS.'Config'.DS.'routes.xml');
        $routes = $xml->children();

        foreach ($routes as $route) {
            $vars = array();

            if (isset($route->attributes()->vars)) {
                $vars = explode(',', (string) $route->attributes()->vars);
            }

            $router->addRoute(new Route((string) $route->attributes()->url,
                                (string) $route->attributes()->module,
                                (string) $route->attributes()->action,
                                $vars));
        }

        try {
            $matchedRoute = $router->getRoute($this->httpRequest->requestURI());
        } catch (\RuntimeException $e) {
            if ($e->getCode() == Router::NO_ROUTE) {
                $this->httpResponse->redirect404();
            }
        }

        $_GET = array_merge($_GET, $matchedRoute->vars());
        $controllerClass = 'Applications\\'.$this->name.'\\Modules\\'.$matchedRoute->module().'\\'.$matchedRoute->module().'Controller';
        $path = ROOT.DS.str_replace('\\', DS, $controllerClass).'.php';
        if (file_exists($path)) {
            return new $controllerClass($this, $matchedRoute->module(), $matchedRoute->action());
        } else {
            throw new \RuntimeException('Le module spécifié n\'existe pas');
        }
    }

    /**
     * Getter's function
     */
    public function httpRequest() { return $this->httpRequest; }
    public function httpResponse() { return $this->httpResponse; }
    public function name() { return $this->name; }
    public function user() { return $this->user; }
    public function config() { return $this->config; }
}
